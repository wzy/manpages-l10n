# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:36+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "LSIPC"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"lsipc - show information on IPC facilities currently employed in the system"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<lsipc> [options]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<lsipc> shows information on the System V inter-process communication "
"facilities for which the calling process has read access."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-i>, B<--id> I<id>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Show full details on just the one resource element identified by I<id>. This "
"option needs to be combined with one of the three resource options: B<-m>, "
"B<-q> or B<-s>. It is possible to override the default output format for "
"this option with the B<--list>, B<--raw>, B<--json> or B<--export> option."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-g>, B<--global>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Show system-wide usage and limits of IPC resources. This option may be "
"combined with one of the three resource options: B<-m>, B<-q> or B<-s>. The "
"default is to show information about all resources."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "Print version and exit."
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Resource options"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-m>, B<--shmems>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Write information about active shared memory segments."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-q>, B<--queues>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Write information about active message queues."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-s>, B<--semaphores>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Write information about active semaphore sets."
msgstr ""

#. type: SS
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "Output formatting"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-c>, B<--creator>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Show creator and owner."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-e>, B<--export>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>). See also option B<--"
"shell>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-J>, B<--json>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Use the JSON output format."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-l>, B<--list>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Use the list output format. This is the default, except when B<--id> is used."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-n>, B<--newline>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display each piece of information on a separate line."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--noheadings>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Do not print a header line."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--notruncate>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Don\\(cqt truncate output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-o>, B<--output> I<list>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Specify which output columns to print. Use B<--help> to get a list of all "
"supported columns."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-b>, B<--bytes>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Print size in bytes rather than in human readable format."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-r>, B<--raw>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Raw output (no columnation)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-t>, B<--time>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Write time information. The time of the last control operation that changed "
"the access permissions for all facilities, the time of the last B<msgsnd>(2) "
"and B<msgrcv>(2) operations on message queues, the time of the last "
"B<shmat>(2) and B<shmdt>(2) operations on shared memory, and the time of the "
"last B<semop>(2) operation on semaphores."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<--time-format> I<type>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Display dates in short, full or iso format. The default is short, this time "
"format is designed to be space efficient and human readable."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-P>, B<--numeric-perms>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Print numeric permissions in PERMS column."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "B<-y>, B<--shell>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"The column name will be modified to contain only characters allowed for "
"shell variable identifiers. This is usable, for example, with B<--export>. "
"Note that this feature has been automatically enabled for B<--export> in "
"version 2.37, but due to compatibility issues, now it\\(cqs necessary to "
"request this behavior by B<--shell>."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "0"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "if OK,"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "1"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "if incorrect arguments specified,"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "2"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "if a serious error occurs."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "The B<lsipc> utility is inspired by the B<ipcs>(1) utility."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<ipcmk>(1), B<ipcrm>(1), B<msgrcv>(2), B<msgsnd>(2), B<semget>(2), "
"B<semop>(2), B<shmat>(2), B<shmdt>(2), B<shmget>(2), B<sysvipc>(7)"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The B<lsipc> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Produce output in the form of key=\"value\" pairs. All potentially unsafe "
"value characters are hex-escaped (\\(rsxE<lt>codeE<gt>). The key (variable "
"name) will be modified to contain only characters allowed for a shell "
"variable identifiers, for example, USE_PCT instead of USE%."
msgstr ""
