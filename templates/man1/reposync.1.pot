# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "REPOSYNC"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Jan 22, 2023"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "4.3.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "dnf-plugins-core"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "reposync - redirecting to DNF reposync Plugin"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Synchronize packages of a remote DNF repository to a local directory."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<dnf reposync [options]>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"I<reposync> makes local copies of remote repositories. Packages that are "
"already present in the local directory are not downloaded again."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"All general DNF options are accepted. Namely, the B<--repoid> option can be "
"used to specify the repositories to synchronize. See I<Options> in B<dnf(8)> "
"for details."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-a E<lt>architectureE<gt>, --arch=E<lt>architectureE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Download only packages of given architectures (default is all "
"architectures). Can be used multiple times."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--delete>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Delete local packages no longer present in repository."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--download-metadata>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable mageia-cauldron
msgid ""
"Download all repository metadata. Downloaded copy is instantly usable as a "
"repository, no need to run createrepo_c on it."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-g, --gpgcheck>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Remove packages that fail GPG signature checking after downloading. Exit "
"code is B<1> if at least one package was removed.  Note that for "
"repositories with B<gpgcheck=0> set in their configuration the GPG signature "
"is not checked even with this option used."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-m, --downloadcomps>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Also download and uncompress comps.xml. Consider using B<--download-"
"metadata> option which will download all available repository metadata."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--metadata-path>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Root path under which the downloaded metadata are stored. It defaults to B<--"
"download-path> value if not given."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-n, --newest-only>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Download only newest packages per-repo."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--norepopath>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Don’t add the reponame to the download path. Can only be used when syncing a "
"single repository (default is to add the reponame)."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-p E<lt>download-pathE<gt>, --download-path=E<lt>download-pathE<gt>>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Root path under which the downloaded repositories are stored, relative to "
"the current working directory. Defaults to the current working directory. "
"Every downloaded repository has a subdirectory named after its ID under this "
"path."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--remote-time>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Try to set the timestamps of the downloaded files to those on the remote "
"side."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--source>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Download only source packages."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-u, --urls>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Just print urls of what would be downloaded, don’t download."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf reposync --repoid=the_repo>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Synchronize all packages from the repository with id “the_repo”. The "
"synchronized copy is saved in “the_repo” subdirectory of the current working "
"directory."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf reposync -p /my/repos/path --repoid=the_repo>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Synchronize all packages from the repository with id “the_repo”. In this "
"case files are saved in “/my/repos/path/the_repo” directory."
msgstr ""

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf reposync --repoid=the_repo --download-metadata>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Synchronize all packages and metadata from “the_repo” repository."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Repository synchronized with B<--download-metadata> option can be directly "
"used in DNF for example by using B<--repofrompath> option:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"B<dnf --repofrompath=syncedrepo,the_repo --repoid=syncedrepo list --"
"available>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: IP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "\\(bu"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<dnf(8)>, DNF Command Reference"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr ""

#. type: TH
#: fedora-38
#, no-wrap
msgid "Apr 05, 2023"
msgstr ""

#. type: TH
#: fedora-38
#, no-wrap
msgid "4.4.0"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide
msgid ""
"Download all repository metadata. Downloaded copy is instantly usable as a "
"repository, no need to run createrepo_c on it. When the option is used with "
"I<--newest-only>, only latest packages will be downloaded, but metadata will "
"still contain older packages. It might be useful to update metadata using "
"I<createrepo_c --update> to remove the packages with missing RPM files from "
"metadata. Otherwise, DNF ends with an error due to the missing files "
"whenever it tries to download older packages."
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Don\\(aqt add the reponame to the download path. Can only be used when "
"syncing a single repository (default is to add the reponame)."
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid "Just print urls of what would be downloaded, don\\(aqt download."
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Synchronize all packages from the repository with id \"the_repo\". The "
"synchronized copy is saved in \"the_repo\" subdirectory of the current "
"working directory."
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Synchronize all packages from the repository with id \"the_repo\". In this "
"case files are saved in \"/my/repos/path/the_repo\" directory."
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron
msgid "Synchronize all packages and metadata from \"the_repo\" repository."
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "May 17, 2023"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "4.4.1"
msgstr ""

#. type: TP
#: fedora-rawhide
#, no-wrap
msgid "B<--safe-write-path>"
msgstr ""

#. type: Plain text
#: fedora-rawhide
msgid ""
"Specify the filesystem path prefix under which the reposync is allowed to "
"write. If not specified it defaults to download path of the repository. "
"Useful for repositories that use relative locations of packages out of "
"repository directory (e.g. \"../packages_store/foo.rpm\"). Use with care, "
"any file under the B<safe-write-path> can be overwritten. Can be only used "
"when syncing a single repository."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Sep 26, 2022"
msgstr ""

#.  Generated by docutils manpage writer.
#. type: Plain text
#: mageia-cauldron
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr ""
