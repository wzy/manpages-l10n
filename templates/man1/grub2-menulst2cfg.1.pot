# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-05-22 18:01+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MENULST2CFG"
msgstr ""

#. type: TH
#: fedora-38
#, no-wrap
msgid "March 2023"
msgstr ""

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Usage: grub-menulst2cfg [INFILE [OUTFILE]]"
msgstr ""

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "grub-menulst2cfg - transform legacy menu.lst into grub.cfg"
msgstr ""

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-menulst2cfg> [I<\\,INFILE \\/>[I<\\,OUTFILE\\/>]]"
msgstr ""

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-mkconfig>(8)"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-menulst2cfg> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-menulst2cfg> programs are properly "
"installed at your site, the command"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<info grub-menulst2cfg>"
msgstr ""

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: fedora-rawhide opensuse-leap-15-5
#, no-wrap
msgid "April 2023"
msgstr ""

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "May 2023"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "grub-menulst2cfg grub-menulst2cfg [INFILE [OUTFILE]]"
msgstr ""
