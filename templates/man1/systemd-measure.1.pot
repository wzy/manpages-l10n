# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:56+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-MEASURE"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "systemd-measure"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"systemd-measure - Pre-calculate and sign expected TPM2 PCR values for booted "
"unified kernel images"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B</usr/lib/systemd/systemd-measure >B<[OPTIONS...]>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note: this command is experimental for now\\&. While it is likely to become "
"a regular component of systemd, it might still change in behaviour and "
"interface\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<systemd-measure> is a tool that may be used to pre-calculate and sign the "
"expected TPM2 PCR 11 values that should be seen when a unified Linux kernel "
"image based on B<systemd-stub>(7)  is booted up\\&. It accepts paths to the "
"ELF kernel image file, initrd image file, devicetree file, kernel command "
"line file, B<os-release>(5)  file, boot splash file, and TPM2 PCR PEM public "
"key file that make up the unified kernel image, and determines the PCR "
"values expected to be in place after booting the image\\&. Calculation "
"starts with a zero-initialized PCR 11, and is executed in a fashion "
"compatible with what systemd-stub does at boot\\&. The result may optionally "
"be signed cryptographically, to allow TPM2 policies that can only be "
"unlocked if a certain set of kernels is booted, for which such a PCR "
"signature can be provided\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The following commands are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<status>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This is the default command if none is specified\\&. This queries the local "
"system\\*(Aqs TPM2 PCR 11+12+13 values and displays them\\&. The data is "
"written in a similar format as the B<calculate> command below, and may be "
"used to quickly compare expectation with reality\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<calculate>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Pre-calculate the expected values seen in PCR register 11 after boot-up of a "
"unified kernel image consisting of the components specified with B<--"
"linux=>, B<--osrel=>, B<--cmdline=>, B<--initrd=>, B<--splash=>, B<--dtb=>, "
"B<--pcrpkey=> see below\\&. Only B<--linux=> is mandatory\\&. "
"(Alternatively, specify B<--current> to use the current values of PCR "
"register 11 instead\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<sign>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"As with the B<calculate> command, pre-calculate the expected value seen in "
"TPM2 PCR register 11 after boot-up of a unified kernel image\\&. Then, "
"cryptographically sign the resulting values with the private/public key pair "
"(RSA) configured via B<--private-key=> and B<--public-key=>\\&. This will "
"write a JSON object to standard output that contains signatures for all "
"specified PCR banks (see the B<--bank=> option below), which may be used to "
"unlock encrypted credentials (see B<systemd-creds>(1)) or LUKS volumes (see "
"B<systemd-cryptsetup@.service>(8))\\&. This allows binding secrets to a set "
"of kernels for which such PCR 11 signatures can be provided\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that a TPM2 device must be available for this signing to take place, "
"even though the result is not tied to any TPM2 device or its state\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<--linux=>I<PATH>, B<--osrel=>I<PATH>, B<--cmdline=>I<PATH>, B<--"
"initrd=>I<PATH>, B<--splash=>I<PATH>, B<--dtb=>I<PATH>, B<--pcrpkey=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When used with the B<calculate> or B<sign> verb, configures the files to "
"read the unified kernel image components from\\&. Each option corresponds "
"with the equally named section in the unified kernel PE file\\&. The B<--"
"linux=> switch expects the path to the ELF kernel file that the unified PE "
"kernel will wrap\\&. All switches except B<--linux=> are optional\\&. Each "
"option may be used at most once\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--current>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When used with the B<calculate> or B<sign> verb, takes the PCR 11 values "
"currently in effect for the system (which should typically reflect the "
"hashes of the currently booted kernel)\\&. This can be used in place of B<--"
"linux=> and the other switches listed above\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--bank=>I<DIGEST>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Controls the PCR banks to pre-calculate the PCR values for \\(en in case "
"B<calculate> or B<sign> is invoked \\(en, or the banks to show in the "
"B<status> output\\&. May be used more then once to specify multiple "
"banks\\&. If not specified, defaults to the four banks \"sha1\", \"sha256\", "
"\"sha384\", \"sha512\"\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--private-key=>I<PATH>, B<--public-key=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These switches take paths to a pair of PEM encoded RSA key files, for use "
"with the B<sign> command\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note the difference between the B<--pcrpkey=> and B<--public-key=> "
"switches\\&. The former selects the data to include in the \"\\&.pcrpkey\" "
"PE section of the unified kernel image, the latter picks the public key of "
"the key pair used to sign the resulting PCR 11 values\\&. The former is the "
"key that the booted system will likely use to lock disk and credential "
"encryption to, the latter is the key used for unlocking such resources "
"again\\&. Hence, typically the same PEM key should be supplied in both "
"cases\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If the B<--public-key=> is not specified but B<--private-key=> is specified "
"the public key is automatically derived from the private key\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--tpm2-device=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Controls which TPM2 device to use\\&. Expects a device node path referring "
"to the TPM2 chip (e\\&.g\\&.  /dev/tpmrm0)\\&. Alternatively the special "
"value \"auto\" may be specified, in order to automatically determine the "
"device node of a suitable TPM2 device (of which there must be exactly "
"one)\\&. The special value \"list\" may be used to enumerate all suitable "
"TPM2 devices currently discovered\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--phase=>I<PHASE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Controls which boot phases to calculate expected PCR 11 values for\\&. This "
"takes a series of colon-separated strings that encode boot \"paths\" for "
"entering a specific phase of the boot process\\&. Each of the specified "
"strings is measured by the systemd-pcrphase-initrd\\&.service and B<systemd-"
"pcrphase.service>(8)  into PCR 11 during different milestones of the boot "
"process\\&. This switch may be specified multiple times to calculate PCR "
"values for multiple boot phases at once\\&. If not used defaults to \"enter-"
"initrd\", \"enter-initrd:leave-initrd\", \"enter-initrd:leave-initrd:"
"sysinit\", \"enter-initrd:leave-initrd:sysinit:ready\", i\\&.e\\&. "
"calculates expected PCR values for the boot phase in the initrd, during "
"early boot, during later boot, and during system runtime, but excluding the "
"phases before the initrd or when shutting down\\&. This setting is honoured "
"both by B<calculate> and B<sign>\\&. When used with the latter it\\*(Aqs "
"particularly useful for generating PCR signatures that can only be used for "
"unlocking resources during specific parts of the boot process\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"For further details about PCR boot phases, see B<systemd-pcrphase."
"service>(8)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--append=>I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"When generating a PCR JSON signature (via the B<sign> command), combine it "
"with a previously generated PCR JSON signature, and output it as one\\&. The "
"specified path must refer to a regular file that contains a valid JSON PCR "
"signature object\\&. The specified file is not modified\\&. It will be read "
"first, then the newly generated signature appended to it, and the resulting "
"object is written to standard output\\&. Use this to generate a single JSON "
"object consisting from signatures made with a number of signing keys (for "
"example, to have one key per boot phase)\\&. The command will suppress "
"duplicates: if a specific signature is already included in a JSON signature "
"object it is not added a second time\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--json=>I<MODE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Shows output formatted as JSON\\&. Expects one of \"short\" (for the "
"shortest possible output without any redundant whitespace or line breaks), "
"\"pretty\" (for a pretty version of the same, with indentation and line "
"breaks) or \"off\" (to turn off JSON output, the default)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--no-pager>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Do not pipe output into a pager\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<Example\\ \\&1.\\ \\&Generate a unified kernel image, and calculate the "
"expected TPM PCR 11 value>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"# ukify --output foo\\&.efi \\e\n"
"     --os-release @os-release\\&.txt \\e\n"
"     --cmdline @cmdline\\&.txt \\e\n"
"     --splash splash\\&.bmp \\e\n"
"     --devicetree devicetree\\&.dtb \\e\n"
"     --measure \\e\n"
"     vmlinux initrd\\&.cpio\n"
"11:sha1=d775a7b4482450ac77e03ee19bda90bd792d6ec7\n"
"11:sha256=bc6170f9ce28eb051ab465cd62be8cf63985276766cf9faf527ffefb66f45651\n"
"11:sha384=1cf67dff4757e61e5a73d2a21a6694d668629bbc3761747d493f7f49ad720be02fd07263e1f93061243aec599d1ee4b4\n"
"11:sha512=8e79acd3ddbbc8282e98091849c3530f996303c8ac8e87a3b2378b71c8b3a6e86d5c4f41ecea9e1517090c3e8ec0c714821032038f525f744960bcd082d937da\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<Example\\ \\&2.\\ \\&Generate a private/public key pair, and a unified "
"kernel image, and a TPM PCR 11 signature for it, and embed the signature and "
"the public key in the image>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"# openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out tpm2-pcr-private\\&.pem\n"
"# openssl rsa -pubout -in tpm2-pcr-private\\&.pem -out tpm2-pcr-public\\&.pem\n"
"# systemd-measure sign \\e\n"
"     --linux=vmlinux \\e\n"
"     --osrel=os-release\\&.txt \\e\n"
"     --cmdline=cmdline\\&.txt \\e\n"
"     --initrd=initrd\\&.cpio \\e\n"
"     --splash=splash\\&.bmp \\e\n"
"     --dtb=devicetree\\&.dtb \\e\n"
"     --pcrpkey=tpm2-pcr-public\\&.pem \\e\n"
"     --bank=sha1 \\e\n"
"     --bank=sha256 \\e\n"
"     --private-key=tpm2-pcr-private\\&.pem \\e\n"
"     --public-key=tpm2-pcr-public\\&.pem E<gt>tpm2-pcr-signature\\&.json\n"
"# ukify --output foo\\&.efi \\e\n"
"     --os-release @os-release\\&.txt \\e\n"
"     --cmdline @cmdline\\&.txt \\e\n"
"     --splash splash\\&.bmp \\e\n"
"     --devicetree devicetree\\&.dtb \\e\n"
"     --pcr-private-key tpm2-pcr-private\\&.pem \\e\n"
"     --pcr-public-key tpm2-pcr-public\\&.pem \\e\n"
"     --pcr-banks sha1,sha256 \\e\n"
"     vmlinux initrd\\&.cpio\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Later on, enroll the signed PCR policy on a LUKS volume:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "# systemd-cryptenroll --tpm2-device=auto --tpm2-public-key=tpm2-pcr-public\\&.pem --tpm2-signature=tpm2-pcr-signature\\&.json /dev/sda5\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "And then unlock the device with the signature:"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "# /usr/lib/systemd/systemd-cryptsetup attach myvolume /dev/sda5 - tpm2-device=auto,tpm2-signature=/path/to/tpm2-pcr-signature\\&.json\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Note that when the generated unified kernel image foo\\&.efi is booted the "
"signature and public key files will be placed at locations B<systemd-"
"cryptenroll> and B<systemd-cryptsetup> will look for anyway, and thus these "
"paths do not actually need to be specified\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<Example\\ \\&3.\\ \\&Introduce a second public key, signing the same "
"kernel PCR measurements, but only for the initrd boot phase>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"This example extends the previous one, but we now introduce a second signing "
"key that is only used to sign PCR policies restricted to the initrd boot "
"phase\\&. This can be used to lock down root volumes in a way that they can "
"only be unlocked before the transition to the host system\\&. Thus we have "
"two classes of secrets or credentials: one that can be unlocked during the "
"entire runtime, and the other that can only be used in the initrd\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"# openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out tpm2-pcr-private\\&.pem\n"
"# openssl rsa -pubout -in tpm2-pcr-private\\&.pem -out tpm2-pcr-public\\&.pem\n"
"# systemd-measure sign \\e\n"
"     --linux=vmlinux \\e\n"
"     --osrel=os-release\\&.txt \\e\n"
"     --cmdline=cmdline\\&.txt \\e\n"
"     --initrd=initrd\\&.cpio \\e\n"
"     --splash=splash\\&.bmp \\e\n"
"     --dtb=devicetree\\&.dtb \\e\n"
"     --pcrpkey=tpm2-pcr-public\\&.pem \\e\n"
"     --bank=sha1 \\e\n"
"     --bank=sha256 \\e\n"
"     --private-key=tpm2-pcr-private\\&.pem \\e\n"
"     --public-key=tpm2-pcr-public\\&.pem E<gt>tpm2-pcr-signature\\&.json\\&.tmp\n"
"# openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out tpm2-pcr-initrd-private\\&.pem\n"
"# openssl rsa -pubout -in tpm2-pcr-initrd-private\\&.pem -out tpm2-pcr-initrd-public\\&.pem\n"
"# systemd-measure sign \\e\n"
"     --linux=vmlinux \\e\n"
"     --osrel=os-release\\&.txt \\e\n"
"     --cmdline=cmdline\\&.txt \\e\n"
"     --initrd=initrd\\&.cpio \\e\n"
"     --splash=splash\\&.bmp \\e\n"
"     --dtb=devicetree\\&.dtb \\e\n"
"     --pcrpkey=tpm2-pcr-public\\&.pem \\e\n"
"     --bank=sha1 \\e\n"
"     --bank=sha256 \\e\n"
"     --private-key=tpm2-pcr-initrd-private\\&.pem \\e\n"
"     --public-key=tpm2-pcr-initrd-public\\&.pem \\e\n"
"     --phase=enter-initrd \\e\n"
"     --append=tpm2-pcr-signature\\&.json\\&.tmp E<gt>tpm2-pcr-signature\\&.json\n"
"# ukify --output foo\\&.efi \\e\n"
"     --os-release @os-release\\&.txt \\e\n"
"     --cmdline @cmdline\\&.txt \\e\n"
"     --splash splash\\&.bmp \\e\n"
"     --devicetree devicetree\\&.dtb \\e\n"
"     --pcr-private-key tpm2-pcr-initrd-private\\&.pem \\e\n"
"     --pcr-public-key tpm2-pcr-initrd-public\\&.pem \\e\n"
"     --section \\&.pcrsig=@tpm2-pcr-signature\\&.json \\e\n"
"     --section \\&.pcrpkey=@tpm2-pcr-public\\&.pem \\e\n"
"     vmlinux initrd\\&.cpio\n"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Note that in this example the \"\\&.pcrpkey\" PE section contains the key "
"covering all boot phases\\&. The \"\\&.pcrpkey\" is used in the default "
"policies of B<systemd-cryptenroll> and B<systemd-creds>\\&. To use the "
"stricter tpm-pcr-initrd-public\\&.pem-bound policy, specify B<--tpm2-public-"
"key=> on the command line of those tools\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<ukify>(1), B<systemd-creds>(1), "
"B<systemd-cryptsetup@.service>(8), B<systemd-pcrphase.service>(8)"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B</lib/systemd/systemd-measure >B<[OPTIONS...]>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"As with the B<calculate> command, pre-calculate the expected value seen in "
"TPM2 PCR register 11 after boot-up of a unified kernel image\\&. Then, "
"cryptographically sign the resulting values with the private/public key pair "
"(RSA) configured via B<--private-key=> and B<--public-key=>\\&. This will "
"write a JSON object to standard output that contains signatures for all "
"specified PCR banks (see B<--pcr-bank=>) below, which may be used to unlock "
"encrypted credentials (see B<systemd-creds>(1)) or LUKS volumes (see "
"B<systemd-cryptsetup@.service>(8))\\&. This allows binding secrets to a set "
"of kernels for which such PCR 11 signatures can be provided\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<--linux=PATH>, B<--osrel=PATH>, B<--cmdline=PATH>, B<--initrd=PATH>, B<--"
"splash=PATH>, B<--dtb=PATH>, B<--pcrpkey=PATH>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--bank=DIGEST>"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid "B<--private-key=PATH>, B<--public-key=PATH>"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"# objcopy \\e\n"
"    --add-section \\&.linux=vmlinux --change-section-vma \\&.linux=0x2000000 \\e\n"
"    --add-section \\&.osrel=os-release\\&.txt --change-section-vma \\&.osrel=0x20000 \\e\n"
"    --add-section \\&.cmdline=cmdline\\&.txt --change-section-vma \\&.cmdline=0x30000 \\e\n"
"    --add-section \\&.initrd=initrd\\&.cpio --change-section-vma \\&.initrd=0x3000000 \\e\n"
"    --add-section \\&.splash=splash\\&.bmp --change-section-vma \\&.splash=0x100000 \\e\n"
"    --add-section \\&.dtb=devicetree\\&.dtb --change-section-vma \\&.dtb=0x40000 \\e\n"
"    /usr/lib/systemd/boot/efi/linuxx64\\&.efi\\&.stub \\e\n"
"    foo\\&.efi\n"
"# systemd-measure calculate \\e\n"
"     --linux=vmlinux \\e\n"
"     --osrel=os-release\\&.txt \\e\n"
"     --cmdline=cmdline\\&.txt \\e\n"
"     --initrd=initrd\\&.cpio \\e\n"
"     --splash=splash\\&.bmp \\e\n"
"     --dtb=devicetree\\&.dtb\n"
"11:sha1=d775a7b4482450ac77e03ee19bda90bd792d6ec7\n"
"11:sha256=bc6170f9ce28eb051ab465cd62be8cf63985276766cf9faf527ffefb66f45651\n"
"11:sha384=1cf67dff4757e61e5a73d2a21a6694d668629bbc3761747d493f7f49ad720be02fd07263e1f93061243aec599d1ee4b4\n"
"11:sha512=8e79acd3ddbbc8282e98091849c3530f996303c8ac8e87a3b2378b71c8b3a6e86d5c4f41ecea9e1517090c3e8ec0c714821032038f525f744960bcd082d937da\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
#, no-wrap
msgid ""
"# openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out tpm2-pcr-private\\&.pem\n"
"# openssl rsa -pubout -in tpm2-pcr-private\\&.pem -out tpm2-pcr-public\\&.pem\n"
"# systemd-measure sign \\e\n"
"     --linux=vmlinux \\e\n"
"     --osrel=os-release\\&.txt \\e\n"
"     --cmdline=cmdline\\&.txt \\e\n"
"     --initrd=initrd\\&.cpio \\e\n"
"     --splash=splash\\&.bmp \\e\n"
"     --dtb=devicetree\\&.dtb \\e\n"
"     --pcrpkey=tpm2-pcr-public\\&.pem \\e\n"
"     --bank=sha1 \\e\n"
"     --bank=sha256 \\e\n"
"     --private-key=tpm2-pcr-private\\&.pem \\e\n"
"     --public-key=tpm2-pcr-public\\&.pem E<gt> tpm2-pcr-signature\\&.json\n"
"# objcopy \\e\n"
"    --add-section \\&.linux=vmlinux --change-section-vma \\&.linux=0x2000000 \\e\n"
"    --add-section \\&.osrel=os-release\\&.txt --change-section-vma \\&.osrel=0x20000 \\e\n"
"    --add-section \\&.cmdline=cmdline\\&.txt --change-section-vma \\&.cmdline=0x30000 \\e\n"
"    --add-section \\&.initrd=initrd\\&.cpio --change-section-vma \\&.initrd=0x3000000 \\e\n"
"    --add-section \\&.splash=splash\\&.bmp --change-section-vma \\&.splash=0x100000 \\e\n"
"    --add-section \\&.dtb=devicetree\\&.dtb --change-section-vma \\&.dtb=0x40000 \\e\n"
"    --add-section \\&.pcrsig=tpm2-pcr-signature\\&.json --change-section-vma \\&.splash=0x80000 \\e\n"
"    --add-section \\&.pcrpkey=tpm2-pcr-public\\&.pem --change-section-vma \\&.splash=0x90000 \\e\n"
"    /usr/lib/systemd/boot/efi/linuxx64\\&.efi\\&.stub \\e\n"
"    foo\\&.efi\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
#, no-wrap
msgid "# /lib/systemd/systemd-cryptsetup attach myvolume /dev/sda5 - tpm2-device=auto,tpm2-signature=/path/to/tpm2-pcr-signature\\&.json\n"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<objcopy>(1), B<systemd-creds>(1), "
"B<systemd-cryptsetup@.service>(8), B<systemd-pcrphase.service>(1)"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-38
msgid ""
"As with the B<calculate> command, pre-calculate the expected value seen in "
"TPM2 PCR register 11 after boot-up of a unified kernel image\\&. Then, "
"cryptographically sign the resulting values with the private/public key pair "
"(RSA) configured via B<--private-key=> and B<--public-key=>\\&. This will "
"write a JSON object to standard output that contains signatures for all "
"specified PCR banks (see the B<--pcr-bank=> option below), which may be used "
"to unlock encrypted credentials (see B<systemd-creds>(1)) or LUKS volumes "
"(see B<systemd-cryptsetup@.service>(8))\\&. This allows binding secrets to a "
"set of kernels for which such PCR 11 signatures can be provided\\&."
msgstr ""

#. type: Plain text
#: debian-unstable
msgid ""
"B<systemd>(1), B<systemd-stub>(7), B<ukify>(1), B<systemd-creds>(1), "
"B<systemd-cryptsetup@.service>(8), B<systemd-pcrphase.service>(1)"
msgstr ""
