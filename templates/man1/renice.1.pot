# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "RENICE"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "renice - alter priority of running processes"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<renice> [B<-n>] I<priority> [B<-g>|B<-p>|B<-u>] I<identifier>..."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<renice> alters the scheduling priority of one or more running processes. "
"The first argument is the I<priority> value to be used. The other arguments "
"are interpreted as process IDs (by default), process group IDs, user IDs, or "
"user names. B<renice>\\(aqing a process group causes all processes in the "
"process group to have their scheduling priority altered. B<renice>\\(aqing a "
"user causes all processes owned by the user to have their scheduling "
"priority altered."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-n>, B<--priority> I<priority>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Specify the scheduling I<priority> to be used for the process, process "
"group, or user. Use of the option B<-n> or B<--priority> is optional, but "
"when used it must be the first argument."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-g>, B<--pgrp>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Interpret the succeeding arguments as process group IDs."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-p>, B<--pid>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Interpret the succeeding arguments as process IDs (the default)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-u>, B<--user>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Interpret the succeeding arguments as usernames or UIDs."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "Print version and exit."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "I</etc/passwd>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "to map user names to user IDs"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Users other than the superuser may only alter the priority of processes they "
"own. Furthermore, an unprivileged user can only I<increase> the \"nice "
"value\" (i.e., choose a lower priority) and such changes are irreversible "
"unless (since Linux 2.6.12) the user has a suitable \"nice\" resource limit "
"(see B<ulimit>(1p) and B<getrlimit>(2))."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The superuser may alter the priority of any process and set the priority to "
"any value in the range -20 to 19. Useful priorities are: 19 (the affected "
"processes will run only when nothing else in the system wants to), 0 (the "
"\"base\" scheduling priority), anything negative (to make things go very "
"fast)."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "The B<renice> command appeared in 4.0BSD."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The following command would change the priority of the processes with PIDs "
"987 and 32, plus all processes owned by the users daemon and root:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<renice +1 987 -u daemon root -p 32>"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<nice>(1), B<chrt>(1), B<getpriority>(2), B<setpriority>(2), "
"B<credentials>(7), B<sched>(7)"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The B<renice> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr ""
