# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:55+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYSTEMD-DISSECT"
msgstr ""

#. type: TH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 253"
msgstr ""

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "systemd-dissect"
msgstr ""

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "systemd-dissect - Dissect Discoverable Disk Images (DDIs)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<systemd-dissect >B<[OPTIONS...]>B< >I<IMAGE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<systemd-dissect >B<[OPTIONS...]>B< >B<--mount>B< >I<IMAGE>B< >I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<systemd-dissect >B<[OPTIONS...]>B< >B<--umount>B< >I<PATH>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<systemd-dissect >B<[OPTIONS...]>B< >B<--list>B< >I<IMAGE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<systemd-dissect >B<[OPTIONS...]>B< >B<--mtree>B< >I<IMAGE>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<systemd-dissect >B<[OPTIONS...]>B< >B<--with>B< >I<IMAGE>B< "
">B<[>I<COMMAND>...]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<systemd-dissect >B<[OPTIONS...]>B< >B<--copy-from>B< >I<IMAGE>B< "
">I<PATH>B< >B<[>I<TARGET>]"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<systemd-dissect >B<[OPTIONS...]>B< >B<--copy-to>B< >I<IMAGE>B< "
">B<[>I<SOURCE>]B< >I<PATH>"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"B<systemd-dissect> is a tool for introspecting and interacting with file "
"system OS disk images, specifically Discoverable Disk Images (DDIs)\\&. It "
"supports four different operations:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Show general OS image information, including the image\\*(Aqs B<os-"
"release>(5)  data, machine ID, partition information and more\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mount an OS image to a local directory\\&. In this mode it will dissect the "
"OS image and mount the included partitions according to their designation "
"onto a directory and possibly sub-directories\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Unmount an OS image from a local directory\\&. In this mode it will "
"recursively unmount the mounted partitions and remove the underlying loop "
"device, including all the partition sub-devices\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Copy files and directories in and out of an OS image\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The tool may operate on three types of OS images:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"OS disk images containing a GPT partition table envelope, with partitions "
"marked according to the \\m[blue]B<Discoverable Partitions "
"Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"OS disk images containing just a plain file-system without an enveloping "
"partition table\\&. (This file system is assumed to be the root file system "
"of the OS\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"OS disk images containing a GPT or MBR partition table, with a single "
"partition only\\&. (This partition is assumed to contain the root file "
"system of the OS\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"OS images may use any kind of Linux-supported file systems\\&. In addition "
"they may make use of LUKS disk encryption, and contain Verity integrity "
"information\\&. Note that qualifying OS images may be booted with B<systemd-"
"nspawn>(1)\\*(Aqs B<--image=> switch, and be used as root file system for "
"system service using the I<RootImage=> unit file setting, see B<systemd."
"exec>(5)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Note that the partition table shown when invoked without command switch (as "
"listed below) does not necessarily show all partitions included in the "
"image, but just the partitions that are understood and considered part of an "
"OS disk image\\&. Specifically, partitions of unknown types are ignored, as "
"well as duplicate partitions (i\\&.e\\&. more than one per partition type), "
"as are root and /usr/ partitions of architectures not compatible with the "
"local system\\&. In other words: this tool will display what it operates "
"with when mounting the image\\&. To display the complete list of partitions "
"use a tool such as B<fdisk>(8)\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COMMANDS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If neither of the command switches listed below are passed the specified "
"disk image is opened and general information about the image and the "
"contained partitions and their use is shown\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--mount>, B<-m>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mount the specified OS image to the specified directory\\&. This will "
"dissect the image, determine the OS root file system \\(em as well as "
"possibly other partitions \\(em and mount them to the specified "
"directory\\&. If the OS image contains multiple partitions marked with the "
"\\m[blue]B<Discoverable Partitions Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2 "
"multiple nested mounts are established\\&. This command expects two "
"arguments: a path to an image file and a path to a directory where to mount "
"the image\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"To unmount an OS image mounted like this use the B<--umount> operation\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"When the OS image contains LUKS encrypted or Verity integrity protected file "
"systems appropriate volumes are automatically set up and marked for "
"automatic disassembly when the image is unmounted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The OS image may either be specified as path to an OS image stored in a "
"regular file or may refer to block device node (in the latter case the block "
"device must be the \"whole\" device, i\\&.e\\&. not a partition device)\\&. "
"(The other supported commands described here support this, too\\&.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All mounted file systems are checked with the appropriate B<fsck>(8)  "
"implementation in automatic fixing mode, unless explicitly turned off (B<--"
"fsck=no>) or read-only operation is requested (B<--read-only>)\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-M>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "This is a shortcut for B<--mount --mkdir>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--umount>, B<-u>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Unmount an OS image from the specified directory\\&. This command expects "
"one argument: a directory where an OS image was mounted\\&."
msgstr ""

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"All mounted partitions will be recursively unmounted, and the underlying "
"loop device will be removed, along with all its partition sub-devices\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<-U>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "This is a shortcut for B<--umount --rmdir>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--list>, B<-l>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Prints the paths of all the files and directories in the specified OS image "
"to standard output\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--mtree>, B<-l>"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Generates a BSD B<mtree>(8)  compatible file manifest of the specified disk "
"image\\&. This is useful for comparing disk image contents in detail, "
"including inode information and other metadata\\&. While the generated "
"manifest will contain detailed inode information, it currently excludes "
"extended attributes, file system capabilities, MAC labels, B<chattr>(1)  "
"file flags, B<btrfs>(5)  subvolume information, and various other file "
"metadata\\&. File content information is shown via a SHA256 digest\\&. "
"Additional fields might be added in future\\&. Note that inode information "
"such as link counts, inode numbers and timestamps is excluded from the "
"output on purpose, as it typically complicates reproducibility\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--with>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Runs the specified command with the specified OS image mounted\\&. This will "
"mount the image to a temporary directory, switch the current working "
"directory to it, and invoke the specified command line as child process\\&. "
"Once the process ends it will unmount the image again, and remove the "
"temporary directory\\&. If no command is specified a shell is invoked\\&. "
"The image is mounted writable, use B<--read-only> to switch to read-only "
"operation\\&. The invoked process will have the I<$SYSTEMD_DISSECT_ROOT> "
"environment variable set, containing the absolute path name of the temporary "
"mount point, i\\&.e\\&. the same directory that is set as the current "
"working directory\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--copy-from>, B<-x>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Copies a file or directory from the specified OS image into the specified "
"location on the host file system\\&. Expects three arguments: a path to an "
"image file, a source path (relative to the image\\*(Aqs root directory) and "
"a destination path (relative to the current working directory, or an "
"absolute path, both outside of the image)\\&. If the destination path is "
"omitted or specified as dash (\"-\"), the specified file is written to "
"standard output\\&. If the source path in the image file system refers to a "
"regular file it is copied to the destination path\\&. In this case access "
"mode, extended attributes and timestamps are copied as well, but file "
"ownership is not\\&. If the source path in the image refers to a directory, "
"it is copied to the destination path, recursively with all containing files "
"and directories\\&. In this case the file ownership is copied too\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--copy-to>, B<-a>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Copies a file or directory from the specified location in the host file "
"system into the specified OS image\\&. Expects three arguments: a path to an "
"image file, a source path (relative to the current working directory, or an "
"absolute path, both outside of the image) and a destination path (relative "
"to the image\\*(Aqs root directory)\\&. If the source path is omitted or "
"specified as dash (\"-\"), the data to write is read from standard input\\&. "
"If the source path in the host file system refers to a regular file, it is "
"copied to the destination path\\&. In this case access mode, extended "
"attributes and timestamps are copied as well, but file ownership is not\\&. "
"If the source path in the host file system refers to a directory it is "
"copied to the destination path, recursively with all containing files and "
"directories\\&. In this case the file ownership is copied too\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"As with B<--mount> file system checks are implicitly run before the copy "
"operation begins\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--discover>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Show a list of DDIs in well-known directories\\&. This will show machine, "
"portable service and system extension disk images in the usual directories /"
"usr/lib/machines/, /usr/lib/portables/, /usr/lib/extensions/, /var/lib/"
"machines/, /var/lib/portables/, /var/lib/extensions/ and so on\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print a short help text and exit\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--version>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Print a short version string and exit\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following options are understood:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--read-only>, B<-r>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Operate in read-only mode\\&. By default B<--mount> will establish writable "
"mount points\\&. If this option is specified they are established in read-"
"only mode instead\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--fsck=no>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Turn off automatic file system checking\\&. By default when an image is "
"accessed for writing (by B<--mount> or B<--copy-to>) the file systems "
"contained in the OS image are automatically checked using the appropriate "
"B<fsck>(8)  command, in automatic fixing mode\\&. This behavior may be "
"switched off using B<--fsck=no>\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--growfs=no>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Turn off automatic growing of accessed file systems to their partition size, "
"if marked for that in the GPT partition table\\&. By default when an image "
"is accessed for writing (by B<--mount> or B<--copy-to>) the file systems "
"contained in the OS image are automatically grown to their partition sizes, "
"if bit 59 in the GPT partition flags is set for partition types that are "
"defined by the \\m[blue]B<Discoverable Partitions "
"Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&. This behavior may be switched "
"off using B<--growfs=no>\\&. File systems are grown automatically on access "
"if all of the following conditions are met:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The file system is mounted writable"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file system currently is smaller than the partition it is contained in "
"(and thus can be grown)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The image contains a GPT partition table"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file system is stored on a partition defined by the Discoverable "
"Partitions Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Bit 59 of the GPT partition flags for this partition is set, as per "
"specification"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<--growfs=no> option is not passed\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--mkdir>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If combined with B<--mount> the directory to mount the OS image to is "
"created if it is missing\\&. Note that the directory is not automatically "
"removed when the disk image is unmounted again\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<--rmdir>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If combined with B<--umount> the specified directory where the OS image is "
"mounted is removed after unmounting the OS image\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--discard=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Takes one of \"disabled\", \"loop\", \"all\", \"crypto\"\\&. If \"disabled\" "
"the image is accessed with empty block discarding turned off\\&. If \"loop\" "
"discarding is enabled if operating on a regular file\\&. If \"crypt\" "
"discarding is enabled even on encrypted file systems\\&. If \"all\" "
"discarding is unconditionally enabled\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<--in-memory>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"If specified an in-memory copy of the specified disk image is used\\&. This "
"may be used to operate with write-access on a (possibly read-only) image, "
"without actually modifying the original file\\&. This may also be used in "
"order to operate on a disk image without keeping the originating file system "
"busy, in order to allow it to be unmounted\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--root-hash=>, B<--root-hash-sig=>, B<--verity-data=>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Configure various aspects of Verity data integrity for the OS image\\&. "
"Option B<--root-hash=> specifies a hex-encoded top-level Verity hash to use "
"for setting up the Verity integrity protection\\&. Option B<--root-hash-"
"sig=> specifies the path to a file containing a PKCS#7 signature for the "
"hash\\&. This signature is passed to the kernel during activation, which "
"will match it against signature keys available in the kernel keyring\\&. "
"Option B<--verity-data=> specifies a path to a file with the Verity data to "
"use for the OS image, in case it is stored in a detached file\\&. It is "
"recommended to embed the Verity data directly in the image, using the Verity "
"mechanisms in the \\m[blue]B<Discoverable Partitions "
"Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--no-pager>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Do not pipe output into a pager\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--no-legend>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Do not print the legend, i\\&.e\\&. column headers and the footer with "
"hints\\&."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<--json=>I<MODE>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Shows output formatted as JSON\\&. Expects one of \"short\" (for the "
"shortest possible output without any redundant whitespace or line breaks), "
"\"pretty\" (for a pretty version of the same, with indentation and line "
"breaks) or \"off\" (to turn off JSON output, the default)\\&."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, 0 is returned, a non-zero failure code otherwise\\&. If the B<--"
"with> command is used the exit status of the invoked command is "
"propagated\\&."
msgstr ""

#. type: SH
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<Example\\ \\&1.\\ \\&Generate a tarball from an OS disk image>"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "$ systemd-dissect --with foo\\&.raw tar cz \\&. E<gt>foo\\&.tar\\&.gz\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<systemd>(1), B<systemd-nspawn>(1), B<systemd.exec>(5), "
"\\m[blue]B<Discoverable Partitions "
"Specification>\\m[]\\&\\s-2\\u[1]\\d\\s+2, B<umount>(8), B<fdisk>(8)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Discoverable Partitions Specification"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"\\%https://uapi-group.org/specifications/specs/"
"discoverable_partitions_specification"
msgstr ""

#. type: TH
#: debian-bookworm
#, no-wrap
msgid "systemd 252"
msgstr ""

#. type: Plain text
#: debian-bookworm
msgid ""
"B<systemd-dissect> is a tool for introspecting and interacting with file "
"system OS disk images, specifically Discoverable Disk Images (DDIs)\\&. It "
"supports five different operations:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"All mounted partitions will be recursively unmounted, and the underlying "
"loop device will be removed, along with all it\\*(Aqs partition sub-"
"devices\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "On success, 0 is returned, a non-zero failure code otherwise\\&."
msgstr ""

#. type: Plain text
#: debian-bookworm opensuse-leap-15-5
msgid "\\%https://systemd.io/DISCOVERABLE_PARTITIONS"
msgstr ""

#. type: Plain text
#: debian-unstable fedora-38
msgid ""
"Generates a BSD B<mtree>(8)  compatible file manifest of the specified disk "
"image\\&. This is useful for comparing disk image contents in detail, "
"including inode information and other metadata\\&. While the generated "
"manifest will contain detailed inode information, it currently excludes "
"extended attributes, file system capabilities, MAC labels, B<chattr>(1)  "
"file flags, btrfs subvolume information, and various other file metadata\\&. "
"File content information is shown via a SHA256 digest\\&. Additional fields "
"might be added in future\\&. Note that inode information such as link "
"counts, inode numbers and timestamps is excluded from the output on purpose, "
"as it typically complicates reproducibility\\&."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "systemd-dissect - Dissect file system OS images"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<systemd-dissect> is a tool for introspecting and interacting with file "
"system OS disk images\\&. It supports four different operations:"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"To unmount an OS image mounted like this use B<umount>(8)\\*(Aqs B<-R> "
"switch (for recursive operation), so that the OS image and all nested "
"partition mounts are unmounted\\&."
msgstr ""
