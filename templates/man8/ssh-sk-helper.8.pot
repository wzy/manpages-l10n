# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:52+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Dd
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "$Mdocdate: April 29 2022 $"
msgstr ""

#. type: Dt
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SSH-SK-HELPER 8"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm ssh-sk-helper>"
msgstr ""

#. type: Nd
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OpenSSH helper for FIDO authenticator support"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm> E<.Op Fl v>"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"E<.Nm> is used by E<.Xr ssh 1>, E<.Xr ssh-agent 1>, and E<.Xr ssh-keygen 1> "
"to access keys provided by a FIDO authenticator."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid "E<.Nm> is not intended to be invoked directly by the user."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "A single option is supported:"
msgstr ""

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl v"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Verbose mode.  Causes E<.Nm> to print debugging messages about its "
"progress.  This is helpful in debugging problems.  Multiple E<.Fl v> options "
"increase the verbosity.  The maximum is 3."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Note that E<.Xr ssh 1>, E<.Xr ssh-agent 1>, and E<.Xr ssh-keygen 1> will "
"automatically pass the E<.Fl v> flag to E<.Nm> when they have themselves "
"been placed in debug mode."
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid "E<.Xr ssh 1>, E<.Xr ssh-agent 1>, E<.Xr ssh-keygen 1>"
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm> first appeared in E<.Ox 6.7>."
msgstr ""

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.An Damien Miller Aq Mt djm@openbsd.org>"
msgstr ""

#. type: Dd
#: fedora-38
#, no-wrap
msgid "$Mdocdate: December 21 2019 $"
msgstr ""

#. type: Plain text
#: fedora-38
msgid ""
"E<.Nm> is used by E<.Xr ssh-agent 1> to access keys provided by a FIDO "
"authenticator."
msgstr ""

#. type: Plain text
#: fedora-38
msgid ""
"E<.Nm> is not intended to be invoked by the user, but from E<.Xr ssh-agent "
"1>."
msgstr ""

#. type: Plain text
#: fedora-38
msgid ""
"Note that E<.Xr ssh-agent 1> will automatically pass the E<.Fl v> flag to E<."
"Nm> when it has itself been placed in debug mode."
msgstr ""

#. type: Plain text
#: fedora-38
msgid "E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-agent 1>"
msgstr ""
