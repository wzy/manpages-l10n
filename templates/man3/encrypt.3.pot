# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-06-27 19:25+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "encrypt"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "encrypt, setkey, encrypt_r, setkey_r - encrypt 64-bit messages"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Encryption and decryption library (I<libcrypto>, I<-lcrypto>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _XOPEN_SOURCE>       /* See feature_test_macros(7) */\n"
"B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] void encrypt(char >I<block>B<[64], int >I<edflag>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _XOPEN_SOURCE>       /* See feature_test_macros(7) */\n"
"B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] void setkey(const char *>I<key>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>         /* See feature_test_macros(7) */\n"
"B<#include E<lt>crypt.hE<gt>>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] void setkey_r(const char *>I<key>B<, struct crypt_data *>I<data>B<);>\n"
"B<[[deprecated]] void encrypt_r(char *>I<block>B<, int >I<edflag>B<,>\n"
"B<                              struct crypt_data *>I<data>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions encrypt and decrypt 64-bit messages.  The B<setkey>()  "
"function sets the key used by B<encrypt>().  The I<key> argument used here "
"is an array of 64 bytes, each of which has numerical value 1 or 0.  The "
"bytes key[n] where n=8*i-1 are ignored, so that the effective key length is "
"56 bits."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<encrypt>()  function modifies the passed buffer, encoding if I<edflag> "
"is 0, and decoding if 1 is being passed.  Like the I<key> argument, also "
"I<block> is a bit vector representation of the actual value that is "
"encoded.  The result is returned in that same vector."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These two functions are not reentrant, that is, the key data is kept in "
"static storage.  The functions B<setkey_r>()  and B<encrypt_r>()  are the "
"reentrant versions.  They use the following structure to hold the key data:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct crypt_data {\n"
"    char keysched[16 * 8];\n"
"    char sb0[32768];\n"
"    char sb1[32768];\n"
"    char sb2[32768];\n"
"    char sb3[32768];\n"
"    char crypt_3_buf[14];\n"
"    char current_salt[2];\n"
"    long current_saltbits;\n"
"    int  direction;\n"
"    int  initialized;\n"
"};\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Before calling B<setkey_r>()  set I<data-E<gt>initialized> to zero."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These functions do not return any value."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set I<errno> to zero before calling the above functions.  On success, "
"I<errno> is unchanged."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function is not provided.  (For example because of former USA export "
"restrictions.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<encrypt>(),\n"
"B<setkey>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:crypt"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<encrypt_r>(),\n"
"B<setkey_r>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<encrypt>()"
msgstr ""

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<setkey>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<encrypt_r>()"
msgstr ""

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<setkey_r>()"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Removed in glibc 2.28."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Because they employ the DES block cipher, which is no longer considered "
"secure, these functions were removed from glibc.  Applications should switch "
"to a modern cryptography library, such as B<libgcrypt>."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SUS, SVr4."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Availability in glibc"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<crypt>(3)."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Features in glibc"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "In glibc 2.2, these functions use the DES algorithm."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE\n"
"#include E<lt>crypt.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    char key[64];\n"
"    char orig[9] = \"eggplant\";\n"
"    char buf[64];\n"
"    char txt[9];\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (size_t i = 0; i E<lt> 64; i++) {\n"
"        key[i] = rand() & 1;\n"
"    }\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (size_t i = 0; i E<lt> 8; i++) {\n"
"        for (size_t j = 0; j E<lt> 8; j++) {\n"
"            buf[i * 8 + j] = orig[i] E<gt>E<gt> j & 1;\n"
"        }\n"
"        setkey(key);\n"
"    }\n"
"    printf(\"Before encrypting: %s\\en\", orig);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    encrypt(buf, 0);\n"
"    for (size_t i = 0; i E<lt> 8; i++) {\n"
"        for (size_t j = 0, txt[i] = \\[aq]\\e0\\[aq]; j E<lt> 8; j++) {\n"
"            txt[i] |= buf[i * 8 + j] E<lt>E<lt> j;\n"
"        }\n"
"        txt[8] = \\[aq]\\e0\\[aq];\n"
"    }\n"
"    printf(\"After encrypting:  %s\\en\", txt);\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    encrypt(buf, 1);\n"
"    for (size_t i = 0; i E<lt> 8; i++) {\n"
"        for (size_t j = 0, txt[i] = \\[aq]\\e0\\[aq]; j E<lt> 8; j++) {\n"
"            txt[i] |= buf[i * 8 + j] E<lt>E<lt> j;\n"
"        }\n"
"        txt[8] = \\[aq]\\e0\\[aq];\n"
"    }\n"
"    printf(\"After decrypting:  %s\\en\", txt);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#. #-#-#-#-#  archlinux: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: encrypt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<cbc_crypt>(3), B<crypt>(3), B<ecb_crypt>(3)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Because they employ the DES block cipher, which is no longer considered "
"secure, B<encrypt>(), B<encrypt_r>(), B<setkey>(), and B<setkey_r>()  were "
"removed in glibc 2.28.  Applications should switch to a modern cryptography "
"library, such as B<libgcrypt>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<encrypt>(), B<setkey>(): POSIX.1-2001, POSIX.1-2008, SUS, SVr4."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "The functions B<encrypt_r>()  and B<setkey_r>()  are GNU extensions."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "ENCRYPT"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2018-04-30"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<void encrypt(char >I<block>B<[64], int >I<edflag>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<void setkey(const char *>I<key>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<void setkey_r(const char *>I<key>B<, struct crypt_data *>I<data>B<);>\n"
"B<void encrypt_r(char *>I<block>B<, int >I<edflag>B<, struct crypt_data *>I<data>B<);>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Each of these requires linking with I<-lcrypt>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct crypt_data {\n"
"    char     keysched[16 * 8];\n"
"    char     sb0[32768];\n"
"    char     sb1[32768];\n"
"    char     sb2[32768];\n"
"    char     sb3[32768];\n"
"    char     crypt_3_buf[14];\n"
"    char     current_salt[2];\n"
"    long int current_saltbits;\n"
"    int      direction;\n"
"    int      initialized;\n"
"};\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Set I<errno> to zero before calling the above functions.  On success, it is "
"unchanged."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"#define _XOPEN_SOURCE\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>unistd.hE<gt>\n"
"#include E<lt>crypt.hE<gt>\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"int\n"
"main(void)\n"
"{\n"
"    char key[64];\n"
"    char orig[9] = \"eggplant\";\n"
"    char buf[64];\n"
"    char txt[9];\n"
"    int i, j;\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    for (i = 0; i E<lt> 64; i++) {\n"
"        key[i] = rand() & 1;\n"
"    }\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    for (i = 0; i E<lt> 8; i++) {\n"
"        for (j = 0; j E<lt> 8; j++) {\n"
"            buf[i * 8 + j] = orig[i] E<gt>E<gt> j & 1;\n"
"        }\n"
"        setkey(key);\n"
"    }\n"
"    printf(\"Before encrypting: %s\\en\", orig);\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    encrypt(buf, 0);\n"
"    for (i = 0; i E<lt> 8; i++) {\n"
"        for (j = 0, txt[i] = \\(aq\\e0\\(aq; j E<lt> 8; j++) {\n"
"            txt[i] |= buf[i * 8 + j] E<lt>E<lt> j;\n"
"        }\n"
"        txt[8] = \\(aq\\e0\\(aq;\n"
"    }\n"
"    printf(\"After encrypting:  %s\\en\", txt);\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    encrypt(buf, 1);\n"
"    for (i = 0; i E<lt> 8; i++) {\n"
"        for (j = 0, txt[i] = \\(aq\\e0\\(aq; j E<lt> 8; j++) {\n"
"            txt[i] |= buf[i * 8 + j] E<lt>E<lt> j;\n"
"        }\n"
"        txt[8] = \\(aq\\e0\\(aq;\n"
"    }\n"
"    printf(\"After decrypting:  %s\\en\", txt);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""

#.  .BR fcrypt (3)
#. type: Plain text
#: opensuse-leap-15-5
msgid "B<cbc_crypt>(3), B<crypt>(3), B<ecb_crypt>(3),"
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
