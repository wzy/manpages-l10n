# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2004.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:53+0200\n"
"PO-Revision-Date: 2023-03-11 00:11+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "strfmon"
msgstr "strfmon"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "strfmon, strfmon_l - convert monetary value to a string"
msgstr "strfmon, strfmon_l - convierte valores monetarios a una cadena"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>monetary.hE<gt>>\n"
msgstr "B<#include E<lt>monetary.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<ssize_t strfmon(char >I<s>B<[restrict .>I<max>B<], size_t >I<max>B<,>\n"
"B<                const char *restrict >I<format>B<, ...);>\n"
"B<ssize_t strfmon_l(char >I<s>B<[restrict .>I<max>B<], size_t >I<max>B<, locale_t >I<locale>B<,>\n"
"B<                const char *restrict >I<format>B<, ...);>\n"
msgstr ""
"B<ssize_t strfmon(char >I<s>B<[restrict .>I<max>B<], size_t >I<max>B<,>\n"
"B<                const char *restrict >I<formato>B<, ...);>\n"
"B<ssize_t strfmon_l(char >I<s>B<[restrict .>I<max>B<], size_t >I<max>B<, locale_t >I<locale>B<,>\n"
"B<                const char *restrict >I<formato>B<, ...);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<strfmon>()  function formats the specified monetary amount according "
"to the current locale and format specification I<format> and places the "
"result in the character array I<s> of size I<max>."
msgstr ""
"La función B<strfmon>() formatea las cantidades monetarias definidas según "
"la especificación de la localización en curso y del formato I<formato>."
"Guarda el resultado en el vector de caracteres I<s> de tamaño I<max>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<strfmon_l>()  function performs the same task, but uses the locale "
"specified by I<locale>.  The behavior of B<strfmon_l>()  is undefined if "
"I<locale> is the special locale object B<LC_GLOBAL_LOCALE> (see "
"B<duplocale>(3))  or is not a valid locale object handle."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Ordinary characters in I<format> are copied to I<s> without conversion.  "
#| "Conversion specifiers are introduced by a \\(aq%\\(aq character.  "
#| "Immediately following it there can be zero or more of the following flags:"
msgid ""
"Ordinary characters in I<format> are copied to I<s> without conversion.  "
"Conversion specifiers are introduced by a \\[aq]%\\[aq] character.  "
"Immediately following it there can be zero or more of the following flags:"
msgstr ""
"Los caracteres ordinarios en I<format> se copian a I<s> sin conversión. Los "
"especificadores de conversión comienzan con un carácter \\(aq%\\(aq. "
"Inmediatamente después puede haber cero o más de las siguientes opciones:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<=>I<f>"
msgstr "B<=>I<f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The single-byte character I<f> is used as the numeric fill character (to be "
"used with a left precision, see below).  When not specified, the space "
"character is used."
msgstr ""
"El carácter de un sólo byte I<f> se usa como carácter de relleno numérico "
"(el relleno se produce a la izquierda, vea más abajo).  Cuando no se "
"especifica, se utiliza el carácter espacio."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<\\[ha]>"
msgstr "B<\\[ha]>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Do not use any grouping characters that might be defined for the current "
"locale.  By default, grouping is enabled."
msgstr ""
"No usar ningún carácter de agrupamiento que pueda estar definido para la "
"localización actual. Por defecto, el agrupamiento está habilitado."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<(> or B<+>"
msgstr "B<(> o B<+>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The ( flag indicates that negative amounts should be enclosed between "
"parentheses.  The + flag indicates that signs should be handled in the "
"default way, that is, amounts are preceded by the locale's sign indication, "
"for example, nothing for positive, \"-\" for negative."
msgstr ""
"La opción ( indica que las cantidades negativas deben ser encerradas entre "
"paréntesis. La opción + indica que los signos deben ser manejados en el modo "
"por defecto, esto es, las cantidades son precedidas por la indicación de "
"signo de la localización, p.e., nada para positivo, \"-\" para negativo."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<!>"
msgstr "B<!>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Omit the currency symbol."
msgstr "Omitir el símbolo monetario."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<->"
msgstr "B<->"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Left justify all fields.  The default is right justification."
msgstr ""
"Justificar a la izquierda todos los campos. Por defecto se justifican a la "
"derecha."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Next, there may be a field width: a decimal digit string specifying a "
"minimum field width in bytes.  The default is 0.  A result smaller than this "
"width is padded with spaces (on the left, unless the left-justify flag was "
"given)."
msgstr ""
"A continuación, puede haber una anchura de campo: una cadena de dígitos "
"decimales especificando una anchura mínima de campo. El valor por defecto es "
"0.  Un resultado más pequeño que esta anchura es rellenado con espacios (a "
"la izquierda, a menos que se especifique la opción de justificado a la "
"izquierda)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Next, there may be a left precision of the form \"#\" followed by a decimal "
"digit string.  If the number of digits left of the radix character is "
"smaller than this, the representation is padded on the left with the numeric "
"fill character.  Grouping characters are not counted in this field width."
msgstr ""
"Seguidamente, pueda haber una precisión izquierda de la forma \"#\" seguido "
"por una cadena de dígitos decimales. Si el número de dígitos a la izquierda "
"del carácter decimal es más pequeño que esto, la representación se rellena a "
"la izquierda con el carácter de relleno numérico. Los caracteres de "
"agrupamiento no se tienen en cuenta en este ancho de campo."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Next, there may be a right precision of the form \".\" followed by a decimal "
"digit string.  The amount being formatted is rounded to the specified number "
"of digits prior to formatting.  The default is specified in the "
"I<frac_digits> and I<int_frac_digits> items of the current locale.  If the "
"right precision is 0, no radix character is printed.  (The radix character "
"here is determined by B<LC_MONETARY>, and may differ from that specified by "
"B<LC_NUMERIC>.)"
msgstr ""
"Tras esto, puede haber una precisión derecha de la forma \".\" seguido por "
"una cadena de dígitos decimales. La cantidad a formatear se redondea al "
"número especificado de dígitos antes de formatear.  El valor por omisión se "
"especifica en los elementos I<frac_digits> y I<int_frac_digits> de la "
"localización actual.  Si la precisión derecha es 0, no se muestra ningún "
"carácter decimal (el carácter decimal lo determina aquí B<LC_MONETARY> y "
"puede diferir del especificado por B<LC_NUMERIC>.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Finally, the conversion specification must be ended with a conversion "
"character.  The three conversion characters are"
msgstr ""
"Finalmente, la especificación de conversión debe terminar con un carácter de "
"conversión. Los tres caracteres de conversión son"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<%>"
msgstr "B<%>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"(In this case, the entire specification must be exactly \"%%\".)  Put a "
"\\[aq]%\\[aq] character in the result string."
msgstr ""
"(En este caso la especificación completa debe ser exactamente \"%%\".)  Pone "
"un carácter \\[aq]%\\[aq] en la cadena resultante."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<i>"
msgstr "B<i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"One argument of type I<double> is converted using the locale's international "
"currency format."
msgstr ""
"Un argumento de tipo I<double> se convierte usando el formato monetario "
"internacional de la localización."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<n>"
msgstr "B<n>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"One argument of type I<double> is converted using the locale's national "
"currency format."
msgstr ""
"Un argumento de tipo I<double> se convierte usando el formato monetario "
"nacional de la localización."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<strfmon()> function returns the number of characters placed in the "
#| "array I<s>, not including the terminating NUL character, provided the "
#| "string, including the terminating NUL, fits.  Otherwise, it sets I<errno> "
#| "to E2BIG, returns -1, and the contents of the array is undefined."
msgid ""
"The B<strfmon>()  function returns the number of characters placed in the "
"array I<s>, not including the terminating null byte, provided the string, "
"including the terminating null byte, fits.  Otherwise, it sets I<errno> to "
"B<E2BIG>, returns -1, and the contents of the array is undefined."
msgstr ""
"La función B<strfmon()> devuelve el número de caracteres puestos en el array "
"I<s> sin incluir el carácter NUL del final, siempre que la cadena, incluendo "
"el NUL del final, quepa.  En otro caso, asigna a I<errno> el valor E2BIG, "
"devuelve -1 y el contenido del array queda indefinido."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<strfmon>()"
msgstr "B<strfmon>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "Configuración regional de multi-hilo seguro"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<strfmon_l>()"
msgstr "B<strfmon_l>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The call"
msgstr "La llamada"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"strfmon(buf, sizeof(buf), \"[%\\[ha]=*#6n] [%=*#6i]\",\n"
"        1234.567, 1234.567);\n"
msgstr ""
"strfmon(buf, sizeof(buf), \"[%\\[ha]=*#6n] [%=*#6i]\",\n"
"        1234.567, 1234.567);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "outputs"
msgstr "produce la salida"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "[€ **1234,57] [EUR **1 234,57]\n"
msgstr "[€ **1234,57] [EUR **1 234,57]\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"in the I<nl_NL> locale.  The I<de_DE>, I<de_CH>, I<en_AU>, and I<en_GB> "
"locales yield"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"[ **1234,57 €] [ **1.234,57 EUR]\n"
"[ Fr. **1234.57] [ CHF **1\\[aq]234.57]\n"
"[ $**1234.57] [ AUD**1,234.57]\n"
"[ £**1234.57] [ GBP**1,234.57]\n"
msgstr ""
"[ **1234,57 €] [ **1.234,57 EUR]\n"
"[ Fr. **1234.57] [ CHF **1\\[aq]234.57]\n"
"[ $**1234.57] [ AUD**1,234.57]\n"
"[ £**1234.57] [ GBP**1,234.57]\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<duplocale>(3), B<setlocale>(3), B<sprintf>(3), B<locale>(7)"
msgstr "B<duplocale>(3), B<setlocale>(3), B<sprintf>(3), B<locale>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "STRFMON"
msgstr "STRFMON"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<ssize_t strfmon(char *>I<s>B<, size_t >I<max>B<, const char *>I<format>B<,>\n"
"B<...);>\n"
msgstr ""
"B<ssize_t strfmon(char *>I<s>B<, size_t >I<max>B<, const char *>I<format>B<,>\n"
"B<...);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<ssize_t strfmon_l(char *>I<s>B<, size_t >I<max>B<, locale_t >I<locale>B<,>\n"
"B<const char *\" format , ...);>\n"
msgstr ""
"B<ssize_t strfmon_l(char *>I<s>B<, size_t >I<max>B<, locale_t >I<locale>B<,>\n"
"B<const char *\" format , ...);>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Ordinary characters in I<format> are copied to I<s> without conversion.  "
"Conversion specifiers are introduced by a \\(aq%\\(aq character.  "
"Immediately following it there can be zero or more of the following flags:"
msgstr ""
"Los caracteres ordinarios en I<format> se copian a I<s> sin conversión. Los "
"especificadores de conversión comienzan con un carácter \\(aq%\\(aq. "
"Inmediatamente después puede haber cero o más de las siguientes opciones:"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<^>"
msgstr "B<^>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"(In this case, the entire specification must be exactly \"%%\".)  Put a "
"\\(aq%\\(aq character in the result string."
msgstr ""
"(En este caso la especificación completa debe ser exactamente \"%%\".)  Pone "
"un carácter \\(aq%\\(aq en la cadena resultado."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EJEMPLO"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"strfmon(buf, sizeof(buf), \"[%^=*#6n] [%=*#6i]\",\n"
"        1234.567, 1234.567);\n"
msgstr ""
"strfmon(buf, sizeof(buf), \"[%^=*#6n] [%=*#6i]\",\n"
"        1234.567, 1234.567);\n"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid ""
#| "[ **1234,57 €] [ **1.234,57 EUR]\n"
#| "[ Fr. **1234.57] [ CHF **1'234.57]\n"
#| "[ $**1234.57] [ AUD**1,234.57]\n"
#| "[ £**1234.57] [ GBP**1,234.57]\n"
msgid ""
"[ **1234,57 €] [ **1.234,57 EUR]\n"
"[ Fr. **1234.57] [ CHF **1'234.57]\n"
"[ $**1234.57] [ AUD**1,234.57]\n"
"[ £**1234.57] [ GBP**1,234.57]\n"
msgstr ""
"[ **1234,57 €] [ **1.234,57 EUR]\n"
"[ Fr. **1234.57] [ CHF **1'234.57]\n"
"[ $**1234.57] [ AUD**1,234.57]\n"
"[ £**1234.57] [ GBP**1,234.57]\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
