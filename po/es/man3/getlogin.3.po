# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Juan Piernas <piernas@ditec.um.es>, 1999.
# Miguel Pérez Ibars <mpi79470@alu.um.es>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:29+0200\n"
"PO-Revision-Date: 2005-03-02 19:53+0200\n"
"Last-Translator: Miguel Pérez Ibars <mpi79470@alu.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getlogin>()"
msgid "getlogin"
msgstr "B<getlogin>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "getlogin, getlogin_r, cuserid - get username"
msgstr "getlogin, getlogin_r, cuserid - obtiene nombre de usuario"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int getlogin_r(char *>I<buf>B<, size_t >I<bufsize>B<);>"
msgid ""
"B<char *getlogin(void);>\n"
"B<int getlogin_r(char >I<buf>B<[.>I<bufsize>B<], size_t >I<bufsize>B<);>\n"
msgstr "B<int getlogin_r(char *>I<buf>B<, size_t >I<bufsize>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<char *cuserid(char *>I<string>B<);>"
msgid "B<char *cuserid(char *>I<string>B<);>\n"
msgstr "B<char *cuserid(char *>I<cadena>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de Macros de Prueba de Características para glibc (véase "
"B<feature_test_macros>(7)):"

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-38: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<getlogin_r>()"
msgid "B<getlogin_r>():"
msgstr "B<getlogin_r>()"

#.  Deprecated: _REENTRANT ||
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "_POSIX_C_SOURCE E<gt>= 200809L"
msgid "    _POSIX_C_SOURCE E<gt>= 199506L\n"
msgstr "_POSIX_C_SOURCE E<gt>= 200809L"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<cuserid>():"
msgstr "B<cuserid>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.24:\n"
"        (_XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"            || _GNU_SOURCE\n"
"    Up to and including glibc 2.23:\n"
"        _XOPEN_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<getlogin>()  returns a pointer to a string containing the name of the user "
"logged in on the controlling terminal of the process, or a null pointer if "
"this information cannot be determined.  The string is statically allocated "
"and might be overwritten on subsequent calls to this function or to "
"B<cuserid>()."
msgstr ""
"B<getlogin>() devuelve un puntero a una cadena de caracteres que contiene el "
"nombre del usuario que está en la terminal controladora del proceso, o un "
"puntero nulo si esta información no puede determinarse. La cadena se reserva "
"estáticamente y puede ser sobreescrita en subsecuentes llamadas a esta "
"función o a B<cuserid>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<getlogin_r>()  returns this same username in the array I<buf> of size "
"I<bufsize>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<cuserid>()  returns a pointer to a string containing a username associated "
"with the effective user ID of the process.  If I<string> is not a null "
"pointer, it should be an array that can hold at least B<L_cuserid> "
"characters; the string is returned in this array.  Otherwise, a pointer to a "
"string in a static area is returned.  This string is statically allocated "
"and might be overwritten on subsequent calls to this function or to "
"B<getlogin>()."
msgstr ""
"B<cuserid>() devuelve un puntero a una cadena de caracteres que contiene un "
"nombre de usuario asociado con el UID efectivo del proceso. Si I<cadena> no "
"es un puntero nulo, debe ser un vector que pueda alojar al menos "
"B<L_cuserid> caracteres; la cadena se coloca en este vector.  De otro modo "
"se devuelve un puntero a una cadena en un área estática. Esta cadena se "
"reserva estáticamente y puede ser sobreescrita en subsecuentes llamadas a "
"esta función o a B<getlogin>()."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The macro B<L_cuserid> is an integer constant that indicates how long an "
"array you might need to store a username.  B<L_cuserid> is declared in "
"I<E<lt>stdio.hE<gt>>."
msgstr ""
"La macro B<L_cuserid> es una constante entera que indica cuán grande puede "
"necesitar ser un vector que almacene un nombre de usuario. Se declara en "
"I<E<lt>stdio.hE<gt>>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions let your program identify positively the user who is running "
"(B<cuserid>())  or the user who logged in this session (B<getlogin>()).  "
"(These can differ when set-user-ID programs are involved.)"
msgstr ""
"Estas funciones permiten a su programa identificar positivamente al usuario "
"que lo está ejecutando (B<cuserid>()) o al que ha entrado en esta sesión "
"(B<getlogin>()). (Que pueden diferir cuando están implicados programas set-"
"user-ID)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For most purposes, it is more useful to use the environment variable "
"B<LOGNAME> to find out who the user is.  This is more flexible precisely "
"because the user can set B<LOGNAME> arbitrarily."
msgstr ""
"Para la mayoría de los propósitos, es más útil emplear la variable de "
"entorno B<LOGNAME> para saber quién es el usuario. Esto es más flexible "
"precisamente porque el usuario puede dar un valor arbitrario a B<LOGNAME>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<getlogin>()  returns a pointer to the username when successful, and NULL "
"on failure, with I<errno> set to indicate the error.  B<getlogin_r>()  "
"returns 0 when successful, and nonzero on failure."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "POSIX specifies:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"Se ha alcanzado el límite de descriptores de archivo abiertos para cada "
"proceso."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"Se ha alcanzado el límite máximo de archivos abiertos para el conjunto del "
"sistema."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENXIO>"
msgstr "B<ENXIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The calling process has no controlling terminal."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ERANGE>"
msgstr "B<ERANGE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"(getlogin_r)  The length of the username, including the terminating null "
"byte (\\[aq]\\e0\\[aq]), is larger than I<bufsize>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Linux/glibc also has:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "There was no corresponding entry in the utmp-file."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient memory to allocate passwd structure."
msgstr "Memoria insuficiente para alojar la estructura passwd."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTTY>"
msgstr "B<ENOTTY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Standard input didn't refer to a terminal.  (See BUGS.)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/passwd>"
msgstr "I</etc/passwd>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "password database file"
msgstr "archivo con los datos de las cuentas de usuario"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</var/run/utmp>"
msgstr "I</var/run/utmp>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "(traditionally I</etc/utmp>; some libc versions used I</var/adm/utmp>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-38: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-leap-15-5: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getlogin>()"
msgstr "B<getlogin>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"MT-Unsafe race:getlogin race:utent\n"
"sig:ALRM timer locale"
msgstr ""

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-38: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-leap-15-5: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TQ
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getlogin_r>()"
msgstr "B<getlogin_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"MT-Unsafe race:utent sig:ALRM timer\n"
"locale"
msgstr ""

#. #-#-#-#-#  archlinux: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  debian-unstable: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-38: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  fedora-rawhide: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-leap-15-5: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: tbl table
#. #-#-#-#-#  opensuse-tumbleweed: getlogin.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<cuserid>()"
msgstr "B<cuserid>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:cuserid/!string locale"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the above table, I<utent> in I<race:utent> signifies that if any of the "
"functions B<setutent>(3), B<getutent>(3), or B<endutent>(3)  are used in "
"parallel in different threads of a program, then data races could occur.  "
"B<getlogin>()  and B<getlogin_r>()  call those functions, so we use race:"
"utent to remind users."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"OpenBSD has B<getlogin>()  and B<setlogin>(), and a username associated with "
"a session, even if it has no controlling terminal."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001."
msgid "POSIX.1-2001.  OpenBSD."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"System V, POSIX.1-1988.  Removed in POSIX.1-1990.  SUSv2.  Removed in "
"POSIX.1-2001."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"System V has a B<cuserid>()  function which uses the real user ID rather "
"than the effective user ID."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Unfortunately, it is often rather easy to fool B<getlogin>().  Sometimes it "
"does not work at all, because some program messed up the utmp file.  Often, "
"it gives only the first 8 characters of the login name.  The user currently "
"logged in on the controlling terminal of our program need not be the user "
"who started it.  Avoid B<getlogin>()  for security-related purposes."
msgstr ""
"Desafortunadamente, es a meundo bastante fácil el liar a B<getlogin>().  A "
"veces no funciona en absoluto, porque algún programa estropeó el fichero "
"utmp. Frecuentemente, sólo da los primeros 8 caracteres del nombre de "
"entrada. El usuario que esté actualmente en la tty controladora de nuestro "
"programa no tiene por qué ser el que lo hubo ejecutado.  Evite B<getlogin>() "
"para propósitos relacionados con la seguridad."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Note that glibc does not follow the POSIX specification and uses I<stdin> "
"instead of I</dev/tty>.  A bug.  (Other recent systems, like SunOS 5.8 and "
"HP-UX 11.11 and FreeBSD 4.8 all return the login name also when I<stdin> is "
"redirected.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Nobody knows precisely what B<cuserid>()  does; avoid it in portable "
"programs.  Or avoid it altogether: use I<getpwuid(geteuid())> instead, if "
"that is what you meant.  B<Do not use> B<cuserid>()."
msgstr ""
"Nadie sabe con precisión qué es lo que hace B<cuserid>(); evítelo en "
"programas transportables; evítelo de todas formas siempre: en su lugar "
"emplee I<getpwuid(getwuid())>, si eso es lo que pretende.  B<No utilice> "
"B<cuserid>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<logname>(1), B<geteuid>(2), B<getuid>(2), B<utmp>(5)"
msgstr "B<logname>(1), B<geteuid>(2), B<getuid>(2), B<utmp>(5)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<getlogin>()  and B<getlogin_r>(): POSIX.1-2001, POSIX.1-2008."
msgstr "B<getlogin>() y B<getlogin_r>(): POSIX.1-2001, POSIX.1-2008."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "POSIX.1.  System V has a B<cuserid> function which uses the real user ID "
#| "rather than the effective user ID. The B<cuserid> function was included "
#| "in the 1988 version of POSIX, but removed from the 1990 version."
msgid ""
"System V has a B<cuserid>()  function which uses the real user ID rather "
"than the effective user ID.  The B<cuserid>()  function was included in the "
"1988 version of POSIX, but removed from the 1990 version.  It was present in "
"SUSv2, but removed in POSIX.1-2001."
msgstr ""
"POSIX.1.  System V tiene una función B<cuserid> que usa el UID real en vez "
"del efectivo. La función B<cuserid> se incluyó en la versión de POSIX del "
"año 1988, pero se quitó en la de 1990."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GETLOGIN"
msgstr "GETLOGIN"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<char *getlogin(void);>"
msgstr "B<char *getlogin(void);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int getlogin_r(char *>I<buf>B<, size_t >I<bufsize>B<);>"
msgstr "B<int getlogin_r(char *>I<buf>B<, size_t >I<bufsize>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>stdio.hE<gt>>"
msgstr "B<#include E<lt>stdio.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<char *cuserid(char *>I<string>B<);>"
msgstr "B<char *cuserid(char *>I<cadena>B<);>"

#.  Deprecated: _REENTRANT ||
#. type: Plain text
#: opensuse-leap-15-5
msgid "B<getlogin_r>(): _POSIX_C_SOURCE\\ E<gt>=\\ 199506L"
msgstr "B<getlogin_r>(): _POSIX_C_SOURCE\\ E<gt>=\\ 199506L"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    Since glibc 2.24:\n"
"        (_XOPEN_SOURCE && ! (_POSIX_C_SOURCE E<gt>= 200112L)\n"
"        || __GNU_SOURCE\n"
"    Up to and including glibc 2.23:\n"
"        _XOPEN_SOURCE\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<getlogin>()  returns a pointer to the username when successful, and NULL "
"on failure, with I<errno> set to indicate the cause of the error.  "
"B<getlogin_r>()  returns 0 when successful, and nonzero on failure."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "POSIX specifies"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"(getlogin_r)  The length of the username, including the terminating null "
"byte (\\(aq\\e0\\(aq), is larger than I<bufsize>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "Linux/glibc also has"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "MT-Unsafe race:getlogin race:utent\n"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "sig:ALRM timer locale"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, no-wrap
msgid "MT-Unsafe race:utent sig:ALRM timer\n"
msgstr ""

#. type: tbl table
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "MT-Safe locale"
msgid "locale"
msgstr "Configuración regional de multi-hilo seguro"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
