# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Sebastian Desimone <chipy@argenet.com.ar>, 1998.
# Juan Pablo Puerta <jppuerta@full-moon.com>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:21+0200\n"
"PO-Revision-Date: 1998-07-10 19:53+0200\n"
"Last-Translator: Juan Pablo Puerta <jppuerta@full-moon.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "bsearch"
msgstr "bsearch"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 Marzo 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "bsearch - binary search of a sorted array"
msgstr "bsearch - búsqueda binaria en un arreglo (array) ordenado"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void *bsearch(const void >I<key>B<[.>I<size>B<], const void >I<base>B<[.>I<size>B< * .>I<nmemb>B<],>\n"
"B<              size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<              int (*>I<compar>B<)(const void [.>I<size>B<], const void [.>I<size>B<]));>\n"
msgstr ""
"B<void *bsearch(const void >I<key>B<[.>I<size>B<], const void >I<base>B<[.>I<size>B< * .>I<nmemb>B<],>\n"
"B<              size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<              int (*>I<compar>B<)(const void [.>I<size>B<], const void [.>I<size>B<]));>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<bsearch>()  function searches an array of I<nmemb> objects, the "
"initial member of which is pointed to by I<base>, for a member that matches "
"the object pointed to by I<key>.  The size of each member of the array is "
"specified by I<size>."
msgstr ""
"La función B<bsearch>() busca en un arreglo de I<nmemb> elementos, donde el "
"primer elemento está apuntado por I<base>, un elemento que coincida con el "
"objecto apuntado por I<key>. El tamaño de cada elementos del arreglo es "
"especificado por I<size>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The contents of the array should be in ascending sorted order according to "
"the comparison function referenced by I<compar>.  The I<compar> routine is "
"expected to have two arguments which point to the I<key> object and to an "
"array member, in that order, and should return an integer less than, equal "
"to, or greater than zero if the I<key> object is found, respectively, to be "
"less than, to match, or be greater than the array member."
msgstr ""
"El contenido del arreglo debe estar ordenado en orden ascendente según la "
"función de comparación referenciada por I<compar>. Se espera que la rutina "
"I<compar> tenga dos argumentos que apunten al objeto I<key> y a un elemento "
"del arreglo, en ese orden, y debe devolver un entero menor, igual o mayor "
"que cero si resulta que el objeto I<key> es menor, igual o mayor "
"(respectivamente) que el elemento del arreglo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<bsearch>()  function returns a pointer to a matching member of the "
"array, or NULL if no match is found.  If there are multiple elements that "
"match the key, the element returned is unspecified."
msgstr ""
"La función B<bsearch>() devuelve un puntero al elemento del arreglo que "
"coincide, o NULL si no hay coincidencia. Si hay múltiples elementos que "
"coinciden con la clave, el elemento devuelto está sin determinar."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<bsearch>()"
msgstr "B<bsearch>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIAL"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgid "POSIX.1-2001, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The example below first sorts an array of structures using B<qsort>(3), then "
"retrieves desired elements using B<bsearch>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"
msgstr "#define ARRAY_SIZE(arr)  (sizeof((arr)) / sizeof((arr)[0]))\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"
msgstr ""
"struct mi {\n"
"    int         nr;\n"
"    const char  *name;\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"static struct mi  months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"
msgstr ""
"static struct mi  months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"
msgstr ""
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    const struct mi *mi1 = m1;\n"
"    const struct mi *mi2 = m2;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
msgstr ""
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    qsort(months, ARRAY_SIZE(months), sizeof(months[0]), compmi);\n"
"    for (size_t i = 1; i E<lt> argc; i++) {\n"
"        struct mi key;\n"
"        struct mi *res;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, ARRAY_SIZE(months),\n"
"                      sizeof(months[0]), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\[aq]%s\\[aq]: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  this example referred to in qsort.3
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: bsearch.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<hsearch>(3), B<lsearch>(3), B<qsort>(3), B<tsearch>(3)"
msgstr "B<hsearch>(3), B<lsearch>(3), B<qsort>(3), B<tsearch>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 Febrero 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Páginas de manual de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "BSEARCH"
msgstr "BSEARCH"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<void *bsearch(const void *>I<key>B<, const void *>I<base>B<,>\n"
"B<              size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<              int (*>I<compar>B<)(const void *, const void *));>\n"
msgstr ""
"B<void *bsearch(const void *>I<key>B<, const void *>I<base>B<,>\n"
"B<              size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<              int (*>I<compar>B<)(const void *, const void *));>\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: Plain text
#: opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EJEMPLO"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct mi {\n"
"    int nr;\n"
"    char *name;\n"
"} months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"
msgstr ""
"struct mi {\n"
"    int nr;\n"
"    char *name;\n"
"} months[] = {\n"
"    { 1, \"jan\" }, { 2, \"feb\" }, { 3, \"mar\" }, { 4, \"apr\" },\n"
"    { 5, \"may\" }, { 6, \"jun\" }, { 7, \"jul\" }, { 8, \"aug\" },\n"
"    { 9, \"sep\" }, {10, \"oct\" }, {11, \"nov\" }, {12, \"dec\" }\n"
"};\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "#define nr_of_months (sizeof(months)/sizeof(months[0]))\n"
msgstr "#define nr_of_months (sizeof(months)/sizeof(months[0]))\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    struct mi *mi1 = (struct mi *) m1;\n"
"    struct mi *mi2 = (struct mi *) m2;\n"
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"
msgstr ""
"static int\n"
"compmi(const void *m1, const void *m2)\n"
"{\n"
"    struct mi *mi1 = (struct mi *) m1;\n"
"    struct mi *mi2 = (struct mi *) m2;\n"
"    return strcmp(mi1-E<gt>name, mi2-E<gt>name);\n"
"}\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"int\n"
"main(int argc, char **argv)\n"
"{\n"
"    int i;\n"
msgstr ""
"int\n"
"main(int argc, char **argv)\n"
"{\n"
"    int i;\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    qsort(months, nr_of_months, sizeof(struct mi), compmi);\n"
"    for (i = 1; i E<lt> argc; i++) {\n"
"        struct mi key, *res;\n"
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, nr_of_months,\n"
"                      sizeof(struct mi), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\(aq%s\\(aq: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    qsort(months, nr_of_months, sizeof(struct mi), compmi);\n"
"    for (i = 1; i E<lt> argc; i++) {\n"
"        struct mi key, *res;\n"
"        key.name = argv[i];\n"
"        res = bsearch(&key, months, nr_of_months,\n"
"                      sizeof(struct mi), compmi);\n"
"        if (res == NULL)\n"
"            printf(\"\\(aq%s\\(aq: unknown month\\en\", argv[i]);\n"
"        else\n"
"            printf(\"%s: month #%d\\en\", res-E<gt>name, res-E<gt>nr);\n"
"    }\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
