# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan José López Mellado <laveneno@hotmail.com>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-01-22 20:56+01:00\n"
"PO-Revision-Date: 1998-10-22 19:53+0200\n"
"Last-Translator: Juan José López Mellado <laveneno@hotmail.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: man1/reset.1:7
#, no-wrap
msgid "RESET"
msgstr "RESET"

#. type: TH
#: man1/reset.1:7
#, no-wrap
msgid "10 October 1993"
msgstr "10 Octubre 1993"

#. type: TH
#: man1/reset.1:7
#, no-wrap
msgid "Linux 0.99"
msgstr "Linux 0.99"

#. type: TH
#: man1/reset.1:7
#, no-wrap
msgid "Manual del Programador de Linux"
msgstr "Manual del Programador de Linux"

#. type: SH
#: man1/reset.1:8
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: man1/reset.1:10
#, fuzzy
msgid "reset - ?"
msgstr "reset - restaurar el terminal"

#. type: SH
#: man1/reset.1:10
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: man1/reset.1:12
msgid "B<reset>"
msgstr "B<reset>"

#. type: SH
#: man1/reset.1:12
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: man1/reset.1:30
#, fuzzy
msgid "B<reset> ..."
msgstr ""
"B<reset> llama a B<tput>(1)  con los argumentos I<clear>,I<rmacs>,I<rmm>,"
"I<rmul>,I<rs1>,I<rs2> y I<rs3>.  Esto provoca que B<tput> envíe las cadenas "
"de restauración al terminal basándose en la información de I</etc/termcap> "
"(para el B<tput> de GNU o BSD)  o en la base de datos terminfo (para el "
"B<tput> de B<ncurses ).>"

#. type: Plain text
#: man1/reset.1:38
#, fuzzy
msgid "...1"
msgstr ""
"Esta secuencia parece ser suficiente para restaurar la Consola Virtual de "
"Linux cuando empieza a escribir caracteres extraños. Como medida de "
"precaución, también se llama a B<stty>(1)  con el argumento I<sane> en un "
"intento por volver al modo ``cooked'' (interpretado)."

#. type: SH
#: man1/reset.1:38
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: man1/reset.1:42
msgid "B<clear>(1), B<stty>(1), B<tput>(1)"
msgstr "B<clear>(1), B<stty>(1), B<tput>(1)"

#. type: SH
#: man1/reset.1:42
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: man1/reset.1:43
msgid "Rik Faith (faith@cs.unc.edu)"
msgstr "Rik Faith (faith@cs.unc.edu)"
