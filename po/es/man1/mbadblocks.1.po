# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Javi Polo <javipolo@cyberjunkie.com>, 1998.
# Marcos Fouces <marcos@debian.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:36+0200\n"
"PO-Revision-Date: 2023-03-05 23:12+0100\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "mbadblocks"
msgstr "mbadblocks"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "21Mar23"
msgstr ""

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.43"
msgstr "mtools-4.0.43"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nombre"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "mbadblocks - tests a floppy disk, and marks the bad blocks in the FAT"
msgstr ""
"mbadblocks - Comprueba un disquette y marca en la FAT los bloques erroneos"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Nota\\ de\\ advertencia"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""
"Esta página de manual ha sido generada automáticamente a partir de la "
"documentación texinfo de mtools. Sin embargo, el proceso es solo aproximado, "
"y algunos elementos tales como referencias cruzadas, notas al pie e índices, "
"se pierden en este proceso de traducción. Consulte el final de esta página "
"para más detalles."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descripción"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The \\&CW<mbadblocks> command is used to mark some clusters on an MS-DOS "
"filesystem bad. It has the following syntax:"
msgstr ""
"La orden \\&CW<mbadblocks> se usa para explorar un disquete MS-DOS y marcar "
"sus bloques erróneos como defectuosos. Usa la siguiente sintaxis:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"\\&\\&CW<mbadblocks> [\\&CW<-s> I<sectorlist>|\\&CW<-c> I<clusterlist>|-w] "
"I<drive>\\&CW<:>"
msgstr ""
"\\&\\&CW<mbadblocks> [\\&CW<-s> I<listasectores>|\\&CW<-c> I<listabloques>|-"
"w] I<disco>\\&CW<:>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If no command line flags are supplied, \\&CW<Mbadblocks> scans an MS-DOS "
"filesystem for bad blocks by simply trying to read them and flag them if "
"read fails. All blocks that are unused are scanned, and if detected bad are "
"marked as such in the FAT."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This command is intended to be used right after \\&CW<mformat>.  It is not "
"intended to salvage data from bad disks."
msgstr ""
"Esta aplicación está pensada para emplearse justo después de \\&CW<mformat>. "
"No está indicada para rescatar datos en discoa defectuosos."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Command\\ line\\ options"
msgstr "Opciones de\\ la línea\\ de órdenes"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<c\\ >I<file>\\&\\ "
msgstr "\\&\\&CW<c\\ >I<fichero>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Use a list of bad clusters, rather than scanning for bad clusters itself."
msgstr ""
"Emplea una lista de clusters erróneos en lugar de escanearlo en su búsqueda."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<s\\ >I<file>\\&\\ "
msgstr "\\&\\&CW<s\\ >I<fichero>\\&\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Use a list of bad sectors (counted from beginning of filesystem), rather "
"than trying for bad clusters itself."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\&\\&CW<w>\\ "
msgstr "\\&\\&CW<w>\\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Write a random pattern to each cluster, then read it back and flag cluster "
"as bad if mismatch. Only free clusters are tested in such a way, so any file "
"data is preserved."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Bugs"
msgstr "Fallos"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"\\&\\&CW<Mbadblocks> should (but doesn't yet :-( ) also try to salvage bad "
"blocks which are in use by reading them repeatedly, and then mark them bad."
msgstr ""
"\\&\\&CW<Mbadblocks> debe también (aunque no lo hace aún :-( ) intentar "
"salvar los bloques erroneos que están en uso leyendolos repetidas veces y "
"luego marcándolos como defectuosos."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr "Véase\\ también"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Documentación texinfo de Mtools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr "Visualizando\\ el \\documento \\texi"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Esta página de manual ha sido generada automáticamente a partir de la "
"documentación texinfo de mtools. Sin embargo, el proceso es solo aproximado, "
"y algunos elementos tales como referencias cruzadas, notas al pie e índices, "
"se pierden en este proceso de traducción.  De hecho, estos elementos no "
"tienen una representación adecuada en el formato de las páginas del manual. "
"Por otra parte, solo se han traducido los elemntos específicos de cada "
"orden, y se ha desechado de la versión de la página del manual la "
"información general de mtools. Por este motivo, le aviso encarecidamente que "
"use el documento texinfo original."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Para generar un copia imprimible del documento texinfo use las siguientes "
"órdenes:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Para generar una copia html, ejecute:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Para generar un copia info (visualizable usando el modo info de emacs), "
"ejecute:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"El documento texinfo parece más bonito en html o cuando se imprime. De "
"hecho, la versión info contiene ciertos ejemplos que son difíciles de leer "
"debido a las convenciones de notación usadas en info."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "10Jul21"
msgstr "10 de julio de 2021"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "06Aug21"
msgstr "6 de agosto de 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "mtools-4.0.35"
msgstr "mtools-4.0.35"
