# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2012-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-06-27 19:40+0200\n"
"PO-Revision-Date: 2023-03-03 21:18+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "nanosleep"
msgstr "nanosleep"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "nanosleep - high-resolution sleep"
msgstr "nanosleep - Sommeil en haute résolution"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>time.hE<gt>>\n"
msgstr "B<#include E<lt>time.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int nanosleep(const struct timespec *>I<req>B<,>\n"
"B<              struct timespec *_Nullable >I<rem>B<);>\n"
msgstr ""
"B<int nanosleep(const struct timespec *>I<req>B<,>\n"
"B<              struct timespec *_Nullable >I<rem>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<nanosleep>():"
msgstr "B<nanosleep>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _POSIX_C_SOURCE E<gt>= 199309L\n"
msgstr "    _POSIX_C_SOURCE E<gt>= 199309L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<nanosleep>()  suspends the execution of the calling thread until either at "
"least the time specified in I<*req> has elapsed, or the delivery of a signal "
"that triggers the invocation of a handler in the calling thread or that "
"terminates the process."
msgstr ""
"B<nanosleep>() suspend l'exécution du thread appelant jusqu'à ce que le "
"temps indiqué dans I<*req> ait expiré, ou que la réception d'un signal ait "
"déclenché l'invocation d'un gestionnaire dans le thread appelant ou ait "
"terminé le processus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the call is interrupted by a signal handler, B<nanosleep>()  returns -1, "
"sets I<errno> to B<EINTR>, and writes the remaining time into the structure "
"pointed to by I<rem> unless I<rem> is NULL.  The value of I<*rem> can then "
"be used to call B<nanosleep>()  again and complete the specified pause (but "
"see NOTES)."
msgstr ""
"Si l'appel est interrompu par un gestionnaire de signal B<nanosleep>() "
"renvoie B<-1>, renseigne I<errno> avec la valeur B<EINTR>, et inscrit le "
"temps restant dans la structure pointée par I<rem> à moins que I<rem> soit "
"NULL. La valeur de I<*rem> peut être utilisée pour rappeler à nouveau "
"B<nanosleep>() afin de terminer la pause (mais voir la section NOTES plus "
"loin)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<timespec>(3)  structure is used to specify intervals of time with "
"nanosecond precision."
msgstr ""
"La structure I<timespec> est utilisée pour indiquer l'intervalle de temps en "
"nanosecondes."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "The value of the nanoseconds field must be in the range [0, 999999999]."
msgstr ""
"La valeur du champ nanosecondes doit être dans l'intervalle [0 à 999\\ 999\\ "
"999]."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Compared to B<sleep>(3)  and B<usleep>(3), B<nanosleep>()  has the following "
"advantages: it provides a higher resolution for specifying the sleep "
"interval; POSIX.1 explicitly specifies that it does not interact with "
"signals; and it makes the task of resuming a sleep that has been interrupted "
"by a signal handler easier."
msgstr ""
"Par rapport à B<sleep>(3) et B<usleep>(3), B<nanosleep>() a les avantages "
"suivants : il fournit une meilleure résolution pour indiquer la durée du "
"sommeil ; POSIX.1 indique explicitement qu'il n'interagit avec aucun "
"signal ; il permet enfin de continuer facilement un sommeil interrompu par "
"un signal."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On successfully sleeping for the requested interval, B<nanosleep>()  returns "
"0.  If the call is interrupted by a signal handler or encounters an error, "
"then it returns -1, with I<errno> set to indicate the error."
msgstr ""
"L'appel B<nanosleep>() renvoie B<0> s'il réussit à suspendre l'exécution "
"pour la durée demandée. Si l'appel est interrompu par un gestionnaire de "
"signal ou rencontre une erreur, il renvoie B<-1> et I<errno> contient le "
"code d'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Problem with copying information from user space."
msgstr ""
"Problème lors de la copie d'information à partir de l'espace utilisateur."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The pause has been interrupted by a signal that was delivered to the thread "
"(see B<signal>(7)).  The remaining sleep time has been written into I<*rem> "
"so that the thread can easily call B<nanosleep>()  again and continue with "
"the pause."
msgstr ""
"La pause a été interrompue par un signal délivré au thread (voir "
"B<signal>(7)). Le temps restant de sommeil a été inscrit dans I<*rem> pour "
"que le thread puisse terminer facilement son sommeil en rappelant "
"B<nanosleep>()."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The value in the I<tv_nsec> field was not in the range [0, 999999999] or "
"I<tv_sec> was negative."
msgstr ""
"La valeur du champ I<tv_nsec> n'est pas dans l'intervalle [0 à 999\\ 999\\ "
"999] ou I<tv_sec> est négatif."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#.  See also http://thread.gmane.org/gmane.linux.kernel/696854/
#.  Subject: nanosleep() uses CLOCK_MONOTONIC, should be CLOCK_REALTIME?
#.  Date: 2008-06-22 07:35:41 GMT
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1 specifies that B<nanosleep>()  should measure time against the "
"B<CLOCK_REALTIME> clock.  However, Linux measures the time using the "
"B<CLOCK_MONOTONIC> clock.  This probably does not matter, since the POSIX.1 "
"specification for B<clock_settime>(2)  says that discontinuous changes in "
"B<CLOCK_REALTIME> should not affect B<nanosleep>():"
msgstr ""
"POSIX.1 indique que B<nanosleep>() doit mesurer le temps avec l'horloge "
"B<CLOCK_REALTIME>. Pourtant, Linux mesure le temps avec l'horloge "
"B<CLOCK_MONOTONIC>. Cela n'a probablement pas d'importance car la "
"spécification POSIX.1 de B<clock_settime>(2) indique que les modifications "
"discontinues dans B<CLOCK_REALTIME> n'affectent pas B<nanosleep>() :"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Setting the value of the B<CLOCK_REALTIME> clock via B<clock_settime>(2)  "
"shall have no effect on threads that are blocked waiting for a relative time "
"service based upon this clock, including the B<nanosleep>()  function; ...  "
"Consequently, these time services shall expire when the requested relative "
"interval elapses, independently of the new or old value of the clock."
msgstr ""
"Configurer la valeur de l'horloge B<CLOCK_REALTIME> avec B<clock_settime>(2) "
"ne doit pas avoir d'effet sur les threads bloqués attendant un service de "
"temps relatif basé sur cette horloge. Cela inclut la fonction "
"B<nanosleep>()\\ ; ... En conséquence, ces services de temps doivent expirer "
"lorsque la durée relative demandée est atteinte, indépendamment de "
"l'ancienne ou la nouvelle valeur de l'horloge."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In order to support applications requiring much more precise pauses (e.g., "
"in order to control some time-critical hardware), B<nanosleep>()  would "
"handle pauses of up to 2 milliseconds by busy waiting with microsecond "
"precision when called from a thread scheduled under a real-time policy like "
"B<SCHED_FIFO> or B<SCHED_RR>.  This special extension was removed in Linux "
"2.5.39, and is thus not available in Linux 2.6.0 and later kernels."
msgstr ""
"Pour gérer des applications nécessitant des pauses plus précises (par "
"exemple pour le contrôle de périphériques matériels avec un délai critique), "
"B<nanosleep>() prend en charge des pauses jusqu'à 2 millisecondes, en étant "
"occupé avec une précision d’une microseconde lorsqu'il est appelé à partir "
"d'un thread ordonnancé par une politique en temps réel comme B<SCHED_FIFO> "
"ou B<SCHED_RR>. Cette extension spéciale a été supprimée dans Linux 2.5.39, "
"et n'est donc plus disponible dans les noyaux 2.6.0 et supérieur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the interval specified in I<req> is not an exact multiple of the "
"granularity underlying clock (see B<time>(7)), then the interval will be "
"rounded up to the next multiple.  Furthermore, after the sleep completes, "
"there may still be a delay before the CPU becomes free to once again execute "
"the calling thread."
msgstr ""
"Si l'intervalle indiqué dans I<req> n'est pas un multiple exact de la "
"granularité de l'horloge sous-jacente (consultez B<time>(7)), l'intervalle "
"est arrondi au multiple supérieur. De plus, après que le sommeil est achevé, "
"il y a toujours un délai avant que le processeur ne redevienne complètement "
"disponible pour le thread appelant."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The fact that B<nanosleep>()  sleeps for a relative interval can be "
"problematic if the call is repeatedly restarted after being interrupted by "
"signals, since the time between the interruptions and restarts of the call "
"will lead to drift in the time when the sleep finally completes.  This "
"problem can be avoided by using B<clock_nanosleep>(2)  with an absolute time "
"value."
msgstr ""
"Le fait que B<nanosleep>() endorme pour une durée relative peut être "
"problématique si l'appel est relancé de manière répétée après avoir été "
"interrompu par des signaux, puisque le temps entre les interruptions et les "
"redémarrages de l'appel connaîtra une dérive lorsque le sommeil sera "
"finalement achevé. Ce problème peut être évité en utilisant "
"B<clock_nanosleep>(2) avec une valeur de temps absolu."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If a program that catches signals and uses B<nanosleep>()  receives signals "
"at a very high rate, then scheduling delays and rounding errors in the "
"kernel's calculation of the sleep interval and the returned I<remain> value "
"mean that the I<remain> value may steadily I<increase> on successive "
"restarts of the B<nanosleep>()  call.  To avoid such problems, use "
"B<clock_nanosleep>(2)  with the B<TIMER_ABSTIME> flag to sleep to an "
"absolute deadline."
msgstr ""
"Si un programme captant les signaux et utilisant B<nanosleep>() reçoit des "
"signaux à très haute vitesse, les délais d'ordonnancement et les erreurs "
"d'arrondi de calcul par le noyau de l'intervalle de sommeil et de la valeur "
"de retour I<remain> signifient que la valeur de I<remain> peut I<augmenter> "
"régulièrement au cours des redémarrages successifs de l'appel "
"B<nanosleep>(). Pour éviter de tels problèmes, utilisez "
"B<clock_nanosleep>(2) avec l'attribut B<TIMER_ABSTIME> pour un sommeil d'une "
"durée absolue."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In Linux 2.4, if B<nanosleep>()  is stopped by a signal (e.g., B<SIGTSTP>), "
"then the call fails with the error B<EINTR> after the thread is resumed by a "
"B<SIGCONT> signal.  If the system call is subsequently restarted, then the "
"time that the thread spent in the stopped state is I<not> counted against "
"the sleep interval.  This problem is fixed in Linux 2.6.0 and later kernels."
msgstr ""
"Dans Linux 2.4, si B<nanosleep>() est arrêté par un signal (par exemple, "
"B<SIGTSTP>), l'appel échoue avec l'erreur B<EINTR> après que le thread a "
"repris avec un signal B<SIGCONT>. Si l'appel système est, par la suite, "
"relancé, le temps passé par le thread dans l'état arrêté I<n'est pas> "
"comptabilisé dans l'intervalle de sommeil. Ce problème est corrigé dans les "
"noyaux Linux 2.6.0 et supérieurs."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<clock_nanosleep>(2), B<restart_syscall>(2), B<sched_setscheduler>(2), "
"B<timer_create>(2), B<sleep>(3), B<timespec>(3), B<usleep>(3), B<time>(7)"
msgstr ""
"B<clock_nanosleep>(2), B<restart_syscall>(2), B<sched_setscheduler>(2), "
"B<timer_create>(2), B<sleep>(3), B<timespec>(3), B<usleep>(3), B<time>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-12"
msgstr "12 février 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SS
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "Old behavior"
msgstr "Ancien comportement"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "NANOSLEEP"
msgstr "NANOSLEEP"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>time.hE<gt>>"
msgstr "B<#include E<lt>time.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<int nanosleep(const struct timespec *>I<req>B<, struct timespec "
"*>I<rem>B<);>"
msgstr ""
"B<int nanosleep(const struct timespec *>I<req>B<, struct timespec "
"*>I<rem>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<nanosleep>(): _POSIX_C_SOURCE\\ E<gt>=\\ 199309L"
msgstr "B<nanosleep>() : _POSIX_C_SOURCE\\ E<gt>=\\ 199309L"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The structure I<timespec> is used to specify intervals of time with "
"nanosecond precision.  It is defined as follows:"
msgstr ""
"La structure I<timespec> est utilisée pour indiquer l'intervalle de temps en "
"nanosecondes. Elle est définie comme ceci :"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct timespec {\n"
"    time_t tv_sec;        /* seconds */\n"
"    long   tv_nsec;       /* nanoseconds */\n"
"};\n"
msgstr ""
"struct timespec {\n"
"    time_t tv_sec;        /* secondes     */\n"
"    long   tv_nsec;       /* nanosecondes */\n"
"};\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "The value of the nanoseconds field must be in the range 0 to 999999999."
msgstr ""
"La valeur du champ nanosecondes doit être dans l'intervalle 0 à 999\\ 999\\ "
"999."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The value in the I<tv_nsec> field was not in the range 0 to 999999999 or "
"I<tv_sec> was negative."
msgstr ""
"La valeur du champ I<tv_nsec> n'est pas dans l'intervalle 0 à 999\\ 999\\ "
"999 ou I<tv_sec> est négatif."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"In order to support applications requiring much more precise pauses (e.g., "
"in order to control some time-critical hardware), B<nanosleep>()  would "
"handle pauses of up to 2 milliseconds by busy waiting with microsecond "
"precision when called from a thread scheduled under a real-time policy like "
"B<SCHED_FIFO> or B<SCHED_RR>.  This special extension was removed in kernel "
"2.5.39, and is thus not available in Linux 2.6.0 and later kernels."
msgstr ""
"Pour gérer des applications nécessitant des pauses plus précises (par "
"exemple pour le contrôle de périphériques matériels avec un délai critique), "
"B<nanosleep>() prend en charge des pauses jusqu'à 2 millisecondes, en étant "
"occupé avec une précision d’une microseconde lorsqu'il est appelé à partir "
"d'un thread ordonnancé par une politique en temps réel comme B<SCHED_FIFO> "
"ou B<SCHED_RR>. Cette extension spéciale a été supprimée dans le "
"noyau 2.5.39, et n'est donc plus disponible dans les noyaux 2.6.0 et "
"supérieur."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<clock_nanosleep>(2), B<restart_syscall>(2), B<sched_setscheduler>(2), "
"B<timer_create>(2), B<sleep>(3), B<usleep>(3), B<time>(7)"
msgstr ""
"B<clock_nanosleep>(2), B<restart_syscall>(2), B<sched_setscheduler>(2), "
"B<timer_create>(2), B<sleep>(3), B<usleep>(3), B<time>(7)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
