# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2023-06-27 19:42+0200\n"
"PO-Revision-Date: 2023-02-19 00:53+0100\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "personality"
msgstr "personality"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "personality - set the process execution domain"
msgstr "personality - Indiquer le domaine d'exécution du processus"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHÈQUE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Bibliothèque C standard (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/personality.hE<gt>>\n"
msgstr "B<#include E<lt>sys/personality.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<int personality(unsigned long >I<persona>B<);>\n"
msgstr "B<int personality(unsigned long >I<persona>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Linux supports different execution domains, or personalities, for each "
"process.  Among other things, execution domains tell Linux how to map signal "
"numbers into signal actions.  The execution domain system allows Linux to "
"provide limited support for binaries compiled under other UNIX-like "
"operating systems."
msgstr ""
"Linux propose différents domaines d'exécution, ou personnalités, pour chaque "
"processus. Entre autres choses, le domaine d'exécution indique au noyau la "
"manière de convertir les numéros de signaux en actions. Le domaine "
"d'exécution permet à Linux d'offrir une prise en charge (limitée) à des "
"fichiers binaires compilés sous d'autres systèmes d'exploitation compatibles "
"UNIX."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<persona> is not 0xffffffff, then B<personality>()  sets the caller's "
"execution domain to the value specified by I<persona>.  Specifying "
"I<persona> as 0xffffffff provides a way of retrieving the current persona "
"without changing it."
msgstr ""
"Si I<persona> n’est pas 0xffffffff, alors B<personality>() définit le "
"domaine d’exécution de l’appelant à la valeur indiquée par I<persona>. "
"Indiquer I<persona> à 0xffffffff permet de récupérer la personnalité "
"actuelle sans la modifier."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A list of the available execution domains can be found in I<E<lt>sys/"
"personality.hE<gt>>.  The execution domain is a 32-bit value in which the "
"top three bytes are set aside for flags that cause the kernel to modify the "
"behavior of certain system calls so as to emulate historical or "
"architectural quirks.  The least significant byte is a value defining the "
"personality the kernel should assume.  The flag values are as follows:"
msgstr ""
"Une liste des domaines d'exécution disponibles se trouve dans I<E<lt>sys/"
"personality.hE<gt>>. Le domaine d'exécution est une valeur 32 bits où les "
"trois premiers octets sont réservés à des attributs qui font modifier par le "
"noyau le comportement de certains appels système pour émuler des "
"excentricités architecturales ou historiques. L'octet le moins significatif "
"est la valeur définissant la personnalité que le noyau doit supposer. Les "
"valeurs des attributs sont les suivantes :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_COMPAT_LAYOUT> (since Linux 2.6.9)"
msgstr "B<ADDR_COMPAT_LAYOUT> (depuis Linux 2.6.9)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "With this flag set, provide legacy virtual address space layout."
msgstr ""
"Avec cet attribut, fournir une couche d'espace d'adressage virtuelle basique."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_NO_RANDOMIZE> (since Linux 2.6.12)"
msgstr "B<ADDR_NO_RANDOMIZE> (depuis Linux 2.6.12)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "With this flag set, disable address-space-layout randomization."
msgstr ""
"Avec cet attribut, désactiver la randomisation de la couche d'espace "
"d'adressage."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_LIMIT_32BIT> (since Linux 2.2)"
msgstr "B<ADDR_LIMIT_32BIT> (depuis Linux 2.20)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Limit the address space to 32 bits."
msgstr "Limiter l'espace d'adressage à 32 bits."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ADDR_LIMIT_3GB> (since Linux 2.4.0)"
msgstr "B<ADDR_LIMIT_3GB> (depuis Linux 2.4.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"With this flag set, use 0xc0000000 as the offset at which to search a "
"virtual memory chunk on B<mmap>(2); otherwise use 0xffffe000."
msgstr ""
"Avec cet attribut, utiliser 0xc0000000 comme position où chercher un bloc de "
"mémoire virtuelle sur B<mmap>(2) ; sinon utiliser 0xffffe000."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<FDPIC_FUNCPTRS> (since Linux 2.6.11)"
msgstr "B<FDPIC_FUNCPTRS> (depuis Linux 2.6.11)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"User-space function pointers to signal handlers point (on certain "
"architectures) to descriptors."
msgstr ""
"Les pointeurs de fonction de l'espace utilisateur vers des gestionnaires de "
"signaux pointent (sur certaines architectures) vers des descripteurs."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<MMAP_PAGE_ZERO> (since Linux 2.4.0)"
msgstr "B<MMAP_PAGE_ZERO> (depuis Linux 2.4.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Map page 0 as read-only (to support binaries that depend on this SVr4 "
"behavior)."
msgstr ""
"La page de projection B<0> en lecture seule (pour prendre en charge les "
"binaires qui dépendent de ce comportement SVr4)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<READ_IMPLIES_EXEC> (since Linux 2.6.8)"
msgstr "B<READ_IMPLIES_EXEC> (depuis Linux 2.6.8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "With this flag set, B<PROT_READ> implies B<PROT_EXEC> for B<mmap>(2)."
msgstr "Avec cet attribut, B<PROT_READ> implique B<PROT_EXEC> pour B<mmap>(2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SHORT_INODE> (since Linux 2.4.0)"
msgstr "B<SHORT_INODE> (depuis Linux 2.4.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "No effects(?)."
msgstr "Aucun effet(?)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<STICKY_TIMEOUTS> (since Linux 1.2.0)"
msgstr "B<STICKY_TIMEOUTS> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"With this flag set, B<select>(2), B<pselect>(2), and B<ppoll>(2)  do not "
"modify the returned timeout argument when interrupted by a signal handler."
msgstr ""
"Avec cet attribut, B<select>(2), B<pselect>(2) et B<ppoll>(2) ne modifient "
"pas le paramètre de délai renvoyé lorsqu'il est interrompu par un "
"gestionnaire de signal."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<UNAME26> (since Linux 3.1)"
msgstr "B<UNAME26> (depuis Linux 3.1)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Have B<uname>(2)  report a 2.6.40+ version number rather than a 3.x version "
"number.  Added as a stopgap measure to support broken applications that "
"could not handle the kernel version-numbering switch from Linux 2.6.x to "
"Linux 3.x."
msgstr ""
"Demander à B<uname>(2) de signaler un numéro de version 2.6.40+ au lieu d'un "
"3.x. Ajouté à titre de rattrapage pour gérer les applications cassées qui ne "
"pouvaient pas gérer le passage de la numérotation de version de Linux 2.6.x "
"à Linux 3.x."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<WHOLE_SECONDS> (since Linux 1.2.0)"
msgstr "B<WHOLE_SECONDS> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "No effect."
msgstr "Aucun effet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The available execution domains are:"
msgstr "Les domaines d'exécution disponibles sont :"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_BSD> (since Linux 1.2.0)"
msgstr "B<PER_BSD> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "BSD. (No effects.)"
msgstr "BSD. (pas d'effet)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_HPUX> (since Linux 2.4)"
msgstr "B<PER_HPUX> (depuis Linux 2.4)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Support for 32-bit HP/UX.  This support was never complete, and was dropped "
"so that since Linux 4.0, this value has no effect."
msgstr ""
"Gestion de HP/UX 32 bits. Cette prise en charge n'a jamais été totale et a "
"été abandonnée, si bien qu'à partir de Linux 4.0, cette valeur n'a pas "
"d'effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIX32> (since Linux 2.2)"
msgstr "B<PER_IRIX32> (depuis Linux 2.2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"IRIX 5 32-bit.  Never fully functional; support dropped in Linux 2.6.27.  "
"Implies B<STICKY_TIMEOUTS>."
msgstr ""
"IRIX 5 32 bits. Jamais totalement opérationnel ; prise en charge abandonnée "
"dans Linux 2.6.27. Implique B<STICKY_TIMEOUTS>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIX64> (since Linux 2.2)"
msgstr "B<PER_IRIX64> (depuis Linux 2.2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "IRIX 6 64-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr "IRIX 6 64 bits. Implique B<STICKY_TIMEOUTS> ; sinon aucun effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_IRIXN32> (since Linux 2.2)"
msgstr "B<PER_IRIXN32> (depuis Linux 2.2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "IRIX 6 new 32-bit.  Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr ""
"IRIX 6 nouveau 32 bits. Implique B<STICKY_TIMEOUTS> ; sinon aucun effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_ISCR4> (since Linux 1.2.0)"
msgstr "B<PER_ISCR4> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS>; otherwise no effects."
msgstr "Implique B<STICKY_TIMEOUTS> ; sinon aucun effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX> (since Linux 1.2.0)"
msgstr "B<PER_LINUX> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX32> (since Linux 2.2)"
msgstr "B<PER_LINUX32> (depuis Linux 2.2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "[To be documented.]"
msgstr "[À documenter]."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX32_3GB> (since Linux 2.4)"
msgstr "B<PER_LINUX32_3GB> (depuis Linux 2.4)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Implies B<ADDR_LIMIT_3GB>."
msgstr "Implique B<ADDR_LIMIT_3GB>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX_32BIT> (since Linux 2.0)"
msgstr "B<PER_LINUX_32BIT> (depuis Linux 2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Implies B<ADDR_LIMIT_32BIT>."
msgstr "Implique B<ADDR_LIMIT_32BIT>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_LINUX_FDPIC> (since Linux 2.6.11)"
msgstr "B<PER_LINUX_FDPIC> (depuis Linux 2.6.11)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Implies B<FDPIC_FUNCPTRS>."
msgstr "Implique B<FDPIC_FUNCPTRS>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_OSF4> (since Linux 2.4)"
msgstr "B<PER_OSF4> (depuis Linux 2.4)"

#.  Following is from a comment in arch/alpha/kernel/osf_sys.c
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"OSF/1 v4.  On alpha, clear top 32 bits of iov_len in the user's buffer for "
"compatibility with old versions of OSF/1 where iov_len was defined as.  "
"I<int>."
msgstr ""
"OSF/1 v4. Sur alpha, vider les 32 premiers bits de iov_len dans le tampon de "
"l'utilisateur pour être compatible avec les anciennes versions de OSF/1 où "
"iov_len était défini comme I<int>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_OSR5> (since Linux 2.4)"
msgstr "B<PER_OSR5> (depuis Linux 2.4)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS> and B<WHOLE_SECONDS>; otherwise no effects."
msgstr "Implique B<STICKY_TIMEOUTS> et B<WHOLE_SECONDS> ; sinon aucun effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_RISCOS> (since Linux 2.2)"
msgstr "B<PER_RISCOS> (depuis Linux 2.2)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SCOSVR3> (since Linux 1.2.0)"
msgstr "B<PER_SCOSVR3> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Implies B<STICKY_TIMEOUTS>, B<WHOLE_SECONDS>, and B<SHORT_INODE>; otherwise "
"no effects."
msgstr ""
"Implique B<STICKY_TIMEOUTS>, B<WHOLE_SECONDS> et B<SHORT_INODE> ; sinon "
"aucun effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SOLARIS> (since Linux 2.4)"
msgstr "B<PER_SOLARIS> (depuis Linux 2.4)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SUNOS> (since Linux 2.4.0)"
msgstr "B<PER_SUNOS> (depuis Linux 2.4.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Implies B<STICKY_TIMEOUTS>.  Divert library and dynamic linker searches to "
"I</usr/gnemul>.  Buggy, largely unmaintained, and almost entirely unused; "
"support was removed in Linux 2.6.26."
msgstr ""
"Implique B<STICKY_TIMEOUTS>. Lance la recherche de la bibliothèque et de "
"l'éditeur de liens dynamique dans I</usr/gnemul>. Bogué, très mal entretenu, "
"presque totalement inusité ; la prise en charge a été supprimée dans "
"Linux 2.6.26."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SVR3> (since Linux 1.2.0)"
msgstr "B<PER_SVR3> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS> and B<SHORT_INODE>; otherwise no effects."
msgstr "Implique B<STICKY_TIMEOUTS> et B<SHORT_INODE> ; sinon aucun effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_SVR4> (since Linux 1.2.0)"
msgstr "B<PER_SVR4> (depuis Linux 1.2.0)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Implies B<STICKY_TIMEOUTS> and B<MMAP_PAGE_ZERO>; otherwise no effects."
msgstr "Implique B<STICKY_TIMEOUTS> et B<MMAP_PAGE_ZERO> ; sinon aucun effet."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_UW7> (since Linux 2.4)"
msgstr "B<PER_UW7> (depuis Linux 2.4)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_WYSEV386> (since Linux 1.2.0)"
msgstr "B<PER_WYSEV386> (depuis Linux 1.2.0)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<PER_XENIX> (since Linux 1.2.0)"
msgstr "B<PER_XENIX> (depuis Linux 1.2.0)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, the previous I<persona> is returned.  On error, -1 is returned, "
"and I<errno> is set to indicate the error."
msgstr ""
"Renvoi de l'ancienne valeur I<persona> s'il réussit. En cas d'échec, B<-1> "
"est renvoyé et I<errno> est défini pour indiquer l'erreur."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The kernel was unable to change the personality."
msgstr "Le noyau n'a pas pu changer la personnalité."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIQUE"

#.  (and thus first in a stable kernel release with Linux 1.2.0)
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux 1.1.20, glibc 2.3."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<setarch>(8)"
msgstr "B<setarch>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 décembre 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pages du manuel de Linux 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. #-#-#-#-#  debian-bookworm: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#. #-#-#-#-#  debian-unstable: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#. #-#-#-#-#  fedora-38: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in glibc 2.2.91.
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: personality.2.pot (PACKAGE VERSION)  #-#-#-#-#
#.  personality wrapper first appeared in glibc 1.90,
#.  <sys/personality.h> was added later in 2.2.91.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"This system call first appeared in Linux 1.1.20 (and thus first in a stable "
"kernel release with Linux 1.2.0); library support was added in glibc 2.3."
msgstr ""
"Cet appel système est apparu dans Linux 1.1.20 (ainsi, pour la première fois "
"dans la version stable du noyau Linux 1.2.0) ; la glibc le gère depuis la "
"version 2.3."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"B<personality>()  is Linux-specific and should not be used in programs "
"intended to be portable."
msgstr ""
"B<personality>() est spécifique à Linux et ne devrait pas être utilisé dans "
"des programmes destinés à être portables."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "PERSONALITY"
msgstr "PERSONALITY"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>sys/personality.hE<gt>>"
msgstr "B<#include E<lt>sys/personality.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int personality(unsigned long >I<persona>B<);>"
msgstr "B<int personality(unsigned long >I<persona>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"A list of the available execution domains can be found in I<E<lt>sys/"
"personality.hE<gt>>.  The execution domain is a 32-bit value in which the "
"top three bytes are set aside for flags that cause the kernel to modify the "
"behavior of certain system calls so as to emulate historical or "
"architectural quirks.  The least significant byte is value defining the "
"personality the kernel should assume.  The flag values are as follows:"
msgstr ""
"Une liste des domaines d'exécution disponibles se trouve dans I<E<lt>sys/"
"personality.hE<gt>>. Le domaine d'exécution est une valeur 32 bits où les "
"trois premiers octets sont réservés à des attributs qui font modifier par le "
"noyau le comportement de certains appels système pour émuler des "
"excentricités architecturales ou historiques. L'octet le moins significatif "
"est la valeur définissant la personnalité que le noyau doit supposer. Les "
"valeurs des attributs sont les suivantes :"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Have B<uname>(2)  report a 2.6.40+ version number rather than a 3.x version "
"number.  Added as a stopgap measure to support broken applications that "
"could not handle the kernel version-numbering switch from 2.6.x to 3.x."
msgstr ""
"Demander à B<uname>(2) de signaler un numéro de version 2.6.40+ au lieu d'un "
"3.x. Ajouté à titre de rattrapage pour gérer les applications cassées qui ne "
"pouvaient pas gérer le passage de la numérotation de version du noyau de 2.6."
"x à 3.x."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, the previous I<persona> is returned.  On error, -1 is returned, "
"and I<errno> is set appropriately."
msgstr ""
"Renvoi de l'ancienne valeur I<persona> s'il réussit, ou B<-1> s'il échoue, "
"auquel cas I<errno> contient le code d'erreur."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
