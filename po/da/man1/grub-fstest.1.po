# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:30+0200\n"
"PO-Revision-Date: 2022-06-18 15:05+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-FSTEST"
msgstr "GRUB-FSTEST"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "June 2011"
msgid "June 2023"
msgstr "juni 2011"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.06.r499.ge67a551a4-1"
msgid "GRUB 2:2.06.r566.g857af0e17-1"
msgstr "GRUB 2:2.06.r499.ge67a551a4-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-fstest - debug tool for GRUB filesystem drivers"
msgstr "grub-fstest - fejlsøgningsværktøj for GRUB-filsystemdrivere"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-fstest> [I<\\,OPTION\\/>...] I<\\,IMAGE_PATH COMMANDS\\/>"
msgstr "B<grub-fstest> [I<\\,TILVALG\\/>...] I<\\,AFTRYKSSTI KOMMANDOER\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Debug tool for filesystem driver."
msgstr "Fejlsøgningsværktøj til filsystemdriver."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Commands:"
msgstr "Kommandoer:"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "blocklist FILE"
msgstr "blocklist FIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Display blocklist of FILE."
msgstr "Vis blokliste for FIL."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cat FILE"
msgstr "cat FIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Copy FILE to standard output."
msgstr "Kopier FIL til standard-ud."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cmp FILE LOCAL"
msgstr "cmp FIL LOKAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Compare FILE with local file LOCAL."
msgstr "Samenlign FIL med lokal fil LOKAL."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "cp FILE LOCAL"
msgstr "cp FIL LOKAL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Copy FILE to local file LOCAL."
msgstr "Kopier FIL til lokal fil LOKAL."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "crc FILE"
msgstr "crc FIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Get crc32 checksum of FILE."
msgstr "Beregn crc32-tjeksummen for FIL."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "hex FILE"
msgstr "hex FIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Show contents of FILE in hex."
msgstr "Vis indholdet af FIL i heks."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "ls PATH"
msgstr "ls STI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "List files in PATH."
msgstr "Vis filer i STI."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "xnu_uuid DEVICE"
msgstr "xnu_uuid ENHED"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Compute XNU UUID of the device."
msgstr "Beregn XNU UUID for enheden."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--diskcount>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--diskcount>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Specify the number of input files."
msgstr "Angiv antallet af inddatafiler."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-C>, B<--crypto>"
msgstr "B<-C>, B<--crypto>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Mount crypto devices."
msgstr "Monter kryptoenheder."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--debug>=I<\\,STRING\\/>"
msgstr "B<-d>, B<--debug>=I<\\,STRENG\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set debug environment variable."
msgstr "Angiv miljøvariabel til fejlsøgning."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-K>, B<--zfs-key>=I<\\,FILE\\/>|prompt"
msgstr "B<-K>, B<--zfs-key>=I<\\,FIL\\/>|prompt"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Load zfs crypto key."
msgstr "Indlæs zfs-kryptonøgle."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-n>, B<--length>=I<\\,NUM\\/>"
msgstr "B<-n>, B<--length>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Handle N bytes in output file."
msgstr "Håndter N byte i udskriftsfil."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--root>=I<\\,DEVICE_NAME\\/>"
msgstr "B<-r>, B<--root>=I<\\,ENHEDSNAVN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Set root device."
msgstr "Sæt rodenhed."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--skip>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--skip>=I<\\,NUM\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Skip N bytes from output file."
msgstr "Spring N byte over fra udskriftsfil."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-u>, B<--uncompress>"
msgstr "B<-u>, B<--uncompress>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Uncompress data."
msgstr "Udpak komprimerede data."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "udskriv uddybende meddelelser."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "vis denne hjælpeliste"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "vis en kort besked om brug af programmet"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "udskriv programversion"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Obligatoriske eller valgfri argumenter til lange tilvalg er også "
"obligatoriske henholdsvis valgfri til de tilsvarende korte."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-probe>(8)"
msgstr "B<grub-probe>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-fstest> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-fstest> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fulde dokumentation for B<grub-fstest> vedligeholdes som Texinfo-manual. "
"Hvis B<info> og B<grub-fstest> programmerne er korrekt installeret på din "
"side, bør kommandoen"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-fstest>"
msgstr "B<info grub-fstest>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "April 2023"
msgstr "April 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, fuzzy, no-wrap
#| msgid "GRUB 2.06-12"
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-12"
