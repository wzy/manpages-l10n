# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2023-06-27 20:03+0200\n"
"PO-Revision-Date: 2022-07-05 20:35+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ZSTDGREP"
msgstr "ZSTDGREP"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "March 2023"
msgstr "marts 2023"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "zstd 1.5.2"
msgid "zstd 1.5.5"
msgstr "zstd 1.5.2"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<zstdgrep> - print lines matching a pattern in zstandard-compressed files"
msgstr ""
"B<zstdgrep> - vis linjer der matcher et mønster i zstandard-komprimerede "
"filer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<zstdgrep> [I<grep-flags>] [--] I<pattern> [I<files> ...]"
msgid "B<zstdgrep> [I<grep-flags>] [--] I<pattern> [I<files> \\|.\\|.\\|.]"
msgstr "B<zstdgrep> [I<grep-flag>] [--] I<mønster> [I<filer> ...]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<zstdgrep> runs B<grep (1)> on files, or B<stdin> if no files argument "
#| "is given, after decompressing them with B<zstdcat (1)>."
msgid ""
"B<zstdgrep> runs B<grep>(1) on files, or B<stdin> if no files argument is "
"given, after decompressing them with B<zstdcat>(1)."
msgstr ""
"B<zstdgrep> afvikler B<grep (1)> på filer eller B<stdin>, hvis ingen "
"filargumenter er angivet, efter dekompression af dem med B<zstdcat (1)>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The grep-flags and pattern arguments are passed on to B<grep (1)>. If an "
#| "B<-e> flag is found in the B<grep-flags>, B<zstdgrep> will not look for a "
#| "pattern argument."
msgid ""
"The I<grep-flags> and I<pattern> arguments are passed on to B<grep>(1). If "
"an B<-e> flag is found in the I<grep-flags>, B<zstdgrep> will not look for a "
"I<pattern> argument."
msgstr ""
"Grep-flagene og mønsterargumenter sendes til B<grep (1)>. Hvis et B<-e>-flag "
"findes i B<grep-flags>, så vil B<zstdgrep> ikke kigge efter et "
"mønsterargument."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Note that modern B<grep> alternatives such as B<ripgrep> (B<rg>) support "
#| "B<zstd>-compressed files out of the box, and can prove better "
#| "alternatives than B<zstdgrep> notably for unsupported complex pattern "
#| "searches. Note though that such alternatives may also feature some minor "
#| "command line differences."
msgid ""
"Note that modern B<grep> alternatives such as B<ripgrep> (B<rg>(1)) support "
"B<zstd>-compressed files out of the box, and can prove better alternatives "
"than B<zstdgrep> notably for unsupported complex pattern searches. Note "
"though that such alternatives may also feature some minor command line "
"differences."
msgstr ""
"Bemærk at moderne B<grep>-alternativer såsom B<ripgrep> (B<rg>) understøtter "
"B<zstd>-komprimerede filer direkte, og kan være et bedre alternativ end "
"B<zstdgrep> specielt for komplekse mønstersøgninger, der ikke er "
"understøttet. Bemærk dog også at sådanne alternativer kan have mindre "
"kommandolinjeforskelle."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "AFSLUT-STATUS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In case of missing arguments or missing pattern, 1 will be returned, "
"otherwise 0."
msgstr ""
"I tilfælde af manglende argumenter eller manglende mønster vil 1 blive "
"returneret, ellers 0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<zstd (1)>"
msgid "B<zstd>(1)"
msgstr "B<zstd (1)>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "FORFATTERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Thomas Klausner I<wiz@NetBSD.org>"
msgstr "Thomas Klausner I<wiz@NetBSD.org>"

#. type: TH
#: debian-bookworm fedora-38
#, no-wrap
msgid "February 2023"
msgstr "februar 2023"

#. type: TH
#: debian-bookworm fedora-38
#, fuzzy, no-wrap
#| msgid "zstd 1.5.2"
msgid "zstd 1.5.4"
msgstr "zstd 1.5.2"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "May 2021"
msgstr "maj 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "zstd 1.5.0"
msgstr "zstd 1.5.0"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<zstdgrep> [I<grep-flags>] [--] I<pattern> [I<files> ...]"
msgstr "B<zstdgrep> [I<grep-flag>] [--] I<mønster> [I<filer> ...]"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<zstdgrep> runs B<grep (1)> on files or stdin, if no files argument is "
"given, after decompressing them with B<zstdcat (1)>."
msgstr ""
"B<zstdgrep> afvikler B<grep (1)> på filer eller standardind, hvis ingen "
"filargumenter er angivet, efter dekompression af dem med B<zstdcat (1)>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The grep-flags and pattern arguments are passed on to B<grep (1)>. If an B<-"
"e> flag is found in the B<grep-flags>, B<zstdgrep> will not look for a "
"pattern argument."
msgstr ""
"Grep-flagene og mønsterargumenter sendes til B<grep (1)>. Hvis et B<-e>-flag "
"findes i B<grep-flags>, så vil B<zstdgrep> ikke kigge efter et "
"mønsterargument."

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<zstd (1)>"
msgstr "B<zstd (1)>"
