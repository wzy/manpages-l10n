# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.2\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: 2022-05-07 10:12+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RMDIR"
msgstr "RMDIR"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "April 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "rmdir - remove empty directories"
msgstr "rmdir - fjern tomme mapper"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<rmdir> [I<\\,OPTION\\/>]... I<\\,DIRECTORY\\/>..."
msgstr "B<rmdir> [I<\\,FLAG\\/>]... I<\\,KATALOG\\/>..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Remove the DIRECTORY(ies), if they are empty."
msgstr "Slet KATALOGer, hvis de er tomme."

#. #-#-#-#-#  archlinux: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-5: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: rmdir.1.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-fail-on-non-empty>"
msgstr "B<--ignore-fail-on-non-empty>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "ignore each failure that is solely because a directory"
msgid "ignore each failure to remove a non-empty directory"
msgstr "ignorér alle fejl som udelukkende skyldes at kataloget"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--parents>"
msgstr "B<-p>, B<--parents>"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "remove DIRECTORY and its ancestors; e.g., 'rmdir B<-p> a/b/c' is similar "
#| "to 'rmdir a/b/c a/b a'"
msgid ""
"remove DIRECTORY and its ancestors; e.g., 'rmdir B<-p> a/b' is similar to "
"'rmdir a/b a'"
msgstr ""
"slet KATALOG og dets overkataloger. F.eks. virker \"rmdir B<-p> a/b/c\" "
"ligesom \"rmdir a/b/c a/b a\""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output a diagnostic for every directory processed"
msgstr "vis meddelelse for hvert katalog som behandles"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "vis denne hjælpetekst og afslut"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "vis versionsinformation og afslut"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by David MacKenzie."
msgstr "Skrevet af David MacKenzie."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Hjælp til GNU coreutils på nettet:: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversættelsesfejl til E<lt>https://translationproject.org/team/da."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPHAVSRET"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er et frit program: du kan frit ændre og videredistribuere det. Der "
"gives INGEN GARANTI, i den grad som loven tillader dette."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<rmdir>(2)"
msgstr "B<rmdir>(2)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/rmdirE<gt>"
msgstr ""
"Fuld dokumentation E<lt>https://www.gnu.org/software/coreutils/rmdirE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) rmdir invocation\\(aq"
msgstr ""
"eller lokalt tilgængelig via: info \\(aq(coreutils) rmdir invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "ignore each failure that is solely because a directory is non-empty"
msgstr "ignorer alle fejl som udelukkende skyldes at mappen ikke er tom"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"remove DIRECTORY and its ancestors; e.g., 'rmdir B<-p> a/b/c' is similar to "
"'rmdir a/b/c a/b a'"
msgstr ""
"slet KATALOG og dets overkataloger. F.eks. virker \"rmdir B<-p> a/b/c\" "
"ligesom \"rmdir a/b/c a/b a\""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-38
#, no-wrap
msgid "January 2023"
msgstr "januar 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-5
msgid "ignore each failure that is solely because a directory"
msgstr "ignorér alle fejl som udelukkende skyldes at kataloget"

#. type: Plain text
#: opensuse-leap-15-5
msgid "is non-empty"
msgstr "ikke er tomt"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-5
msgid "rmdir(2)"
msgstr "rmdir(2)"
