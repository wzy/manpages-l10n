# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.2\n"
"POT-Creation-Date: 2023-06-27 19:25+0200\n"
"PO-Revision-Date: 2022-05-01 10:23+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ENV"
msgstr "ENV"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "April 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "env - run a program in a modified environment"
msgstr "env - afvikling af et program i et ændret miljø"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<env> [I<\\,OPTION\\/>]... [I<\\,-\\/>] [I<\\,NAME=VALUE\\/>]... [I<\\,"
"COMMAND \\/>[I<\\,ARG\\/>]...]"
msgstr ""
"B<env> [I<\\,FLAG\\/>]... [I<\\,-\\/>] [I<\\,NAVN=VÆRDI\\/>]... [I<\\,"
"KOMMANDO \\/>[I<\\,ARG\\/>]...]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Set each NAME to VALUE in the environment and run COMMAND."
msgstr "Sæt hvert NAVN til VÆRDI i miljøet og kør KOMMANDO."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Obligatoriske argumenter til lange flag er også obligatoriske for de korte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-environment>"
msgstr "B<-i>, B<--ignore-environment>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "start with an empty environment"
msgstr "start uden miljøvariable"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-0>, B<--null>"
msgstr "B<-0>, B<--null>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "end each output line with NUL, not newline"
msgstr "afslut hver udlinje med NUL frem for linjeskift"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--unset>=I<\\,NAME\\/>"
msgstr "B<-u>, B<--unset>=I<\\,NAVN\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "remove variable from the environment"
msgstr "fjern miljøvariablen NAVN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-C>, B<--chdir>=I<\\,DIR\\/>"
msgstr "B<-C>, B<--chdir>=I<\\,KAT\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "change working directory to DIR"
msgstr "ændr arbejdskatalog til KAT"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-S>, B<--split-string>=I<\\,S\\/>"
msgstr "B<-S>, B<--split-string>=I<\\,S\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"process and split S into separate arguments; used to pass multiple arguments "
"on shebang lines"
msgstr ""
"behandl og opdel S i adskilte argumenter; bruges til at give flere "
"argumenter i shebang-linjer"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--block-signal>[=I<\\,SIG\\/>]"
msgstr "B<--block-signal>[=I<\\,SIG\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "block delivery of SIG signal(s) to COMMAND"
msgstr "blokér levering af SIG-signaler til KOMMANDO"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--default-signal>[=I<\\,SIG\\/>]"
msgstr "B<--default-signal>[=I<\\,SIG\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "reset handling of SIG signal(s) to the default"
msgstr "nulstil håndtering af SIG-signaler til standard"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-signal>[=I<\\,SIG\\/>]"
msgstr "B<--ignore-signal>[=I<\\,SIG\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "set handling of SIG signal(s) to do nothing"
msgstr "sæt håndtering af SIG-signaler til ingenting"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--list-signal-handling>"
msgstr "B<--list-signal-handling>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "list non default signal handling to stderr"
msgstr "vis ændrede indstillinger for signaler på stderr"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--debug>"
msgstr "B<-v>, B<--debug>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "print verbose information for each processing step"
msgstr "vis uddybende information om hvert trin i behandling"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "vis denne hjælpetekst og afslut"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "vis versionsinformation og afslut"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A mere - implies B<-i>.  If no COMMAND, print the resulting environment."
msgstr ""
"En - for sig selv medfører B<-i>.  Hvis ingen KOMMANDO er angivet, udskriv "
"det resulterende miljø."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "SIG may be a signal name like 'PIPE', or a signal number like '13'.  "
#| "Without SIG, all known signals are included.  Multiple signals can be "
#| "comma-separated."
msgid ""
"SIG may be a signal name like 'PIPE', or a signal number like '13'.  Without "
"SIG, all known signals are included.  Multiple signals can be comma-"
"separated.  An empty SIG argument is a no-op."
msgstr ""
"SIG kan være et signalnavn såsom \"PIPE\" eller et signalnummer såsom "
"\"13\". Uden SIG medtages alle kendte signaler.  Signaler kan adskilles med "
"komma."

#. type: SS
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Afslutningskode:"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "125"
msgstr "125"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "if the env command itself fails"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "126"
msgstr "126"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND is found but cannot be invoked"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "127"
msgstr "127"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND cannot be found"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr "-"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "the exit status of COMMAND otherwise"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "TILVALG"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-S/--split-string usage in scripts"
msgstr "Brug af -S/--split-string i skripter"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<-S> option allows specifying multiple parameters in a script.  Running "
"a script named B<1.pl> containing the following first line:"
msgstr ""
"Tilvalget B<-S> gør det muligt at angive flere parametre i et skript. "
"Afvikling af et skript navngivet B<1.pl> indeholdende den følgende første "
"linje:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#!/usr/bin/env -S perl -w -T\n"
"\\&...\n"
msgstr ""
"#!/usr/bin/env -S perl -w -T\n"
"\\&...\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Will execute B<perl -w -T 1.pl .>"
msgstr "Vil afvikle B<perl -w -T 1.pl .>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Without the B<'-S'> parameter the script will likely fail with:"
msgstr "Uden parameteren B<'-S'> vil skriptet sandsynligvis fejle med:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/usr/bin/env: 'perl -w -T': No such file or directory\n"
msgstr "/usr/bin/env: 'perl -w -T': Ingen sådan fil eller mappe\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See the full documentation for more details."
msgstr "Se den fulde dokumentation for yderligere detaljer."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "--default-signal[=SIG] usage"
msgstr "--default-signal[=SIG] brug"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This option allows setting a signal handler to its default action, which is "
"not possible using the traditional shell trap command.  The following "
"example ensures that seq will be terminated by SIGPIPE no matter how this "
"signal is being handled in the process invoking the command."
msgstr ""
"Dette tilvalg gør det muligt at indstille en signalhåndtering til sin "
"standardhandling, hvilket ikke er muligt via den traditionelle shell trap-"
"kommando. De følgende eksempler sikrer, at seq vil blive afsluttet af "
"SIGPIPE uanset hvordan dette signal bliver håndteret i processen, der sætter "
"kommandoen i gang."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "sh -c 'env --default-signal=PIPE seq inf | head -n1'\n"
msgstr "sh -c 'env --default-signal=PIPE seq inf | head -n1'\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "POSIX's B<exec>(3p) pages says:"
msgstr "POSIX's B<exec>(3p) sider siger:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"many existing applications wrongly assume that they start with certain "
"signals set to the default action and/or unblocked.... Therefore, it is best "
"not to block or ignore signals across execs without explicit reason to do "
"so, and especially not to block signals across execs of arbitrary (not "
"closely cooperating) programs."
msgstr ""
"mange eksisterende programmer antager fejlagtigt, at de starter med bestemte "
"signaler sat til standardhandlingen og/eller ublokeret ... Derfor er det "
"bedst ikke at blokere eller ignorere signaler på tværs af kørbare filer uden "
"en eksplicit grund til dette, og specielt ikke blokere signaler på tværs af "
"kørbare filer for arbitrære (ikke tæt samarbejdende) programmer."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Richard Mlynarik, David MacKenzie, and Assaf Gordon."
msgstr "Skrevet af Richard Mlynarik, David MacKenzie og Assaf Gordon."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Hjælp til GNU coreutils på nettet:: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversættelsesfejl til E<lt>https://translationproject.org/team/da."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPHAVSRET"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er et frit program: du kan frit ændre og videredistribuere det. Der "
"gives INGEN GARANTI, i den grad som loven tillader dette."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<sigaction>(2), B<sigprocmask>(2), B<signal>(7)"
msgstr "B<sigaction>(2), B<sigprocmask>(2), B<signal>(7)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/envE<gt>"
msgstr ""
"Fuld dokumentation E<lt>https://www.gnu.org/software/coreutils/envE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) env invocation\\(aq"
msgstr ""
"eller lokalt tilgængelig via: info \\(aq(coreutils) env invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"SIG may be a signal name like 'PIPE', or a signal number like '13'.  Without "
"SIG, all known signals are included.  Multiple signals can be comma-"
"separated."
msgstr ""
"SIG kan være et signalnavn såsom \"PIPE\" eller et signalnummer såsom "
"\"13\". Uden SIG medtages alle kendte signaler.  Signaler kan adskilles med "
"komma."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-38
#, no-wrap
msgid "January 2023"
msgstr "januar 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-5
msgid "set handling of SIG signals(s) to do nothing"
msgstr "sæt håndtering af SIG-signaler til ingenting"

#. type: Plain text
#: opensuse-leap-15-5
msgid "POSIX's exec(2) pages says:"
msgstr "POSIX's exec(2) sider siger:"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-5
msgid "sigaction(2), sigprocmask(2), signal(7)"
msgstr "sigaction(2), sigprocmask(2), signal(7)"
