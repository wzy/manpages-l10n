# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 정강훈 <skyeyes@soback.kornet.net>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:52+0200\n"
"PO-Revision-Date: 2000-09-16 08:57+0900\n"
"Last-Translator: 정강훈 <skyeyes@soback.kornet.net>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "statfs"
msgstr "statfs"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "statfs, fstatfs - get file system statistics"
msgid "statfs, fstatfs - get filesystem statistics"
msgstr "statfs, fstatfs - 파일 시스템 통계를 가져온다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>sys/vfs.hE<gt>>"
msgid "B<#include E<lt>sys/vfs.hE<gt>    >/* or E<lt>sys/statfs.hE<gt> */\n"
msgstr "B<#include E<lt>sys/vfs.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] int statfs(const char *>I<path>B<, struct statfs *>I<buf>B<);>\n"
"B<[[deprecated]] int fstatfs(int >I<fd>B<, struct statfs *>I<buf>B<);>\n"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<statfs> returns information about a mounted file system.  I<path> is "
#| "the path name of any file within the mounted filesystem.  I<buf> is a "
#| "pointer to a I<statfs> structure defined as follows:"
msgid ""
"The B<statfs>()  system call returns information about a mounted "
"filesystem.  I<path> is the pathname of any file within the mounted "
"filesystem.  I<buf> is a pointer to a I<statfs> structure defined "
"approximately as follows:"
msgstr ""
"B<statfs> 는 마운트된 파일시스템에 관한 정보를 반환한다.  I<path> 는 마운트"
"된 파일 시스템내 파일의 경로 이름이다.  I<buf> 는 다음과 같이 정의된 "
"I<statfs> 구조체에 대한 포인터이다:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct statfs {\n"
#| "   long    f_type;     /* type of filesystem (see below) */\n"
#| "   long    f_bsize;    /* optimal transfer block size */\n"
#| "   long    f_blocks;   /* total data blocks in file system */\n"
#| "   long    f_bfree;    /* free blocks in fs */\n"
#| "   long    f_bavail;   /* free blocks avail to non-superuser */\n"
#| "   long    f_files;    /* total file nodes in file system */\n"
#| "   long    f_ffree;    /* free file nodes in fs */\n"
#| "   fsid_t  f_fsid;     /* file system id */\n"
#| "   long    f_namelen;  /* maximum length of filenames */\n"
#| "   long    f_spare[6]; /* spare for later */\n"
#| "};\n"
msgid ""
"struct statfs {\n"
"    __fsword_t f_type;    /* Type of filesystem (see below) */\n"
"    __fsword_t f_bsize;   /* Optimal transfer block size */\n"
"    fsblkcnt_t f_blocks;  /* Total data blocks in filesystem */\n"
"    fsblkcnt_t f_bfree;   /* Free blocks in filesystem */\n"
"    fsblkcnt_t f_bavail;  /* Free blocks available to\n"
"                             unprivileged user */\n"
"    fsfilcnt_t f_files;   /* Total inodes in filesystem */\n"
"    fsfilcnt_t f_ffree;   /* Free inodes in filesystem */\n"
"    fsid_t     f_fsid;    /* Filesystem ID */\n"
"    __fsword_t f_namelen; /* Maximum length of filenames */\n"
"    __fsword_t f_frsize;  /* Fragment size (since Linux 2.6) */\n"
"    __fsword_t f_flags;   /* Mount flags of filesystem\n"
"                             (since Linux 2.6.36) */\n"
"    __fsword_t f_spare[xxx];\n"
"                    /* Padding bytes reserved for future use */\n"
"};\n"
msgstr ""
"struct statfs {\n"
"   long    f_type;     /* 파일 시스템 타입(아래에서 보여준다) */\n"
"   long    f_bsize;    /* 최적화된 전송 블럭 크기 */\n"
"   long    f_blocks;   /* 파일 시스템내 총 데이터 블럭들 */\n"
"   long    f_bfree;    /* 파일 시스템내 여유 블럭들 */\n"
"   long    f_bavail;   /* 비-슈퍼 유저를 위한 여유 블럭들 */\n"
"   long    f_files;    /* 파일 시스템내 총 파일 노드들 */\n"
"   long    f_ffree;    /* 파일 시스템내 여유 파일 노드들 */\n"
"   fsid_t  f_fsid;     /* 파일 시스템 ID */\n"
"   long    f_namelen;  /* 파일 이름의 최대 길이 */\n"
"   long    f_spare[6]; /* 나중을 위한 여유분 */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following filesystem types may appear in I<f_type>:"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"ADFS_SUPER_MAGIC      0xadf5\n"
"AFFS_SUPER_MAGIC      0xadff\n"
"AFS_SUPER_MAGIC       0x5346414f\n"
"ANON_INODE_FS_MAGIC   0x09041934 /* Anonymous inode FS (for\n"
"                                    pseudofiles that have no name;\n"
"                                    e.g., epoll, signalfd, bpf) */\n"
"AUTOFS_SUPER_MAGIC    0x0187\n"
"BDEVFS_MAGIC          0x62646576\n"
"BEFS_SUPER_MAGIC      0x42465331\n"
"BFS_MAGIC             0x1badface\n"
"BINFMTFS_MAGIC        0x42494e4d\n"
"BPF_FS_MAGIC          0xcafe4a11\n"
"BTRFS_SUPER_MAGIC     0x9123683e\n"
"BTRFS_TEST_MAGIC      0x73727279\n"
"CGROUP_SUPER_MAGIC    0x27e0eb   /* Cgroup pseudo FS */\n"
"CGROUP2_SUPER_MAGIC   0x63677270 /* Cgroup v2 pseudo FS */\n"
"CIFS_MAGIC_NUMBER     0xff534d42\n"
"CODA_SUPER_MAGIC      0x73757245\n"
"COH_SUPER_MAGIC       0x012ff7b7\n"
"CRAMFS_MAGIC          0x28cd3d45\n"
"DEBUGFS_MAGIC         0x64626720\n"
"DEVFS_SUPER_MAGIC     0x1373     /* Linux 2.6.17 and earlier */\n"
"DEVPTS_SUPER_MAGIC    0x1cd1\n"
"ECRYPTFS_SUPER_MAGIC  0xf15f\n"
"EFIVARFS_MAGIC        0xde5e81e4\n"
"EFS_SUPER_MAGIC       0x00414a53\n"
"EXT_SUPER_MAGIC       0x137d     /* Linux 2.0 and earlier */\n"
"EXT2_OLD_SUPER_MAGIC  0xef51\n"
"EXT2_SUPER_MAGIC      0xef53\n"
"EXT3_SUPER_MAGIC      0xef53\n"
"EXT4_SUPER_MAGIC      0xef53\n"
"F2FS_SUPER_MAGIC      0xf2f52010\n"
"FUSE_SUPER_MAGIC      0x65735546\n"
"FUTEXFS_SUPER_MAGIC   0xbad1dea  /* Unused */\n"
"HFS_SUPER_MAGIC       0x4244\n"
"HOSTFS_SUPER_MAGIC    0x00c0ffee\n"
"HPFS_SUPER_MAGIC      0xf995e849\n"
"HUGETLBFS_MAGIC       0x958458f6\n"
"ISOFS_SUPER_MAGIC     0x9660\n"
"JFFS2_SUPER_MAGIC     0x72b6\n"
"JFS_SUPER_MAGIC       0x3153464a\n"
"MINIX_SUPER_MAGIC     0x137f     /* original minix FS */\n"
"MINIX_SUPER_MAGIC2    0x138f     /* 30 char minix FS */\n"
"MINIX2_SUPER_MAGIC    0x2468     /* minix V2 FS */\n"
"MINIX2_SUPER_MAGIC2   0x2478     /* minix V2 FS, 30 char names */\n"
"MINIX3_SUPER_MAGIC    0x4d5a     /* minix V3 FS, 60 char names */\n"
"MQUEUE_MAGIC          0x19800202 /* POSIX message queue FS */\n"
"MSDOS_SUPER_MAGIC     0x4d44\n"
"MTD_INODE_FS_MAGIC    0x11307854\n"
"NCP_SUPER_MAGIC       0x564c\n"
"NFS_SUPER_MAGIC       0x6969\n"
"NILFS_SUPER_MAGIC     0x3434\n"
"NSFS_MAGIC            0x6e736673\n"
"NTFS_SB_MAGIC         0x5346544e\n"
"OCFS2_SUPER_MAGIC     0x7461636f\n"
"OPENPROM_SUPER_MAGIC  0x9fa1\n"
"OVERLAYFS_SUPER_MAGIC 0x794c7630\n"
"PIPEFS_MAGIC          0x50495045\n"
"PROC_SUPER_MAGIC      0x9fa0     /* /proc FS */\n"
"PSTOREFS_MAGIC        0x6165676c\n"
"QNX4_SUPER_MAGIC      0x002f\n"
"QNX6_SUPER_MAGIC      0x68191122\n"
"RAMFS_MAGIC           0x858458f6\n"
"REISERFS_SUPER_MAGIC  0x52654973\n"
"ROMFS_MAGIC           0x7275\n"
"SECURITYFS_MAGIC      0x73636673\n"
"SELINUX_MAGIC         0xf97cff8c\n"
"SMACK_MAGIC           0x43415d53\n"
"SMB_SUPER_MAGIC       0x517b\n"
"SMB2_MAGIC_NUMBER     0xfe534d42\n"
"SOCKFS_MAGIC          0x534f434b\n"
"SQUASHFS_MAGIC        0x73717368\n"
"SYSFS_MAGIC           0x62656572\n"
"SYSV2_SUPER_MAGIC     0x012ff7b6\n"
"SYSV4_SUPER_MAGIC     0x012ff7b5\n"
"TMPFS_MAGIC           0x01021994\n"
"TRACEFS_MAGIC         0x74726163\n"
"UDF_SUPER_MAGIC       0x15013346\n"
"UFS_MAGIC             0x00011954\n"
"USBDEVICE_SUPER_MAGIC 0x9fa2\n"
"V9FS_MAGIC            0x01021997\n"
"VXFS_SUPER_MAGIC      0xa501fcf5\n"
"XENFS_SUPER_MAGIC     0xabba1974\n"
"XENIX_SUPER_MAGIC     0x012ff7b4\n"
"XFS_SUPER_MAGIC       0x58465342\n"
"_XIAFS_SUPER_MAGIC    0x012fd16d /* Linux 2.0 and earlier */\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Most of these MAGIC constants are defined in I</usr/include/linux/magic.h>, "
"and some are hardcoded in kernel sources."
msgstr ""

#.  XXX Keep this list in sync with statvfs(3)
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<f_flags> field is a bit mask indicating mount options for the "
"filesystem.  It contains zero or more of the following bits:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ST_MANDLOCK>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Mandatory locking is permitted on the filesystem (see B<fcntl>(2))."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<PROT_NONE>"
msgid "B<ST_NOATIME>"
msgstr "B<PROT_NONE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Do not update access times; see B<mount>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<ENODEV>"
msgid "B<ST_NODEV>"
msgstr "B<ENODEV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Disallow access to device special files on this filesystem."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ST_NODIRATIME>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Do not update directory access times; see B<mount>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<ENOEXEC>"
msgid "B<ST_NOEXEC>"
msgstr "B<ENOEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Execution of programs is disallowed on this filesystem."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<HOST_NOT_FOUND>"
msgid "B<ST_NOSUID>"
msgstr "B<HOST_NOT_FOUND>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The set-user-ID and set-group-ID bits are ignored by B<exec>(3)  for "
"executable files on this filesystem"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ST_RDONLY>"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "The file is on a read-only filesystem."
msgid "This filesystem is mounted read-only."
msgstr "파일은 오직 읽기가 가능한 파일시스템상에 있다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<PROT_READ>"
msgid "B<ST_RELATIME>"
msgstr "B<PROT_READ>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Update atime relative to mtime/ctime; see B<mount>(2)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<O_SYNC>"
msgid "B<ST_SYNCHRONOUS>"
msgstr "B<O_SYNC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Writes are synched to the filesystem immediately (see the description of "
"B<O_SYNC> in B<open>(2))."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<ST_NOSYMFOLLOW> (since Linux 5.10)"
msgstr ""

#.  dab741e0e02bd3c4f5e2e97be74b39df2523fc6e
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "Too many symbolic links were encountered in resolving I<path>."
msgid "Symbolic links are not followed when resolving paths; see B<mount>(2)."
msgstr "I<path>를 찾아갈 때, 너무 많은 심볼릭 링크가 존재한다."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Nobody knows what I<f_fsid> is supposed to contain (but see below)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Fields that are undefined for a particular filesystem are set to 0."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Fields that are undefined for a particular file system are set to 0.  "
#| "B<fstatfs> returns the same information about an open file referenced by "
#| "descriptor I<fd>."
msgid ""
"B<fstatfs>()  returns the same information about an open file referenced by "
"descriptor I<fd>."
msgstr ""
"특별한 파일 시스템을 위해 정의되지 않은 필드들은 -1로 설정된다.  B<fstatfs> "
"는 기술자 I<fd>에 의해 참조되는 열린 파일에 관한 같은 정보를 반환한다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"성공시, 0이 리턴된다. 에러시, -1이 리턴되며, I<errno>는 적당한 값으로 설정된"
"다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "에러"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Search permission is denied for a component of the path prefix of I<path>."
msgid ""
"(B<statfs>())  Search permission is denied for a component of the path "
"prefix of I<path>.  (See also B<path_resolution>(7).)"
msgstr "탐색 허가권이 I<path>의 경로 요소에서 거부되었다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "I<fd> is not a valid open file descriptor."
msgid "(B<fstatfs>())  I<fd> is not a valid open file descriptor."
msgstr "I<fd> 는 유효한 열려진 파일 기술자가 아니다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "I<Buf> or I<path> points to an invalid address."
msgid "I<buf> or I<path> points to an invalid address."
msgstr "I<Buf> 나 I<path> 가 유효하지 않는 주소를 가리키고 있다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The call was interrupted by a signal; see B<signal>(7)."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EIO>"
msgstr "B<EIO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "An I/O error occurred while reading from or writing to the file system."
msgid "An I/O error occurred while reading from the filesystem."
msgstr "파일 시스템을 읽거나 슬는 동안 I/O 에러가 발생했다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ELOOP>"
msgstr "B<ELOOP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Too many symbolic links were encountered in translating I<path>."
msgid ""
"(B<statfs>())  Too many symbolic links were encountered in translating "
"I<path>."
msgstr "너무 많은 상징 연결들을 I<path>해석시에 만났다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr "B<ENAMETOOLONG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "I<path> is too long."
msgid "(B<statfs>())  I<path> is too long."
msgstr "I<path> 가 너무 길다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "The file referred to by I<path> does not exist."
msgid "(B<statfs>())  The file referred to by I<path> does not exist."
msgstr "I<path> 가 가리키는 파일이 존재하지 않는다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient kernel memory was available."
msgstr "이용할수 있는 커널 메모리가 충분하지 않다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "The filesystem I<path> is on does not support B<statfs>."
msgid "The filesystem does not support this call."
msgstr "파일 시스템 I<path> 는 B<statfs>를 지원하지 않는다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTDIR>"
msgstr "B<ENOTDIR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "A component of the path prefix of I<path> is not a directory."
msgid ""
"(B<statfs>())  A component of the path prefix of I<path> is not a directory."
msgstr "I<path> 의 경로 요소가 디렉토리가 아니다."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EOVERFLOW>"
msgstr "B<EOVERFLOW>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Some values were too large to be represented in the returned struct."
msgstr ""

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "버전S"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "The f_fsid field"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Solaris, Irix, and POSIX have a system call B<statvfs>(2)  that returns a "
"I<struct statvfs> (defined in I<E<lt>sys/statvfs.hE<gt>>)  containing an "
"I<unsigned long> I<f_fsid>.  Linux, SunOS, HP-UX, 4.4BSD have a system call "
"B<statfs>()  that returns a I<struct statfs> (defined in I<E<lt>sys/vfs."
"hE<gt>>)  containing a I<fsid_t> I<f_fsid>, where I<fsid_t> is defined as "
"I<struct { int val[2]; }>.  The same holds for FreeBSD, except that it uses "
"the include file I<E<lt>sys/mount.hE<gt>>."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The general idea is that I<f_fsid> contains some random stuff such that the "
"pair (I<f_fsid>,I<ino>)  uniquely determines a file.  Some operating systems "
"use (a variation on) the device number, or the device number combined with "
"the filesystem type.  Several operating systems restrict giving out the "
"I<f_fsid> field to the superuser only (and zero it for unprivileged users), "
"because this field is used in the filehandle of the filesystem when NFS-"
"exported, and giving it out is a security concern."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Under some operating systems, the I<fsid> can be used as the second argument "
"to the B<sysfs>(2)  system call."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "리눅스."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"The Linux B<statfs>()  was inspired by the 4.4BSD one (but they do not use "
"the same structure)."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The original Linux B<statfs>()  and B<fstatfs>()  system calls were not "
"designed with extremely large file sizes in mind.  Subsequently, Linux 2.6 "
"added new B<statfs64>()  and B<fstatfs64>()  system calls that employ a new "
"structure, I<statfs64>.  The new structure contains the same fields as the "
"original I<statfs> structure, but the sizes of various fields are increased, "
"to accommodate large file sizes.  The glibc B<statfs>()  and B<fstatfs>()  "
"wrapper functions transparently deal with the kernel differences."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"LSB has deprecated the library calls B<statfs>()  and B<fstatfs>()  and "
"tells us to use B<statvfs>(3)  and B<fstatvfs>(3)  instead."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<__fsword_t> type used for various fields in the I<statfs> structure "
"definition is a glibc internal type, not intended for public use.  This "
"leaves the programmer in a bit of a conundrum when trying to copy or compare "
"these fields to local variables in a program.  Using I<unsigned\\ int> for "
"such variables suffices on most systems."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Some systems have only I<E<lt>sys/vfs.hE<gt>>, other systems also have "
"I<E<lt>sys/statfs.hE<gt>>, where the former includes the latter.  So it "
"seems including the former is the best choice."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "버그"

#.  broken in commit ff0c7d15f9787b7e8c601533c015295cc68329f8
#.  fixed in commit d70ef97baf048412c395bb5d65791d8fe133a52b
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"From Linux 2.6.38 up to and including Linux 3.1, B<fstatfs>()  failed with "
"the error B<ENOSYS> for file descriptors created by B<pipe>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<stat>(2), B<statvfs>(3), B<path_resolution>(7)"
msgstr ""

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "2022년 10월 30일"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"Linux-specific.  The Linux B<statfs>()  was inspired by the 4.4BSD one (but "
"they do not use the same structure)."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "STATFS"
msgstr "STATFS"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "2017년 9월 15일"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "리눅스"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "B<#include E<lt>sys/vfs.hE<gt>>"
msgid "B<#include E<lt>sys/vfs.hE<gt> >/* or E<lt>sys/statfs.hE<gt> */"
msgstr "B<#include E<lt>sys/vfs.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int statfs(const char *>I<path>B<, struct statfs *>I<buf>B<);>"
msgstr "B<int statfs(const char *>I<path>B<, struct statfs *>I<buf>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int fstatfs(int >I<fd>B<, struct statfs *>I<buf>B<);>"
msgstr "B<int fstatfs(int >I<fd>B<, struct statfs *>I<buf>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid ""
#| "struct statfs {\n"
#| "   long    f_type;     /* type of filesystem (see below) */\n"
#| "   long    f_bsize;    /* optimal transfer block size */\n"
#| "   long    f_blocks;   /* total data blocks in file system */\n"
#| "   long    f_bfree;    /* free blocks in fs */\n"
#| "   long    f_bavail;   /* free blocks avail to non-superuser */\n"
#| "   long    f_files;    /* total file nodes in file system */\n"
#| "   long    f_ffree;    /* free file nodes in fs */\n"
#| "   fsid_t  f_fsid;     /* file system id */\n"
#| "   long    f_namelen;  /* maximum length of filenames */\n"
#| "   long    f_spare[6]; /* spare for later */\n"
#| "};\n"
msgid ""
"struct statfs {\n"
"    __fsword_t f_type;    /* Type of filesystem (see below) */\n"
"    __fsword_t f_bsize;   /* Optimal transfer block size */\n"
"    fsblkcnt_t f_blocks;  /* Total data blocks in filesystem */\n"
"    fsblkcnt_t f_bfree;   /* Free blocks in filesystem */\n"
"    fsblkcnt_t f_bavail;  /* Free blocks available to\n"
"                             unprivileged user */\n"
"    fsfilcnt_t f_files;   /* Total file nodes in filesystem */\n"
"    fsfilcnt_t f_ffree;   /* Free file nodes in filesystem */\n"
"    fsid_t     f_fsid;    /* Filesystem ID */\n"
"    __fsword_t f_namelen; /* Maximum length of filenames */\n"
"    __fsword_t f_frsize;  /* Fragment size (since Linux 2.6) */\n"
"    __fsword_t f_flags;   /* Mount flags of filesystem\n"
"                             (since Linux 2.6.36) */\n"
"    __fsword_t f_spare[xxx];\n"
"                    /* Padding bytes reserved for future use */\n"
"};\n"
msgstr ""
"struct statfs {\n"
"   long    f_type;     /* 파일 시스템 타입(아래에서 보여준다) */\n"
"   long    f_bsize;    /* 최적화된 전송 블럭 크기 */\n"
"   long    f_blocks;   /* 파일 시스템내 총 데이터 블럭들 */\n"
"   long    f_bfree;    /* 파일 시스템내 여유 블럭들 */\n"
"   long    f_bavail;   /* 비-슈퍼 유저를 위한 여유 블럭들 */\n"
"   long    f_files;    /* 파일 시스템내 총 파일 노드들 */\n"
"   long    f_ffree;    /* 파일 시스템내 여유 파일 노드들 */\n"
"   fsid_t  f_fsid;     /* 파일 시스템 ID */\n"
"   long    f_namelen;  /* 파일 이름의 최대 길이 */\n"
"   long    f_spare[6]; /* 나중을 위한 여유분 */\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"ADFS_SUPER_MAGIC      0xadf5\n"
"AFFS_SUPER_MAGIC      0xadff\n"
"AFS_SUPER_MAGIC       0x5346414f\n"
"ANON_INODE_FS_MAGIC   0x09041934 /* Anonymous inode FS (for\n"
"                                    pseudofiles that have no name;\n"
"                                    e.g., epoll, signalfd, bpf) */\n"
"AUTOFS_SUPER_MAGIC    0x0187\n"
"BDEVFS_MAGIC          0x62646576\n"
"BEFS_SUPER_MAGIC      0x42465331\n"
"BFS_MAGIC             0x1badface\n"
"BINFMTFS_MAGIC        0x42494e4d\n"
"BPF_FS_MAGIC          0xcafe4a11\n"
"BTRFS_SUPER_MAGIC     0x9123683e\n"
"BTRFS_TEST_MAGIC      0x73727279\n"
"CGROUP_SUPER_MAGIC    0x27e0eb   /* Cgroup pseudo FS */\n"
"CGROUP2_SUPER_MAGIC   0x63677270 /* Cgroup v2 pseudo FS */\n"
"CIFS_MAGIC_NUMBER     0xff534d42\n"
"CODA_SUPER_MAGIC      0x73757245\n"
"COH_SUPER_MAGIC       0x012ff7b7\n"
"CRAMFS_MAGIC          0x28cd3d45\n"
"DEBUGFS_MAGIC         0x64626720\n"
"DEVFS_SUPER_MAGIC     0x1373     /* Linux 2.6.17 and earlier */\n"
"DEVPTS_SUPER_MAGIC    0x1cd1\n"
"ECRYPTFS_SUPER_MAGIC  0xf15f\n"
"EFIVARFS_MAGIC        0xde5e81e4\n"
"EFS_SUPER_MAGIC       0x00414a53\n"
"EXT_SUPER_MAGIC       0x137d     /* Linux 2.0 and earlier */\n"
"EXT2_OLD_SUPER_MAGIC  0xef51\n"
"EXT2_SUPER_MAGIC      0xef53\n"
"EXT3_SUPER_MAGIC      0xef53\n"
"EXT4_SUPER_MAGIC      0xef53\n"
"F2FS_SUPER_MAGIC      0xf2f52010\n"
"FUSE_SUPER_MAGIC      0x65735546\n"
"FUTEXFS_SUPER_MAGIC   0xbad1dea  /* Unused */\n"
"HFS_SUPER_MAGIC       0x4244\n"
"HOSTFS_SUPER_MAGIC    0x00c0ffee\n"
"HPFS_SUPER_MAGIC      0xf995e849\n"
"HUGETLBFS_MAGIC       0x958458f6\n"
"ISOFS_SUPER_MAGIC     0x9660\n"
"JFFS2_SUPER_MAGIC     0x72b6\n"
"JFS_SUPER_MAGIC       0x3153464a\n"
"MINIX_SUPER_MAGIC     0x137f     /* original minix FS */\n"
"MINIX_SUPER_MAGIC2    0x138f     /* 30 char minix FS */\n"
"MINIX2_SUPER_MAGIC    0x2468     /* minix V2 FS */\n"
"MINIX2_SUPER_MAGIC2   0x2478     /* minix V2 FS, 30 char names */\n"
"MINIX3_SUPER_MAGIC    0x4d5a     /* minix V3 FS, 60 char names */\n"
"MQUEUE_MAGIC          0x19800202 /* POSIX message queue FS */\n"
"MSDOS_SUPER_MAGIC     0x4d44\n"
"MTD_INODE_FS_MAGIC    0x11307854\n"
"NCP_SUPER_MAGIC       0x564c\n"
"NFS_SUPER_MAGIC       0x6969\n"
"NILFS_SUPER_MAGIC     0x3434\n"
"NSFS_MAGIC            0x6e736673\n"
"NTFS_SB_MAGIC         0x5346544e\n"
"OCFS2_SUPER_MAGIC     0x7461636f\n"
"OPENPROM_SUPER_MAGIC  0x9fa1\n"
"OVERLAYFS_SUPER_MAGIC 0x794c7630\n"
"PIPEFS_MAGIC          0x50495045\n"
"PROC_SUPER_MAGIC      0x9fa0     /* /proc FS */\n"
"PSTOREFS_MAGIC        0x6165676c\n"
"QNX4_SUPER_MAGIC      0x002f\n"
"QNX6_SUPER_MAGIC      0x68191122\n"
"RAMFS_MAGIC           0x858458f6\n"
"REISERFS_SUPER_MAGIC  0x52654973\n"
"ROMFS_MAGIC           0x7275\n"
"SECURITYFS_MAGIC      0x73636673\n"
"SELINUX_MAGIC         0xf97cff8c\n"
"SMACK_MAGIC           0x43415d53\n"
"SMB_SUPER_MAGIC       0x517b\n"
"SOCKFS_MAGIC          0x534f434b\n"
"SQUASHFS_MAGIC        0x73717368\n"
"SYSFS_MAGIC           0x62656572\n"
"SYSV2_SUPER_MAGIC     0x012ff7b6\n"
"SYSV4_SUPER_MAGIC     0x012ff7b5\n"
"TMPFS_MAGIC           0x01021994\n"
"TRACEFS_MAGIC         0x74726163\n"
"UDF_SUPER_MAGIC       0x15013346\n"
"UFS_MAGIC             0x00011954\n"
"USBDEVICE_SUPER_MAGIC 0x9fa2\n"
"V9FS_MAGIC            0x01021997\n"
"VXFS_SUPER_MAGIC      0xa501fcf5\n"
"XENFS_SUPER_MAGIC     0xabba1974\n"
"XENIX_SUPER_MAGIC     0x012ff7b4\n"
"XFS_SUPER_MAGIC       0x58465342\n"
"_XIAFS_SUPER_MAGIC    0x012fd16d /* Linux 2.0 and earlier */\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"성공시, 0이 리턴된다. 에러시, -1이 리턴되며, I<errno>는 적당한 값으로 설정된"
"다."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"LSB has deprecated the library calls B<statfs>()  and B<fstatfs>()  and "
"tells us to use B<statvfs>(2)  and B<fstatvfs>(2)  instead."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Solaris, Irix and POSIX have a system call B<statvfs>(2)  that returns a "
"I<struct statvfs> (defined in I<E<lt>sys/statvfs.hE<gt>>)  containing an "
"I<unsigned long> I<f_fsid>.  Linux, SunOS, HP-UX, 4.4BSD have a system call "
"B<statfs>()  that returns a I<struct statfs> (defined in I<E<lt>sys/vfs."
"hE<gt>>)  containing a I<fsid_t> I<f_fsid>, where I<fsid_t> is defined as "
"I<struct { int val[2]; }>.  The same holds for FreeBSD, except that it uses "
"the include file I<E<lt>sys/mount.hE<gt>>."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
