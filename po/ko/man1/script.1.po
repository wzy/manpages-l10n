# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:49+0200\n"
"PO-Revision-Date: 2000-03-28 08:57+0900\n"
"Last-Translator: Unknown <>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "SCRIPT 1"
msgid "SCRIPT"
msgstr "SCRIPT 1"

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "2022-08-04"
msgstr "2022년 8월 4일"

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "사용자 명령"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "make typescript of terminal session"
msgid "script - make typescript of terminal session"
msgstr "터미날에서 발생하는 모든 세션을 기록한다."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<script> [options] [I<file>]"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<script> makes a typescript of everything on your terminal session. The "
"terminal data are stored in raw form to the log file and information about "
"timing to another (optional) structured log file. The timing log file is "
"necessary to replay the session later by B<scriptreplay>(1) and to store "
"additional information about the session."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"Since version 2.35, B<script> supports multiple streams and allows the "
"logging of input and output to separate files or all the one file. This "
"version also supports a new timing file which records additional "
"information. The command B<scriptreplay --summary> then provides all the "
"information."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
msgid ""
"If the argument I<file> or option B<--log-out> I<file> is given, B<script> "
"saves the dialogue in this I<file>. If no filename is given, the dialogue is "
"saved in the file I<typescript>."
msgstr ""
"E<.Ar 파일> 인자가 있으면, 그 파일에 모든 내용을 기록하고, 없으면, E<.Pa "
"typescript> 파일에 기록한다."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Note that logging input using B<--log-in> or B<--log-io> may record security-"
"sensitive information as the log file contains all terminal session input (e."
"g., passwords) independently of the terminal echo flag setting."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "옵션"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Below, the I<size> argument may be followed by the multiplicative suffixes "
"KiB (=1024), MiB (=1024*1024), and so on for GiB, TiB, PiB, EiB, ZiB and YiB "
"(the \"iB\" is optional, e.g., \"K\" has the same meaning as \"KiB\"), or "
"the suffixes KB (=1000), MB (=1000*1000), and so on for GB, TB, PB, EB, ZB "
"and YB."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-a>, B<--append>"
msgstr "B<-a>, B<--append>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Append the output to E<.Ar file> or E<.Pa typescript>, retaining the "
#| "prior contents."
msgid ""
"Append the output to I<file> or to I<typescript>, retaining the prior "
"contents."
msgstr ""
"새로 기록하는 것이 아니라, 이미 있는 E<.Ar file> 에나, E<.Pa typescript> 파일"
"에 내용을 덧붙힌다."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-Q>, B<--no-warn>"
msgid "B<-c>, B<--command> I<command>"
msgstr "B<-Q>, B<--no-warn>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Run the I<command> rather than an interactive shell. This makes it easy for "
"a script to capture the output of a program that behaves differently when "
"its stdout is not a tty."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-Q>, B<--no-warn>"
msgid "B<-E>, B<--echo> I<when>"
msgstr "B<-Q>, B<--no-warn>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"This option controls the B<ECHO> flag for the slave end of the session\\(cqs "
"pseudoterminal. The supported modes are I<always>, I<never>, or I<auto>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The default is I<auto> \\(em in this case, B<ECHO> enabled for the "
"pseudoterminal slave; if the current standard input is a terminal, B<ECHO> "
"is disabled for it to prevent double echo; if the current standard input is "
"not a terminal (for example pipe: B<echo date | script>) then keeping "
"B<ECHO> enabled for the pseudoterminal slave enables the standard input data "
"to be viewed on screen while being recorded to session log simultaneously."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Note that \\(aqnever\\(aq mode affects content of the session output log, "
"because users input is not repeated on output."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-t>, B<--test>"
msgid "B<-e>, B<--return>"
msgstr "B<-t>, B<--test>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Return the exit status of the child process. Uses the same format as bash "
"termination on signal termination (i.e., exit status is 128 + the signal "
"number). The exit status of the child process is always stored in the type "
"script file too."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-l>, B<--list>"
msgid "B<-f>, B<--flush>"
msgstr "B<-l>, B<--list>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"Flush output after each write. This is nice for telecooperation: one person "
"does B<mkfifo> I<foo>; B<script -f> I<foo>, and another can supervise in "
"real-time what is being done using B<cat> I<foo>. Note that flush has an "
"impact on performance; it\\(cqs possible to use B<SIGUSR1> to flush logs on "
"demand."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-f --force>"
msgid "B<--force>"
msgstr "B<-f --force>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Allow the default output file I<typescript> to be a hard or symbolic link. "
"The command will follow a symbolic link."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-a>, B<--all>"
msgid "B<-B>, B<--log-io> I<file>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Log input and output to the same I<file>. Note, this option makes sense only "
"if B<--log-timing> is also specified, otherwise it\\(cqs impossible to "
"separate output and input streams from the log I<file>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-a>, B<--all>"
msgid "B<-I>, B<--log-in> I<file>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Log input to the I<file>. The log output is disabled if only B<--log-in> "
"specified."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Use this logging functionality carefully as it logs all input, including "
"input when terminal has disabled echo flag (for example, password inputs)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-O >I<file>"
msgid "B<-O>, B<--log-out> I<file>"
msgstr "B<-O >I<화일>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Log output to the I<file>. The default is to log output to the file with "
"name I<typescript> if the option B<--log-out> or B<--log-in> is not given. "
"The log output is disabled if only B<--log-in> specified."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-T>, B<--log-timing> I<file>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Log timing information to the I<file>. Two timing file formats are supported "
"now. The classic format is used when only one stream (input or output) "
"logging is enabled. The multi-stream format is used on B<--log-io> or when "
"B<--log-in> and B<--log-out> are used together. See also B<--logging-format>."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-m>, B<--logging-format> I<format>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Force use of I<advanced> or I<classic> format. The default is the classic "
"format to log only output and the advanced format when input as well as "
"output logging is requested."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<Classic format>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The log contains two fields, separated by a space. The first field indicates "
"how much time elapsed since the previous output. The second field indicates "
"how many characters were output this time."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<Advanced (multi-stream) format>"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid ""
"The first field is an entry type identifier (\\(aqI\\(cqnput, "
"\\(aqO\\(cqutput, \\(aqH\\(cqeader, \\(aqS\\(cqignal). The second field is "
"how much time elapsed since the previous entry, and the rest of the entry is "
"type-specific data."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgid "B<-o>, B<--output-limit> I<size>"
msgstr "B<-o>, B<--output>=I<\\,파일\\/>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Limit the size of the typescript and timing files to I<size> and stop the "
"child process after this size is exceeded. The calculated file size does not "
"include the start and done messages that the B<script> command prepends and "
"appends to the child process output. Due to buffering, the resulting output "
"file might be larger than the specified value."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-q>, B<--quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Be quiet (do not write start and done messages to standard output)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-t>[I<file>], B<--timing>[=I<file>]"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Output timing data to standard error, or to I<file> when given. This option "
"is deprecated in favour of B<--log-timing> where the I<file> argument is not "
"optional."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "도움말을 보여주고 마친다."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "Print version and exit."
msgstr "버전 정보를 보여주고 마친다."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SIGNALS"
msgstr "시그널(SIGNALS)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Upon receiving B<SIGUSR1>, B<script> immediately flushes the output files."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "ENVIRONMENT"
msgstr "환경"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "The following environment variable is utilized by E<.Nm script>:"
msgid "The following environment variable is utilized by B<script>:"
msgstr "다음은 E<.Nm script>에서 사용되는 환경변수들이다:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<SHELL>"
msgstr "B<SHELL>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
msgid ""
"If the variable B<SHELL> exists, the shell forked by B<script> will be that "
"shell. If B<SHELL> is not set, the Bourne shell is assumed. (Most shells set "
"this variable automatically)."
msgstr ""
"이 값이 지정되어 있으면, B<script>에서 사용할 쉘로 이것을 사용하고, 지정되어 "
"있지 않으면, /bin/sh로 가정한다."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
msgid ""
"The script ends when the forked shell exits (a I<control-D> for the Bourne "
"shell (B<sh>(1p)), and I<exit>, I<logout> or I<control-d> (if I<ignoreeof> "
"is not set) for the C-shell, B<csh>(1))."
msgstr ""
"B<script>를 마치기 위해서는 쉘 종료를 호출하면 된다. (대게 exit, logout, ^D)"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Certain interactive commands, such as E<.Xr vi 1>, create garbage in the "
#| "typescript file.  E<.Nm Script> works best with commands that do not "
#| "manipulate the screen, the results are meant to emulate a hardcopy "
#| "terminal."
msgid ""
"Certain interactive commands, such as B<vi>(1), create garbage in the "
"typescript file. B<script> works best with commands that do not manipulate "
"the screen, the results are meant to emulate a hardcopy terminal."
msgstr ""
"E<.Xr vi 1> 와 같은 터미날의 출력을 사용하는 풀그림의 사용도 그대로 기록된"
"다. 이때는 조금 지저분하게 보일 것이다.  E<.Nm script> 명령은 현재 창, 화면"
"에 대한 내용만 기록한다.  즉, 한 터미날의 모든 출력을 그대로 기록한다."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"It is not recommended to run B<script> in non-interactive shells. The inner "
"shell of B<script> is always interactive, and this could lead to unexpected "
"results. If you use B<script> in the shell initialization file, you have to "
"avoid entering an infinite loop. You can use for example the B<.profile> "
"file, which is read by login shells only:"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid ""
"if test -t 0 ; then\n"
"    script\n"
"    exit\n"
"fi\n"
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"You should also avoid use of B<script> in command pipes, as B<script> can "
"read more input than you would expect."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid "The E<.Nm script> command appeared in E<.Bx 3.0>."
msgid "The B<script> command appeared in 3.0BSD."
msgstr "E<.Nm script> 명령은 E<.Bx 3.0> 시스템에서 처음 나타났다."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "BUGS"
msgstr "버그"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "E<.Nm Script> places E<.Sy everything> in the log file, including "
#| "linefeeds and backspaces.  This is not what the naive user expects."
msgid ""
"B<script> places I<everything> in the log file, including linefeeds and "
"backspaces. This is not what the naive user expects."
msgstr ""
"E<.Nm script> 명령은 앞에서 말했듯이 터미날로 출력되는 모든 것을 기록하기 때"
"문에, 각종 특수문자(줄바꿈문자, 백스페이스, 안시..)들을 그대로 기록한다."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<script> is primarily designed for interactive terminal sessions. When "
"stdin is not a terminal (for example: B<echo foo | script>), then the "
"session can hang, because the interactive shell within the script session "
"misses EOF and B<script> has no clue when to close the session. See the "
"B<NOTES> section for more information."
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<csh>(1) (for the I<history> mechanism), B<scriptreplay>(1), "
"B<scriptlive>(1)"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "버그 보고"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "가용성"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The B<script> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "2022년 2월 14일"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Since version 2.35, B<script> supports multiple streams and allows the "
"logging of input and output to separate files or all the one file. This "
"version also supports new timing file which records additional information. "
"The command B<scriptreplay --summary> then provides all the information."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Flush output after each write. This is nice for telecooperation: one person "
"does B<mkfifo foo; script -f foo>, and another can supervise in real-time "
"what is being done using B<cat foo>. Note that flush has an impact on "
"performance; it\\(cqs possible to use B<SIGUSR1> to flush logs on demand."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The first field is an entry type identifier (\\(aqI\\(cqnput, "
"\\(aqO\\(cqutput, \\(aqH\\(cqeader, \\(aqS\\(cqignal). The socond field is "
"how much time elapsed since the previous entry, and the rest of the entry is "
"type-specific data."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "Display version information."
msgid "Display version information and exit."
msgstr "버전 정보를 출력합니다."
