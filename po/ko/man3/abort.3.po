# Korean translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# 한글 Manpage 프로젝트 <http://man.kldp.org>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:18+0200\n"
"PO-Revision-Date: 2005-01-30 08:57+0900\n"
"Last-Translator: 한글 Manpage 프로젝트 <http://man.kldp.org>\n"
"Language-Team: Korean <translation-team-ko@googlegroups.com>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "abort"
msgstr "abort"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "2023년 3월 30일"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "이름"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "abort - cause abnormal program termination"
msgid "abort - cause abnormal process termination"
msgstr "abort - 비정상적인 프로그램 종료 발생"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "라이브러리"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "표준 C 라이브러리 (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "요약"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void abort(void);>\n"
msgid "B<[[noreturn]] void abort(void);>\n"
msgstr "B<void abort(void);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "설명"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<abort>()  function first unblocks the B<SIGABRT> signal, and then "
"raises that signal for the calling process (as though B<raise>(3)  was "
"called).  This results in the abnormal termination of the process unless the "
"B<SIGABRT> signal is caught and the signal handler does not return (see "
"B<longjmp>(3))."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the B<SIGABRT> signal is ignored, or caught by a handler that returns, "
"the B<abort>()  function will still terminate the process.  It does this by "
"restoring the default disposition for B<SIGABRT> and then raising the signal "
"for a second time."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "반환값"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<abort>()  function never returns."
msgstr "B<abort>() 함수는 반환값을 가지지않는다."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "속성"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"이 섹션에서 사용되는 용어에 대한 설명은 B<attributes>(7)을 참조하십시오."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "상호 작용"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "속성"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "번호"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<abort>()"
msgstr "B<abort>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "표준"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "이력"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "SVr4, POSIX.1-2001, POSIX.1-2008, 4.3BSD, C99."
msgid "SVr4, POSIX.1-2001, 4.3BSD, C89."
msgstr "SVr4, POSIX.1-2001, POSIX.1-2008, 4.3BSD, C99."

#.  glibc commit 91e7cf982d0104f0e71770f5ae8e3faf352dea9f
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Up until glibc 2.26, if the B<abort>()  function caused process termination, "
"all open streams were closed and flushed (as with B<fclose>(3)).  However, "
"in some cases this could result in deadlocks and data corruption.  "
"Therefore, starting with glibc 2.27, B<abort>()  terminates the process "
"without flushing streams.  POSIX.1 permits either possible behavior, saying "
"that B<abort>()  \"may include an attempt to effect fclose() on all open "
"streams\"."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "추가 참조"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<gdb>(1), B<sigaction>(2), B<assert>(3), B<exit>(3), B<longjmp>(3), "
"B<raise>(3)"
msgstr ""
"B<gdb>(1), B<sigaction>(2), B<assert>(3), B<exit>(3), B<longjmp>(3), "
"B<raise>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-29"
msgstr "2022년 12월 29일"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "SVr4, POSIX.1-2001, POSIX.1-2008, 4.3BSD, C99."
msgstr "SVr4, POSIX.1-2001, POSIX.1-2008, 4.3BSD, C99."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "주의"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "ABORT"
msgstr "ABORT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-11-26"
msgstr "2017년 11월 26일"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "리눅스 프로그래머 매뉴얼"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<void abort(void);>\n"
msgstr "B<void abort(void);>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<abort>()  first unblocks the B<SIGABRT> signal, and then raises that "
"signal for the calling process (as though B<raise>(3)  was called).  This "
"results in the abnormal termination of the process unless the B<SIGABRT> "
"signal is caught and the signal handler does not return (see B<longjmp>(3))."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "호환"

#. type: Plain text
#: opensuse-leap-15-5
msgid "SVr4, POSIX.1-2001, POSIX.1-2008, 4.3BSD, C89, C99."
msgstr "SVr4, POSIX.1-2001, POSIX.1-2008, 4.3BSD, C89, C99."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
