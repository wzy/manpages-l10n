# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Stinovlas <stinovlas@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-08-27 16:32+0200\n"
"PO-Revision-Date: 2009-09-02 20:06+0100\n"
"Last-Translator: Stinovlas <stinovlas@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "PGREP"
msgstr "PGREP"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-04"
msgstr "4. června 2020"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "User Commands"
msgstr "Příručka uživatele"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "pgrep, pkill - look up or signal processes based on name and other "
#| "attributes"
msgid ""
"pgrep, pkill, pidwait - look up, signal, or wait for processes based on name "
"and other attributes"
msgstr ""
"pgrep, pkill - vyhledává procesy, nebo posílá procesům signály, podle jména "
"a jiných vlastností"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: debian-bullseye
msgid "B<pgrep> [options] pattern"
msgstr "B<pgrep> [volby] vzor"

#. type: Plain text
#: debian-bullseye
msgid "B<pkill> [options] pattern"
msgstr "B<pkill> [volby] vzor"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "B<pkill> [options] pattern"
msgid "B<pidwait> [options] pattern"
msgstr "B<pkill> [volby] vzor"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<pgrep> looks through the currently running processes and lists the process "
"IDs which match the selection criteria to stdout.  All the criteria have to "
"match.  For example,"
msgstr ""
"B<pgrep> prohledává běžící procesy a vypisuje ID procesů, které vyhovují "
"zadaným kritériím. Musí souhlasit všechna kritéria. Např."

#. type: Plain text
#: debian-bullseye
msgid "$ pgrep -u root sshd"
msgstr "$ pgrep -u root sshd"

#. type: Plain text
#: debian-bullseye
msgid ""
"will only list the processes called B<sshd> AND owned by B<root>.  On the "
"other hand,"
msgstr ""
"vypíše jenom procesy které se jmenují B<sshd> a zároveň je jejich vlastníkem "
"root.  Na druhou stranu,"

#. type: Plain text
#: debian-bullseye
msgid "$ pgrep -u root,daemon"
msgstr "$ pgrep -u root,daemon"

#. type: Plain text
#: debian-bullseye
msgid "will list the processes owned by B<root> OR B<daemon>."
msgstr "vypíše procesy vlastněné uživatelem B<root> nebo uživatelem B<daemon>."

#. type: Plain text
#: debian-bullseye
msgid ""
"B<pkill> will send the specified signal (by default B<SIGTERM>)  to each "
"process instead of listing them on stdout."
msgstr ""
"B<pkill> posílá zadaný signál (standartně B<SIGTERM>) každému vyhovujícímu "
"procesu místo vypisování na standartní výstup."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "B<pkill> will send the specified signal (by default B<SIGTERM>)  to each "
#| "process instead of listing them on stdout."
msgid ""
"B<pidwait> will wait for each process instead of listing them on stdout."
msgstr ""
"B<pkill> posílá zadaný signál (standartně B<SIGTERM>) každému vyhovujícímu "
"procesu místo vypisování na standartní výstup."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "OPTIONS"
msgstr "VOLBY"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<->I<signal>"
msgstr "B<->I<signál>"

#. type: TQ
#: debian-bullseye
#, no-wrap
msgid "B<--signal> I<signal>"
msgstr "B<--signal> I<signál>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Defines the signal to send to each matched process.  Either the numeric or "
"the symbolic signal name can be used.  (B<pkill> only.)"
msgstr ""
"Definuje signál, který je poslán všem vyhovujícím procesům.  Může být "
"použita jak číselná, tak symbolická hodnota.  (Dostupné pouze v B<pkill>u.)"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-c>, B<--count>"
msgstr "B<-c>, B<--count>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Suppress normal output; instead print a count of matching processes.  When "
"count does not match anything, e.g. returns zero, the command will return "
"non-zero value. Note that for pkill and pidwait, the count is the number of "
"matching processes, not the processes that were successfully signaled or "
"waited for."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-d>, B<--delimiter> I<delimiter>"
msgstr "B<-d>, B<--delimiter> I<oddělovač>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Sets the string used to delimit each process ID in the output (by default a "
"newline).  (B<pgrep> only.)"
msgstr ""
"Nastavuje použitý oddělovač mezi čísly procesů na výstupu (standartně je "
"použit znak nového řádku). (Dostupné pouze v B<pgrep>u.)"

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-x>, B<--exact>"
msgid "B<-e>, B<--echo>"
msgstr "B<-x>, B<--exact>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "List the process name as well as the process ID.  (B<pgrep> only.)"
msgid "Display name and PID of the process being killed.  (B<pkill> only.)"
msgstr "Kromě ID procesů vypíše i jejich jména. (Dostupné pouze v B<pgrep>u.)"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-f>, B<--full>"
msgstr "B<-f>, B<--full>"

#. type: Plain text
#: debian-bullseye
msgid ""
"The I<pattern> is normally only matched against the process name.  When B<-"
"f> is set, the full command line is used."
msgstr ""
"I<Vzor> je normálně vyhledáván pouze ve jménech procesů.  S přepínačem B<-f> "
"se vyhledává v celém příkazu, kterým byly spuštěny."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-g>, B<--pgroup> I<pgrp>,..."
msgstr "B<-g>, B<--pgroup> I<skupina-procesů>,..."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Only match processes in the process group IDs listed.  Process group 0 is "
#| "translated into B<pgrep>'s or B<pkill>'s own process group."
msgid ""
"Only match processes in the process group IDs listed.  Process group 0 is "
"translated into B<pgrep>'s, B<pkill>'s, or B<pidwait>'s own process group."
msgstr ""
"Vyhoví procesy, které jsou součástí I<skupiny-procesů>. Skupina procesů 0 je "
"pgrep nebo pkill sám."

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "-G I<gid>,..."
msgid "B<-G>, B<--group> I<gid>,..."
msgstr "-G I<id-skupiny>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose real group ID is listed.  Either the numerical or "
"symbolical value may be used."
msgstr ""
"Vyhoví procesy, jejichž GID (identifikační číslo skupiny)  odpovídá vzoru. "
"Může být zadán jak číselně, tak symbolicky."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: debian-bullseye
msgid "Match processes case-insensitively."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-l>, B<--list-name>"
msgstr "B<-l>, B<--list-name>"

#. type: Plain text
#: debian-bullseye
msgid "List the process name as well as the process ID.  (B<pgrep> only.)"
msgstr "Kromě ID procesů vypíše i jejich jména. (Dostupné pouze v B<pgrep>u.)"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-a>, B<--list-full>"
msgstr "B<-a>, B<--list-full>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "List the process name as well as the process ID. (B<pgrep> only.)"
msgid "List the full command line as well as the process ID.  (B<pgrep> only.)"
msgstr "Kromě ID procesů vypíše i jejich jména. (Dostupné pouze v B<pgrep>u.)"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-n>, B<--newest>"
msgstr "B<-n>, B<--newest>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Select only the newest (most recently started) of the matching processes."
msgstr ""
"Vybere pouze nejnovější (naposledy spuštěný) proces, který vyhovuje "
"podmínkám."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-o>, B<--oldest>"
msgstr "B<-o>, B<--oldest>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Select only the oldest (least recently started) of the matching processes."
msgstr ""
"Vybere pouze nejstarší (nejdříve spuštěný) proces, který vyhovuje podmínkám."

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-o>, B<--oldest>"
msgid "B<-O>, B<--older> I<secs>"
msgstr "B<-o>, B<--oldest>"

#. type: Plain text
#: debian-bullseye
msgid "Select processes older than secs."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-P>, B<--parent> I<ppid>,..."
msgstr "B<-P>, B<--parent> I<id-rodičovského-procesu>,..."

#. type: Plain text
#: debian-bullseye
msgid "Only match processes whose parent process ID is listed."
msgstr "Vyhoví pouze procesy, jejihž rodič je I<id-rodičovského procesu>."

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-V>, B<--version>"
msgid "B<-s>, B<--session> I<sid>,..."
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Only match processes whose process session ID is listed.  Session ID 0 is "
#| "translated into B<pgrep>'s or B<pkill>'s own session ID."
msgid ""
"Only match processes whose process session ID is listed.  Session ID 0 is "
"translated into B<pgrep>'s, B<pkill>'s, or B<pidwait>'s own session ID."
msgstr ""
"Vyhoví jenom procesy, které jsou součástí dané relace. Ralace 0 je "
"interpretována jako relace samotného B<pgrep>u nebo B<pkill>u."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-t>, B<--terminal> I<term>,..."
msgstr "B<-t>, B<--terminal> I<terminál>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose controlling terminal is listed.  The terminal "
"name should be specified without the \"/dev/\" prefix."
msgstr ""
"Vyhoví pouze procesy, které jsou spuštěny na daném terminálu.  Jméno "
"terminálu může být specifikováno s prefixem \"/dev/\"."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-u>, B<--euid> I<euid>,..."
msgstr "B<-u>, B<--euid> I<euid>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose effective user ID is listed.  Either the "
"numerical or symbolical value may be used."
msgstr ""
"Vyhoví pouze procesy s odpovídajícím efektivním uživatelským ID.  Může být "
"použita číselná i symbolická hodnota."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-U>, B<--uid> I<uid>,..."
msgstr "B<-U>, B<--uid> I<uid>,..."

#. type: Plain text
#: debian-bullseye
msgid ""
"Only match processes whose real user ID is listed.  Either the numerical or "
"symbolical value may be used."
msgstr ""
"Vyhoví pouze procesy s odpovídajícím uživatelským ID.  Může být použita "
"číselná i symbolická hodnota."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-v>, B<--inverse>"
msgstr "B<-v>, B<--inverse>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Negates the matching.  This option is usually used in B<pgrep>'s or "
"B<pidwait>'s context.  In B<pkill>'s context the short option is disabled to "
"avoid accidental usage of the option."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-w>, B<--lightweight>"
msgstr "B<-w>, B<--lightweight>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Shows all thread ids instead of pids in B<pgrep>'s or B<pidwait>'s context.  "
"In B<pkill>'s context this option is disabled."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-x>, B<--exact>"
msgstr "B<-x>, B<--exact>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Only match processes whose names (or command line if -f is specified)  "
#| "B<exactly> match the I<pattern>."
msgid ""
"Only match processes whose names (or command lines if B<-f> is specified)  "
"B<exactly> match the I<pattern>."
msgstr ""
"Vyhoví pouze procesy, jejichž jméno (nebo, pokud je použito -f, příkaz) "
"B<přesně> odpovídá I<vzoru>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-F>, B<--pidfile> I<file>"
msgstr "B<-F>, B<--pidfile> I<soubor>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Read I<PID>s from I<file>.  This option is more useful for "
"B<pkill>orB<pidwait> than B<pgrep>."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-L>, B<--logpidfile>"
msgstr "B<-L>, B<--logpidfile>"

#. type: Plain text
#: debian-bullseye
msgid "Fail if pidfile (see B<-F>) not locked."
msgstr ""

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-P>, B<--parent> I<ppid>,..."
msgid "B<-r>, B<--runstates> I<D,R,S,Z,>..."
msgstr "B<-P>, B<--parent> I<id-rodičovského-procesu>,..."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "Only match processes whose parent process ID is listed."
msgid "Match only processes which match the process state."
msgstr "Vyhoví pouze procesy, jejihž rodič je I<id-rodičovského procesu>."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--ns >I<pid>"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"Match processes that belong to the same namespaces. Required to run as root "
"to match processes from other users. See B<--nslist> for how to limit which "
"namespaces to match."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<--nslist >I<name>B<,...>"
msgstr "B<--nslist >I<jméno>B<,...>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Match only the provided namespaces. Available namespaces: ipc, mnt, net, "
"pid, user,uts."
msgstr ""

#. type: TP
#: debian-bullseye
#, fuzzy, no-wrap
#| msgid "B<-F>, B<--pidfile> I<file>"
msgid "B<-q>, B<--queue >I<value>"
msgstr "B<-F>, B<--pidfile> I<soubor>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Use B<sigqueue(3)> rather than B<kill(2)> and the value argument is used to "
"specify an integer to be sent with the signal. If the receiving process has "
"installed a handler for this signal using the SA_SIGINFO flag to "
"B<sigaction(2)> , then it can obtain this data via the si_value field of the "
"siginfo_t structure."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "output version information and exit"
msgid "Display version information and exit."
msgstr "Vypíše číslo verze na standardní výstup a bezchybně skončí."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "display this help and exit"
msgid "Display help and exit."
msgstr "Vypíše návod k použití na standardní výstup a bezchybně skončí."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "OPERANDS"
msgstr "OPERANDY"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "I<pattern>"
msgstr "I<vzor>"

#. type: Plain text
#: debian-bullseye
msgid ""
"Specifies an Extended Regular Expression for matching against the process "
"names or command lines."
msgstr ""
"Specifikuje rozšířený regulární výraz, který je porovnáván se jmény procesů, "
"nebo jejich příkazy."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "EXAMPLES"
msgstr "PŘÍKLADY"

#. type: Plain text
#: debian-bullseye
msgid "Example 1: Find the process ID of the B<named> daemon:"
msgstr "Příklad 1: Vyhledá procesy jménem B<jméno> vlastněné uživatelem root:"

#. type: Plain text
#: debian-bullseye
msgid "$ pgrep -u root named"
msgstr "$ pgrep -u root named"

#. type: Plain text
#: debian-bullseye
msgid "Example 2: Make B<syslog> reread its configuration file:"
msgstr "Příklad 2: Přinutí B<syslog> znovu načíst svůj konfigurační soubor:"

#. type: Plain text
#: debian-bullseye
msgid "$ pkill -HUP syslogd"
msgstr "$ pkill -HUP syslogd"

#. type: Plain text
#: debian-bullseye
msgid "Example 3: Give detailed information on all B<xterm> processes:"
msgstr "Příklad 3: Vypíše detailní informace o všech procesech B<xterm>:"

#. type: Plain text
#: debian-bullseye
msgid "$ ps -fp $(pgrep -d, -x xterm)"
msgstr "$ ps -fp $(pgrep -d, -x xterm)"

#. type: Plain text
#: debian-bullseye
msgid "Example 4: Make all B<chrome> processes run nicer:"
msgstr "Příklad 4: Všechny procesy B<chrome> přinutí běžet lépe:"

#. type: Plain text
#: debian-bullseye
msgid "$ renice +4 $(pgrep chrome)"
msgstr "$ renice +4 $(pgrep chrome)"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "EXIT STATUS"
msgstr "NÁVRATOVÁ HODNOTA"

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: debian-bullseye
msgid ""
"One or more processes matched the criteria. For pkill and pidwait, one or "
"more processes must also have been successfully signalled or waited for."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: debian-bullseye
msgid "No processes matched or none of them could be signalled."
msgstr ""

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: debian-bullseye
msgid "Syntax error in the command line."
msgstr "Chyba v syntaxi na příkazovém řádku."

#. type: TP
#: debian-bullseye
#, no-wrap
msgid "3"
msgstr "3"

#. type: Plain text
#: debian-bullseye
msgid "Fatal error: out of memory etc."
msgstr "Závažná chyba: nedostatek paměti apod."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "NOTES"
msgstr "POZNÁMKY"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "The process name used for matching is limited to the 15 characters "
#| "present in the output of /proc/I<pid>/stat.  Use the -f option to match "
#| "against the complete command line, /proc/I<pid>/cmdline."
msgid ""
"The process name used for matching is limited to the 15 characters present "
"in the output of /proc/I<pid>/stat.  Use the B<-f> option to match against "
"the complete command line, /proc/I<pid>/cmdline."
msgstr ""
"Jména procesů pro porovnání jsou maximálně 15 znaků dlouhá, protože se berou "
"ze suboru /proc/I<pid>/stat. Pro delší jména použijte parametr -f, který "
"bere informace ze souboru /proc/I<pid>/cmdline."

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "The running B<pgrep> or B<pkill> process will never report itself as a "
#| "match."
msgid ""
"The running B<pgrep>, B<pkill>, or B<pidwait> process will never report "
"itself as a match."
msgstr "Běžící B<pgrep> nebo B<pkill> nikdy nevrátí sám sebe jako výsledek."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "BUGS"
msgstr "CHYBY"

#. type: Plain text
#: debian-bullseye
msgid ""
"The options B<-n> and B<-o> and B<-v> can not be combined.  Let me know if "
"you need to do this."
msgstr ""
"Přepínače B<-n>, B<-o> a B<-v> nemohou být kombinovány. Pokud to budete "
"potřebovat, dejte mi vědět."

#. type: Plain text
#: debian-bullseye
msgid "Defunct processes are reported."
msgstr "Vypisuje i zaniklé procesy (zombie)."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "B<ps>(1), B<regex>(7), B<signal>(7), B<killall>(1), B<skill>(1), "
#| "B<kill>(1), B<kill>(2)"
msgid ""
"B<ps>(1), B<regex>(7), B<signal>(7), B<sigqueue>(3), B<killall>(1), "
"B<skill>(1), B<kill>(1), B<kill>(2)"
msgstr ""
"B<ps>(1), B<regex>(7), B<signal>(7), B<killall>(1), B<skill>(1), B<kill>(1), "
"B<kill>(2)"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bullseye
msgid "E<.UR kjetilho@ifi.uio.no> Kjetil Torgrim Homme E<.UE>"
msgstr "E<.UR kjetilho@ifi.uio.no> Kjetil Torgrim Homme E<.UE>"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HLÁŠENÍ CHYB"

#. type: Plain text
#: debian-bullseye
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Chyby týkající se programu prosím zasílejte na E<.UR procps@freelists.org> "
"E<.UE>"
