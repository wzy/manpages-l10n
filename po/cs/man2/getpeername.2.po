# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pavel Heimlich <tropikhajma@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:29+0200\n"
"PO-Revision-Date: 2009-02-09 20:06+0100\n"
"Last-Translator: Pavel Heimlich <tropikhajma@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getpeername"
msgstr "getpeername"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-04-03"
msgstr "3. dubna 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "getpeername - get name of connected peer socket"
msgstr "getpeername - vrať adresu vzdáleného konce soketu"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "KNIHOVNA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardní knihovna C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"
msgstr ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<                socklen_t *restrict >I<addrlen>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "POPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<getpeername>()  returns the name of the peer connected to socket I<s>.  "
#| "The I<namelen> argument should be initialized to indicate the amount of "
#| "space pointed to by I<name>.  On return it contains the actual size of "
#| "the name returned (in bytes).  The name is truncated if the buffer "
#| "provided is too small."
msgid ""
"B<getpeername>()  returns the address of the peer connected to the socket "
"I<sockfd>, in the buffer pointed to by I<addr>.  The I<addrlen> argument "
"should be initialized to indicate the amount of space pointed to by "
"I<addr>.  On return it contains the actual size of the name returned (in "
"bytes).  The name is truncated if the buffer provided is too small."
msgstr ""
"B<Getpeername> vrací jméno (adresu) počítače, který je na druhém konci "
"soketu.  I<s>.  Parametr I<namelen> by měl být inicializován tak, aby "
"indikoval velikost paměťového prostoru, na který ukazuje argument I<name>.  "
"Po návratu bude obsahovat aktuální velikost vrácené adresy (v bajtech).  "
"Jméno je příslušně zkráceno, je-li velikost příliš malá."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The returned address is truncated if the buffer provided is too small; in "
"this case, I<addrlen> will return a value greater than was supplied to the "
"call."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "NÁVRATOVÉ HODNOTY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"V případě úspěchu je vrácena 0, jinak -1 a proměnná I<errno> je příslušně "
"nastavena."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "CHYBOVÉ STAVY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The argument I<sockfd> is not a valid file descriptor."
msgstr "Argument I<sockfd> není platným deskriptorem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<addr> argument points to memory not in a valid part of the process "
"address space."
msgstr ""
"Argument I<addr> ukazuje na paměť v neplatné části adresového prostoru "
"procesu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<addrlen> is invalid (e.g., is negative)."
msgstr "I<addrlen> je neplatné (například záporné číslo)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOBUFS>"
msgstr "B<ENOBUFS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Insufficient resources were available in the system to perform the operation."
msgstr "Nedostatek systémových zdrojů k provedení operace."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTCONN>"
msgstr "B<ENOTCONN>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The socket is not connected."
msgstr "Soket není spojen."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr "B<ENOTSOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The file descriptor I<sockfd> does not refer to a socket."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getpeername>()  first "
#| "appeared in 4.2BSD)."
msgid "POSIX.1-2001, SVr4, 4.4BSD (first appeared in 4.2BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getpeername>() se poprvé "
"objevilo ve 4.2BSD)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "POZNÁMKY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For stream sockets, once a B<connect>(2)  has been performed, either socket "
"can call B<getpeername>()  to obtain the address of the peer socket.  On the "
"other hand, datagram sockets are connectionless.  Calling B<connect>(2)  on "
"a datagram socket merely sets the peer address for outgoing datagrams sent "
"with B<write>(2)  or B<recv>(2).  The caller of B<connect>(2)  can use "
"B<getpeername>()  to obtain the peer address that it earlier set for the "
"socket.  However, the peer socket is unaware of this information, and "
"calling B<getpeername>()  on the peer socket will return no useful "
"information (unless a B<connect>(2)  call was also executed on the peer).  "
"Note also that the receiver of a datagram can obtain the address of the "
"sender when using B<recvfrom>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<accept>(2), B<bind>(2), B<getsockname>(2), B<ip>(7), B<socket>(7), "
"B<unix>(7)"
msgstr ""
"B<accept>(2), B<bind>(2), B<getsockname>(2), B<ip>(7), B<socket>(7), "
"B<unix>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-10-30"
msgstr "30. října 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getpeername>()  first appeared "
"in 4.2BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<getpeername>() se poprvé "
"objevilo ve 4.2BSD)."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "For background on the I<socklen_t> type, see B<accept>(2)."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GETPEERNAME"
msgstr "GETPEERNAME"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. září 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux - příručka programátora"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>sys/socket.hE<gt>>"
msgstr "B<#include E<lt>sys/socket.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t "
"*>I<addrlen>B<);>"
msgstr ""
"B<int getpeername(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t "
"*>I<addrlen>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Je-li volání úspěšné, vrací se 0, jinak -1 a je nastavena proměnná I<errno>."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "SPLŇUJE STANDARDY"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "TIRÁŽ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Tato stránka je součástí projektu Linux I<man-pages> v4.16. Popis projektu a "
"informace o hlášení chyb najdete na \\%https://www.kernel.org/doc/man\\-"
"pages/."
