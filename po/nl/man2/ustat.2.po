# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-06-27 20:00+0200\n"
"PO-Revision-Date: 2023-05-21 20:58+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "ustat"
msgstr "ustat"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 maart 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ustat - get filesystem statistics"
msgstr "ustat - geef bestand systeem statistieken"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>unistd.hE<gt>>    /* libc[45] */\n"
"B<#include E<lt>ustat.hE<gt>>     /* glibc2 */\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>unistd.hE<gt>>    /* libc[45] */\n"
"B<#include E<lt>ustat.hE<gt>>     /* glibc2 */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<[[deprecated]] int ustat(dev_t >I<dev>B<, struct ustat *>I<ubuf>B<);>\n"
msgstr "B<[[afgekeurd]]  int ustat(dev_t >I<appar>B<, struct ustat *>I<ubuf>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ustat>()  returns information about a mounted filesystem.  I<dev> is a "
"device number identifying a device containing a mounted filesystem.  I<ubuf> "
"is a pointer to a I<ustat> structure that contains the following members:"
msgstr ""
"B<ustat>() geeft informatie over een gemount bestand systeem. I<appar> is "
"een apparaat nummer, een apparaat identificerend dat het gemounte "
"bestandsysteem bevat. I<ubuf> is een pointer naar een I<ustat> structuur die "
"de volgende leden bevat:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"daddr_t f_tfree;      /* Total free blocks */\n"
"ino_t   f_tinode;     /* Number of free inodes */\n"
"char    f_fname[6];   /* Filsys name */\n"
"char    f_fpack[6];   /* Filsys pack name */\n"
msgstr ""
"daddr_t f_tfree;      /* Totale vrije blokken */\n"
"ino_t   f_tinode;     /* Aantal vrije inodes */\n"
"char    f_fname[6];   /* Bestandsyst naam */\n"
"char    f_fpack[6];   /* Bestandsyst \"pack\" naam */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The last two fields, I<f_fname> and I<f_fpack>, are not implemented and will "
"always be filled with null bytes (\\[aq]\\e0\\[aq])."
msgstr ""
"De laatste twee velden, I<f_fname> en I<f_fpack> zijn niet verwezijnlijkt en "
"zullen altijd gevuld worden met null karakters (\\[aq]\\e0\\[aq])."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned and the I<ustat> structure pointed to by "
"I<ubuf> will be filled in.  On error, -1 is returned, and I<errno> is set to "
"indicate the error."
msgstr ""
"Bij succes wordt nul teruggegeven en de I<ustat> structuur aangewezen  door "
"I<ubuf> zal gevuld worden. Bij falen wordt -1 teruggegeven en wordtI<errno> "
"overeenkomstig gezet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<ubuf> points outside of your accessible address space."
msgstr "I<ubuf> wijst buiten uw toegankelijke adres ruimte."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<dev> does not refer to a device containing a mounted filesystem."
msgstr ""
"I<appar> wijst niet naar een apparaat dat een mount bestandsysteem bevat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOSYS>"
msgstr "B<ENOSYS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The mounted filesystem referenced by I<dev> does not support this operation, "
"or any version of Linux before Linux 1.3.16."
msgstr ""
"Het gekoppelde bestandssysteem aangehaald door I<appar> ondersteund de "
"handeling niet, of enige  versie voor Linux  1.3.16."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr "Geen."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#.  SVr4 documents additional error conditions ENOLINK, ECOMM, and EINTR
#.  but has no ENOSYS condition.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "SVr4.  Removed in glibc 2.28."
msgstr "SVr4.  Verwijderd in glibc 2.28."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ustat>()  is deprecated and has been provided only for compatibility.  All "
"new programs should use B<statfs>(2)  instead."
msgstr ""
"B<ustat>() wordt ontraden en is alleen voorzien vanwege overdraagbaarheid. "
"Alle nieuwe programma's moeten in plaats daarvan B<statfs>(2) gebruiken."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "HP-UX notes"
msgstr "HP-UX notities"

#.  Some software tries to use this in order to test whether the
#.  underlying filesystem is NFS.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The HP-UX version of the I<ustat> structure has an additional field, "
"I<f_blksize>, that is unknown elsewhere.  HP-UX warns: For some filesystems, "
"the number of free inodes does not change.  Such filesystems will return -1 "
"in the field I<f_tinode>.  For some filesystems, inodes are dynamically "
"allocated.  Such filesystems will return the current number of free inodes."
msgstr ""
"De HP-UX versie van I<ustat> structuur heeft een additioneel veld, "
"I<f_blksize>, dat elders onbekend is. HP-UX waarschuwt: Voor enkele "
"bestandssystemen, verandert het aantal vrije inodes not. Zulke "
"bestandssystemen geven -1 terug in het veld I<f_tinode>. Voor enkele "
"bestandssystemen worden inodes dynamisch toegekend. Zulke bestandssystemen "
"geven het huidige aantal vrije inodes terug."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<stat>(2), B<statfs>(2)"
msgstr "B<stat>(2), B<statfs>(2)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Since glibc 2.28, glibc no longer provides a wrapper for this system call."
msgstr ""
"Vanaf glibc 2.28, voorziet glibc niet meer in een omwikkel functie voor deze "
"systeem aanroep."

#.  SVr4 documents additional error conditions ENOLINK, ECOMM, and EINTR
#.  but has no ENOSYS condition.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "SVr4."
msgstr "SVr4."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "USTAT"
msgstr "USTAT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 september 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<int ustat(dev_t >I<dev>B<, struct ustat *>I<ubuf>B<);>\n"
msgstr "B<int ustat(dev_t >I<appar>B<, struct ustat *>I<ubuf>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The last two fields, I<f_fname> and I<f_fpack>, are not implemented and will "
"always be filled with null bytes (\\(aq\\e0\\(aq)."
msgstr ""
"De laatste twee velden, I<f_fname> en I<f_fpack> zijn niet verwezijnlijkt en "
"zullen altijd gevuld worden met null karakters (\\(aq\\e0\\(aq)."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, zero is returned and the I<ustat> structure pointed to by "
"I<ubuf> will be filled in.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Bij success wordt nul teruggegeven en de I<ustat> structuur verwezen naar "
"door I<ubuf> zal ingevuld worden. Bij falen wordt -1 teruggegeven en "
"I<errno> wordt naar behoren gezet."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The mounted filesystem referenced by I<dev> does not support this operation, "
"or any version of Linux before 1.3.16."
msgstr ""
"Het gekoppelde bestandssysteem aangehaald door I<appar> ondersteund de "
"handeling niet, of een versie van Linux voor 1.3.16."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."
