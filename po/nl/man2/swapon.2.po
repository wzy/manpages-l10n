# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
#
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021-2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2023-06-27 19:53+0200\n"
"PO-Revision-Date: 2023-05-21 21:44+0200\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl_NL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "swapon"
msgstr "swapon"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 maart 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "swapon, swapoff - start/stop swapping to file/device"
msgstr "swapon, swapoff - start/stop swappen naar bestand/apparaat"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard C bibliotheek  (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/swap.hE<gt>>\n"
msgstr "B<#include E<lt>sys/swap.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int swapon(const char *>I<path>B<, int >I<swapflags>B<);>\n"
"B<int swapoff(const char *>I<path>B<);>\n"
msgstr ""
"B<int swapon(const char *>I<pad>B<, int >I<swapvlaggen>B<);> \n"
"B<int swapoff(const char *>I<pad>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<swapon>()  sets the swap area to the file or block device specified by "
"I<path>.  B<swapoff>()  stops swapping to the file or block device specified "
"by I<path>."
msgstr ""
"B<swapon>() zet het swap gebied naar het bestand of blok apparaat opgegeven "
"met I<pad>. B<swapoff>() stopt swappen naar het bestand of blok apparaat "
"opgegeven met I<pad>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the B<SWAP_FLAG_PREFER> flag is specified in the B<swapon>()  "
"I<swapflags> argument, the new swap area will have a higher priority than "
"default.  The priority is encoded within I<swapflags> as:"
msgstr ""
"Als de B<SWAP_FLAG_PREFER> vlag werd opgegeven in het B<swapon>() "
"I<swapvlaggen> argument dan heeft het nieuwe swap gebied een hogere "
"prioriteit dan standaard.  De prioriteit is gecodeerd in I<swapvlaggen> als:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<(prio E<lt>E<lt> SWAP_FLAG_PRIO_SHIFT) & SWAP_FLAG_PRIO_MASK>\n"
msgstr "I<(prio E<lt>E<lt> SWAP_FLAG_PRIO_SHIFT) & SWAP_FLAG_PRIO_MASK>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the B<SWAP_FLAG_DISCARD> flag is specified in the B<swapon>()  "
"I<swapflags> argument, freed swap pages will be discarded before they are "
"reused, if the swap device supports the discard or trim operation.  (This "
"may improve performance on some Solid State Devices, but often it does "
"not.)  See also NOTES."
msgstr ""
"Als de B<SWAP_FLAG_DISCARD> vlag werd opgegeven in het B<swapon>() "
"I<swapvlaggen> argument, dan zullen vrijgemaakte swap pagina´s worden "
"weggegooid voordat ze hergebruikt worden, mits het swap apparaat weggooien "
"en bijsnijden ondersteund. (Dit kan prestaties verbeteren on sommige Solid "
"State Devices, maar meestal niet).  Zie ook NOTITIES."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions may be used only by a privileged process (one having the "
"B<CAP_SYS_ADMIN> capability)."
msgstr ""
"Deze functies mogen alleen door een geprivilegieerd process (een dat de "
"B<CAPS_SYS_ADMIN> capaciteit heeft)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Priority"
msgstr "Prioriteit"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Each swap area has a priority, either high or low.  The default priority is "
"low.  Within the low-priority areas, newer areas are even lower priority "
"than older areas."
msgstr ""
"Elk swap gebied heeft een prioriteit, of hoog of laag. De standaard "
"prioriteit is laag. Binnen de lage prioriteit gebieden hebben nieuwere "
"gebieden nog lagere prioriteit dan oudere."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"All priorities set with I<swapflags> are high-priority, higher than "
"default.  They may have any nonnegative value chosen by the caller.  Higher "
"numbers mean higher priority."
msgstr ""
"Alle prioriteiten gezet met I<swapvlaggen> zijn hoge-prioriteit, hoger dan "
"standaard. Ze mogen elke niet-negatieve waarde gekozen door de aanroeper. "
"Hogere nummers betekenen hogere prioriteit."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Swap pages are allocated from areas in priority order, highest priority "
"first.  For areas with different priorities, a higher-priority area is "
"exhausted before using a lower-priority area.  If two or more areas have the "
"same priority, and it is the highest priority available, pages are allocated "
"on a round-robin basis between them."
msgstr ""
"Swap pagina's worden toegewezen van gebieden in prioriteit volgorde, hoge "
"prioriteit eerst voor gebieden met verschillende prioriteiten: een hoger-"
"prioriteit gebied wordt verbruikt voordat een lager-prioriteit gebied wordt "
"gebruikt. Als twee of meer gebieden dezelfde prioriteit hebben, en als het "
"de hoogste beschikbare prioriteit is, dan worden pagina's toegewezen op een "
"rondedans basis tussen hen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"As of Linux 1.3.6, the kernel usually follows these rules, but there are "
"exceptions."
msgstr ""
"Vanaf Linux 1.3.6, volgt de kernel gewoonlijk deze regels, maar er zijn "
"uitzonderingen."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"Bij succes wordt nul teruggegeven. Bij falen wordt -1 teruggegeven en wordt "
"I<errno> overeenkomstig gezet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBUSY>"
msgstr "B<EBUSY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(for B<swapon>())  The specified I<path> is already being used as a swap "
"area."
msgstr ""
"(voor B<swapon>())  Het opgegeven I<pad> werd al gebruikt als swap gebied."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The file I<path> exists, but refers neither to a regular file nor to a block "
"device;"
msgstr ""
"Wordt teruggegeven als I<pad> bestaat, maar nóg een normale pad is, nóg een "
"blok apparaat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(B<swapon>())  The indicated path does not contain a valid swap signature or "
"resides on an in-memory filesystem such as B<tmpfs>(5)."
msgstr ""
"(B<swapon>())  Het aangegeven pad bevat geen geldige swap signature of "
"bevindt zich op een in-geheugen bestandssysteem zoals B<tmpfs>(5)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL> (since Linux 3.4)"
msgstr "B<EINVAL> (sinds Linux 3.4)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "(B<swapon>())  An invalid flag value was specified in I<swapflags>."
msgstr "(B<swapon>()) Een ongeldige vlag werd in I<vlaggen> opgegeven."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "(B<swapoff>())  I<path> is not currently a swap area."
msgstr "(B<swapoff>())  I<pad> is momenteel geen swap gebied."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr "De grens aan het aantal open bestanden van het systeem is bereikt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOENT>"
msgstr "B<ENOENT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The file I<path> does not exist."
msgstr "Wordt teruggegeven als I<pad> niet bestaat."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The system has insufficient memory to start swapping."
msgstr "Wordt teruggegeven als er onvoldoende geheugen is om swap te starten."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The caller does not have the B<CAP_SYS_ADMIN> capability.  Alternatively, "
"the maximum number of swap files are already in use; see NOTES below."
msgstr ""
"De aanroeper heeft niet de B<CAP_SYS_ADMIN> capaciteit. Mogelijk is ook het "
"maximum aantal swap bestanden in gebruik; zie NOTITIES hieronder."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "VOLDOET AAN"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHIEDENIS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "The I<swapflags> argument was introduced in Linux 1.3.2."
msgstr "Het I<swapvlaggen> argument werd geïntroduceerd in Linux 1.3.2."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The partition or path must be prepared with B<mkswap>(8)."
msgstr ""
"Het schijf-gedeelte (partitie) moet voorbereid worden met B<mkswap>(8)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"There is an upper limit on the number of swap files that may be used, "
"defined by the kernel constant B<MAX_SWAPFILES>.  Before Linux 2.4.10, "
"B<MAX_SWAPFILES> has the value 8; since Linux 2.4.10, it has the value 32.  "
"Since Linux 2.6.18, the limit is decreased by 2 (thus: 30)  if the kernel is "
"built with the B<CONFIG_MIGRATION> option (which reserves two swap table "
"entries for the page migration features of B<mbind>(2)  and "
"B<migrate_pages>(2)).  Since Linux 2.6.32, the limit is further decreased by "
"1 if the kernel is built with the B<CONFIG_MEMORY_FAILURE> option.  Since "
"Linux 5.14, the limit is further decreased by 4 if the kernel is built with "
"the B<CONFIG_DEVICE_PRIVATE> option."
msgstr ""
"Er is een bovengrens aan het aantal swap bestanden dat gebruikt kan worden, "
"gedefinieerd door de kernel constante B<MAX_SWAPFILES>. Vóór kernel 2.4.10, "
"had  B<MAX_SWAPFILES> de waarde 8, vanaf kernel 2.4.10 heeft die de waarde "
"32. Vanaf Linux 2.6.18 is de limiet verlaagd met 2 (dus: 30) mits de kernel "
"werd gebouwd met de  B<CONFIG_MIGRATION> optie (die reserveert twee swap "
"tabel ingangen voor de pagina migratie eigenschappen van B<mbind>(2)  en  "
"B<migrate_pages>(2)).  Vanaf Linux 2.6.32, werd de limiet verder verlaagd "
"met 1 als de kernel gebouwd werd met de B<CONFIG_MEMORY_FAILURE> optie. "
"Vanaf Linux 5.14 werd de limiet verder vermindert met 4 indien de kernel "
"werd gebouwd met de B<CONFIG_DEVICE_PRIVATE> optie."

#.  To be precise: 2.6.35.5
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Discard of swap pages was introduced in Linux 2.6.29, then made conditional "
"on the B<SWAP_FLAG_DISCARD> flag in Linux 2.6.36, which still discards the "
"entire swap area when B<swapon>()  is called, even if that flag bit is not "
"set."
msgstr ""
"Weggooien van swap pagina´s werd geïntroduceerd in Linux 2.6.29, vervolgens "
"werd het conditioneel gemaakt afhankelijk van de B<SWAP_FLAG_DISCARD> vlag "
"in Linux 2.6.36, die nog steeds het gehele swap gebied weggegooid wanneer "
"B<swapon>() wordt aangeroepen, zelfs als dat bit in de vlag niet werd gezet."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<mkswap>(8), B<swapoff>(8), B<swapon>(8)"
msgstr "B<mkswap>(8), B<swapoff>(8), B<swapon>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 december 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pagina's 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"These functions are Linux-specific and should not be used in programs "
"intended to be portable.  The second I<swapflags> argument was introduced in "
"Linux 1.3.2."
msgstr ""
"Deze functies zijn Linux-specifiek en zouden niet gebruikt moeten worden in "
"programma's die overdraagbaar moeten zijn. Het tweede I<swapvlaggen> "
"argument werd ingevoerd in Linux 1.3.2."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "SWAPON"
msgstr "SWAPON"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 september 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>sys/swap.hE<gt>>"
msgstr "B<#include E<lt>sys/swap.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int swapon(const char *>I<path>B<, int >I<swapflags>B<);>"
msgstr "B<int swapon(const char *>I<pad>B<, int >I<swapvlaggen>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int swapoff(const char *>I<path>B<);>"
msgstr "B<int swapoff(const char *>I<pad>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"Bij succes wordt nul teruggegeven. Bij falen wordt -1 teruggegeven en wordt "
"I<errno> overeenkomstig gezet."

#. type: Plain text
#: opensuse-leap-15-5
msgid "(B<swapon>())  An invalid flag value was specified in I<flags>."
msgstr "(B<swapon>()) Een ongeldig vlag werd in I<vlaggen> opgegeven."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"There is an upper limit on the number of swap files that may be used, "
"defined by the kernel constant B<MAX_SWAPFILES>.  Before kernel 2.4.10, "
"B<MAX_SWAPFILES> has the value 8; since kernel 2.4.10, it has the value 32.  "
"Since kernel 2.6.18, the limit is decreased by 2 (thus: 30)  if the kernel "
"is built with the B<CONFIG_MIGRATION> option (which reserves two swap table "
"entries for the page migration features of B<mbind>(2)  and "
"B<migrate_pages>(2)).  Since kernel 2.6.32, the limit is further decreased "
"by 1 if the kernel is built with the B<CONFIG_MEMORY_FAILURE> option."
msgstr ""
"Er is een bovengrens aan het aantal swap bestanden dat gebruikt kan worden, "
"gedefinieerd door de kernel constante B<MAX_SWAPFILES>. Vóór kernel 2.4.10, "
"had  B<MAX_SWAPFILES> de waarde 8, vanaf kernel 2.4.10 heeft die de waarde "
"32. Vanaf kernel 2.6.18 is de limiet verlaagd met 2 (dus: 30) mits de kernel "
"werd gebouwd met de  B<CONFIG_MIGRATION> optie (die reserveert twee swap "
"tabel ingangen voor de pagina migratie eigenschappen van B<mbind>(2)  en  "
"B<migrate_pages>(2)).  Vanaf kernel 2.6.32, werd de limiet verder verlaagd "
"met 1 als de kernel gebouwd werd met de B<CONFIG_MEMORY_FAILURE> optie."

#.  To be precise: 2.6.35.5
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Discard of swap pages was introduced in kernel 2.6.29, then made conditional "
"on the B<SWAP_FLAG_DISCARD> flag in kernel 2.6.36, which still discards the "
"entire swap area when B<swapon>()  is called, even if that flag bit is not "
"set."
msgstr ""
"Weggooien van swap pagina´s werd geïntroduceerd in kernel 2.6.29, vervolgens "
"werd het conditioneel gemaakt afhankelijk van de B<SWAP_FLAG_DISCARD> vlag "
"in kernel 2.6.36, die nog steeds het gehele swap gebied weggegooid wanneer "
"B<swapon>() wordt aangeroepen, zelfs als dat bit in de vlag niet werd gezet."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."
