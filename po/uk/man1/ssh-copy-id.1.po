# Ukrainian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Yuri Chornoivan <yurchor@ukr.net>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:52+0200\n"
"PO-Revision-Date: 2022-03-29 15:20+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <trans-uk@lists.fedoraproject.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: Dd
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "$Mdocdate: June 17 2010 $"
msgstr "$Mdocdate: 17 червня 2010 року $"

#. type: Dt
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SSH-COPY-ID 1"
msgstr "SSH-COPY-ID 1"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "НАЗВА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm ssh-copy-id>"
msgstr "E<.Nm ssh-copy-id>"

#. type: Nd
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "use locally available keys to authorise logins on a remote machine"
msgstr "використання доступних локально ключів для уповноважених входів до віддаленої системи"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "КОРОТКИЙ ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"E<.Nm> E<.Op Fl f> E<.Op Fl n> E<.Op Fl s> E<.Op Fl i Op Ar identity_file> "
"E<.Op Fl p Ar port> E<.Op Fl o Ar ssh_option> E<.Op Ar user Ns @ Ns> E<.Ar "
"hostname> E<.Nm> E<.Fl h | Fl ?>"
msgstr ""
"E<.Nm> E<.Op Fl f> E<.Op Fl n> E<.Op Fl s> E<.Op Fl i Op Ar файл_профілю> E<."
"Op Fl p Ar порт> E<.Op Fl o Ar параметр_ssh> E<.Op Ar користувач Ns @ Ns> E<."
"Ar назва_вузла> E<.Nm> E<.Fl h | Fl ?>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"E<.Nm> is a script that uses E<.Xr ssh 1> to log into a remote machine "
"(presumably using a login password, so password authentication should be "
"enabled, unless you've done some clever use of multiple identities).  It "
"assembles a list of one or more fingerprints (as described below) and tries "
"to log in with each key, to see if any of them are already installed (of "
"course, if you are not using E<.Xr ssh-agent 1> this may result in you being "
"repeatedly prompted for pass-phrases).  It then assembles a list of those "
"that failed to log in, and using ssh, enables logins with those keys on the "
"remote server.  By default it adds the keys by appending them to the remote "
"user's E<.Pa ~/.ssh/authorized_keys> (creating the file, and directory, if "
"necessary).  It is also capable of detecting if the remote system is a "
"NetScreen, and using its E<.Ql set ssh pka-dsa key ...> command instead."
msgstr ""
"E<.Nm> є скриптом, який використовує E<.Xr ssh 1> для входу до віддалених "
"машин (ймовірно, якщо не налаштовано якогось вигадливого використання "
"декількох профілів, за допомогою пароля до облікового запису, тому має бути "
"увімкнено розпізнавання за паролем). Скрипт збирає список з одного або "
"декількох відбитків (як це описано нижче) і намагається здійснити вхід за "
"допомогою кожного з ключів, щоб перевірити, чи якийсь з них вже встановлено "
"(Звичайною, якщо ви не користуєтеся E<.Xr ssh-agent 1>, це може призвести до "
"потреби у повторному введенні пароля). Потім скрипт збирає список тих "
"відбитків, за якими увійти не вдалося, і, використовуючи ssh, вмикає вхід за "
"ключами на віддаленому сервері. Типово, скрипт додає ключі, дописуючи їх до "
"файла E<.Pa ~/.ssh/authorized_keys> віддаленого користувача (створюючи файл, "
"і каталог, якщо це потрібно). Скрипт також може виявляти, чи є віддалена "
"система системою NetScreen, і користуватися її командою E<.Ql set ssh pka-"
"dsa key ...>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "The options are as follows:"
msgstr "Параметри є такими:"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl i Ar identity_file"
msgstr "Fl i Ar файл_профілю"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Use only the key(s) contained in E<.Ar identity_file> (rather than looking "
"for identities via E<.Xr ssh-add 1> or in the E<.Ic default_ID_file>).  If "
"the filename does not end in E<.Pa .pub> this is added.  If the filename is "
"omitted, the E<.Ic default_ID_file> is used."
msgstr ""
"Використовувати лише ключі, які містяться у E<.Ar файлі_профілю> (а не "
"шукати профілі за допомогою E<.Xr ssh-add 1> або E<.Ic default_ID_file>). "
"Якщо назва файла не завершується на E<.Pa .pub>, цей суфікс буде додано. "
"Якщо назви файла не вказано, буде використано назву E<.Ic default_ID_file>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Note that this can be used to ensure that the keys copied have the comment "
"one prefers and/or extra options applied, by ensuring that the key file has "
"these set as preferred before the copy is attempted."
msgstr ""
"Зауважте, що цим можна скористатися для забезпечення того, щоб до "
"скопійованих ключів було застосовано бажаний коментар і/або додаткові "
"параметри, шляхом забезпечення цього набору параметрів до спроби копіювання "
"ключа."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl f"
msgstr "Fl f"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Forced mode: doesn't check if the keys are present on the remote server.  "
"This means that it does not need the private key.  Of course, this can "
"result in more than one copy of the key being installed on the remote system."
msgstr ""
"Примусовий режим: не перевіряти, якщо ключі є на віддаленому сервері. Це "
"означає, що закритий ключ не потрібен. Звичайно ж, це може призвести до "
"встановлення у віддаленій системі декількох копій ключа."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl n"
msgstr "Fl n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"do a dry-run.  Instead of installing keys on the remote system simply prints "
"the key(s) that would have been installed."
msgstr ""
"Виконати тестовий пуск. Замість встановлення ключів до віддаленої системи, "
"просто вивести список ключів, які було б встановлено."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl s"
msgstr "Fl s"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"SFTP mode: usually the public keys are installed by executing commands on "
"the remote side.  With this option the user's E<.Pa ~/.ssh/authorized_keys> "
"file will be downloaded, modified locally and uploaded with sftp.  This "
"option is useful if the server has restrictions on commands which can be "
"used on the remote side."
msgstr ""
"Режим SFTP: зазвичай, відкриті ключі встановлюють виконанням команд на боці "
"віддаленої системи. Якщо вказано цей параметр, буде отримано файл E<.Pa ~/."
"ssh/authorized_keys> користувача, до файла буде внесено зміни у локальній "
"системі, а результати буде вивантажено за допомогою sftp на віддалену "
"систему. Цей параметр корисний, якщо на сервері є обмеження щодо команд, "
"якими можна скористатися на віддаленому боці."

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl h , Fl ?"
msgstr "Fl h , Fl ?"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "Print Usage summary"
msgstr "Вивести дані щодо користування"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl p Ar port , Fl o Ar ssh_option"
msgstr "Fl p Ar порт , Fl o Ar параметр_ssh"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"These two options are simply passed through untouched, along with their "
"argument, to allow one to set the port or other E<.Xr ssh 1> options, "
"respectively."
msgstr ""
"Ці два параметри буде просто передано незмінними, разом із їхніми "
"аргументами, щоб уможливити встановлення порту або інших параметрів E<.Xr "
"ssh 1>, відповідно."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Rather than specifying these as command line options, it is often better to "
"use (per-host) settings in E<.Xr ssh 1 Ns 's> configuration file: E<.Xr "
"ssh_config 5>."
msgstr ""
"Замість встановлення цих параметрів у рядку команди, часто краще "
"скористатися (окремими для вузла) параметрами у файлі налаштувань E<.Xr ssh "
"1>: E<.Xr ssh_config 5>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Default behaviour without E<.Fl i>, is to check if E<.Ql ssh-add -L> "
"provides any output, and if so those keys are used.  Note that this results "
"in the comment on the key being the filename that was given to E<.Xr ssh-add "
"1> when the key was loaded into your E<.Xr ssh-agent 1> rather than the "
"comment contained in that file, which is a bit of a shame.  Otherwise, if E<."
"Xr ssh-add 1> provides no keys contents of the E<.Ic default_ID_file> will "
"be used."
msgstr ""
"Типовою поведінкою без зазначення E<.Fl i>, буде перевірка того, чи виводить "
"щось E<.Ql ssh-add -L>, і якщо щось буде виведено, використати виведені "
"ключі. Зауважте, що результатом цього буде те, що назвою файла буде коментар "
"щодо ключа, який було задано E<.Xr ssh-add 1>, коли ключ було завантажено до "
"вашого E<.Xr ssh-agent 1>, а не коментар, що міститься у цьому файлі, і це "
"трохи сумно. В інших випадках, якщо E<.Xr ssh-add 1> не надасть ключів, буде "
"використано вміст E<.Ic default_ID_file>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"The E<.Ic default_ID_file> is the most recent file that matches: E<.Pa ~/."
"ssh/id*.pub>, (excluding those that match E<.Pa ~/.ssh/*-cert.pub>)  so if "
"you create a key that is not the one you want E<.Nm> to use, just use E<.Xr "
"touch 1> on your preferred key's E<.Pa .pub> file to reinstate it as the "
"most recent."
msgstr ""
"E<.Ic default_ID_file> найсвіжіший файл, який відповідає: E<.Pa ~/.ssh/id*."
"pub>, (виключаючи відповідники взірцю E<.Pa ~/.ssh/*-cert.pub>), отже, якщо "
"ви створите ключ, який не слід використовувати в E<.Nm>, просто "
"скористайтеся E<.Xr touch 1> для файла E<.Pa .pub> бажаного ключа, щоб "
"зробити його найсвіжішим."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИКЛАДИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"If you have already installed keys from one system on a lot of remote hosts, "
"and you then create a new key, on a new client machine, say, it can be "
"difficult to keep track of which systems on which you've installed the new "
"key.  One way of dealing with this is to load both the new key and old "
"key(s) into your E<.Xr ssh-agent 1>.  Load the new key first, without the E<."
"Fl c> option, then load one or more old keys into the agent, possibly by ssh-"
"ing to the client machine that has that old key, using the E<.Fl A> option "
"to allow agent forwarding:"
msgstr ""
"Якщо ви вже встановили ключі з однієї системи на багатьох віддалених вузлах, "
"а потім, скажімо, створили ключ на новій клієнтській машині, буде важкою "
"стежити за тим, у яких системах ви вже встановили новий ключ. Одним із "
"вирішень цієї проблеми є завантаження нового і старого ключів до вашого E<."
"Xr ssh-agent 1>. Спочатку завантажте новий ключ без параметра E<.Fl c>, "
"потім завантажте до агента один або декілька старих ключів, можливо, "
"скориставшись ssh до клієнтської машини, де є цей старий ключ, з параметром "
"E<.Fl A> для уможливлення переспрямування агента:"

#. type: D1
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "user@newclient$ ssh-add"
msgstr "user@newclient$ ssh-add"

#. type: D1
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "user@newclient$ ssh -A old.client"
msgstr "user@newclient$ ssh -A old.client"

#. type: D1
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "user@oldl$ ssh-add -c"
msgstr "user@oldl$ ssh-add -c"

#. type: D1
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "No ... prompt for pass-phrase ..."
msgstr "Ні ... запит щодо пароля ..."

#. type: D1
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "user@old$ logoff"
msgstr "user@old$ logoff"

#. type: D1
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "user@newclient$ ssh someserver"
msgstr "user@newclient$ ssh someserver"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"now, if the new key is installed on the server, you'll be allowed in "
"unprompted, whereas if you only have the old key(s) enabled, you'll be asked "
"for confirmation, which is your cue to log back out and run"
msgstr ""
"тепер, якщо новий ключ встановлено на сервері, ви зможете увійти до системи "
"без запиту, оскільки, якщо у вас увімкнено лише старі ключі, система "
"попросить підтвердження, що і є вашим сигналом для входу і віддання команди"

#. type: D1
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "user@newclient$ ssh-copy-id -i someserver"
msgstr "user@newclient$ ssh-copy-id -i someserver"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"The reason you might want to specify the -i option in this case is to ensure "
"that the comment on the installed key is the one from the E<.Pa .pub> file, "
"rather than just the filename that was loaded into your agent.  It also "
"ensures that only the id you intended is installed, rather than all the keys "
"that you have in your E<.Xr ssh-agent 1>.  Of course, you can specify "
"another id, or use the contents of the E<.Xr ssh-agent 1> as you prefer."
msgstr ""
"Причиною для використання тут параметра B<-i> є забезпечення того, щоб "
"коментарем до встановленого ключа був коментар з файла E<.Pa .pub>, а не "
"просто назва файла, який було завантажено до вашого агента. Цим також "
"забезпечується те, що буде встановлено лише потрібний вам ідентифікатор, а "
"не усі ключі, які є у вашому E<.Xr ssh-agent 1>. Звичайно ж, ви можете "
"вказати інший ідентифікатор або скористатися, якщо хочете, даними E<.Xr ssh-"
"agent 1>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Having mentioned E<.Xr ssh-add 1 Ns 's> E<.Fl c> option, you might consider "
"using this whenever using agent forwarding to avoid your key being hijacked, "
"but it is much better to instead use E<.Xr ssh 1 Ns 's> E<.Ar ProxyCommand> "
"and E<.Fl W> option, to bounce through remote servers while always doing "
"direct end-to-end authentication. This way the middle hop(s) don't get "
"access to your E<.Xr ssh-agent 1>.  A web search for E<.Ql ssh proxycommand "
"nc> should prove enlightening (N.B. the modern approach is to use the E<.Fl "
"W> option, rather than E<.Xr nc 1>)."
msgstr ""
"Якщо вже згадали параметр E<.Xr ssh-add 1> E<.Fl c>, вам варто користуватися "
"ним кожного разу при переспрямуванні агента, щоб уникнути перехоплення "
"вашого ключа. Втім, набагато кращим виходом є використання параметрів E<.Xr "
"ssh 1> E<.Ar ProxyCommand> та E<.Fl W> для усування проміжних віддалених "
"серверів і безумовного використання безпосереднього розпізнавання між "
"кінцями ланцюга зв'язку. Якщо користуватися цими параметрами, проміжні ланки "
"не отримуватимуть доступу до вашого E<.Xr ssh-agent 1>. Якщо пошукати в "
"інтернеті E<.Ql ssh proxycommand nc>, ви отримаєте достатні матеріали (N.B. "
"сучасним підходом є використання параметра E<.Fl W>, а не E<.Xr nc 1>)."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ДИВ. ТАКОЖ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Xr ssh 1>, E<.Xr ssh-agent 1>, E<.Xr sshd 8>"
msgstr "E<.Xr ssh 1>, E<.Xr ssh-agent 1>, E<.Xr sshd 8>"
