# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:32+0200\n"
"PO-Revision-Date: 2019-10-05 08:17+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<inet_pton>()"
msgid "inet_pton"
msgstr "B<inet_pton>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "inet_pton - convert IPv4 and IPv6 addresses from text to binary form"
msgstr ""
"inet_pton - преобразует адреса IPv4 и IPv6 из текстового вида в двоичный "
"формат"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>arpa/inet.hE<gt>>\n"
msgstr "B<#include E<lt>arpa/inet.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int inet_pton(int >I<af>B<, const char *>I<src>B<, void *>I<dst>B<);>\n"
msgid "B<int inet_pton(int >I<af>B<, const char *restrict >I<src>B<, void *restrict >I<dst>B<);>\n"
msgstr "B<int inet_pton(int >I<af>B<, const char *>I<src>B<, void *>I<dst>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This function converts the character string I<src> into a network address "
"structure in the I<af> address family, then copies the network address "
"structure to I<dst>.  The I<af> argument must be either B<AF_INET> or "
"B<AF_INET6>.  I<dst> is written in network byte order."
msgstr ""
"Данная функция преобразует строку символов I<src> в структуру сетевого "
"адреса сетевого семейства адресов  I<af>, а затем копирует полученную "
"структуру по адресу I<dst>. Значение аргумента I<af> должно быть равно "
"B<AF_INET> или B<AF_INET6>. Значение I<dst> записывается в сетевом порядке "
"байт."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The following address families are currently supported:"
msgstr "В настоящее время поддерживаются следующие семейства адресов:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AF_INET>"
msgstr "B<AF_INET>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<src> points to a character string containing an IPv4 network address in "
"dotted-decimal format, \"I<ddd.ddd.ddd.ddd>\", where I<ddd> is a decimal "
"number of up to three digits in the range 0 to 255.  The address is "
"converted to a I<struct in_addr> and copied to I<dst>, which must be "
"I<sizeof(struct in_addr)> (4) bytes (32 bits) long."
msgstr ""
"Значение I<src> указывает на строку символов, содержащую сетевой адрес IPv4 "
"в точечном формате «I<ddd.ddd.ddd.ddd>», где I<ddd> — десятичное число в "
"диапазоне от 0 до 255, состоящее максимум из трёх цифр. Адрес преобразуется "
"в I<struct in_addr> и копируется в I<dst>, размер которой должен быть равен "
"I<sizeof(struct in_addr)> (4) байтам (32 бита)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<AF_INET6>"
msgstr "B<AF_INET6>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<src> points to a character string containing an IPv6 network address.  The "
"address is converted to a I<struct in6_addr> and copied to I<dst>, which "
"must be I<sizeof(struct in6_addr)> (16) bytes (128 bits) long.  The allowed "
"formats for IPv6 addresses follow these rules:"
msgstr ""
"Значение I<src> указывает на строку символов, содержащую сетевой адрес IPv6. "
"Адрес преобразуется в I<struct in6_addr> и копируется в I<dst>, размер "
"которой должен быть равен I<sizeof(struct in6_addr)> (16) байтам (128 бит). "
"Допустимые форматы адресов IPv6:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The preferred format is I<x:x:x:x:x:x:x:x>.  This form consists of eight "
"hexadecimal numbers, each of which expresses a 16-bit value (i.e., each I<x> "
"can be up to 4 hex digits)."
msgstr ""
"Предпочтительный формат — I<x:x:x:x:x:x:x:x>. Он состоит из восьми "
"шестнадцатеричных чисел, каждое из которых представляет 16-битное значение "
"(т. е., каждый I<x> может содержать до 4 шестнадцатеричных цифр)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A series of contiguous zero values in the preferred format can be "
"abbreviated to I<::>.  Only one instance of I<::> can occur in an address.  "
"For example, the loopback address I<0:0:0:0:0:0:0:1> can be abbreviated as "
"I<::1>.  The wildcard address, consisting of all zeros, can be written as "
"I<::>."
msgstr ""
"Серия непрерывных нулевых значений в предпочтительном формате может "
"сокращённо записываться как I<::>. В адресе допускается только одно "
"появление I<::>. Например, адрес интерфейса обратной петли "
"I<0:0:0:0:0:0:0:1> может быть сокращён до I<::1>. Шаблонный адрес, состоящий "
"из всех нулей, может быть записан как I<::>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"An alternate format is useful for expressing IPv4-mapped IPv6 addresses.  "
"This form is written as I<x:x:x:x:x:x:d.d.d.d>, where the six leading I<x>s "
"are hexadecimal values that define the six most-significant 16-bit pieces of "
"the address (i.e., 96 bits), and the I<d>s express a value in dotted-decimal "
"notation that defines the least significant 32 bits of the address.  An "
"example of such an address is I<::FFFF:204.152.189.116>."
msgstr ""
"Альтернативный формат — полезен для записи IPv4-отображённых адресов IPv6. "
"Этот формат имеет вид I<x:x:x:x:x:x:d.d.d.d>, где шесть начальных I<x> "
"представляют собой шестнадцатеричные значения, которые определяют шесть "
"наиболее значимых 16-битных частей адреса (т. е., 96 бит), а символами I<d> "
"выражается значение в точечно-десятичном формате, которое определяет "
"наименее значимые 32 бита адреса. Пример адреса: I<::FFFF:204.152.189.116>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"See RFC 2373 for further details on the representation of IPv6 addresses."
msgstr ""
"Дополнительную информацию о представлении адресов IPv6 смотрите в RFC 2373."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<inet_pton>()  returns 1 on success (network address was successfully "
"converted).  0 is returned if I<src> does not contain a character string "
"representing a valid network address in the specified address family.  If "
"I<af> does not contain a valid address family, -1 is returned and I<errno> "
"is set to B<EAFNOSUPPORT>."
msgstr ""
"При успешном выполнении функция B<inet_pton>() возвращает 1 (адрес "
"преобразован). Функция возвращает 0, если I<src> не содержит строку "
"символов, представляющую правильный сетевой адрес для указанного семейства "
"адресов. Если  I<af> не содержит допустимого значения семейства адресов, то "
"возвращается -1 и I<errno> присваивается значение B<EAFNOSUPPORT>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<inet_pton>()"
msgstr "B<inet_pton>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe locale"
msgstr "MT-Safe locale"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Unlike B<inet_aton>(3)  and B<inet_addr>(3), B<inet_pton>()  supports IPv6 "
"addresses.  On the other hand, B<inet_pton>()  accepts only IPv4 addresses "
"in dotted-decimal notation, whereas B<inet_aton>(3)  and B<inet_addr>(3)  "
"allow the more general numbers-and-dots notation (hexadecimal and octal "
"number formats, and formats that don't require all four bytes to be "
"explicitly written).  For an interface that handles both IPv6 addresses, and "
"IPv4 addresses in numbers-and-dots notation, see B<getaddrinfo>(3)."
msgstr ""
"В отличие от B<inet_aton>(3) и B<inet_addr>(3), B<inet_pton>() поддерживает "
"адреса IPv6. Но стоит отметить, что B<inet_pton>() работает только с "
"адресами IPv4 в точечно-десятичном формате, в то время как B<inet_aton>(3) и "
"B<inet_addr>(3) поддерживают более распространённый формат чисел-с-точками "
"(numbers-and-dots notation) (шестнадцатеричный и восьмеричный формат чисел, "
"и форматы, которые не требуют явного указания всех четырёх байт). "
"Программный интерфейс, который понимает одновременно адреса и IPv6, и IPv4 в "
"формате чисел-с-точками, смотрите B<getaddrinfo>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001."
msgstr "POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "ДЕФЕКТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<AF_INET6> does not recognize IPv4 addresses.  An explicit IPv4-mapped IPv6 "
"address must be supplied in I<src> instead."
msgstr ""
"При B<AF_INET6> не распознаются адреса IPv4. В этом случае в I<src> должен "
"указываться IPv4-отображённый адрес IPv6."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "ПРИМЕРЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The program below demonstrates the use of B<inet_pton>()  and "
"B<inet_ntop>(3).  Here are some example runs:"
msgstr ""
"Представленная ниже программа показывает использование B<inet_pton>() и "
"B<inet_ntop>(3). Пример работы программы:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$B< ./a.out i6 0:0:0:0:0:0:0:0>\n"
"::\n"
"$B< ./a.out i6 1:0:0:0:0:0:0:8>\n"
"1::8\n"
"$B< ./a.out i6 0:0:0:0:0:FFFF:204.152.189.116>\n"
"::ffff:204.152.189.116\n"
msgstr ""
"$B< ./a.out i6 0:0:0:0:0:0:0:0>\n"
"::\n"
"$B< ./a.out i6 1:0:0:0:0:0:0:8>\n"
"1::8\n"
"$B< ./a.out i6 0:0:0:0:0:FFFF:204.152.189.116>\n"
"::ffff:204.152.189.116\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Program source"
msgstr "Исходный код программы"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>arpa/inet.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>arpa/inet.hE<gt>\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    unsigned char buf[sizeof(struct in6_addr)];\n"
"    int domain, s;\n"
"    char str[INET6_ADDRSTRLEN];\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    unsigned char buf[sizeof(struct in6_addr)];\n"
"    int domain, s;\n"
"    char str[INET6_ADDRSTRLEN];\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (argc != 3) {\n"
"        fprintf(stderr, \"Usage: %s {i4|i6|E<lt>numE<gt>} string\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc != 3) {\n"
"        fprintf(stderr,\n"
"          \"Использование: %s {i4|i6|E<lt>числоE<gt>} строка\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    domain = (strcmp(argv[1], \"i4\") == 0) ? AF_INET :\n"
"             (strcmp(argv[1], \"i6\") == 0) ? AF_INET6 : atoi(argv[1]);\n"
msgstr ""
"    domain = (strcmp(argv[1], \"i4\") == 0) ? AF_INET :\n"
"             (strcmp(argv[1], \"i6\") == 0) ? AF_INET6 : atoi(argv[1]);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    s = inet_pton(domain, argv[2], buf);\n"
"    if (s E<lt>= 0) {\n"
"        if (s == 0)\n"
"            fprintf(stderr, \"Not in presentation format\");\n"
"        else\n"
"            perror(\"inet_pton\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    s = inet_pton(domain, argv[2], buf);\n"
"    if (s E<lt>= 0) {\n"
"        if (s == 0)\n"
"            fprintf(stderr, \"Неверный формат представления\");\n"
"        else\n"
"            perror(\"inet_pton\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    if (inet_ntop(domain, buf, str, INET6_ADDRSTRLEN) == NULL) {\n"
"        perror(\"inet_ntop\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (inet_ntop(domain, buf, str, INET6_ADDRSTRLEN) == NULL) {\n"
"        perror(\"inet_ntop\");\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "    printf(\"%s\\en\", str);\n"
msgstr "    printf(\"%s\\en\", str);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: inet_pton.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<getaddrinfo>(3), B<inet>(3), B<inet_ntop>(3)"
msgstr "B<getaddrinfo>(3), B<inet>(3), B<inet_ntop>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "INET_PTON"
msgstr "INET_PTON"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<int inet_pton(int >I<af>B<, const char *>I<src>B<, void *>I<dst>B<);>\n"
msgstr "B<int inet_pton(int >I<af>B<, const char *>I<src>B<, void *>I<dst>B<);>\n"

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "1."
msgstr "1."

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "2."
msgstr "2."

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "3."
msgstr "3."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "ПРИМЕР"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
