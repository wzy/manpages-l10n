# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# aereiae <aereiae@gmail.com>, 2014.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2016.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Lockal <lockalsash@gmail.com>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Баринов Владимир, 2016.
# Иван Павлов <pavia00@gmail.com>, 2017,2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: 2019-10-06 09:20+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "round"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "round, roundf, roundl - round to nearest integer, away from zero"
msgstr ""
"round, roundf, roundl - округление до ближайшего целого в направлении от нуля"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>, I<-lm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double round(double >I<x>B<);>\n"
"B<float roundf(float >I<x>B<);>\n"
"B<long double roundl(long double >I<x>B<);>\n"
msgstr ""
"B<double round(double >I<x>B<);>\n"
"B<float roundf(float >I<x>B<);>\n"
"B<long double roundl(long double >I<x>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<round>(), B<roundf>(), B<roundl>():"
msgstr "B<round>(), B<roundf>(), B<roundl>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
msgstr "    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions round I<x> to the nearest integer, but round halfway cases "
"away from zero (regardless of the current rounding direction, see "
"B<fenv>(3)), instead of to the nearest even integer like B<rint>(3)."
msgstr ""
"Эти функции округляют аргумент I<x> до ближайшего целого значения, но при "
"округлении в половине случаев используют направление округления от нуля "
"(независимо от текущего направления округления, смотрите B<fenv>(3)), вместо "
"округления до ближайшего чётного целого, как это делает функция B<rint>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For example, I<round(0.5)> is 1.0, and I<round(-0.5)> is -1.0."
msgstr ""
"Например, результат I<round(0.5)> будет равен 1.0, а I<round(-0.5)> будет "
"равен -1.0."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These functions return the rounded integer value."
msgstr "Данные функции возвращают округлённое целое число."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If I<x> is integral, +0, -0, NaN, or infinite, I<x> itself is returned."
msgstr ""
"Если I<x> целое, +0, -0, NaN или стремится к бесконечности, то будет "
"возвращено значение I<x>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"No errors occur.  POSIX.1-2001 documents a range error for overflows, but "
"see NOTES."
msgstr ""
"Ошибки не возникают. В документах POSIX.1-2001 описывается ошибка диапазона "
"при переполнениях, однако смотрите ЗАМЕЧАНИЯ."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<round>(),\n"
"B<roundf>(),\n"
"B<roundl>()"
msgstr ""
"B<round>(),\n"
"B<roundf>(),\n"
"B<roundl>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.1.  C99, POSIX.1-2001."
msgstr "glibc 2.1.  C99, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#.  The POSIX.1-2001 APPLICATION USAGE SECTION discusses this point.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "POSIX.1-2001 contains text about overflow (which might set I<errno> to "
#| "B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In practice, the "
#| "result cannot overflow on any current machine, so this error-handling "
#| "stuff is just nonsense.  (More precisely, overflow can happen only when "
#| "the maximum value of the exponent is smaller than the number of mantissa "
#| "bits.  For the IEEE-754 standard 32-bit and 64-bit floating-point numbers "
#| "the maximum value of the exponent is 128 (respectively, 1024), and the "
#| "number of mantissa bits is 24 (respectively, 53).)"
msgid ""
"POSIX.1-2001 contains text about overflow (which might set I<errno> to "
"B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In practice, the result "
"cannot overflow on any current machine, so this error-handling stuff is just "
"nonsense.  (More precisely, overflow can happen only when the maximum value "
"of the exponent is smaller than the number of mantissa bits.  For the "
"IEEE-754 standard 32-bit and 64-bit floating-point numbers the maximum value "
"of the exponent is 127 (respectively, 1023), and the number of mantissa bits "
"including the implicit bit is 24 (respectively, 53).)"
msgstr ""
"В POSIX.1-2001 есть текст о переполнении (которое может установить I<errno> "
"в B<ERANGE> или вызвать исключение B<FE_OVERFLOW>). На практике, результат "
"не может выйти за диапазон ни на каком компьютере, поэтому обработка этой "
"ошибки не имеет смысла (точнее говоря, переполнение возможно только в том "
"случае, когда максимальное значение экспоненты меньше числа бит мантиссы. В "
"стандарте IEEE-754 для 32- и 64-битных чисел с плавающей точкой максимальное "
"значение экспоненты равно 128 и 1024 соответственно, а число бит мантиссы — "
"24 и 53 соответственно)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If you want to store the rounded value in an integer type, you probably want "
"to use one of the functions described in B<lround>(3)  instead."
msgstr ""
"Если вы хотите сохранить округлённое значение в целочисленном типе, то лучше "
"использовать одну из функций, описанных в B<lround>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ceil>(3), B<floor>(3), B<lround>(3), B<nearbyint>(3), B<rint>(3), "
"B<trunc>(3)"
msgstr ""
"B<ceil>(3), B<floor>(3), B<lround>(3), B<nearbyint>(3), B<rint>(3), "
"B<trunc>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, fuzzy
#| msgid "These functions first appeared in glibc in version 2.1."
msgid "These functions were added in glibc 2.1."
msgstr "Эти функции впервые появились в glibc 2.1."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "C99, POSIX.1-2001, POSIX.1-2008."
msgstr "C99, POSIX.1-2001, POSIX.1-2008."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "ROUND"
msgstr "ROUND"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Link with I<-lm>."
msgstr "Компонуется при указании параметра I<-lm>."

#. type: Plain text
#: opensuse-leap-15-5
msgid "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"
msgstr "_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L"

#. type: Plain text
#: opensuse-leap-15-5
msgid "These functions first appeared in glibc in version 2.1."
msgstr "Эти функции впервые появились в glibc 2.1."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#.  The POSIX.1-2001 APPLICATION USAGE SECTION discusses this point.
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"POSIX.1-2001 contains text about overflow (which might set I<errno> to "
"B<ERANGE>, or raise an B<FE_OVERFLOW> exception).  In practice, the result "
"cannot overflow on any current machine, so this error-handling stuff is just "
"nonsense.  (More precisely, overflow can happen only when the maximum value "
"of the exponent is smaller than the number of mantissa bits.  For the "
"IEEE-754 standard 32-bit and 64-bit floating-point numbers the maximum value "
"of the exponent is 128 (respectively, 1024), and the number of mantissa bits "
"is 24 (respectively, 53).)"
msgstr ""
"В POSIX.1-2001 есть текст о переполнении (которое может установить I<errno> "
"в B<ERANGE> или вызвать исключение B<FE_OVERFLOW>). На практике, результат "
"не может выйти за диапазон ни на каком компьютере, поэтому обработка этой "
"ошибки не имеет смысла (точнее говоря, переполнение возможно только в том "
"случае, когда максимальное значение экспоненты меньше числа бит мантиссы. В "
"стандарте IEEE-754 для 32- и 64-битных чисел с плавающей точкой максимальное "
"значение экспоненты равно 128 и 1024 соответственно, а число бит мантиссы — "
"24 и 53 соответственно)."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
