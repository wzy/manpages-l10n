# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:29+0200\n"
"PO-Revision-Date: 2019-10-05 08:06+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "getttyent"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "getttyent, getttynam, setttyent, endttyent - get ttys file entry"
msgstr ""
"getttyent, getttynam, setttyent, endttyent - возвращает запись из файла ttys"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>ttyent.hE<gt>>"
msgid "B<#include E<lt>ttyent.hE<gt>>\n"
msgstr "B<#include E<lt>ttyent.hE<gt>>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<struct ttyent *getttynam(const char *>I<name>B<);>"
msgid ""
"B<struct ttyent *getttyent(void);>\n"
"B<struct ttyent *getttynam(const char *>I<name>B<);>\n"
msgstr "B<struct ttyent *getttynam(const char *>I<name>B<);>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int setttyent(void);>"
msgid ""
"B<int setttyent(void);>\n"
"B<int endttyent(void);>\n"
msgstr "B<int setttyent(void);>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"These functions provide an interface to the file B<_PATH_TTYS> (e.g., I</etc/"
"ttys>)."
msgstr ""
"Эти функции предоставляют интерфейс к файлу из B<_PATH_TTYS> (например, I</"
"etc/ttys>)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<setttyent>()  opens the file or rewinds it if already open."
msgstr ""
"Функция B<setttyent>() открывает файл или сбрасывает указатель чтения на "
"начало, если он уже открыт."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The function B<endttyent>()  closes the file."
msgstr "Функция B<endttyent>() закрывает файл."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<getttynam>()  searches for a given terminal name in the "
"file.  It returns a pointer to a I<ttyent> structure (description below)."
msgstr ""
"Функция B<getttynam>() ищет заданное имя терминала в файле. Возвращается "
"указатель на структуру I<ttyent> (описана далее)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<getttyent>()  opens the file B<_PATH_TTYS> (if necessary) and "
"returns the first entry.  If the file is already open, the next entry.  The "
"I<ttyent> structure has the form:"
msgstr ""
"Функция B<getttyent>() открывает файл из переменной B<_PATH_TTYS> (если "
"требуется) и возвращает его первую запись. Если файл уже открыт, то "
"возвращается его следующая запись. Структура I<ttyent>:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"struct ttyent {\n"
"    char *ty_name;     /* terminal device name */\n"
"    char *ty_getty;    /* command to execute, usually getty */\n"
"    char *ty_type;     /* terminal type for termcap */\n"
"    int   ty_status;   /* status flags */\n"
"    char *ty_window;   /* command to start up window manager */\n"
"    char *ty_comment;  /* comment field */\n"
"};\n"
msgstr ""
"struct ttyent {\n"
"    char *ty_name;     /* имя устройства терминала */\n"
"    char *ty_getty;    /* команда для исполнения, обычно getty */\n"
"    char *ty_type;     /* тип терминала для termcap */\n"
"    int   ty_status;   /* флаги состояния */\n"
"    char *ty_window;   /* команда для запуска менеджера окон */\n"
"    char *ty_comment;  /* поле комментария */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<ty_status> can be:"
msgstr "Значением I<ty_status> может быть:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#define TTY_ON     0x01  /* enable logins (start ty_getty program) */\n"
"#define TTY_SECURE 0x02  /* allow UID 0 to login */\n"
msgstr ""
"#define TTY_ON     0x01  /* разрешить регистрации\n"
"                            (запускаемые программой ty_getty) */\n"
"#define TTY_SECURE 0x02  /* разрешить регистрацию с UID 0 */\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<getttyent>(),\n"
"B<setttyent>(),\n"
"B<endttyent>(),\n"
"B<getttynam>()"
msgstr ""
"B<getttyent>(),\n"
"B<setttyent>(),\n"
"B<endttyent>(),\n"
"B<getttynam>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:ttyent"
msgstr "MT-Unsafe race:ttyent"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "BSD."
msgstr "BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Under Linux, the file I</etc/ttys>, and the functions described above, are "
"not used."
msgstr "В Linux файл I</etc/ttys> и описанные выше функции не используются."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ttyname>(3), B<ttyslot>(3)"
msgstr "B<ttyname>(3), B<ttyslot>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Not in POSIX.1.  Present on the BSDs, and perhaps other systems."
msgstr ""
"Нет в POSIX.1. Присутствует в BSD и, возможно, во многих других системах."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GETTTYENT"
msgstr "GETTTYENT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>ttyent.hE<gt>>"
msgstr "B<#include E<lt>ttyent.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<struct ttyent *getttyent(void);>"
msgstr "B<struct ttyent *getttyent(void);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<struct ttyent *getttynam(const char *>I<name>B<);>"
msgstr "B<struct ttyent *getttynam(const char *>I<name>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int setttyent(void);>"
msgstr "B<int setttyent(void);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<int endttyent(void);>"
msgstr "B<int endttyent(void);>"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
