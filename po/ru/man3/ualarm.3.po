# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:59+0200\n"
"PO-Revision-Date: 2019-10-15 18:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<ualarm>()"
msgid "ualarm"
msgstr "B<ualarm>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ualarm - schedule signal after given number of microseconds"
msgstr "ualarm - планирует отправку сигнала через заданное число микросекунд"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<useconds_t ualarm(useconds_t >I<usecs>B<, useconds_t >I<interval>B<);>\n"
msgstr "B<useconds_t ualarm(useconds_t >I<usecs>B<, useconds_t >I<interval>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ualarm>():"
msgstr "B<ualarm>():"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
#| "    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
#| "    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgid ""
"    Since glibc 2.12:\n"
"        (_XOPEN_SOURCE E<gt>= 500) && ! (_POSIX_C_SOURCE E<gt>= 200809L)\n"
"            || /* glibc E<gt>= 2.19: */ _DEFAULT_SOURCE\n"
"            || /* glibc E<lt>= 2.19: */ _BSD_SOURCE\n"
"    Before glibc 2.12:\n"
"        _BSD_SOURCE || _XOPEN_SOURCE E<gt>= 500\n"
msgstr ""
"(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версиях glibc E<lt>= 2.19: */ _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<ualarm>()  function causes the signal B<SIGALRM> to be sent to the "
"invoking process after (not less than)  I<usecs> microseconds.  The delay "
"may be lengthened slightly by any system activity or by the time spent "
"processing the call or by the granularity of system timers."
msgstr ""
"Функция B<ualarm>() планирует отправку сигнала B<SIGALRM> вызывающему "
"процессу (не менее чем) через I<usecs> микросекунд. Задержка может быть "
"слегка больше при большой загруженности системы, из-за, собственно, времени "
"обработки этого вызова или из-за неточности хода системных часов."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Unless caught or ignored, the B<SIGALRM> signal will terminate the process."
msgstr ""
"Если сигнал не будет пойман или проигнорирован, то B<SIGALRM> уничтожит "
"процесс."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the I<interval> argument is nonzero, further B<SIGALRM> signals will be "
"sent every I<interval> microseconds after the first."
msgstr ""
"Если аргумент I<interval> не равен нулю, то сигналы B<SIGALRM> будут "
"отправляться повторно каждые I<interval> микросекунд после первого."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This function returns the number of microseconds remaining for any alarm "
"that was previously set, or 0 if no alarm was pending."
msgstr ""
"Эта функция возвращает число микросекунд, оставшихся от любого ранее "
"установленного сигнала, или 0, если сигналов не запланировано."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Interrupted by a signal; see B<signal>(7)."
msgstr "Прервано сигналом; см. B<signal>(7)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"I<usecs> or I<interval> is not smaller than 1000000.  (On systems where that "
"is considered an error.)"
msgstr ""
"Значение I<usec> или I<interval> больше 1000000 (в тех системах, где это "
"считается ошибкой)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ualarm>()"
msgstr "B<ualarm>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 marks it as obsolete.  Removed in "
"POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "4.3BSD, SUSv2, and POSIX do not define any errors."
msgstr ""

#.  This case is not documented in HP-US, Solar, FreeBSD, NetBSD, or OpenBSD!
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 does not specify what happens if the I<usecs> argument is 0.  "
"On Linux (and probably most other systems), the effect is to cancel any "
"pending alarm."
msgstr ""
"В POSIX.1-2001 не указано, что случится, если значение I<usecs> равно 0. В "
"Linux (и, вероятно, в большинстве систем) произойдёт отмена любого "
"взведённого будильника."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The type I<useconds_t> is an unsigned integer type capable of holding "
#| "integers in the range [0,1000000].  On the original BSD implementation, "
#| "and in glibc before version 2.1, the arguments to B<ualarm>()  were "
#| "instead typed as I<unsigned int>.  Programs will be more portable if they "
#| "never mention I<useconds_t> explicitly."
msgid ""
"The type I<useconds_t> is an unsigned integer type capable of holding "
"integers in the range [0,1000000].  On the original BSD implementation, and "
"in glibc before glibc 2.1, the arguments to B<ualarm>()  were instead typed "
"as I<unsigned int>.  Programs will be more portable if they never mention "
"I<useconds_t> explicitly."
msgstr ""
"Тип I<useconds_t> является беззнаковым целым типом, способным хранить целые "
"числа в диапазоне [0,1000000]. В первоначальной реализации BSD и glibc до "
"версии 2.1, аргументы B<ualarm>() имели тип I<unsigned int>. Программы будут "
"более переносимы, если они никогда не будут явно упоминать тип I<useconds_t>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The interaction of this function with other timer functions such as "
"B<alarm>(2), B<sleep>(3), B<nanosleep>(2), B<setitimer>(2), "
"B<timer_create>(2), B<timer_delete>(2), B<timer_getoverrun>(2), "
"B<timer_gettime>(2), B<timer_settime>(2), B<usleep>(3)  is unspecified."
msgstr ""
"Взаимодействие этой функции с другими функциями таймера, такими как "
"B<alarm>(2), B<sleep>(3), B<nanosleep>(2), B<setitimer>(2), "
"B<timer_create>(2), B<timer_delete>(2), B<timer_getoverrun>(2), "
"B<timer_gettime>(2), B<timer_settime>(2), B<usleep>(3) не определено."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This function is obsolete.  Use B<setitimer>(2)  or POSIX interval timers "
"(B<timer_create>(2), etc.)  instead."
msgstr ""
"Эта функция устарела. Используйте вместо неё B<setitimer>(2) или "
"интервальные таймеры POSIX (B<timer_create>(2) и т.д.)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<usleep>(3), B<time>(7)"
msgstr ""
"B<alarm>(2), B<getitimer>(2), B<nanosleep>(2), B<select>(2), "
"B<setitimer>(2), B<usleep>(3), B<time>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"4.3BSD, POSIX.1-2001.  POSIX.1-2001 marks B<ualarm>()  as obsolete.  "
"POSIX.1-2008 removes the specification of B<ualarm>().  4.3BSD, SUSv2, and "
"POSIX do not define any errors."
msgstr ""
"4.3BSD, POSIX.1-2001. В POSIX.1-2001 функция B<ualarm>() помечена как "
"устаревшая. В POSIX.1-2008 описание B<ualarm>() было удалено. В 4.3BSD, "
"SUSv2 и POSIX нет описания ошибок."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "UALARM"
msgstr "UALARM"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "Since glibc 2.12:"
msgstr "Начиная с glibc 2.12:"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
"    || /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE\n"
msgstr ""
"(_XOPEN_SOURCE\\ E<gt>=\\ 500) && ! (_POSIX_C_SOURCE\\ E<gt>=\\ 200809L)\n"
"    || /* начиная с glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* в версиях glibc E<lt>= 2.19: */ _BSD_SOURCE\n"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: opensuse-leap-15-5
msgid "Before glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"
msgstr "до glibc 2.12: _BSD_SOURCE || _XOPEN_SOURCE\\ E<gt>=\\ 500"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The type I<useconds_t> is an unsigned integer type capable of holding "
"integers in the range [0,1000000].  On the original BSD implementation, and "
"in glibc before version 2.1, the arguments to B<ualarm>()  were instead "
"typed as I<unsigned int>.  Programs will be more portable if they never "
"mention I<useconds_t> explicitly."
msgstr ""
"Тип I<useconds_t> является беззнаковым целым типом, способным хранить целые "
"числа в диапазоне [0,1000000]. В первоначальной реализации BSD и glibc до "
"версии 2.1, аргументы B<ualarm>() имели тип I<unsigned int>. Программы будут "
"более переносимы, если они никогда не будут явно упоминать тип I<useconds_t>."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
