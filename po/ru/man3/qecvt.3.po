# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Artyom Kunyov <artkun@guitarplayer.ru>, 2012.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2016.
# Konstantin Shvaykovskiy <kot.shv@gmail.com>, 2012.
# Yuri Kozlov <yuray@komyakino.ru>, 2012-2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:45+0200\n"
"PO-Revision-Date: 2019-07-20 06:57+0000\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<qecvt>()"
msgid "qecvt"
msgstr "B<qecvt>()"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "qecvt, qfcvt, qgcvt - convert a floating-point number to a string"
msgstr "qecvt, qfcvt, qgcvt - преобразуют число с плавающей точкой в строку"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<[[deprecated]] char *qecvt(long double >I<number>B<, int >I<ndigits>B<,>\n"
"B<                           int *restrict >I<decpt>B<, int *restrict >I<sign>B<);>\n"
"B<[[deprecated]] char *qfcvt(long double >I<number>B<, int >I<ndigits>B<,>\n"
"B<                           int *restrict >I<decpt>B<, int *restrict >I<sign>B<);>\n"
"B<[[deprecated]] char *qgcvt(long double >I<number>B<, int >I<ndigit>B<, char *>I<buf>B<);>\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Требования макроса тестирования свойств для glibc (см. "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "B<qecvt>(), B<qfcvt>(), B<qgcvt>(): _SVID_SOURCE"
msgid "B<qecvt>(), B<qfcvt>(), B<qgcvt>():"
msgstr "B<qecvt>(), B<qfcvt>(), B<qgcvt>(): _SVID_SOURCE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "    Since glibc 2.19:\n"
#| "        _DEFAULT_SOURCE\n"
#| "    In glibc up to and including 2.19:\n"
#| "        _BSD_SOURCE\n"
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    In glibc up to and including 2.19:\n"
"        _SVID_SOURCE\n"
msgstr ""
"    начиная с glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    в glibc до версии 2.19 включительно:\n"
"        _BSD_SOURCE\n"

#. #-#-#-#-#  archlinux: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have been something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#. #-#-#-#-#  debian-bookworm: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have been something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#. #-#-#-#-#  debian-unstable: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have been something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#. #-#-#-#-#  fedora-38: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have been something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#. #-#-#-#-#  fedora-rawhide: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have been something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#. #-#-#-#-#  mageia-cauldron: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have been something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have be something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: qecvt.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  FIXME . The full FTM picture looks to have been something like the
#.  following mess:
#.     glibc 2.20 onward
#.         _DEFAULT_SOURCE
#.     glibc 2.18 to glibc 2.19
#.         _BSD_SOURCE || _SVID_SOURCE
#.     glibc 2.10 to glibc 2.17
#.         _SVID_SOURCE || (_XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED) &&
#.                 ! (_POSIX_C_SOURCE >= 200809L))
#.     Before glibc 2.10:
#.         _SVID_SOURCE || _XOPEN_SOURCE >= 500 ||
#.             (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The functions B<qecvt>(), B<qfcvt>(), and B<qgcvt>()  are identical to "
"B<ecvt>(3), B<fcvt>(3), and B<gcvt>(3)  respectively, except that they use a "
"I<long double> argument I<number>.  See B<ecvt>(3)  and B<gcvt>(3)."
msgstr ""
"Функции B<qecvt>(), B<qfcvt>() и B<qgcvt>() идентичны B<ecvt>(3), B<fcvt>(3) "
"и B<gcvt>(3), соответственно, за исключением того, что аргумент I<number> "
"имеет тип I<long double>. Смотрите B<ecvt>(3) и B<gcvt>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "АТРИБУТЫ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Описание терминов данного раздела смотрите в B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Интерфейс"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Атрибут"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Значение"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<qecvt>()"
msgstr "B<qecvt>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Безвредность в нитях"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:qecvt"
msgstr "MT-Unsafe race:qecvt"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<qfcvt>()"
msgstr "B<qfcvt>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Unsafe race:qfcvt"
msgstr "MT-Unsafe race:qfcvt"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<qgcvt>()"
msgstr "B<qgcvt>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "None"
msgid "None."
msgstr "None"

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#.  Not supported by libc4 and libc5.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "SVr4, SunOS, GNU."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "These functions are obsolete.  Instead, B<snprintf>(3)  is recommended."
msgstr ""
"Данные функции устарели. Вместо них рекомендуется использовать "
"B<snprintf>(3)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<ecvt>(3), B<ecvt_r>(3), B<gcvt>(3), B<sprintf>(3)"
msgstr "B<ecvt>(3), B<ecvt_r>(3), B<gcvt>(3), B<sprintf>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-15"
msgstr "15 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#.  Not supported by libc4 and libc5.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"SVr4.  Not seen in most common UNIX implementations, but occurs in SunOS.  "
"Supported by glibc."
msgstr ""
"SVr4. Отсутствуют в наиболее распространённых реализациях UNIX, но есть в "
"SunOS. Поддерживаются в glibc."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "QECVT"
msgstr "QECVT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-03-15"
msgstr "15 марта 2016 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>stdlib.hE<gt>>"
msgstr "B<#include E<lt>stdlib.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<char *qecvt(long double >I<number>B<, int >I<ndigits>B<, int *>I<decpt>B<,"
"> B<int *>I<sign>B<);>"
msgstr ""
"B<char *qecvt(long double >I<number>B<, int >I<ndigits>B<, int *>I<decpt>B<,"
"> B<int *>I<sign>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<char *qfcvt(long double >I<number>B<, int >I<ndigits>B<, int *>I<decpt>B<,"
"> B<int *>I<sign>B<);>"
msgstr ""
"B<char *qfcvt(long double >I<number>B<, int >I<ndigits>B<, int *>I<decpt>B<,"
"> B<int *>I<sign>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<char *qgcvt(long double >I<number>B<, int >I<ndigit>B<, char *>I<buf>B<);>"
msgstr ""
"B<char *qgcvt(long double >I<number>B<, int >I<ndigit>B<, char *>I<buf>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<qecvt>(), B<qfcvt>(), B<qgcvt>(): _SVID_SOURCE"
msgstr "B<qecvt>(), B<qfcvt>(), B<qgcvt>(): _SVID_SOURCE"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
