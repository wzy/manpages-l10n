# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013, 2016.
# Dmitriy Ovchinnikov <dmitriyxt5@gmail.com>, 2012.
# Dmitry Bolkhovskikh <d20052005@yandex.ru>, 2017.
# Katrin Kutepova <blackkatelv@gmail.com>, 2018.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:59+0200\n"
"PO-Revision-Date: 2019-10-15 18:59+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "umask"
msgstr "umask"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "umask - set file mode creation mask"
msgstr "umask - устанавливает маску создания режима доступа к файлу"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/stat.hE<gt>>\n"
msgstr "B<#include E<lt>sys/stat.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<mode_t umask(mode_t >I<mask>B<);>\n"
msgstr "B<mode_t umask(mode_t >I<mask>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<umask>()  sets the calling process's file mode creation mask (umask) to "
"I<mask> & 0777 (i.e., only the file permission bits of I<mask> are used), "
"and returns the previous value of the mask."
msgstr ""
"B<umask>() устанавливает в вызывающем процессе значение маски (umask) "
"создания режима доступа к файлу равным I<mask> & 0777 (т.е. из I<mask> "
"используются только биты прав доступа к файлу) и возвращает предыдущее "
"значение маски."

#.  e.g., mkfifo(), creat(), mknod(), sem_open(), mq_open(), shm_open()
#.  but NOT the System V IPC *get() calls
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The umask is used by B<open>(2), B<mkdir>(2), and other system calls that "
"create files to modify the permissions placed on newly created files or "
"directories.  Specifically, permissions in the umask are turned off from the "
"I<mode> argument to B<open>(2)  and B<mkdir>(2)."
msgstr ""
"Значение umask используется в B<open>(2), B<mkdir>(2) и других системных "
"вызовах, которые создают файлы, для изменения прав, назначаемых на "
"создаваемые файлы или каталоги. В частности, права в umask исключаются из "
"аргумента I<mode> у вызовов B<open>(2) и B<mkdir>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Alternatively, if the parent directory has a default ACL (see B<acl>(5)), "
"the umask is ignored, the default ACL is inherited, the permission bits are "
"set based on the inherited ACL, and permission bits absent in the I<mode> "
"argument are turned off.  For example, the following default ACL is "
"equivalent to a umask of 022:"
msgstr ""
"Также, если у родительского каталога указан ACL по умолчанию (смотрите "
"B<acl>(5)), то umask игнорируется, выполняется наследование ACL по "
"умолчанию, бита прав назначаются согласно унаследованному ACL, а биты прав, "
"отсутствующие в аргументе I<mode>, выключаются. Например, следующий ACL по "
"умолчанию эквивалентен umask 022:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "u::rwx,g::r-x,o::r-x\n"
msgstr "u::rwx,g::r-x,o::r-x\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Combining the effect of this default ACL with a I<mode> argument of 0666 (rw-"
"rw-rw-), the resulting file permissions would be 0644 (rw-r--r--)."
msgstr ""
"Объединение эффекта этого ACL по умолчанию с аргументом I<mode> 0666 (rw-rw-"
"rw-) приводит установке прав на файл 0644 (rw-r--r--)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The constants that should be used to specify I<mask> are described in "
"B<inode>(7)."
msgstr ""
"Константы, которые нужно использовать в I<mask>, описаны в B<inode>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The typical default value for the process umask is B<S_IWGRP> | B<S_IWOTH> "
"(octal 022).  In the usual case where the I<mode> argument to B<open>(2)  is "
"specified as:"
msgstr ""
"Типичным значением umask в процессе является B<S_IWGRP> | B<S_IWOTH> "
"(восьмеричное 022). Обычно, когда аргумент I<mode> у B<open>(2) задаётся как:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<S_IRUSR> | B<S_IWUSR> | B<S_IRGRP> | B<S_IWGRP> | B<S_IROTH> | B<S_IWOTH>\n"
msgstr "B<S_IRUSR> | B<S_IWUSR> | B<S_IRGRP> | B<S_IWGRP> | B<S_IROTH> | B<S_IWOTH>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(octal 0666) when creating a new file, the permissions on the resulting file "
"will be:"
msgstr ""
"(восьмеричное 0666) при создании файла, права получившегося файла будут:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<S_IRUSR> | B<S_IWUSR> | B<S_IRGRP> | B<S_IROTH>\n"
msgstr "B<S_IRUSR> | B<S_IWUSR> | B<S_IRGRP> | B<S_IROTH>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "(because 0666 & \\(ti022 = 0644; i.e. rw-r--r--)."
msgid "(because 0666 & \\[ti]022 = 0644; i.e. rw-r--r--)."
msgstr "(так как 0666 & \\(ti022 = 0644; т.е., rw-r--r--)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This system call always succeeds and the previous value of the mask is "
"returned."
msgstr ""
"Данный системный вызов всегда выполняется успешно и возвращает предыдущее "
"значение маски."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, SVr4, 4.3BSD."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A child process created via B<fork>(2)  inherits its parent's umask.  The "
"umask is left unchanged by B<execve>(2)."
msgstr ""
"Дочерний процесс, созданный с помощью B<fork>(2), наследует umask родителя. "
"Значение umask не изменяется при вызове B<execve>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It is impossible to use B<umask>()  to fetch a process's umask without at "
"the same time changing it.  A second call to B<umask>()  would then be "
"needed to restore the umask.  The nonatomicity of these two steps provides "
"the potential for races in multithreaded programs."
msgstr ""
"Невозможно использовать B<umask>() для получения umask процесса без её "
"изменения. Для восстановления umask требуется второй вызов B<umask>(). "
"Неатомарность этих двух шагов приводит к появлению состязательности в "
"многонитевых программах."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Since Linux 4.7, the umask of any process can be viewed via the I<Umask> "
"field of I</proc/>pidI</status>.  Inspecting this field in I</proc/self/"
"status> allows a process to retrieve its umask without at the same time "
"changing it."
msgstr ""
"Начиная с Linux 4.7 значение umask любого процесса доступно в поле I<Umask> "
"файла I</proc/>pidI</status>. Просмотр этого поля в I</proc/self/status> "
"позволяет процессу узнать свою umask без её изменения."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The umask setting also affects the permissions assigned to POSIX IPC objects "
"(B<mq_open>(3), B<sem_open>(3), B<shm_open>(3)), FIFOs (B<mkfifo>(3)), and "
"UNIX domain sockets (B<unix>(7))  created by the process.  The umask does "
"not affect the permissions assigned to System\\ V IPC objects created by the "
"process (using B<msgget>(2), B<semget>(2), B<shmget>(2))."
msgstr ""
"Настройка umask также влияет на права, назначаемые IPC-объектам POSIX "
"(B<mq_open>(3), B<sem_open>(3), B<shm_open>(3)), FIFO (B<mkfifo>(3)) и "
"доменным сокетам UNIX (B<unix>(7)), создаваемых процессом. Значение umask не "
"влияет на права, назначаемые IPC-объектам System\\ V, создаваемых процессом "
"(с помощью B<msgget>(2), B<semget>(2), B<shmget>(2))."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<chmod>(2), B<mkdir>(2), B<open>(2), B<stat>(2), B<acl>(5)"
msgstr "B<chmod>(2), B<mkdir>(2), B<open>(2), B<stat>(2), B<acl>(5)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "UMASK"
msgstr "UMASK"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr "B<#include E<lt>sys/types.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>sys/stat.hE<gt>>"
msgstr "B<#include E<lt>sys/stat.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<mode_t umask(mode_t >I<mask>B<);>"
msgstr "B<mode_t umask(mode_t >I<mask>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "    u::rwx,g::r-x,o::r-x\n"
msgstr "    u::rwx,g::r-x,o::r-x\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The typical default value for the process umask is I<S_IWGRP\\ |\\ S_IWOTH> "
"(octal 022).  In the usual case where the I<mode> argument to B<open>(2)  is "
"specified as:"
msgstr ""
"Типичным значением umask в процессе является I<S_IWGRP\\ |\\ S_IWOTH> "
"(восьмеричное 022). Обычно, когда аргумент I<mode> у B<open>(2) задаётся как:"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH\n"
msgstr "S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH\n"
msgstr "S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "(because 0666 & ~022 = 0644; i.e., rw-r--r--)."
msgstr "(так как 0666 & ~022 = 0644; т.е., rw-r--r--)."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Since Linux 4.7, the umask of any process can be viewed via the I<Umask> "
"field of I</proc/[pid]/status>.  Inspecting this field in I</proc/self/"
"status> allows a process to retrieve its umask without at the same time "
"changing it."
msgstr ""
"Начиная с Linux 4.7 значение umask любого процесса доступно в поле I<Umask> "
"файла I</proc/[pid]/status>. Просмотр этого поля в I</proc/self/status> "
"позволяет процессу узнать свою umask без её изменения."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
