# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2013-2014, 2017.
# Dmitriy S. Seregin <dseregin@59.ru>, 2013.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017, 2019.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:32+0200\n"
"PO-Revision-Date: 2019-10-05 08:17+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "inotify_init"
msgstr "inotify_init"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "inotify_init, inotify_init1 - initialize an inotify instance"
msgstr "inotify_init, inotify_init1 - инициализирует экземпляр inotify"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/inotify.hE<gt>>\n"
msgstr "B<#include E<lt>sys/inotify.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int inotify_init(void);>\n"
"B<int inotify_init1(int >I<flags>B<);>\n"
msgstr ""
"B<int inotify_init(void);>\n"
"B<int inotify_init1(int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For an overview of the inotify API, see B<inotify>(7)."
msgstr "Обзор программного интерфейса inotify смотрите в B<inotify>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<inotify_init>()  initializes a new inotify instance and returns a file "
"descriptor associated with a new inotify event queue."
msgstr ""
"B<inotify_init>() инициализирует новый экземпляр inotify и возвращает "
"файловый дескриптор, связанный с очередью событий нового inotify."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<flags> is 0, then B<inotify_init1>()  is the same as "
"B<inotify_init>().  The following values can be bitwise ORed in I<flags> to "
"obtain different behavior:"
msgstr ""
"Если I<flags> равен 0, то B<inotify_init1>() выполняет тоже, что и "
"B<inotify_init>(). Чтобы получить другое поведение, следующие значения могут "
"быть сложены с помощью побитового ИЛИ в I<flags>:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<IN_NONBLOCK>"
msgstr "B<IN_NONBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the B<O_NONBLOCK> file status flag on the open file description (see "
"B<open>(2))  referred to by the new file descriptor.  Using this flag saves "
"extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""
"Устанавливает флаг состояния файла B<O_NONBLOCK> для нового открытого "
"файлового описания (смотрите B<open>(2)), на которое ссылается новый "
"файловый дескриптор. Использование данного флага делает ненужными "
"дополнительные вызовы B<fcntl>(2) для достижения того же результата."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<IN_CLOEXEC>"
msgstr "B<IN_CLOEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Set the close-on-exec (B<FD_CLOEXEC>)  flag on the new file descriptor.  See "
"the description of the B<O_CLOEXEC> flag in B<open>(2)  for reasons why this "
"may be useful."
msgstr ""
"Устанавливает флаг close-on-exec (B<FD_CLOEXEC>) для нового открытого "
"файлового дескриптора. Смотрите описание флага B<O_CLOEXEC> в B<open>(2) для "
"того, чтобы узнать как это может пригодиться."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On success, these system calls return a new file descriptor.  On error, -1 "
"is returned, and I<errno> is set to indicate the error."
msgstr ""
"При успешном выполнении данные системные вызовы возвращают новый файловый "
"дескриптор. При ошибке возвращается -1 и I<errno> устанавливается в "
"соответствующее значение."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "(B<inotify_init1>())  An invalid value was specified in I<flags>."
msgstr "(B<inotify_init1>()) Указано неверное значение в I<flags>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The user limit on the total number of inotify instances has been reached."
msgstr ""
"Достигнуто максимальное количество экземпляров inotify доступных "
"пользователю."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"Было достигнуто ограничение по количеству открытых файловых дескрипторов на "
"процесс."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr "Достигнуто максимальное количество открытых файлов в системе."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Insufficient kernel memory is available."
msgstr "Не хватает памяти ядра."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "inotify_init"
msgid "B<inotify_init>()"
msgstr "inotify_init"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.6.13, glibc 2.4."
msgstr ""

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "inotify_init"
msgid "B<inotify_init1>()"
msgstr "inotify_init"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux 2.6.27, glibc 2.9."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<inotify_add_watch>(2), B<inotify_rm_watch>(2), B<inotify>(7)"
msgstr "B<inotify_add_watch>(2), B<inotify_rm_watch>(2), B<inotify>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 декабря 2022 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, fuzzy
#| msgid ""
#| "B<inotify_init>()  first appeared in Linux 2.6.13; library support was "
#| "added to glibc in version 2.4.  B<inotify_init1>()  was added in Linux "
#| "2.6.27; library support was added to glibc in version 2.9."
msgid ""
"B<inotify_init>()  first appeared in Linux 2.6.13; library support was added "
"in glibc 2.4.  B<inotify_init1>()  was added in Linux 2.6.27; library "
"support was added in glibc 2.9."
msgstr ""
"Системный вызов B<inotify_init>() впервые появился в Linux 2.6.13; поддержка "
"в glibc доступна с версии 2.4. Системный вызов B<inotify_init1>() был "
"добавлен в Linux 2.6.27; поддержка в glibc доступна с версии 2.9."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "These system calls are Linux-specific."
msgstr "Данные системные вызовы есть только в Linux."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "INOTIFY_INIT"
msgstr "INOTIFY_INIT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "Set the B<O_NONBLOCK> file status flag on the open file description (see "
#| "B<open>(2))  referred to by the new file descriptor.  Using this flag "
#| "saves extra calls to B<fcntl>(2)  to achieve the same result."
msgid ""
"Set the B<O_NONBLOCK> file status flag on the new open file description.  "
"Using this flag saves extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""
"Устанавливает флаг состояния файла B<O_NONBLOCK> для нового открытого "
"файлового описания (смотрите B<open>(2)), на которое ссылается новый "
"файловый дескриптор. Использование данного флага делает ненужными "
"дополнительные вызовы B<fcntl>(2) для достижения того же результата."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<inotify_init>()  first appeared in Linux 2.6.13; library support was added "
"to glibc in version 2.4.  B<inotify_init1>()  was added in Linux 2.6.27; "
"library support was added to glibc in version 2.9."
msgstr ""
"Системный вызов B<inotify_init>() впервые появился в Linux 2.6.13; поддержка "
"в glibc доступна с версии 2.4. Системный вызов B<inotify_init1>() был "
"добавлен в Linux 2.6.27; поддержка в glibc доступна с версии 2.9."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
