# Russian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Alexander Golubev <fatzer2@gmail.com>, 2018.
# Azamat Hackimov <azamat.hackimov@gmail.com>, 2011, 2014-2016.
# Hotellook, 2014.
# Nikita <zxcvbnm3230@mail.ru>, 2014.
# Spiros Georgaras <sng@hellug.gr>, 2016.
# Vladislav <ivladislavefimov@gmail.com>, 2015.
# Yuri Kozlov <yuray@komyakino.ru>, 2011-2019.
# Иван Павлов <pavia00@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:51+0200\n"
"PO-Revision-Date: 2019-10-15 18:55+0300\n"
"Last-Translator: Yuri Kozlov <yuray@komyakino.ru>\n"
"Language-Team: Russian <man-pages-ru-talks@lists.sourceforge.net>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || "
"(n%100>=11 && n%100<=14)? 2 : 3);\n"
"X-Generator: Lokalize 2.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "signal"
msgstr "signal"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 марта 2023 г."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "ИМЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "signal - ANSI C signal handling"
msgstr "signal - работа с сигналами ANSI C"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "СИНТАКСИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<typedef void (*sighandler_t)(int);>\n"
msgstr "B<typedef void (*sighandler_t)(int);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>\n"
msgstr "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИСАНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The behavior of B<signal>()  varies across UNIX versions, and has also "
#| "varied historically across different versions of Linux.  B<Avoid its "
#| "use>: use B<sigaction>(2)  instead.  See I<Portability> below."
msgid ""
"B<WARNING>: the behavior of B<signal>()  varies across UNIX versions, and "
"has also varied historically across different versions of Linux.  B<Avoid "
"its use>: use B<sigaction>(2)  instead.  See I<Portability> below."
msgstr ""
"Работа вызова B<signal>() различается в различных версиях UNIX, и такая же "
"ситуация исторически сложилась и в различных версиях Linux. B<Не используйте "
"его>: используйте вместо него B<sigaction>(2). Смотрите абзац "
"I<Переносимость> далее."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<signal>()  sets the disposition of the signal I<signum> to I<handler>, "
"which is either B<SIG_IGN>, B<SIG_DFL>, or the address of a programmer-"
"defined function (a \"signal handler\")."
msgstr ""
"Вызов B<signal>() устанавливает обработчик сигнала с номером I<signum> в "
"соответствии с параметром I<handler>, который может быть равен B<SIG_IGN>, "
"B<SIG_DFL> или адресу функции пользователя (\"обработчик сигнала\")."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the signal I<signum> is delivered to the process, then one of the "
"following happens:"
msgstr "Если сигнал I<signum> доставляется процессу, то происходит следующее:"

#. #-#-#-#-#  archlinux: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-bookworm: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  debian-unstable: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-38: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-rawhide: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  mageia-cauldron: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  opensuse-leap-15-5: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: IP
#. #-#-#-#-#  opensuse-tumbleweed: signal.2.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If the disposition is set to B<SIG_IGN>, then the signal is ignored."
msgstr "Если значение обработчика равно B<SIG_IGN>, то сигнал игнорируется."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the disposition is set to B<SIG_DFL>, then the default action associated "
"with the signal (see B<signal>(7))  occurs."
msgstr ""
"Если значение обработчика равно B<SIG_DFL>, то выполняется стандартное "
"действие, связанное с сигналом (см. B<signal>(7))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If the disposition is set to a function, then first either the disposition "
"is reset to B<SIG_DFL>, or the signal is blocked (see I<Portability> below), "
"and then I<handler> is called with argument I<signum>.  If invocation of the "
"handler caused the signal to be blocked, then the signal is unblocked upon "
"return from the handler."
msgstr ""
"Если значение обработчика равно адресу функции, то сначала значение "
"обработчика сбрасывается в B<SIG_DFL> или сигнал блокируется (см. "
"I<Переносимость> далее), а затем вызывается функция I<handler> с аргументом "
"I<signum>. Если вызов обработчика приводит к блокировке сигнала, то сигнал "
"разблокируется после возврата из обработчика."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The signals B<SIGKILL> and B<SIGSTOP> cannot be caught or ignored."
msgstr ""
"Сигналы B<SIGKILL> и B<SIGSTOP> не могут быть перехвачены или игнорированы."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<signal>()  returns the previous value of the signal handler, or "
#| "B<SIG_ERR> on error.  In the event of an error, I<errno> is set to "
#| "indicate the cause."
msgid ""
"B<signal>()  returns the previous value of the signal handler.  On failure, "
"it returns B<SIG_ERR>, and I<errno> is set to indicate the error."
msgstr ""
"Вызов B<signal>() возвращает предыдущее значение обработчика сигнала или "
"B<SIG_ERR> при ошибке. В случае ошибки в I<errno> указывается номер ошибки."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ОШИБКИ"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<signum> is invalid."
msgstr "Неверное значение I<signum>."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "ВЕРСИИ"

#.  libc4 and libc5 define
#.  .IR SignalHandler ;
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The use of I<sighandler_t> is a GNU extension, exposed if B<_GNU_SOURCE> is "
"defined; glibc also defines (the BSD-derived)  I<sig_t> if B<_BSD_SOURCE> "
"(glibc 2.19 and earlier)  or B<_DEFAULT_SOURCE> (glibc 2.19 and later)  is "
"defined.  Without use of such a type, the declaration of B<signal>()  is the "
"somewhat harder to read:"
msgstr ""
"Используемый I<sighandler_t> является расширением GNU, который определён, "
"если существует макрос B<_GNU_SOURCE>; в glibc также есть I<sig_t>, если "
"определён B<_BSD_SOURCE> (glibc 2.19 и старее) или B<_DEFAULT_SOURCE> (glibc "
"2.19 и новее). Без использования этого типа объявление B<signal>() сложнее "
"читать:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<void ( *>I<signal>B<(int >I<signum>B<, void (*>I<handler>B<)(int)) ) (int);>\n"
msgstr "B<void ( *>I<signal>B<(int >I<signum>B<, void (*>I<handler>B<)(int)) ) (int);>\n"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Portability"
msgstr "Переносимость"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The only portable use of B<signal>()  is to set a signal's disposition to "
"B<SIG_DFL> or B<SIG_IGN>.  The semantics when using B<signal>()  to "
"establish a signal handler vary across systems (and POSIX.1 explicitly "
"permits this variation); B<do not use it for this purpose.>"
msgstr ""
"Вызов B<signal>() считается переносимым, если обработчик сигнала равен "
"B<SIG_DFL> или B<SIG_IGN>. Семантика при использовании B<signal>() для "
"установки обработчика сигнала отличается в различных системах (и в POSIX.1 "
"явно разрешена такая перемена); B<не используйте данный вызов для этой цели.>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1 solved the portability mess by specifying B<sigaction>(2), which "
"provides explicit control of the semantics when a signal handler is invoked; "
"use that interface instead of B<signal>()."
msgstr ""
"В POSIX.1 проблема переносимости решена введением B<sigaction>(2), который "
"предоставляет явное управление семантикой при вызове обработчика сигнала; "
"используйте этот интерфейс вместо B<signal>()."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "СТАНДАРТЫ"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the original UNIX systems, when a handler that was established using "
"B<signal>()  was invoked by the delivery of a signal, the disposition of the "
"signal would be reset to B<SIG_DFL>, and the system did not block delivery "
"of further instances of the signal.  This is equivalent to calling "
"B<sigaction>(2)  with the following flags:"
msgstr ""
"В первых системах UNIX, когда обработчик, установленный с помощью "
"B<signal>(), вызывался по получению сигнала, обработчик сигнала был бы "
"сброшен в B<SIG_DFL>, и система не заблокировала бы доставку этого сигнала в "
"последующие экземпляры."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"
msgstr "sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"System\\ V also provides these semantics for B<signal>().  This was bad "
"because the signal might be delivered again before the handler had a chance "
"to reestablish itself.  Furthermore, rapid deliveries of the same signal "
"could result in recursive invocations of the handler."
msgstr ""
"System\\ V также предоставляет эту семантику B<signal>(). Это плохо, так как "
"сигнал может быть доставлен снова, до того как обработчик сможет получить "
"шанс его переустановить. Кроме того, скоростные доставки одного сигнала "
"приводили к рекурсивным вызовам обработчика."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"BSD improved on this situation, but unfortunately also changed the semantics "
"of the existing B<signal>()  interface while doing so.  On BSD, when a "
"signal handler is invoked, the signal disposition is not reset, and further "
"instances of the signal are blocked from being delivered while the handler "
"is executing.  Furthermore, certain blocking system calls are automatically "
"restarted if interrupted by a signal handler (see B<signal>(7)).  The BSD "
"semantics are equivalent to calling B<sigaction>(2)  with the following "
"flags:"
msgstr ""
"В BSD улучшили эту ситуацию, но, к сожалению, изменили существующую "
"семантику для установки обработчика с помощью B<signal>(). В BSD при вызове "
"обработчика сигнала обработчик сигнала не сбрасывается, и дальнейшие "
"экземпляры сигнала блокируются и не доставляются пока выполняется "
"обработчик. Кроме этого, некоторые блокирующие системные вызовы "
"автоматически перезапускаются при прерывании обработчиком сигнала (смотрите "
"B<signal>(7)). Семантика BSD эквивалентна вызову B<sigaction>(2) со "
"следующими флагами:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "sa.sa_flags = SA_RESTART;\n"
msgstr "sa.sa_flags = SA_RESTART;\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The situation on Linux is as follows:"
msgstr "Ситуация в Linux:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The kernel's B<signal>()  system call provides System\\ V semantics."
msgstr "Системный вызов ядра B<signal>() предоставляет семантику System\\ V."

#.  System V semantics are also provided if one uses the separate
#.  .BR sysv_signal (3)
#.  function.
#.  .IP *
#.  The
#.  .BR signal ()
#.  function in Linux libc4 and libc5 provide System\ V semantics.
#.  If one on a libc5 system includes
#.  .I <bsd/signal.h>
#.  instead of
#.  .IR <signal.h> ,
#.  then
#.  .BR signal ()
#.  provides BSD semantics.
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"By default, in glibc 2 and later, the B<signal>()  wrapper function does not "
"invoke the kernel system call.  Instead, it calls B<sigaction>(2)  using "
"flags that supply BSD semantics.  This default behavior is provided as long "
"as a suitable feature test macro is defined: B<_BSD_SOURCE> on glibc 2.19 "
"and earlier or B<_DEFAULT_SOURCE> in glibc 2.19 and later.  (By default, "
"these macros are defined; see B<feature_test_macros>(7)  for details.)  If "
"such a feature test macro is not defined, then B<signal>()  provides "
"System\\ V semantics."
msgstr ""
"По умолчанию в glibc 2 и новее обёрточная функция B<signal>() не вызывает "
"системный вызов ядра. Вместо этого она вызывает B<sigaction>(2) с флагами, "
"которые активируют семантику BSD. Такое поведение по умолчанию "
"устанавливается, если определён макрос тестирования свойств B<_BSD_SOURCE> в "
"glibc 2.19 и старее или B<_DEFAULT_SOURCE> в glibc 2.19 и новее (по "
"умолчанию эти макросы определены; подробности смотрите в "
"B<feature_test_macros>(7)) . Если этот макрос тестирования свойств не "
"определён, B<signal>() работает согласно семантики System\\ V."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The effects of B<signal>()  in a multithreaded process are unspecified."
msgstr ""
"Результат работы B<signal>() в многонитевом процессе не регламентирован."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"According to POSIX, the behavior of a process is undefined after it ignores "
"a B<SIGFPE>, B<SIGILL>, or B<SIGSEGV> signal that was not generated by "
"B<kill>(2)  or B<raise>(3).  Integer division by zero has undefined result.  "
"On some architectures it will generate a B<SIGFPE> signal.  (Also dividing "
"the most negative integer by -1 may generate B<SIGFPE>.)  Ignoring this "
"signal might lead to an endless loop."
msgstr ""
"В соответствии с POSIX поведение процесса после игнорирования сигнала "
"B<SIGFPE>, B<SIGILL> или B<SIGSEGV> не определено, если эти сигналы не были "
"посланы при помощи функций B<kill>(2) или B<raise>(3). Деление целого числа "
"на ноль имеет непредсказуемый результат. В некоторых архитектурах это "
"приводит к появлению сигнала B<SIGFPE>. (Также, деление самого большого по "
"модулю отрицательного числа на -1 тоже может приводить к B<SIGFPE>.) "
"Игнорирование этого сигнала может привести к появлению бесконечного цикла."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"See B<sigaction>(2)  for details on what happens when the disposition "
"B<SIGCHLD> is set to B<SIG_IGN>."
msgstr ""
"О том, что происходит когда обработчик B<SIGCHLD> приравнивается к "
"B<SIG_IGN>, смотрите на странице B<sigaction>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"See B<signal-safety>(7)  for a list of the async-signal-safe functions that "
"can be safely called from inside a signal handler."
msgstr ""
"Список асинхронных функций работы с сигналами, которые можно безопасно "
"вызывать из обработчика сигналов, смотрите на странице B<signal-safety>(7)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "СМ. ТАКЖЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<kill>(1), B<alarm>(2), B<kill>(2), B<pause>(2), B<sigaction>(2), "
"B<signalfd>(2), B<sigpending>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<bsd_signal>(3), B<killpg>(3), B<raise>(3), B<siginterrupt>(3), "
"B<sigqueue>(3), B<sigsetops>(3), B<sigvec>(3), B<sysv_signal>(3), "
"B<signal>(7)"
msgstr ""
"B<kill>(1), B<alarm>(2), B<kill>(2), B<pause>(2), B<sigaction>(2), "
"B<signalfd>(2), B<sigpending>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<bsd_signal>(3), B<killpg>(3), B<raise>(3), B<siginterrupt>(3), "
"B<sigqueue>(3), B<sigsetops>(3), B<sigvec>(3), B<sysv_signal>(3), "
"B<signal>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 февраля 2023 г."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C99."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "SIGNAL"
msgstr "SIGNAL"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 сентября 2017 г."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Руководство программиста Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>signal.hE<gt>>"
msgstr "B<#include E<lt>signal.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<typedef void (*sighandler_t)(int);>"
msgstr "B<typedef void (*sighandler_t)(int);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"
msgstr "B<sighandler_t signal(int >I<signum>B<, sighandler_t >I<handler>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The behavior of B<signal>()  varies across UNIX versions, and has also "
"varied historically across different versions of Linux.  B<Avoid its use>: "
"use B<sigaction>(2)  instead.  See I<Portability> below."
msgstr ""
"Работа вызова B<signal>() различается в различных версиях UNIX, и такая же "
"ситуация исторически сложилась и в различных версиях Linux. B<Не используйте "
"его>: используйте вместо него B<sigaction>(2). Смотрите абзац "
"I<Переносимость> далее."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<signal>()  returns the previous value of the signal handler, or B<SIG_ERR> "
"on error.  In the event of an error, I<errno> is set to indicate the cause."
msgstr ""
"Вызов B<signal>() возвращает предыдущее значение обработчика сигнала или "
"B<SIG_ERR> при ошибке. В случае ошибки в I<errno> указывается номер ошибки."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "СООТВЕТСТВИЕ СТАНДАРТАМ"

#. type: Plain text
#: opensuse-leap-15-5
msgid "POSIX.1-2001, POSIX.1-2008, C89, C99."
msgstr "POSIX.1-2001, POSIX.1-2008, C89, C99."

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "    sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"
msgstr "    sa.sa_flags = SA_RESETHAND | SA_NODEFER;\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "    sa.sa_flags = SA_RESTART;\n"
msgstr "    sa.sa_flags = SA_RESTART;\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "ЗАМЕЧАНИЯ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Эта страница является частью проекта Linux I<man-pages> версии 4.16. "
"Описание проекта, информацию об ошибках и последнюю версию этой страницы "
"можно найти по адресу \\%https://www.kernel.org/doc/man-pages/."
