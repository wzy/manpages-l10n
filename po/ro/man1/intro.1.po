# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Laurențiu Buzdugan <lbuz@rolix.org>, 2004.
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.16\n"
"POT-Creation-Date: 2023-06-27 19:32+0200\n"
"PO-Revision-Date: 2023-05-04 17:17+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "intro"
msgstr "intro"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2023-02-05"
msgstr "5 februarie 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "intro - introduction to user commands"
msgstr "intro - introducere la comenzile utilizatorului"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Section 1 of the manual describes user commands and tools, for example, file "
"manipulation tools, shells, compilers, web browsers, file and image viewers "
"and editors, and so on."
msgstr ""
"Secțiunea 1 a manualului descrie comenzile și instrumentele utilizatorului, "
"de exemplu, instrumente de manipulare a fișierelor, shell-uri, compilatoare, "
"navigatoare web, vizualizatoare și editoare de fișiere și imagini și așa mai "
"departe."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Linux is a flavor of UNIX, and as a first approximation all user commands "
"under UNIX work precisely the same under Linux (and FreeBSD and lots of "
"other UNIX-like systems)."
msgstr ""
"Linux este o savoare a UNIX și, ca primă aproximare, toate comenzile "
"utilizatorului sub UNIX funcționează exact la fel sub Linux (și FreeBSD și "
"multe alte sisteme asemănătoare UNIX-ului)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Under Linux, there are GUIs (graphical user interfaces), where you can point "
"and click and drag, and hopefully get work done without first reading lots "
"of documentation.  The traditional UNIX environment is a CLI (command line "
"interface), where you type commands to tell the computer what to do.  That "
"is faster and more powerful, but requires finding out what the commands "
"are.  Below a bare minimum, to get started."
msgstr ""
"Sub Linux există GUI („graphical user interfaces”, adică interfețe grafice "
"pentru utilizator), unde puteți interacționa cu aplicațiile folosind mausul "
"(indica, clic și tragere) și care sperăm să vă ajute să vă rezolvați "
"problemele fără a citi o tonă de documentație.  Mediul tradițional Unix este "
"CLI („command line interface”, adică interfață de linie de comandă), unde "
"tastați comenzi pentru a-i spune calculatorului ce să facă.  Aceasta din "
"urmă este mai rapidă și mai puternică, dar trebuie să știți (sau să "
"descoperiți) ce comenzi sunt disponibile.  În cele ce urmează, vă este "
"prezentat strictul necesar pentru a interacționa cu sistemul."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Login"
msgstr "Autentificarea"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In order to start working, you probably first have to open a session by "
"giving your username and password.  The program B<login>(1)  now starts a "
"I<shell> (command interpreter) for you.  In case of a graphical login, you "
"get a screen with menus or icons and a mouse click will start a shell in a "
"window.  See also B<xterm>(1)."
msgstr ""
"Pentru a începe să lucrați, probabil că va trebui mai întâi să deschideți o "
"sesiune, autentificându-vă față de calculator, furnizând numele dvs. de "
"utilizator și parola.  Programul B<login>(1) va porni pentru dvs. un "
"I<shell> (interpret de comenzi).  În cazul unei autentificări grafice, vă va "
"fi prezentat un ecran cu meniuri sau pictograme, iar un clic cu mausul va "
"porni un shell într-o fereastră. Vedeți și B<xterm>(1)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "The shell"
msgstr "Shell-ul"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"One types commands to the I<shell>, the command interpreter.  It is not "
"built-in, but is just a program and you can change your shell.  Everybody "
"has their own favorite one.  The standard one is called I<sh>.  See also "
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<zsh>(1)."
msgstr ""
"Utilizatorul tastează comenzi în I<shell>, interpretul de comenzi.  Acesta "
"nu este ceva intrinsec sistemului, ci este doar un program pe care puteți să-"
"l înlocuiți.  Fiecare persoană are un shell favorit.  Shell-ul standard se "
"numește I<sh>.  Vedeți și B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), "
"B<dash>(1), B<ksh>(1), B<zsh>(1)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "A session might go like:"
msgstr "O sesiune ar putea arăta astfel:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"knuth login: B<aeb>\n"
"Password: B<********>\n"
"$ B<date>\n"
"Tue Aug  6 23:50:44 CEST 2002\n"
"$ B<cal>\n"
"     August 2002\n"
"Su Mo Tu We Th Fr Sa\n"
"             1  2  3\n"
" 4  5  6  7  8  9 10\n"
"11 12 13 14 15 16 17\n"
"18 19 20 21 22 23 24\n"
"25 26 27 28 29 30 31\n"
msgstr ""
"poarta-stelară login: B<gabriel>\n"
"Password: B<********>\n"
"$ B<date>\n"
"joi 4 mai 2023, 10:10:22 +0300\n"
"$ B<cal>\n"
"     mai 2023\n"
"Lu Ma Mi Jo Vi Sb Du\n"
" 1  2  3  4  5  6  7\n"
" 8  9 10 11 12 13 14\n"
"15 16 17 18 19 20 21\n"
"22 23 24 25 26 27 28\n"
"29 30 31\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<ls>\n"
"bin  tel\n"
"$ B<ls -l>\n"
"total 2\n"
"drwxrwxr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-rw-r--   1 aeb         37 Aug  6 23:52 tel\n"
"$ B<cat tel>\n"
"maja    0501-1136285\n"
"peter   0136-7399214\n"
"$ B<cp tel tel2>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:52 tel\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:53 tel2\n"
"$ B<mv tel tel1>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x   2 aeb       1024 Aug  6 23:51 bin\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:52 tel1\n"
"-rw-r--r--   1 aeb         37 Aug  6 23:53 tel2\n"
"$ B<diff tel1 tel2>\n"
"$ B<rm tel1>\n"
"$ B<grep maja tel2>\n"
"maja    0501-1136285\n"
"$\n"
msgstr ""
"$ B<ls>\n"
"bin  listă-tel.txt\n"
"$ B<ls -l>\n"
"total 2\n"
"drwxr-xr-x  3 gabriel gabriel     4096 apr 17 17:14  bin\n"
"-rw-r--r--  1 gabriel gabriel       39 mar  4  2023  listă-tel.txt\n"
"$ B<cat listă-tel.txt>\n"
"maria    +34-915758696\n"
"daniel   +40-212055792\n"
"$ B<cp listă-tel.txt listă-tel2.txt>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x  3 gabriel gabriel     4096 apr 17 17:14  bin\n"
"-rw-r--r--  1 gabriel gabriel       39 mar  4  2023  listă-tel.txt\n"
"-rw-r--r--  1 gabriel gabriel       39 mai  4  2023  listă-tel2.txt\n"
"$ B<mv listă-tel.txt listă-tel1.txt>\n"
"$ B<ls -l>\n"
"total 3\n"
"drwxr-xr-x  3 gabriel gabriel     4096 apr 17 17:14  bin\n"
"-rw-r--r--  1 gabriel gabriel       39 mai  4  2023  listă-tel1.txt\n"
"-rw-r--r--  1 gabriel gabriel       39 mai  4  2023  listă-tel2.txt\n"
"$ B<diff listă-tel1.txt listă-tel2.txt>\n"
"$ B<rm listă-tel1.txt>\n"
"$ B<grep maria listă-tel2.txt>\n"
"maria    +34-915758696\n"
"$\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Here typing Control-D ended the session."
msgstr "Sesiunea este terminată apăsând combinația de taste I<Control-D>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The B<$> here was the command prompt\\[em]it is the shell's way of "
"indicating that it is ready for the next command.  The prompt can be "
"customized in lots of ways, and one might include stuff like username, "
"machine name, current directory, time, and so on.  An assignment PS1=\"What "
"next, master? \" would change the prompt as indicated."
msgstr ""
"Caracterul B<$> a fost promptul de comandă \\[em] este modul shell-ului de a "
"indica faptul că este gata pentru următoarea comandă.  Promptul poate fi "
"personalizat în nenumărate moduri și unul ar putea include lucruri precum "
"numele de utilizator, numele mașinii, directorul curent, data și ora și așa "
"mai departe.  O instrucțiune PS1=\"Ce urmează, maestre?\" ar schimba "
"promptul așa cum este indicat."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"We see that there are commands I<date> (that gives date and time), and "
"I<cal> (that gives a calendar)."
msgstr ""
"În sesiunea de mai sus puteți vedea că există comenzi cum ar fi I<date> "
"(care raportează data și ora) și I<cal> (care vă prezintă un calendar)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The command I<ls> lists the contents of the current directory\\[em]it tells "
"you what files you have.  With a I<-l> option it gives a long listing, that "
"includes the owner and size and date of the file, and the permissions people "
"have for reading and/or changing the file.  For example, the file \"tel\" "
"here is 37 bytes long, owned by aeb and the owner can read and write it, "
"others can only read it.  Owner and permissions can be changed by the "
"commands I<chown> and I<chmod>."
msgstr ""
"Comanda I<ls> listează/prezintă conținutul directorului curent \\[em] vă "
"spune ce fișiere aveți.  Cu opțiunea B<-l>, comanda vă prezintă o listă "
"amănunțită, care include proprietarul, dimensiunea și data fișierelor, "
"precum și permisiunile pe care utilizatorii le au pentru a citi și/sau "
"modifica fișierele.  De exemplu, fișierul „listă-tel.txt” în cazul de față "
"are 39 de octeți, este proprietatea lui gabriel, care îl poate citi și "
"scrie, în vreme ce alții îl pot doar citi.  Proprietarul și permisiunile pot "
"fi schimbate cu comenzile I<chown> și I<chmod>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<cat> will show the contents of a file.  (The name is from "
"\"concatenate and print\": all files given as parameters are concatenated "
"and sent to \"standard output\" (see B<stdout>(3)), here the terminal "
"screen.)"
msgstr ""
"Comanda I<cat> va afișa conținutul unui fișier.  Numele I<cat> vine de la "
"„concatenate and print” (concatenează și afișează): toate fișierele "
"furnizate ca parametri sunt concatenate și trimise la „ieșirea standard”, în "
"cazul de față ecranul terminalului (a se vedea B<stdout>(3))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<cp> (from \"copy\") will copy a file."
msgstr "Comanda I<cp> ce vine de la „copy” (copiază) va copia un fișier."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<mv> (from \"move\"), on the other hand, only renames it."
msgstr ""
"Pe de altă parte, comanda I<mv> ce vine de la „move” (mută) doar redenumește "
"fișierul."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<diff> lists the differences between two files.  Here there was "
"no output because there were no differences."
msgstr ""
"Comanda I<diff> listează diferențele dintre două fișiere.  În sesiunea "
"prezentată ca exemplu, «diff» nu a raportat nimic pentru că nu a existat "
"nici o diferență."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<rm> (from \"remove\") deletes the file, and be careful! it is "
"gone.  No wastepaper basket or anything.  Deleted means lost."
msgstr ""
"Comanda I<rm> ce vine de la „remove” (îndepărtează/elimină) șterge fișierul, "
"așa că fiți atenți cu această comandă.  Nu există nici un coș de gunoi sau "
"ceva similar din care să-l recuperați.  Șters înseamnă pierdut!"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<grep> (from \"g/re/p\") finds occurrences of a string in one "
"or more files.  Here it finds Maja's telephone number."
msgstr ""
"Comanda I<grep> ce vine de la „g/re/p” (căutare globală de expresii "
"regulate) găsește un șir de caractere în unul sau mai multe fișiere.  În "
"exemplul de mai sus I<grep> găsește numărul de telefon al Mariei."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Pathnames and the current directory"
msgstr "Nume de rute și directorul curent"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Files live in a large tree, the file hierarchy.  Each has a I<pathname> "
"describing the path from the root of the tree (which is called I</>)  to the "
"file.  For example, such a full pathname might be I</home/aeb/tel>.  Always "
"using full pathnames would be inconvenient, and the name of a file in the "
"current directory may be abbreviated by giving only the last component.  "
"That is why I</home/aeb/tel> can be abbreviated to I<tel> when the current "
"directory is I</home/aeb>."
msgstr ""
"Fișierele se află într-un arbore larg organizat ierarhic.  Fiecare dintre "
"fișiere are un I<rută de nume> ce descrie ruta de la rădăcina arborelui "
"(care este numit I</>) până la fișier.  De exemplu, un asemenea nume de rută "
"poate fi I</home/gabriel/listă-tel.txt>.  Ar fi inconvenient dacă ar trebui "
"specificată întotdeauna întreaga rută; prin urmare numele unui fișier în "
"directorul curent poate fi abreviat dând numai ultima componentă.  Din acest "
"motiv I</home/gabriel/listă-tel.txt> poate fi abreviat ca I<listă-tel.txt> "
"atunci când directorul curent este I</home/gabriel>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<pwd> prints the current directory."
msgstr "Comanda I<pwd> afișează directorul curent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<cd> changes the current directory."
msgstr "Comanda I<cd> schimbă directorul curent."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Try alternatively I<cd> and I<pwd> commands and explore I<cd> usage: \"cd\", "
"\"cd .\", \"cd ..\", \"cd /\", and \"cd \\[ti]\"."
msgstr ""
"Încercați alternativ comenzile I<cd> și I<pwd> și explorați utilizarea "
"I<cd>: «cd», «cd .», «cd ..», «cd /» și «cd \\[ti]»."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Directories"
msgstr "Directoare"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The command I<mkdir> makes a new directory."
msgstr "Comanda I<mkdir> crează un director nou."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<rmdir> removes a directory if it is empty, and complains "
"otherwise."
msgstr ""
"Comanda I<rmdir> îndepărtează/elimină un director dacă acesta este gol și "
"raportează o eroare în caz contrar."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<find> (with a rather baroque syntax) will find files with "
"given name or other properties.  For example, \"find . -name tel\" would "
"find the file I<tel> starting in the present directory (which is called I<."
">).  And \"find / -name tel\" would do the same, but starting at the root of "
"the tree.  Large searches on a multi-GB disk will be time-consuming, and it "
"may be better to use B<locate>(1)."
msgstr ""
"Comanda I<find> (cu o sintaxă destul de complicată) va găsi fișierele cu un "
"anumit nume sau anumite proprietăți.  De exemplu, «find . -name listă-tel."
"txt» va găsi fișierul „listă-tel.txt” începând în directorul curent (care "
"este numit I<.>). Iar «find / -name listă-tel.txt» ar face același lucru, "
"dar începând de la rădăcina arborelui.  Căutările pe discuri de mai mulți Go "
"pot lua un timp îndelungat așa că ar fi mai bine să folosiți B<locate>(1)."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Disks and filesystems"
msgstr "Discuri și sisteme de fișiere"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The command I<mount> will attach the filesystem found on some disk (or "
"floppy, or CDROM or so)  to the big filesystem hierarchy.  And I<umount> "
"detaches it again.  The command I<df> will tell you how much of your disk is "
"still free."
msgstr ""
"Comanda I<mount> va atașa sistemul de fișiere găsit pe un disc (sau dischetă "
"sau CDROM sau altele) la ierarhia întregului sistem de fișiere.  Iar "
"I<umount> îl detașează din nou.  Comanda I<df> vă va indica cât spațiu liber "
"mai este disponibil."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Processes"
msgstr "Procese"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On a UNIX system many user and system processes run simultaneously.  The one "
"you are talking to runs in the I<foreground>, the others in the "
"I<background>.  The command I<ps> will show you which processes are active "
"and what numbers these processes have.  The command I<kill> allows you to "
"get rid of them.  Without option this is a friendly request: please go "
"away.  And \"kill -9\" followed by the number of the process is an immediate "
"kill.  Foreground processes can often be killed by typing Control-C."
msgstr ""
"Pe un sistem UNIX, multe procese de utilizator și sistem rulează simultan.  "
"Cel cu care interacționați dvs. rulează în I<prim-plan> (foreground), iar "
"celelalte rulează în I<fundal> (background).  Comanda I<ps> vă va arăta care "
"procese sunt active și care sunt numerele acestora.  Comanda I<kill> vă "
"permite să scăpați de acestea.  Fără vreo opțiune, aceasta este o cerere "
"prietenească: te rog termină.  Dar comanda \"kill -9\" urmată de numărul "
"procesului este o terminare imediată.  Procesele din prim-plan pot fi adesea "
"terminate apăsând «Control-C»."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Getting information"
msgstr "Obținerea de informații"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"There are thousands of commands, each with many options.  Traditionally "
"commands are documented on I<man pages>, (like this one), so that the "
"command \"man kill\" will document the use of the command \"kill\" (and "
"\"man man\" document the command \"man\").  The program I<man> sends the "
"text through some I<pager>, usually I<less>.  Hit the space bar to get the "
"next page, hit q to quit."
msgstr ""
"Pe un sistem Linux există mii de comenzi, fiecare dintre acestea cu multe "
"opțiuni.  Tradițional, comenzile sunt documentate în I<paginile de manual>, "
"(cum este și aceasta).  De exemplu, comanda «man kill» vă va arăta cum poate "
"fi folosită comanda «kill» (iar comanda «man man» documentează comanda "
"«man»).  Programul I<man> trimite textul printr-un I<paginator>, care de "
"obicei este I<less>.  Apăsați tasta de spațiu pentru a vedea pagina "
"următoare sau apăsați tasta q pentru a termina."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In documentation it is customary to refer to man pages by giving the name "
"and section number, as in B<man>(1).  Man pages are terse, and allow you to "
"find quickly some forgotten detail.  For newcomers an introductory text with "
"more examples and explanations is useful."
msgstr ""
"În documentație se obișnuiește să se facă referire la paginile de manual "
"prin indicarea numelui și numărului secțiunii, ca în B<man>(1).  Paginile "
"man sunt sumare și vă permit să găsiți rapid un detaliu pe care l-ați "
"uitat.  Pentru noii veniți ar fi folositoare o carte introductivă cu exemple "
"și explicații suplimentare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A lot of GNU/FSF software is provided with info files.  Type \"info info\" "
"for an introduction on the use of the program I<info>."
msgstr ""
"Multe dintre programele GNU/FSF sunt furnizate cu fișiere info.  Tastați "
"«info info» pentru o introducere în folosirea programului I<info>."

#
#.  Actual examples? Separate section for each of cat, cp, ...?
#.  gzip, bzip2, tar, rpm
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Special topics are often treated in HOWTOs.  Look in I</usr/share/doc/howto/"
"en> and use a browser if you find HTML files there."
msgstr ""
"Subiecte speciale sunt adesea tratate în documente HOWTO (cum să).  Uitați-"
"vă în I</usr/share/doc/howto/ro> (atenție, ruta aceasta este orientativă, "
"depinzând de distribuția folosită și/sau de pachetele instalate în ea) și "
"folosiți un navigator dacă găsiți fișiere HTML acolo."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<locate>(1), B<login>(1), B<man>(1), B<xterm>(1), B<zsh>(1), B<wait>(2), "
"B<stdout>(3), B<man-pages>(7), B<standards>(7)"
msgstr ""
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<locate>(1), B<login>(1), B<man>(1), B<xterm>(1), B<zsh>(1), B<wait>(2), "
"B<stdout>(3), B<man-pages>(7), B<standards>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "INTRO"
msgstr "INTRO"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2015-07-23"
msgstr "23 iulie 2015"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux User's Manual"
msgstr "Manualul utilizatorului Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"One types commands to the I<shell>, the command interpreter.  It is not "
"built-in, but is just a program and you can change your shell.  Everybody "
"has her own favorite one.  The standard one is called I<sh>.  See also "
"B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), B<dash>(1), B<ksh>(1), "
"B<zsh>(1)."
msgstr ""
"Utilizatorul tastează comenzi în I<shell>, interpretul de comenzi.  Acesta "
"nu este ceva intrinsec sistemului, ci este doar un program pe care puteți să-"
"l înlocuiți.  Fiecare persoană are un shell favorit.  Shell-ul standard se "
"numește I<sh>.  Vedeți și B<ash>(1), B<bash>(1), B<chsh>(1), B<csh>(1), "
"B<dash>(1), B<ksh>(1), B<zsh>(1)."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<$> here was the command prompt\\(emit is the shell's way of indicating "
"that it is ready for the next command.  The prompt can be customized in lots "
"of ways, and one might include stuff like username, machine name, current "
"directory, time, and so on.  An assignment PS1=\"What next, master? \" would "
"change the prompt as indicated."
msgstr ""
"Caracterul B<$> a fost promptul de comandă \\[em] este modul shell-ului de a "
"indica faptul că este gata pentru următoarea comandă.  Promptul poate fi "
"personalizat în nenumărate moduri și unul ar putea include lucruri precum "
"numele de utilizator, numele mașinii, directorul curent, data și ora și așa "
"mai departe.  O instrucțiune PS1=\"Ce urmează, maestre?\" ar schimba "
"promptul așa cum este indicat."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The command I<ls> lists the contents of the current directory\\(emit tells "
"you what files you have.  With a I<-l> option it gives a long listing, that "
"includes the owner and size and date of the file, and the permissions people "
"have for reading and/or changing the file.  For example, the file \"tel\" "
"here is 37 bytes long, owned by aeb and the owner can read and write it, "
"others can only read it.  Owner and permissions can be changed by the "
"commands I<chown> and I<chmod>."
msgstr ""
"Comanda I<ls> listează/prezintă conținutul directorului curent \\[em] vă "
"spune ce fișiere aveți.  Cu opțiunea B<-l>, comanda vă prezintă o listă "
"amănunțită, care include proprietarul, dimensiunea și data fișierelor, "
"precum și permisiunile pe care utilizatorii le au pentru a citi și/sau "
"modifica fișierele.  De exemplu, fișierul „listă-tel.txt” în cazul de față "
"are 39 de octeți, este proprietatea lui gabriel, care îl poate citi și "
"scrie, în vreme ce alții îl pot doar citi.  Proprietarul și permisiunile pot "
"fi schimbate cu comenzile I<chown> și I<chmod>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Try alternatively I<cd> and I<pwd> commands and explore I<cd> usage: \"cd\", "
"\"cd .\", \"cd ..\", \"cd /\" and \"cd ~\"."
msgstr ""
"Încercați alternativ comenzile I<cd> și I<pwd> și explorați utilizarea "
"I<cd>: «cd», «cd .», «cd ..», «cd /» și «cd ~»."

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."
