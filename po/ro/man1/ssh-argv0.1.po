# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:52+0200\n"
"PO-Revision-Date: 2023-07-01 21:37+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: Dd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 7, 2001"
msgstr "7 septembrie 2001"

#. type: Dt
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SSH-ARGV0 1"
msgstr "SSH-ARGV0 1"

#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Debian"
msgstr "Debian"

#. type: Os
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Project"
msgstr "Proiect"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Nm ssh-argv0>"
msgstr "E<.Nm ssh-argv0>"

#. type: Nd
#: debian-bookworm debian-unstable
#, no-wrap
msgid "replaces the old ssh command-name as hostname handling"
msgstr "înlocuiește vechea comandă «ssh command-name» ca manipulare a numelui de gazdă"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Ar hostname | user@hostname> E<.Op Fl l Ar login_name> E<.Op Ar command>"
msgstr ""
"E<.Ar nume-gazdă | utilizator@nume-gazdă> E<.Op Fl l Ar nume-utilizator-"
"autentificare> E<.Op Ar comanda>"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Ar hostname | user@hostname> E<.Op Fl afgknqstvxACNTX1246> E<.Op Fl b Ar "
"bind_address> E<.Op Fl c Ar cipher_spec> E<.Op Fl e Ar escape_char> E<.Op Fl "
"i Ar identity_file> E<.Op Fl l Ar login_name> E<.Op Fl m Ar mac_spec> E<.Op "
"Fl o Ar option> E<.Op Fl p Ar port> E<.Op Fl F Ar configfile> E<.Oo Fl L Xo> "
"E<.Sm off> E<.Ar port>: E<.Ar host>: E<.Ar hostport> E<.Sm on> E<.Xc> E<.Oc> "
"E<.Oo Fl R Xo> E<.Sm off> E<.Ar port>: E<.Ar host>: E<.Ar hostport> E<.Sm "
"on> E<.Xc> E<.Oc> E<.Op Fl D Ar port> E<.Op Ar command>"
msgstr ""
"E<.Ar nume-gazdă | utilizator@nume-gazdă> E<.Op Fl afgknqstvxACNTX1246> E<."
"Op Fl b Ar adresă-bind> E<.Op Fl c Ar specificare-cifru> E<.Op Fl e Ar "
"caracter-eludare> E<.Op Fl i Ar fișier-identificare> E<.Op Fl l Ar nume-"
"utilizator-autentificare> E<.Op Fl m Ar specificare-mac> E<.Op Fl o Ar "
"opțiune> E<.Op Fl p Ar port> E<.Op Fl F Ar fișier-configurare> E<.Oo Fl L "
"Xo> E<.Sm off> E<.Ar port>: E<.Ar gazda>: E<.Ar port-gazdă> E<.Sm on> E<.Xc> "
"E<.Oc> E<.Oo Fl R Xo> E<.Sm off> E<.Ar port>: E<.Ar gazda>: E<.Ar port-"
"gazdă> E<.Sm on> E<.Xc> E<.Oc> E<.Op Fl D Ar port> E<.Op Ar comanda>"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"E<.Nm> replaces the old ssh command-name as hostname handling.  If you link "
"to this script with a hostname then executing the link is equivalent to "
"having executed ssh with that hostname as an argument.  All other arguments "
"are passed to ssh and will be processed normally."
msgstr ""
"E<.Nm> înlocuiește vechea comandă «ssh command-name» ca manipulare a numelui "
"de gazdă.  Dacă creați o legătură către acest script cu un nume de gazdă, "
"atunci executarea legăturii este echivalentă cu executarea ssh cu acel nume "
"de gazdă ca argument.  Toate celelalte argumente sunt transmise către ssh și "
"vor fi procesate în mod normal."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPȚIUNI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "See E<.Xr ssh 1>."
msgstr "Consultați E<.Xr ssh 1>."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"OpenSSH is a derivative of the original and free ssh 1.2.12 release by Tatu "
"Ylonen.  Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos, Theo de "
"Raadt and Dug Song removed many bugs, re-added newer features and created "
"OpenSSH.  Markus Friedl contributed the support for SSH protocol versions "
"1.5 and 2.0.  Natalie Amery wrote this ssh-argv0 script and the associated "
"documentation."
msgstr ""
"OpenSSH este un derivat al versiunii originale și gratuite ssh 1.2.12 de "
"Tatu Ylonen.  Aaron Campbell, Bob Beck, Markus Friedl, Niels Provos, Theo de "
"Raadt și Dug Song au eliminat multe erori, au adăugat din nou caracteristici "
"noi și au creat OpenSSH.  Markus Friedl a contribuit la suportul pentru "
"versiunile 1.5 și 2.0 ale protocolului SSH.  Natalie Amery a scris acest "
"script ssh-argv0 și documentația asociată."

#. type: Sh
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "E<.Xr ssh 1>"
msgstr "E<.Xr ssh 1>"
