# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:59+0200\n"
"PO-Revision-Date: 2023-07-02 14:22+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "undocumented"
msgstr "undocumented"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2022-10-30"
msgstr "30 octombrie 2022"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Pagini de manual de Linux 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "undocumented - undocumented library functions"
msgstr "undocumented - funcții de bibliotecă nedocumentate"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "REZUMAT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Undocumented library functions\n"
msgstr "Funcții de bibliotecă nedocumentate\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This man page mentions those library functions which are implemented in the "
"standard libraries but not yet documented in man pages."
msgstr ""
"Această pagină de manual menționează acele funcții de bibliotecă care sunt "
"implementate în bibliotecile standard, dar care nu sunt încă documentate în "
"paginile de manual."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Solicitation"
msgstr "Solicitare"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If you have information about these functions, please look in the source "
"code, write a man page (using a style similar to that of the other Linux "
"section 3 man pages), and send it to B<mtk.manpages@gmail.com> for inclusion "
"in the next man page release."
msgstr ""
"Dacă aveți informații despre aceste funcții, vă rugăm să căutați în codul "
"sursă, să scrieți o pagină de manual (folosind un stil similar cu cel al "
"celorlalte pagini de manual din secțiunea 3 din Linux) și să o trimiteți la "
"B<mtk.manpages@gmail.com> pentru a fi inclusă în următoarea versiune a "
"paginii de manual."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "The list"
msgstr "Lista"

#.  .BR chflags (3),
#.  .BR fattach (3),
#.  .BR fchflags (3),
#.  .BR fclean (3),
#.  .BR fdetach (3),
#.  .BR obstack stuff (3),
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<authdes_create>(3), B<authdes_getucred>(3), B<authdes_pk_create>(3), "
"B<clntunix_create>(3), B<creat64>(3), B<dn_skipname>(3), B<fcrypt>(3), "
"B<fp_nquery>(3), B<fp_query>(3), B<fp_resstat>(3), B<freading>(3), "
"B<freopen64>(3), B<fseeko64>(3), B<ftello64>(3), B<ftw64>(3), B<fwscanf>(3), "
"B<get_avphys_pages>(3), B<getdirentries64>(3), B<getmsg>(3), "
"B<getnetname>(3), B<get_phys_pages>(3), B<getpublickey>(3), "
"B<getsecretkey>(3), B<h_errlist>(3), B<host2netname>(3), B<hostalias>(3), "
"B<inet_nsap_addr>(3), B<inet_nsap_ntoa>(3), B<init_des>(3), "
"B<libc_nls_init>(3), B<mstats>(3), B<netname2host>(3), B<netname2user>(3), "
"B<nlist>(3), B<obstack_free>(3), B<parse_printf_format>(3), B<p_cdname>(3), "
"B<p_cdnname>(3), B<p_class>(3), B<p_fqname>(3), B<p_option>(3), "
"B<p_query>(3), B<printf_size>(3), B<printf_size_info>(3), B<p_rr>(3), "
"B<p_time>(3), B<p_type>(3), B<putlong>(3), B<putshort>(3), "
"B<re_compile_fastmap>(3), B<re_compile_pattern>(3), "
"B<register_printf_function>(3), B<re_match>(3), B<re_match_2>(3), "
"B<re_rx_search>(3), B<re_search>(3), B<re_search_2>(3), "
"B<re_set_registers>(3), B<re_set_syntax>(3), B<res_send_setqhook>(3), "
"B<res_send_setrhook>(3), B<ruserpass>(3), B<setfileno>(3), "
"B<sethostfile>(3), B<svc_exit>(3), B<svcudp_enablecache>(3), B<tell>(3), "
"B<thrd_create>(3), B<thrd_current>(3), B<thrd_equal>(3), B<thrd_sleep>(3), "
"B<thrd_yield>(3), B<tr_break>(3), B<tzsetwall>(3), B<ufc_dofinalperm>(3), "
"B<ufc_doit>(3), B<user2netname>(3), B<wcschrnul>(3), B<wcsftime>(3), "
"B<wscanf>(3), B<xdr_authdes_cred>(3), B<xdr_authdes_verf>(3), "
"B<xdr_cryptkeyarg>(3), B<xdr_cryptkeyres>(3), B<xdr_datum>(3), "
"B<xdr_des_block>(3), B<xdr_domainname>(3), B<xdr_getcredres>(3), "
"B<xdr_keybuf>(3), B<xdr_keystatus>(3), B<xdr_mapname>(3), "
"B<xdr_netnamestr>(3), B<xdr_netobj>(3), B<xdr_passwd>(3), "
"B<xdr_peername>(3), B<xdr_rmtcall_args>(3), B<xdr_rmtcallres>(3), "
"B<xdr_unixcred>(3), B<xdr_yp_buf>(3), B<xdr_yp_inaddr>(3), "
"B<xdr_ypbind_binding>(3), B<xdr_ypbind_resp>(3), B<xdr_ypbind_resptype>(3), "
"B<xdr_ypbind_setdom>(3), B<xdr_ypdelete_args>(3), B<xdr_ypmaplist>(3), "
"B<xdr_ypmaplist_str>(3), B<xdr_yppasswd>(3), B<xdr_ypreq_key>(3), "
"B<xdr_ypreq_nokey>(3), B<xdr_ypresp_all>(3), B<xdr_ypresp_all_seq>(3), "
"B<xdr_ypresp_key_val>(3), B<xdr_ypresp_maplist>(3), B<xdr_ypresp_master>(3), "
"B<xdr_ypresp_order>(3), B<xdr_ypresp_val>(3), B<xdr_ypstat>(3), "
"B<xdr_ypupdate_args>(3), B<yp_all>(3), B<yp_bind>(3), B<yperr_string>(3), "
"B<yp_first>(3), B<yp_get_default_domain>(3), B<yp_maplist>(3), "
"B<yp_master>(3), B<yp_match>(3), B<yp_next>(3), B<yp_order>(3), "
"B<ypprot_err>(3), B<yp_unbind>(3), B<yp_update>(3)"
msgstr ""
"B<authdes_create>(3), B<authdes_getucred>(3), B<authdes_pk_create>(3), "
"B<clntunix_create>(3), B<creat64>(3), B<dn_skipname>(3), B<fcrypt>(3), "
"B<fp_nquery>(3), B<fp_query>(3), B<fp_resstat>(3), B<freading>(3), "
"B<freopen64>(3), B<fseeko64>(3), B<ftello64>(3), B<ftw64>(3), B<fwscanf>(3), "
"B<get_avphys_pages>(3), B<getdirentries64>(3), B<getmsg>(3), "
"B<getnetname>(3), B<get_phys_pages>(3), B<getpublickey>(3), "
"B<getsecretkey>(3), B<h_errlist>(3), B<host2netname>(3), B<hostalias>(3), "
"B<inet_nsap_addr>(3), B<inet_nsap_ntoa>(3), B<init_des>(3), "
"B<libc_nls_init>(3), B<mstats>(3), B<netname2host>(3), B<netname2user>(3), "
"B<nlist>(3), B<obstack_free>(3), B<parse_printf_format>(3), B<p_cdname>(3), "
"B<p_cdnname>(3), B<p_class>(3), B<p_fqname>(3), B<p_option>(3), "
"B<p_query>(3), B<printf_size>(3), B<printf_size_info>(3), B<p_rr>(3), "
"B<p_time>(3), B<p_type>(3), B<putlong>(3), B<putshort>(3), "
"B<re_compile_fastmap>(3), B<re_compile_pattern>(3), "
"B<register_printf_function>(3), B<re_match>(3), B<re_match_2>(3), "
"B<re_rx_search>(3), B<re_search>(3), B<re_search_2>(3), "
"B<re_set_registers>(3), B<re_set_syntax>(3), B<res_send_setqhook>(3), "
"B<res_send_setrhook>(3), B<ruserpass>(3), B<setfileno>(3), "
"B<sethostfile>(3), B<svc_exit>(3), B<svcudp_enablecache>(3), B<tell>(3), "
"B<thrd_create>(3), B<thrd_current>(3), B<thrd_equal>(3), B<thrd_sleep>(3), "
"B<thrd_yield>(3), B<tr_break>(3), B<tzsetwall>(3), B<ufc_dofinalperm>(3), "
"B<ufc_doit>(3), B<user2netname>(3), B<wcschrnul>(3), B<wcsftime>(3), "
"B<wscanf>(3), B<xdr_authdes_cred>(3), B<xdr_authdes_verf>(3), "
"B<xdr_cryptkeyarg>(3), B<xdr_cryptkeyres>(3), B<xdr_datum>(3), "
"B<xdr_des_block>(3), B<xdr_domainname>(3), B<xdr_getcredres>(3), "
"B<xdr_keybuf>(3), B<xdr_keystatus>(3), B<xdr_mapname>(3), "
"B<xdr_netnamestr>(3), B<xdr_netobj>(3), B<xdr_passwd>(3), "
"B<xdr_peername>(3), B<xdr_rmtcall_args>(3), B<xdr_rmtcallres>(3), "
"B<xdr_unixcred>(3), B<xdr_yp_buf>(3), B<xdr_yp_inaddr>(3), "
"B<xdr_ypbind_binding>(3), B<xdr_ypbind_resp>(3), B<xdr_ypbind_resptype>(3), "
"B<xdr_ypbind_setdom>(3), B<xdr_ypdelete_args>(3), B<xdr_ypmaplist>(3), "
"B<xdr_ypmaplist_str>(3), B<xdr_yppasswd>(3), B<xdr_ypreq_key>(3), "
"B<xdr_ypreq_nokey>(3), B<xdr_ypresp_all>(3), B<xdr_ypresp_all_seq>(3), "
"B<xdr_ypresp_key_val>(3), B<xdr_ypresp_maplist>(3), B<xdr_ypresp_master>(3), "
"B<xdr_ypresp_order>(3), B<xdr_ypresp_val>(3), B<xdr_ypstat>(3), "
"B<xdr_ypupdate_args>(3), B<yp_all>(3), B<yp_bind>(3), B<yperr_string>(3), "
"B<yp_first>(3), B<yp_get_default_domain>(3), B<yp_maplist>(3), "
"B<yp_master>(3), B<yp_match>(3), B<yp_next>(3), B<yp_order>(3), "
"B<ypprot_err>(3), B<yp_unbind>(3), B<yp_update>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Pagini de manual de Linux 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "UNDOCUMENTED"
msgstr "UNDOCUMENTED"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembrie 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manualul programatorului Linux"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Undocumented library functions"
msgstr "Funcții de bibliotecă nedocumentate"

#.  .BR chflags (3),
#.  .BR fattach (3),
#.  .BR fchflags (3),
#.  .BR fclean (3),
#.  .BR fdetach (3),
#.  .BR obstack stuff (3),
#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<authdes_create>(3), B<authdes_getucred>(3), B<authdes_pk_create>(3), "
"B<clntunix_create>(3), B<creat64>(3), B<dn_skipname>(3), B<fcrypt>(3), "
"B<fp_nquery>(3), B<fp_query>(3), B<fp_resstat>(3), B<freading>(3), "
"B<freopen64>(3), B<fseeko64>(3), B<ftello64>(3), B<ftw64>(3), B<fwscanf>(3), "
"B<get_avphys_pages>(3), B<getdirentries64>(3), B<getmsg>(3), "
"B<getnetname>(3), B<get_phys_pages>(3), B<getpublickey>(3), "
"B<getsecretkey>(3), B<h_errlist>(3), B<host2netname>(3), B<hostalias>(3), "
"B<inet_nsap_addr>(3), B<inet_nsap_ntoa>(3), B<init_des>(3), "
"B<libc_nls_init>(3), B<mstats>(3), B<netname2host>(3), B<netname2user>(3), "
"B<nlist>(3), B<obstack_free>(3), B<parse_printf_format>(3), B<p_cdname>(3), "
"B<p_cdnname>(3), B<p_class>(3), B<p_fqname>(3), B<p_option>(3), "
"B<p_query>(3), B<printf_size>(3), B<printf_size_info>(3), B<p_rr>(3), "
"B<p_time>(3), B<p_type>(3), B<putlong>(3), B<putshort>(3), "
"B<re_compile_fastmap>(3), B<re_compile_pattern>(3), "
"B<register_printf_function>(3), B<re_match>(3), B<re_match_2>(3), "
"B<re_rx_search>(3), B<re_search>(3), B<re_search_2>(3), "
"B<re_set_registers>(3), B<re_set_syntax>(3), B<res_send_setqhook>(3), "
"B<res_send_setrhook>(3), B<ruserpass>(3), B<setfileno>(3), "
"B<sethostfile>(3), B<svc_exit>(3), B<svcudp_enablecache>(3), B<tell>(3), "
"B<tr_break>(3), B<tzsetwall>(3), B<ufc_dofinalperm>(3), B<ufc_doit>(3), "
"B<user2netname>(3), B<wcschrnul>(3), B<wcsftime>(3), B<wscanf>(3), "
"B<xdr_authdes_cred>(3), B<xdr_authdes_verf>(3), B<xdr_cryptkeyarg>(3), "
"B<xdr_cryptkeyres>(3), B<xdr_datum>(3), B<xdr_des_block>(3), "
"B<xdr_domainname>(3), B<xdr_getcredres>(3), B<xdr_keybuf>(3), "
"B<xdr_keystatus>(3), B<xdr_mapname>(3), B<xdr_netnamestr>(3), "
"B<xdr_netobj>(3), B<xdr_passwd>(3), B<xdr_peername>(3), "
"B<xdr_rmtcall_args>(3), B<xdr_rmtcallres>(3), B<xdr_unixcred>(3), "
"B<xdr_yp_buf>(3), B<xdr_yp_inaddr>(3), B<xdr_ypbind_binding>(3), "
"B<xdr_ypbind_resp>(3), B<xdr_ypbind_resptype>(3), B<xdr_ypbind_setdom>(3), "
"B<xdr_ypdelete_args>(3), B<xdr_ypmaplist>(3), B<xdr_ypmaplist_str>(3), "
"B<xdr_yppasswd>(3), B<xdr_ypreq_key>(3), B<xdr_ypreq_nokey>(3), "
"B<xdr_ypresp_all>(3), B<xdr_ypresp_all_seq>(3), B<xdr_ypresp_key_val>(3), "
"B<xdr_ypresp_maplist>(3), B<xdr_ypresp_master>(3), B<xdr_ypresp_order>(3), "
"B<xdr_ypresp_val>(3), B<xdr_ypstat>(3), B<xdr_ypupdate_args>(3), "
"B<yp_all>(3), B<yp_bind>(3), B<yperr_string>(3), B<yp_first>(3), "
"B<yp_get_default_domain>(3), B<yp_maplist>(3), B<yp_master>(3), "
"B<yp_match>(3), B<yp_next>(3), B<yp_order>(3), B<ypprot_err>(3), "
"B<yp_unbind>(3), B<yp_update>(3)"
msgstr ""
"B<authdes_create>(3), B<authdes_getucred>(3), B<authdes_pk_create>(3), "
"B<clntunix_create>(3), B<creat64>(3), B<dn_skipname>(3), B<fcrypt>(3), "
"B<fp_nquery>(3), B<fp_query>(3), B<fp_resstat>(3), B<freading>(3), "
"B<freopen64>(3), B<fseeko64>(3), B<ftello64>(3), B<ftw64>(3), B<fwscanf>(3), "
"B<get_avphys_pages>(3), B<getdirentries64>(3), B<getmsg>(3), "
"B<getnetname>(3), B<get_phys_pages>(3), B<getpublickey>(3), "
"B<getsecretkey>(3), B<h_errlist>(3), B<host2netname>(3), B<hostalias>(3), "
"B<inet_nsap_addr>(3), B<inet_nsap_ntoa>(3), B<init_des>(3), "
"B<libc_nls_init>(3), B<mstats>(3), B<netname2host>(3), B<netname2user>(3), "
"B<nlist>(3), B<obstack_free>(3), B<parse_printf_format>(3), B<p_cdname>(3), "
"B<p_cdnname>(3), B<p_class>(3), B<p_fqname>(3), B<p_option>(3), "
"B<p_query>(3), B<printf_size>(3), B<printf_size_info>(3), B<p_rr>(3), "
"B<p_time>(3), B<p_type>(3), B<putlong>(3), B<putshort>(3), "
"B<re_compile_fastmap>(3), B<re_compile_pattern>(3), "
"B<register_printf_function>(3), B<re_match>(3), B<re_match_2>(3), "
"B<re_rx_search>(3), B<re_search>(3), B<re_search_2>(3), "
"B<re_set_registers>(3), B<re_set_syntax>(3), B<res_send_setqhook>(3), "
"B<res_send_setrhook>(3), B<ruserpass>(3), B<setfileno>(3), "
"B<sethostfile>(3), B<svc_exit>(3), B<svcudp_enablecache>(3), B<tell>(3), "
"B<tr_break>(3), B<tzsetwall>(3), B<ufc_dofinalperm>(3), B<ufc_doit>(3), "
"B<user2netname>(3), B<wcschrnul>(3), B<wcsftime>(3), B<wscanf>(3), "
"B<xdr_authdes_cred>(3), B<xdr_authdes_verf>(3), B<xdr_cryptkeyarg>(3), "
"B<xdr_cryptkeyres>(3), B<xdr_datum>(3), B<xdr_des_block>(3), "
"B<xdr_domainname>(3), B<xdr_getcredres>(3), B<xdr_keybuf>(3), "
"B<xdr_keystatus>(3), B<xdr_mapname>(3), B<xdr_netnamestr>(3), "
"B<xdr_netobj>(3), B<xdr_passwd>(3), B<xdr_peername>(3), "
"B<xdr_rmtcall_args>(3), B<xdr_rmtcallres>(3), B<xdr_unixcred>(3), "
"B<xdr_yp_buf>(3), B<xdr_yp_inaddr>(3), B<xdr_ypbind_binding>(3), "
"B<xdr_ypbind_resp>(3), B<xdr_ypbind_resptype>(3), B<xdr_ypbind_setdom>(3), "
"B<xdr_ypdelete_args>(3), B<xdr_ypmaplist>(3), B<xdr_ypmaplist_str>(3), "
"B<xdr_yppasswd>(3), B<xdr_ypreq_key>(3), B<xdr_ypreq_nokey>(3), "
"B<xdr_ypresp_all>(3), B<xdr_ypresp_all_seq>(3), B<xdr_ypresp_key_val>(3), "
"B<xdr_ypresp_maplist>(3), B<xdr_ypresp_master>(3), B<xdr_ypresp_order>(3), "
"B<xdr_ypresp_val>(3), B<xdr_ypstat>(3), B<xdr_ypupdate_args>(3), "
"B<yp_all>(3), B<yp_bind>(3), B<yperr_string>(3), B<yp_first>(3), "
"B<yp_get_default_domain>(3), B<yp_maplist>(3), B<yp_master>(3), "
"B<yp_match>(3), B<yp_next>(3), B<yp_order>(3), B<ypprot_err>(3), "
"B<yp_unbind>(3), B<yp_update>(3)"

# R-GC, scrie:
# COLOPHON (în eng.) = COLOFON (în rom.)
# *****
# Colofon = Notă la sfârșitul unei publicații sau
# pe verso foii de titlu, cuprinzând datele editoriale.
# sau:
# Colofon =  Însemnare la sfârșitul unei cărți în
# epoca manuscriselor și incunabulelor, cuprinzând
# date privind tipograful, locul unde a lucrat,
# autorul și titlul lucrării.
# sau:
# Colofon = Notă, însemnare finală a unei cărți,
# care reproduce sau completează cele spuse în titlu.
# ===============
# M-am decis pentru:
# COLOFON -- NOTĂ FINALĂ
#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON -- NOTĂ FINALĂ"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Această pagină face parte din versiunea 4.16 a proiectului Linux I<man-"
"pages>.  O descriere a proiectului, informații despre raportarea erorilor și "
"cea mai recentă versiune a acestei pagini pot fi găsite la \\%https://www."
"kernel.org/doc/man-pages/."
