# Romanian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:47+0200\n"
"PO-Revision-Date: 2023-07-06 15:09+0200\n"
"Last-Translator: Remus-Gabriel Chelu <remusgabriel.chelu@disroot.org>\n"
"Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Poedit 3.2.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "sane-as6e"
msgstr "sane-as6e"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "11 Jul 2008"
msgstr "11 iulie 2008"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SANE Scanner Access Now Easy"
msgstr "SANE: „Scanner Access Now Easy”"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NUME"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"sane-as6e - SANE backend for using the Artec AS6E parallel port interface "
"scanner."
msgstr ""
"sane-as6e - controlor SANE pentru utilizarea scanerului cu interfață pentru "
"portul paralel Artec AS6E."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<sane-as6e> library implements a SANE (Scanner Access Now Easy) backend "
"that provides access to Artec AS6E flatbed scanner.  It requires the "
"B<as6edriver> program in order to operate. The B<as6edriver> program is not "
"included with the SANE package. It can be found at I<http://as6edriver."
"sourceforge.net>.  See the as6edriver documentation for technical "
"information."
msgstr ""
"Biblioteca B<sane-as6e> implementează un controlor SANE (Scanner Access Now "
"Easy) care oferă acces la scanerul plat Artec AS6E.  Aceasta necesită "
"programul B<as6edriver> pentru a funcționa. Programul B<as6edriver> nu este "
"inclus în pachetul SANE. Acesta poate fi găsit la adresa I<http://as6edriver."
"sourceforge.net>.  Consultați documentația as6edriver pentru informații "
"tehnice."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<as6edriver> program must be in the path for executables (B<$PATH>).  "
"Especially if you run B<saned>(8)  (the SANE network scanning daemon), take "
"care to setup the path for B<inetd>(8)  or B<xinetd>(8)  correctly or place "
"the program in a directory that is in the path."
msgstr ""
"Programul B<as6edriver> trebuie să se afle în ruta pentru executabile "
"(B<$PATH>).  Mai ales dacă executați B<saned>(8) (demonul de scanare a "
"rețelei SANE), aveți grijă să configurați corect ruta pentru B<inetd>(8) sau "
"B<xinetd>(8) sau să plasați programul într-un director care se află în "
"această rută."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FIȘIERE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<as6edriver> - driver program that controls the scanner."
msgstr "B<as6edriver> - programul de control care controlează scanerul."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "CONSULTAȚI ȘI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<sane>(7), B<as6edriver>(5), B<saned>(8), B<inetd>(8), B<xinetd>(8)"
msgstr "B<sane>(7), B<as6edriver>(5), B<saned>(8), B<inetd>(8), B<xinetd>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<http://as6edriver.sourceforge.net>"
msgstr "I<http://as6edriver.sourceforge.net>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Eugene S. Weiss"
msgstr "Eugene S. Weiss"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EMAIL-CONTACT"
msgstr "ADRESA_DE_EMAIL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<yossarian@users.sourceforge.net>"
msgstr "I<yossarian@users.sourceforge.net>"
