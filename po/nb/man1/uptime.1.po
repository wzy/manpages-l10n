# Norwegian bokmål translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-04-30 21:04+0200\n"
"PO-Revision-Date: 2021-09-03 20:09+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Norwegian bokmål <>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "UPTIME"
msgstr "UPTIME"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: TH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brukerkommandoer"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "uptime - tell how long the system has been running"
msgstr ""

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "OVERSIKT"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<uptime> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]"
msgstr "B<uptime> [I<\\,VALG\\/>] … [I<\\,FIL\\/>]"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Print the current time, the length of time the system has been up, the "
"number of users on the system, and the average number of jobs in the run "
"queue over the last 1, 5 and 15 minutes.  Processes in an uninterruptible "
"sleep state also contribute to the load average.  If FILE is not specified, "
"use I<\\,/var/run/utmp\\/>.  I<\\,/var/log/wtmp\\/> as FILE is common."
msgstr ""
"Skriv ut klokkeslett, hvor lenge systemet har kjørt, antall brukere på "
"systemet og gjennomsnittlig antall jobber i kjørekøen de siste 1, 5 og 15 "
"minuttene. Prosesser som er i sovemodus og ikke kan forstyrres bidrar også "
"til snittbelastning. Hvis FIL ikke er valgt, brukes I<\\,/var/run/utmp\\/>. "
"Det er vanlig å bruke I<\\,/var/log/wtmp\\/> som FIL."

#. type: TP
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "Vis denne hjelpeteksten og avslutt."

#. type: TP
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Vis versjonsinformasjon og avslutt."

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "OPPHAVSMANN"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Joseph Arceneaux, David MacKenzie, and Kaveh Ghazi."
msgstr "Skrevet av Joseph Arceneaux, David MacKenzie og Kaveh Ghazi."

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERING AV FEIL"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Få hjelp til bruk av GNU coreutils på nett: E<lt>https://www.gnu.org/"
"software/coreutils/E<gt>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapporter oversettelsesfeil til E<lt>https://translationproject.org/team/nb."
"htmlE<gt>"

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPPHAVSRETT"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er fri programvare. Du kan endre og dele den videre. Det stilles INGEN "
"GARANTI, i den grad dette tillates av gjeldende lovverk."

#. type: SH
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/uptimeE<gt>"
msgstr ""
"Fullstendig dokumentasjon: E<lt>https://www.gnu.org/software/coreutils/"
"uptimeE<gt>"

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) uptime invocation\\(aq"
msgstr "eller lokalt: info \\(aq(coreutils) uptime invocation\\(aq"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "April 2023"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: Plain text
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Lisens GPLv3+: GNU GPL "
"versjon 3 eller senere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
