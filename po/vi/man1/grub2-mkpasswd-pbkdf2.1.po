# Vietnamese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-05-22 18:01+0200\n"
"PO-Revision-Date: 2022-01-18 19:49+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Vietnamese <>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB-MKPASSWD-PBKDF2"
msgstr "GRUB-MKPASSWD-PBKDF2"

#. type: TH
#: fedora-38
#, no-wrap
msgid "March 2023"
msgstr "Tháng 3 năm 2023"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB 2.06"
msgstr "GRUB 2.06"

#. type: TH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Các câu lệnh"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "TÊN"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "grub-mkpasswd-pbkdf2 - generate hashed password for GRUB"
msgstr ""

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "TÓM TẮT"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-mkpasswd-pbkdf2> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-mkpasswd-pbkdf2> [I<\\,TÙY_CHỌN\\/>…] [I<\\,CÁC-TÙY_CHỌN\\/>]"

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "MÔ TẢ"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Generate PBKDF2 password hash."
msgstr "Tạo mã băm mật khẩu kiểu PBKDF2"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--iteration-count>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--iteration-count>=I<\\,SỐ\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Number of PBKDF2 iterations"
msgstr "Số lần lặp của PBKDF2"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--buflen>=I<\\,NUM\\/>"
msgstr "B<-l>, B<--buflen>=I<\\,SỐ\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Length of generated hash"
msgstr "Độ dài của mã băm tạo ra"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--salt>=I<\\,NUM\\/>"
msgstr "B<-s>, B<--salt>=I<\\,SỐ\\/>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Length of salt"
msgstr "Độ dài của salt"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "give this help list"
msgstr "hiển thị trợ giúp này"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "give a short usage message"
msgstr "hiển thị cách sử dụng dạng ngắn gọn"

#. type: TP
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "print program version"
msgstr "in ra phiên bản chương trình"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Tham số là bắt buộc hay tham số chỉ là tùy chọn cho các tùy chọn dài cũng "
"đồng thời là bắt buộc hay không bắt buộc cho các tùy chọn ngắn tương ứng với "
"nó."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "THÔNG BÁO LỖI"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Hãy thông báo lỗi cho  E<lt>bug-grub@gnu.orgE<gt>. Thông báo lỗi dịch cho: "
"E<lt>http://translationproject.org/team/vi.htmlE<gt>."

#. type: SH
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "XEM THÊM"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"The full documentation for B<grub-mkpasswd-pbkdf2> is maintained as a "
"Texinfo manual.  If the B<info> and B<grub-mkpasswd-pbkdf2> programs are "
"properly installed at your site, the command"
msgstr ""
"Tài liệu hướng dẫn đầy đủ về B<grub-mkpasswd-pbkdf2> được bảo trì dưới dạng "
"một sổ tay Texinfo.  Nếu chương trình B<info> và B<grub-mkpasswd-pbkdf2> "
"được cài đặt đúng ở địa chỉ của bạn thì câu lệnh"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<info grub-mkpasswd-pbkdf2>"
msgstr "B<info grub-mkpasswd-pbkdf2>"

#. type: Plain text
#: fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "nên cho phép bạn truy cập đến toàn bộ sổ tay."

#. type: TH
#: fedora-rawhide opensuse-leap-15-5
#, no-wrap
msgid "April 2023"
msgstr "Tháng 4 năm 2023"

#. type: TH
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "May 2023"
msgstr "Tháng 5 năm 2023"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "grub-mkpasswd-pbkdf2 (GRUB2) 2.06"
msgstr "grub-mkpasswd-pbkdf2 (GRUB2) 2.06"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GRUB2 2.06"
msgstr "GRUB2 2.06"
