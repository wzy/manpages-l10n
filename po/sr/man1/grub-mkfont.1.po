# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-06-27 19:30+0200\n"
"PO-Revision-Date: 2022-07-23 16:01+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-MKFONT"
msgstr "GRUB-MKFONT"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "January 2023"
msgid "June 2023"
msgstr "јануара 2023"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "GRUB 2:2.06.r499.ge67a551a4-1"
msgid "GRUB 2:2.06.r566.g857af0e17-1"
msgstr "ГРУБ 2:2.06.r499.ge67a551a4-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-mkfont - make GRUB font files"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"B<grub-mkfont> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>] I<\\,FONT_FILES\\/>"
msgstr ""
"B<grub-mkfont> [I<\\,ОПЦИЈА\\/>...] [I<\\,ОПЦИЈЕ\\/>] I<\\,"
"ДАТОТЕКЕ_СЛОВОЛИКА\\/>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Convert common font file formats into PF2"
msgstr "Претвара записе датотека општих словних ликова у ПФ2"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-a>, B<--force-autohint>"
msgstr "B<-a>, B<--force-autohint>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "force autohint"
msgstr "приморава самонаговештавање"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-b>, B<--bold>"
msgstr "B<-b>, B<--bold>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "convert to bold font"
msgstr "претвара у подебљани словни лик"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--asce>=I<\\,NUM\\/>"
msgstr "B<-c>, B<--asce>=I<\\,БРОЈ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set font ascent"
msgstr "подешава словни лик узлазно"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-d>, B<--desc>=I<\\,NUM\\/>"
msgstr "B<-d>, B<--desc>=I<\\,БРОЈ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set font descent"
msgstr "подешава словни лик силазно"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-i>, B<--index>=I<\\,NUM\\/>"
msgstr "B<-i>, B<--index>=I<\\,БРОЈ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "select face index"
msgstr "бира индекс изгледа"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--no-bitmap>"
msgstr "B<--no-bitmap>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "ignore bitmap strikes when loading"
msgstr "занемарује наносе битмапе приликом учитавања"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--no-hinting>"
msgstr "B<--no-hinting>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "disable hinting"
msgstr "искључује наговештавање"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-n>, B<--name>=I<\\,NAME\\/>"
msgstr "B<-n>, B<--name>=I<\\,НАЗИВ\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set font family name"
msgstr "поставља назив породице словног лика"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,ДАТОТЕКА\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "save output in FILE [required]"
msgstr "чува излаз у ДАТОТЕКУ [потребно]"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-r>, B<--range>=I<\\,FROM-TO[\\/>,FROM-TO]"
msgstr "B<-r>, B<--range>=I<\\,ОД-ДО[\\/>,ОД-ДО]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set font range"
msgstr "поставља опсег словног лика"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-s>, B<--size>=I<\\,SIZE\\/>"
msgstr "B<-s>, B<--size>=I<\\,ВЕЛИЧИНА\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set font size"
msgstr "поставља величину словног лика"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "исписује опширне поруке."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "-?, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "приказује овај списак помоћи"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "приказује кратку поруку коришћења"

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "исписује издање програма"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Обавезни или изборни аргументи за дуге опције су такође обавезни или изборни "
"за било које одговарајуће кратке опције."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Грешке пријавите на: E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-mkconfig>(8)"
msgstr "B<grub-mkconfig>(8)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-mkfont> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkfont> programs are properly installed "
"at your site, the command"
msgstr ""
"Потпуна документација за B<grub-mkfont> је одржавана као Тексинфо упутство.  "
"Ако су B<info> и B<grub-mkfont> исправно инсталирани на вашем сајту, наредба"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-mkfont>"
msgstr "B<info grub-mkfont>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "треба да вам да приступ потпуном упутству."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "April 2023"
msgstr "Априла 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "ГРУБ 2.06-13"
