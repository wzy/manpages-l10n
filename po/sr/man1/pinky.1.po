# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2023-06-27 19:42+0200\n"
"PO-Revision-Date: 2021-09-03 20:06+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "PINKY"
msgstr "PINKY"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "Априла 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "ГНУ coreutils 9.3"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "pinky - lightweight finger"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<pinky> [I<\\,OPTION\\/>]... [I<\\,USER\\/>]..."
msgstr "B<pinky> [I<\\,ОПЦИЈА\\/>]... [I<\\,КОРИСНИК\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "produce long format output for the specified USERs"
msgstr "даје излаз дугог записа за наведеног КОРИСНИКА"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>"
msgstr "B<-b>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit the user's home directory and shell in long format"
msgstr "изоставља корисников лични директоријум и шкољку у дугом запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>"
msgstr "B<-h>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit the user's project file in long format"
msgstr "изоставља корисникову датотеку пројекта у дугом запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>"
msgstr "B<-p>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit the user's plan file in long format"
msgstr "изоставља корисникову датотеку плана у дугом запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do short format output, this is the default"
msgstr "даје излаз кратког записа, ово је основно"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>"
msgstr "B<-f>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit the line of column headings in short format"
msgstr "изоставља ред заглавља ступца у кратком запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>"
msgstr "B<-w>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit the user's full name in short format"
msgstr "изоставља корисниково име и презиме у кратком запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>"
msgstr "B<-i>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit the user's full name and remote host in short format"
msgstr ""
"изоставља корисниково име и презиме, удаљеног домаћина у кратком запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-q>"
msgstr "B<-q>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "omit the user's full name, remote host and idle time in short format"
msgstr ""
"изоставља корисниково име и презиме, удаљеног домаћина и време мировања у "
"кратком запису"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "приказује ову помоћ и излази"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "исписује податке о издању и излази"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A lightweight 'finger' program; print user information.  The utmp file will "
"be I<\\,/var/run/utmp\\/>."
msgstr ""
"Лаган програм „finger“;  исписује податке о кориснику. Датотека „utmp“ ће "
"бити I<\\,/var/run/utmp\\/>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АУТОР"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Joseph Arceneaux, David MacKenzie, and Kaveh Ghazi."
msgstr "Написали су Јозеф Арсено, Дејвид Мек Кензи и Кавех Гази."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Помоћ на мрежи за ГНУ coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Грешке у преводу пријавите на E<lt>https://translationproject.org/team/sr."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АУТОРСКА ПРАВА"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ово је слободан софтвер: слободни сте да га мењате и расподељујете. Не "
"постоји НИКАКВА ГАРАНЦИЈА, у оквирима дозвољеним законом."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/pinkyE<gt>"
msgstr ""
"Сва документација се налази на E<lt>https://www.gnu.org/software/coreutils/"
"pinkyE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) pinky invocation\\(aq"
msgstr ""
"или је доступна на рачунару путем наредбе „info \\(aq(coreutils) pinky "
"invocation\\(aq“"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "Септембра 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "ГНУ coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-38
#, no-wrap
msgid "January 2023"
msgstr "јануара 2023"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Априла 2022"

#. type: Plain text
#: mageia-cauldron
msgid ""
"A lightweight 'finger' program; print user information.  The utmp file will "
"be I<\\,/run/utmp\\/>."
msgstr ""
"Лаган програм „finger“;  исписује податке о кориснику. Датотека „utmp“ ће "
"бити I<\\,/run/utmp\\/>."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Октобра 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "ГНУ coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
