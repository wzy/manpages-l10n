# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-06-27 20:02+0200\n"
"PO-Revision-Date: 2023-05-23 08:52+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "YUM-CONFIG-MANAGER"
msgstr "YUM-CONFIG-MANAGER"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "Jan 22, 2023"
msgstr "22. Januar 2023"

#. type: TH
#: debian-bookworm debian-unstable mageia-cauldron
#, no-wrap
msgid "4.3.1"
msgstr "4.3.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "dnf-plugins-core"
msgstr "dnf-plugins-core"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "yum-config-manager - redirecting to DNF config-manager Plugin"
msgstr "yum-config-manager - Weiterleitung zum Config-Manager-Plugin von DNF"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Manage main and repository DNF configuration options, toggle which "
"repositories are enabled or disabled, and add new repositories."
msgstr ""
"Kann die Optionen der DNF-Konfiguration verwalten, Paketquellen aktivieren "
"oder deaktivieren und neue Paketquellen hinzufügen."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<dnf config-manager [options] E<lt>sectionE<gt>...>"
msgstr "B<dnf config-manager [Optionen] E<lt>AbschnittE<gt> …>"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "ARGUMENTS"
msgstr "ARGUMENTE"

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<E<lt>sectionE<gt>>"
msgstr "B<E<lt>AbschnittE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"This argument can be used to explicitly select the configuration sections to "
"manage.  A section can either be B<main> or a repoid.  If not specified, the "
"program will select the B<main> section and each repoid used within any B<--"
"setopt> options.  A repoid can be specified using globs."
msgstr ""
"Dieses Argument kann zur expliziten Auswahl der zu verwaltenden "
"Konfigurationsabschnitte verwendet werden. Ein Abschnitt kann entweder als "
"B<main> oder als Paketquellen-ID angegeben werden. Falls nichts angegeben "
"ist, wählt das Programm den Abschnitt B<main> und jede der innerhalb der "
"Option B<--setopt> verwendete Paketquellen-ID. Eine Paketquellen-ID kann "
"mittels Platzhaltern angegeben werden."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"All general DNF options are accepted, see I<Options> in B<dnf(8)> for "
"details."
msgstr ""
"Alle allgemeinen DNF-Optionen werden akzeptiert, siehe I<Optionen> in "
"B<dnf>(8) für Details."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--help-cmd>"
msgstr "B<--help-cmd>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Show this help."
msgstr "zeigt die Hilfe an."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--add-repo=URL>"
msgstr "B<--add-repo=URL>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Add (and enable) the repo from the specified file or url. If it has to be "
"added into installroot, combine it with B<--setopt=reposdir=/"
"E<lt>installrootE<gt>/etc/yum.repos.d> command-line option."
msgstr ""
"fügt die Paketquelle aus der angegebenen Datei oder URL hinzu und aktiviert "
"sie. Falls Sie zur Installationswurzel hinzugefügt werden muss, kombinieren "
"Sie sie mit der Befehlszeilenoption B<--setopt=reposdir=/"
"E<lt>installrootE<gt>/etc/yum.repos.d>."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--dump>"
msgstr "B<--dump>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Print dump of current configuration values to stdout."
msgstr "schreibt die aktuellen Konfigurationswerte in die Standardausgabe."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--set-disabled, --disable>"
msgstr "B<--set-disabled, --disable>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Disable the specified repos (implies B<--save>)."
msgstr "deaktiviert die angegebenen Paketquellen (impliziert B<--save>)."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--set-enabled, --enable>"
msgstr "B<--set-enabled, --enable>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Enable the specified repos (implies B<--save>)."
msgstr "aktiviert die angegebenen Paketquellen (impliziert B<--save>)."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--save>"
msgstr "B<--save>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Save the current options (useful with B<--setopt>)."
msgstr "speichert die aktuellen Optionen (nützlich mit B<--setopt>)."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<--setopt=E<lt>optionE<gt>=E<lt>valueE<gt>>"
msgstr "B<--setopt=E<lt>OptionE<gt>=E<lt>WertE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Set a configuration option. To set configuration options for repositories, "
"use B<repoid.option> for the B<E<lt>optionE<gt>>\\&. Globs are supported in "
"repoid."
msgstr ""
"setzt eine Konfigurationsoption. Um Konfigurationsoptionen für Paketquellen "
"zu setzen, verwenden Sie B<Paketquellen-ID.Option> für die "
"B<E<lt>OptionE<gt>>\\&. In der Paketquellen-ID werden Platzhalter akzeptiert."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "B<WARNING:>"
msgstr "B<WARNUNG:>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"DNF B<config-manager> can misbehave when enabling/disabling repositories "
"generated by tools like B<subscription-manager> on RHEL. In this case you "
"should use B<subscription-manager> to perform such actions."
msgstr ""
"DNF B<config-manager> kann sich falsch verhalten, wenn Paketquellen, die mit "
"Werkzeugen wie B<subscription-manager> auf RHEL erzeugt wurden, aktiviert "
"oder deaktiviert werden. In diesem Falle sollten Sie B<subscription-manager> "
"zur Ausführung solcher Aktionen verwenden."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --add-repo http://example.com/some/additional.repo>"
msgstr "B<dnf config-manager --add-repo http://example.com/eine/zusätzliche/Paketquelle.repo>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Download additional.repo and store it in repodir."
msgstr ""
"lädt Paketquelle.repo herunter und speichert sie im Paketquellenverzeichnis."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --add-repo http://example.com/different/repo>"
msgstr "B<dnf config-manager --add-repo http://example.com/andere/Paketquelle>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Create new repo file with I<\\%http://example.com/different/repo> as baseurl "
"and enable it."
msgstr ""
"erstellt eine neue Paketquellendatei mit I<\\%http://example.com/andere/"
"Paketquelle> als Basis-URL und aktiviert sie."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --dump>"
msgstr "B<dnf config-manager --dump>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Display main DNF configuration."
msgstr "zeigt die DNF-Hauptkonfiguration an."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --dump E<lt>sectionE<gt>>"
msgstr "B<dnf config-manager --dump E<lt>AbschnittE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "Display configuration of a repository identified by E<lt>sectionE<gt>."
msgstr ""
"zeigt die Konfiguration der als E<lt>AbschnittE<gt> angegebenen Paketquelle "
"an."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --set-enabled E<lt>repoidE<gt>>"
msgstr "B<dnf config-manager --set-enabled E<lt>Paketquellen-IDE<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Enable repository identified by E<lt>repoidE<gt> and make the change "
"permanent."
msgstr ""
"aktiviert die als E<lt>Paketquellen-IDE<gt> angegebene Paketquelle und "
"speichert diese Änderung dauerhaft."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --set-disabled E<lt>repoid1E<gt> E<lt>repoid2E<gt>>"
msgstr "B<dnf config-manager --set-disabled E<lt>Paketquellen-ID1E<gt> E<lt>Paketquellen-ID2E<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Disable repositories identified by E<lt>repoid1E<gt> and E<lt>repoid2E<gt>"
msgstr ""
"deaktiviert die als E<lt>Paketquellen-ID1E<gt> und E<lt>Paketquellen-"
"ID2E<gt> angegebenen Paketquellen."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --set-disabled E<lt>repoid1E<gt>,E<lt>repoid2E<gt>>"
msgstr "B<dnf config-manager --set-disabled E<lt>Paketquellen-ID1E<gt>,E<lt>Paketquellen-ID2E<gt>>"

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --save --setopt=*.proxy=http://proxy.example.com:3128/ E<lt>repo1E<gt> E<lt>repo2E<gt>>"
msgstr "B<dnf config-manager --save --setopt=*.proxy=http://proxy.example.com:3128/ E<lt>Paketquelle1E<gt> E<lt>Paketquelle2E<gt>>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Update proxy setting in repositories with repoid E<lt>repo1E<gt> and "
"E<lt>repo2E<gt> and make the change permanent."
msgstr ""
"aktualisiert die Proxy-Einstellung in den als E<lt>Paketquelle1E<gt> "
"E<lt>Paketquelle2E<gt> angegebenen Paketquellen und speichert die Änderung "
"dauerhaft."

#. type: TP
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<dnf config-manager --save --setopt=*-debuginfo.gpgcheck=0>"
msgstr "B<dnf config-manager --save --setopt=*-debuginfo.gpgcheck=0>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid ""
"Update gpgcheck setting in all repositories whose id ends with -debuginfo "
"and make the change permanent."
msgstr ""
"aktualisiert die Einstellung zur GPG-Überprüfung (gpgcheck) in allen "
"Paketquellen, die auf -debuginfo enden und speichert die Änderung dauerhaft."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
msgid "See AUTHORS in your Core DNF Plugins distribution"
msgstr "Siehe AUTHORS im Paket der Core DNF Plugins."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#.  Generated by docutils manpage writer.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 fedora-rawhide
msgid "2023, Red Hat, Licensed under GPLv2+"
msgstr "2023, Red Hat, lizenziert unter GPLv2+"

#. type: TH
#: fedora-38
#, no-wrap
msgid "Apr 05, 2023"
msgstr "5. April 2023"

#. type: TH
#: fedora-38
#, no-wrap
msgid "4.4.0"
msgstr "4.4.0"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "May 17, 2023"
msgstr "17. Mai 2023"

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "4.4.1"
msgstr "4.4.1"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "Sep 26, 2022"
msgstr "26. September 2022"

#.  Generated by docutils manpage writer.
#. type: Plain text
#: mageia-cauldron
msgid "2014, Red Hat, Licensed under GPLv2+"
msgstr "2014, Red Hat, lizenziert unter GPLv2+"
