# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-06-27 19:41+0200\n"
"PO-Revision-Date: 2016-03-09 21:00+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ogginfo"
msgstr "ogginfo"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "July 10, 2002"
msgstr "10. Juli 2002"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Xiph.Org Foundation"
msgstr "Xiph.Org Foundation"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Vorbis Tools"
msgstr "Vorbis Tools"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"ogginfo - gives information about Ogg files, and does extensive validity "
"checking"
msgstr ""
"ogginfo - liefert Informationen zu Ogg-Dateien und führt ausführliche "
"Validitätsprüfungen aus"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ogginfo> [ B<-q> ] [ B<-v> ] [ B<-h> ] I<file1.ogg> B<...> I<fileN.ogg>"
msgstr ""
"B<ogginfo> [ B<-q> ] [ B<-v> ] [ B<-h> ] I<Datei1.ogg> B<…> I<DateiN.ogg>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<ogginfo> reads one or more Ogg files and prints information about stream "
"contents (including chained and/or multiplexed streams) to standard output. "
"It will detect (but not correct) a wide range of common defects, with many "
"additional checks specifically for Ogg Vorbis streams."
msgstr ""
"B<ogginfo> liest eine oder mehrere Ogg-Dateien und gibt Informationen über "
"die Bestandteile des Datenstroms an die Standardausgabe aus (auch für "
"verkettete und/oder gebündelte Datenströme). Es erkennt (korrigiert aber "
"nicht) eine breite Palette häufiger Defekte mit vielen zusätzlichen "
"Überprüfungen speziell für OGG-Vorbis-Datenströme."

# Diverse Bedingungen für häufige Fehler?
# Häufige Bedingungen für diverse Fehler?
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For all stream types B<ogginfo> will print the filename being processed, the "
"stream serial numbers, and various common error conditions."
msgstr ""
"Bei allen Datenstromtypen gibt B<ogginfo> den Namen der verarbeiteten Datei, "
"die Seriennummern der Datenströme und diverse Fehlerbedingungen aus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For B<Vorbis> streams, information including the version used for encoding, "
"the sample rate and number of channels, the bitrate and playback length, and "
"the contents of the comment header are printed."
msgstr ""
"Für B<Vorbis>-Datenströme werden Informationen wie die Version des "
"Kodierers, die Abtastrate, die Anzahl der Kanäle, die Bitrate, die "
"Spieldauer und der Inhalt des Kommentar-Headers ausgegeben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Similarly, for B<Theora> streams, basic information about the video is "
"provided, including frame rate, aspect ratio, bitrate, length, and the "
"comment header."
msgstr ""
"Analog werden bei B<Theora>-Datenströmen grundlegende Informationen über das "
"Video ausgegeben, etwa Einzelbildrate, Seitenverhältnis, Bitrate, Länge und "
"der Inhalt des Kommentar-Headers."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-h"
msgstr "-h"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Show a help and usage message."
msgstr "zeigt Hilfe zur Benutzung an."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-q"
msgstr "-q"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Quiet mode. This may be specified multiple times. Doing so once will remove "
"the detailed informative messages, twice will remove warnings as well."
msgstr ""
"Stiller Modus. Dies kann mehrmals angegeben werden. Die einmalige Angabe "
"entfernt detaillierte Informationsausgaben, bei zweimaliger Angabe werden "
"außerdem Warnungen unterdrückt."

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "-v"
msgstr "-v"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Verbose mode. At the current time, this does not do anything."
msgstr "Ausführlicher Modus. Gegenwärtig hat diese Option keine Wirkung."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Michael Smith E<lt>msmith@xiph.orgE<gt>"
msgstr "Michael Smith E<lt>msmith@xiph.orgE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<vorbiscomment>(1), B<ogg123>(1), B<oggdec>(1), B<oggenc>(1)"
msgstr "B<vorbiscomment>(1), B<ogg123>(1), B<oggdec>(1), B<oggenc>(1)"
