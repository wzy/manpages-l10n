# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2023-06-27 19:30+0200\n"
"PO-Revision-Date: 2023-05-23 08:47+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB-RENDER-LABEL"
msgstr "GRUB-RENDER-LABEL"

#. type: TH
#: archlinux
#, no-wrap
msgid "June 2023"
msgstr "Juni 2023"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r566.g857af0e17-1"
msgstr "GRUB 2:2.06.r566.g857af0e17-1"

#. type: TH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "grub-render-label - generate a .disk_label for Apple Macs."
msgstr "grub-render-label - eine .disk_label-Datei für Apple Macs erzeugen."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<grub-render-label> [I<\\,OPTION\\/>...] [I<\\,OPTIONS\\/>]"
msgstr "B<grub-render-label> [I<\\,OPTION\\/>…] [I<\\,OPTIONEN\\/>]"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Render Apple .disk_label."
msgstr "Erzeugt Apple-.disk_label."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-b>, B<--bgcolor>=I<\\,COLOR\\/>"
msgstr "B<-b>, B<--bgcolor>=I<\\,FARBE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "use COLOR for background"
msgstr "verwendet die angegebene FARBE für den Hintergrund."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-c>, B<--color>=I<\\,COLOR\\/>"
msgstr "B<-c>, B<--color>=I<\\,FARBE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "use COLOR for text"
msgstr "verwendet die angegebene FARBE für Text."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-f>, B<--font>=I<\\,FILE\\/>"
msgstr "B<-f>, B<--font>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "use FILE as font (PF2)."
msgstr "verwendet die angegebene DATEI als Schrift (PF2)."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-i>, B<--input>=I<\\,FILE\\/>"
msgstr "B<-i>, B<--input>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "read text from FILE."
msgstr "liest den Text aus der angegebenen DATEI."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set output filename. Default is STDOUT"
msgstr ""
"legt den Namen der Ausgabedatei fest. Per Vorgabe wird in die "
"Standardausgabe geschrieben."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-t>, B<--text>=I<\\,STRING\\/>"
msgstr "B<-t>, B<--text>=I<\\,ZEICHENKETTE\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "set the label to render"
msgstr "gibt die zu erzeugende Bezeichnung an."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print verbose messages."
msgstr "gibt ausführliche Meldungen aus."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"Mandatory or optional arguments to long options are also mandatory or "
"optional for any corresponding short options."
msgstr ""
"Erforderliche oder optionale Argumente für lange Optionen sind ebenso "
"erforderlich bzw. optional für die entsprechenden Kurzoptionen."

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<.MT bug-grub@gnu.org> E<.ME .>"

#. type: SH
#: archlinux debian-bookworm debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid ""
"The full documentation for B<grub-render-label> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-render-label> programs are properly "
"installed at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-render-label> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-render-label> "
"auf Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "B<info grub-render-label>"
msgstr "B<info grub-render-label>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "April 2023"
msgstr "April 2023"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "GRUB 2.06-13"
msgstr "GRUB 2.06-13"
