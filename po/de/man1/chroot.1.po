# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Karl Eichwalder <ke@suse.de>
# Lutz Behnke <lutz.behnke@gmx.de>
# Michael Piefel <piefel@debian.org>
# Michael Schmidt <michael@guug.de>
# Dr. Tobias Quathamer <toddy@debian.org>, 2010.
# Helge Kreutzmann <debian@helgefjell.de>, 2016.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-04-30 20:23+0200\n"
"PO-Revision-Date: 2023-03-26 01:04+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "CHROOT"
msgstr "CHROOT"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "April 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "chroot - run command or interactive shell with special root directory"
msgstr ""
"chroot - einen Befehl oder eine interaktive Shell mit einem speziellen "
"Wurzelverzeichnis ausführen"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"B<chroot> [I<\\,OPTION\\/>] I<\\,NEWROOT \\/>[I<\\,COMMAND \\/>[I<\\,ARG\\/"
">]...]"
msgstr ""
"B<chroot> [I<\\,OPTION\\/>] I<\\,NEUESWURZELVERZEICHNIS \\/>[I<\\,BEFEHL \\/"
">[I<\\,ARG\\/>]…]"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "B<chroot> I<\\,OPTION\\/>"
msgstr "B<chroot> I<\\,OPTION\\/>"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Run COMMAND with root directory set to NEWROOT."
msgstr ""
"BEFEHL ausführen, wobei das Wurzelverzeichnis auf NEUESWURZELVERZEICHNIS "
"gesetzt wird."

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--groups>=I<\\,G_LIST\\/>"
msgstr "B<--groups>=I<\\,G_LIST\\/>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "specify supplementary groups as g1,g2,..,gN"
msgstr "gibt zusätzliche Gruppen als g1, g2, …, gN an."

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--userspec>=I<\\,USER\\/>:GROUP"
msgstr "B<--userspec>=I<\\,BENUTZER\\/>:GRUPPE"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "specify user and group (ID or name) to use"
msgstr ""
"gibt Benutzer und Gruppe (als ID oder Namen) an, die verwendet werden sollen."

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--skip-chdir>"
msgstr "B<--skip-chdir>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "do not change working directory to '/'"
msgstr "ändert das Arbeitsverzeichnis nicht auf »/«."

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"If no command is given, run '\"$SHELL\" B<-i>' (default: '/bin/sh B<-i>')."
msgstr ""
"führt »\"$SHELL\" B<-i>« aus, wenn kein Befehl angegeben ist (Vorgabe: »/bin/"
"sh B<-i>«)."

#. type: SS
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Rückgabewert:"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "125"
msgstr "125"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "if the chroot command itself fails"
msgstr "falls der Befehl B<chroot> selbst fehlschlägt"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "126"
msgstr "126"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND is found but cannot be invoked"
msgstr ""
"falls der BEFEHL zwar gefunden wurde, aber nicht aufgerufen werden kann"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "127"
msgstr "127"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "if COMMAND cannot be found"
msgstr "falls der BEFEHL nicht gefunden wurde"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "-"
msgstr "-"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "the exit status of COMMAND otherwise"
msgstr "ansonsten der Rückgabewert des BEFEHLs"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "Written by Roland McGrath."
msgstr "Geschrieben von Roland McGrath."

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Onlinehilfe für GNU coreutils: E<.UR https://www.gnu.org/software/coreutils/"
">E<.UE>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Melden Sie Fehler in der Übersetzung an E<.UR https://translationproject.org/"
"team/de.html> das deutschsprachige Team beim GNU Translation ProjectE<.UE .>"

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<chroot>(2)"
msgstr "B<chroot>(2)"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chrootE<gt>"
msgstr ""
"Die vollständige Dokumentation ist auf E<lt>https://www.gnu.org/software/"
"coreutils/chrootE<gt>"

#. type: Plain text
#: archlinux fedora-38 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chroot invocation\\(aq"
msgstr "oder lokal via: info \\(aq(coreutils) chroot invocation\\(aq"

#. type: TH
#: fedora-38
#, no-wrap
msgid "January 2023"
msgstr "Januar 2023"

#. type: TH
#: fedora-38 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: fedora-38 mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU Coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

# WONTFIX missing markup // I'm not sure about marking up references like this.
#. type: Plain text
#: opensuse-leap-15-5
msgid "chroot(2)"
msgstr "B<chroot>(2)"
