# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2023-06-27 20:02+0200\n"
"PO-Revision-Date: 2023-03-18 17:55+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "xsetmode"
msgstr "xsetmode"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "xsetmode 1.0.0"
msgstr "xsetmode 1.0.0"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "X Version 11"
msgstr "X Version 11"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "xsetmode - set the mode for an X Input device"
msgstr "xsetmode - Den Modus für ein X-Eingabegerät setzen"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable
msgid "B<xsetmode> I<device-name> B<ABSOLUTE> | B<RELATIVE>"
msgstr "B<xsetmode> I<Gerätename> B<ABSOLUTE> | B<RELATIVE>"

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# WONTFIX Xsetmode → B<Xsetmode> // This utility was declared obsolete and abandoned many years ago, so we will not fix any bugs in their code nor man pages
#. type: Plain text
#: debian-bookworm debian-unstable
msgid ""
"Xsetmode sets the mode of an XInput device to either absolute or relative.  "
"This isn't appropriate for all device types."
msgstr ""
"Xsetmode setzt den Modus eines XInput-Gerätes entweder auf absolut oder "
"relativ. Dies ist nicht für alle Gerätetypen angemessen."

#. type: SH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

# WONTFIX Frederic Lepied → Frédéric Lepied // This utility was declared obsolete and abandoned many years ago, so we will not fix any bugs in their code nor man pages
#. type: Plain text
#: debian-bookworm debian-unstable
msgid "Frederic Lepied"
msgstr "Frédéric Lepied"
