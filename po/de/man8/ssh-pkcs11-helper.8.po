# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2020,2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.15.0\n"
"POT-Creation-Date: 2023-06-27 19:52+0200\n"
"PO-Revision-Date: 2022-11-03 21:31+0100\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: Dd
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "$Mdocdate: April 29 2022 $"
msgstr "$Mdocdate: 29. April 2022 $"

#. type: Dt
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SSH-PKCS11-HELPER 8"
msgstr "SSH-PKCS11-HELPER 8"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm ssh-pkcs11-helper>"
msgstr "E<.Nm ssh-pkcs11-helper>"

#. type: Nd
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OpenSSH helper for PKCS#11 support"
msgstr "OpenSSH-Hilfsprogramm für die PKCS#11-Unterstützung"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm> E<.Op Fl v>"
msgstr "E<.Nm> E<.Op Fl v>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"E<.Nm> is used by E<.Xr ssh 1>, E<.Xr ssh-agent 1>, and E<.Xr ssh-keygen 1> "
"to access keys provided by a PKCS#11 token."
msgstr ""
"E<.Nm> wird von E<.Xr ssh 1>, E<.Xr ssh-agent 1> und E<.Xr ssh-keygen 1> zum "
"Zugriff auf Schlüssel verwandt, die durch ein PKCS#11-Token bereitgestellt "
"werden."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid "E<.Nm> is not intended to be invoked directly by the user."
msgstr "E<.Nm> ist nicht zum direkten Aufruf durch Benutzer gedacht."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "A single option is supported:"
msgstr "Es wird eine einzige Option unterstützt:"

#. type: It
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl v"
msgstr "Fl v"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid ""
"Verbose mode.  Causes E<.Nm> to print debugging messages about its "
"progress.  This is helpful in debugging problems.  Multiple E<.Fl v> options "
"increase the verbosity.  The maximum is 3."
msgstr ""
"Ausführlicher Modus. Führt dazu, dass E<.Nm> Fehlersuchmeldungen über seinen "
"Fortschritt ausgibt. Dies ist zur Fehlersuche bei Problemen hilfreich. Wird "
"die Option E<.Fl v> mehrmals angegeben, erhöht dies die Ausführlichkeit. "
"Maximal drei sind möglich."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid ""
"Note that E<.Xr ssh 1>, E<.Xr ssh-agent 1>, and E<.Xr ssh-keygen 1> will "
"automatically pass the E<.Fl v> flag to E<.Nm> when they have themselves "
"been placed in debug mode."
msgstr ""
"Beachten Sie, dass E<.Xr ssh 1>, E<.Xr ssh-agent 1> und E<.Xr ssh-keygen 1> "
"automatisch den Schalter E<.Fl v> an E<.Nm> übergeben werden, wenn sie "
"selbst in den Fehlersuchmodus versetzt wurden."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-rawhide mageia-cauldron
msgid "E<.Xr ssh 1>, E<.Xr ssh-agent 1>, E<.Xr ssh-keygen 1>"
msgstr "E<.Xr ssh 1>, E<.Xr ssh-agent 1>, E<.Xr ssh-keygen 1>"

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm> first appeared in E<.Ox 4.7>."
msgstr "E<.Nm> erschien erstmalig in E<.Ox 4.7>."

#. type: Sh
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "AUTOREN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron
msgid "E<.An Markus Friedl Aq Mt markus@openbsd.org>"
msgstr "E<.An Markus Friedl Aq Mt markus@openbsd.org>"

#. type: Dd
#: fedora-38
#, no-wrap
msgid "$Mdocdate: November 30 2019 $"
msgstr "$Mdocdate: 30. November 2019 $"

#. type: Plain text
#: fedora-38
msgid ""
"E<.Nm> is used by E<.Xr ssh-agent 1> to access keys provided by a PKCS#11 "
"token."
msgstr ""
"E<.Nm> wird von E<.Xr ssh-agent 1> zum Zugriff auf Schlüssel verwandt, die "
"durch ein PKCS#11-Token bereitgestellt werden."

#. type: Plain text
#: fedora-38
msgid ""
"E<.Nm> is not intended to be invoked by the user, but from E<.Xr ssh-agent "
"1>."
msgstr ""
"E<.Nm> ist nicht zum Aufruf durch Benutzer gedacht, sondern von E<.Xr ssh-"
"agent 1>."

#. type: Plain text
#: fedora-38
msgid ""
"Note that E<.Xr ssh-agent 1> will automatically pass the E<.Fl v> flag to E<."
"Nm> when it has itself been placed in debug mode."
msgstr ""
"Beachten Sie, dass E<.Xr ssh-agent 1> automatisch den Schalter E<.Fl v> an "
"E<.Nm> übergeben wird, wenn es selbst in den Fehlersuchmodus versetzt wurde."

#. type: Plain text
#: fedora-38
msgid "E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-agent 1>"
msgstr "E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-agent 1>"
