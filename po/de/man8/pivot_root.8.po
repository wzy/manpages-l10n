# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-06-27 19:42+0200\n"
"PO-Revision-Date: 2021-06-02 21:21+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.04.1\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "PIVOT_ROOT"
msgstr "PIVOT_ROOT"

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "2022-05-11"
msgstr "11. Mai 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "System Administration"
msgstr "System-Administration"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "pivot_root - change the root filesystem"
msgstr "pivot_root - die Wurzeleinhängung ändern"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<pivot_root> I<new_root> I<put_old>"
msgstr "B<pivot_root> I<neue_Wurzel> I<alte_Wurzel>"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<pivot_root> moves the root file system of the current process to the "
"directory I<put_old> and makes I<new_root> the new root file system. Since "
"B<pivot_root>(8) simply calls B<pivot_root>(2), we refer to the man page of "
"the latter for further details."
msgstr ""
"B<pivot_root> verschiebt die Dateisystemwurzel des aktuellen Prozesses in "
"das Verzeichnis I<alte_Wurzel> und macht I<neue_Wurzel> zur neuen "
"Dateisystemwurzel. B<pivot_root>(8) ruft einfach B<pivot_root>(2) auf, daher "
"finden Sie in dessen Handbuchseite weitere Details."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Note that, depending on the implementation of B<pivot_root>, root and "
"current working directory of the caller may or may not change. The following "
"is a sequence for invoking B<pivot_root> that works in either case, assuming "
"that B<pivot_root> and B<chroot> are in the current B<PATH>:"
msgstr ""
"Ob Wurzel und aktuelles Arbeitsverzeichnis beim Aufruf geändert werden oder "
"nicht, ist von der Implementierung von B<pivot_root> abhängig. Die folgende, "
"in jedem Fall funktionierende Sequenz ruft B<pivot_root> auf, wobei "
"angenommen wird, dass sich B<pivot_root> und B<chroot> im aktuellen B<PATH> "
"befinden:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid ""
"cd new_root\n"
"pivot_root . put_old\n"
"exec chroot . command\n"
msgstr ""
"cd neue_Wurzel\n"
"pivot_root . alte_Wurzel\n"
"exec chroot . Befehl\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Note that B<chroot> must be available under the old root and under the new "
"root, because B<pivot_root> may or may not have implicitly changed the root "
"directory of the shell."
msgstr ""
"Beachten Sie, dass B<chroot> sowohl in der alten als auch in der neuen "
"Dateisystemwurzel verfügbar sein muss, da B<pivot_root> das "
"Wurzelverzeichnis der Shell entweder geändert haben kann oder nicht."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Note that B<exec chroot> changes the running executable, which is necessary "
"if the old root directory should be unmounted afterwards. Also note that "
"standard input, output, and error may still point to a device on the old "
"root file system, keeping it busy. They can easily be changed when invoking "
"B<chroot> (see below; note the absence of leading slashes to make it work "
"whether B<pivot_root> has changed the shell\\(cqs root or not)."
msgstr ""
"Beachten Sie, dass B<exec chroot> die ausgeführte Datei ändert, was "
"notwendig ist, falls das alte Wurzelverzeichnis danach ausgehängt werden "
"soll. Auch die Standardeingabe, Standardausgabe und deren Fehlerkanal kann "
"noch auf ein Gerät in der alten Dateisystemwurzel zeigen, wodurch dieses "
"noch belegt ist. Dies kann beim Aufruf von B<chroot> einfach geändert "
"werden, siehe nachfolgendes Beispiel. Beachten Sie hierbei, dass keine "
"führenden Schrägstriche gesetzt sind, damit B<pivot_root> stets "
"funktioniert, egal ob die Dateisystemwurzel der Shell geändert wurde oder "
"nicht."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Display help text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38
msgid "Print version and exit."
msgstr "zeigt die Versionsnummer an und beendet das Programm."

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "Change the root file system to I</dev/hda1> from an interactive shell:"
msgstr ""
"Ändern der Dateisystemwurzel in I</dev/hda1> aus einer interaktiven Shell:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid ""
"mount /dev/hda1 /new-root\n"
"cd /new-root\n"
"pivot_root . old-root\n"
"exec chroot . sh E<lt>dev/console E<gt>dev/console 2E<gt>&1\n"
"umount /old-root\n"
msgstr ""
"mount /dev/hda1 /new-root\n"
"cd /new-root\n"
"pivot_root . old-root\n"
"exec chroot . sh E<lt>dev/console E<gt>dev/console 2E<gt>&1\n"
"umount /old-root\n"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"Mount the new root file system over NFS from 10.0.0.1:/my_root and run "
"B<init>:"
msgstr ""
"Einhängen des neuen Wurzeldateisystems über NFS von 10.0.0.1:/my_root und "
"B<init> ausführen:"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid ""
"ifconfig lo 127.0.0.1 up   # for portmap\n"
"# configure Ethernet or such\n"
"portmap   # for lockd (implicitly started by mount)\n"
"mount -o ro 10.0.0.1:/my_root /mnt\n"
"killall portmap   # portmap keeps old root busy\n"
"cd /mnt\n"
"pivot_root . old_root\n"
"exec chroot . sh -c \\(aqumount /old_root; exec /sbin/init\\(aq \\(rs\n"
"  E<lt>dev/console E<gt>dev/console 2E<gt>&1\n"
msgstr ""
"ifconfig lo 127.0.0.1 up   # für Portmap\n"
"# Ethernet konfigurieren\n"
"portmap   # für lockd (implizit von mount gestartet)\n"
"mount -o ro 10.0.0.1:/meine_Wurzel /mnt\n"
"killall portmap   # portmap belegt noch die alte Wurzel\n"
"cd /mnt\n"
"pivot_root . alte_Wurzel\n"
"exec chroot . sh -c \\(aqumount /alte_Wurzel; exec /sbin/init\\(aq \\(rs\n"
"  E<lt>dev/console E<gt>dev/console 2E<gt>&1\n"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"B<chroot>(1), B<pivot_root>(2), B<mount>(8), B<switch_root>(8), B<umount>(8)"
msgstr ""
"B<chroot>(1), B<pivot_root>(2), B<mount>(8), B<switch_root>(8), B<umount>(8)"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid "For bug reports, use the issue tracker at"
msgstr "Verwenden Sie zum Melden von Fehlern das Fehlererfassungssystem auf"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "VERFÜGBARKEIT"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 opensuse-leap-15-5
msgid ""
"The B<pivot_root> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""
"Der Befehl B<pivot_root> ist Teil des Pakets util-linux, welches "
"heruntergeladen werden kann von:"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2022-02-14"
msgstr "14. Februar 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "zeigt Versionsinformationen an und beendet das Programm."
