# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerd Koenig <koenig.bodensee@googlemail.com>, 2012-2013.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.3\n"
"POT-Creation-Date: 2023-06-27 19:54+0200\n"
"PO-Revision-Date: 2023-05-04 11:49+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "syslog"
msgstr "syslog"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "closelog, openlog, syslog, vsyslog - send messages to the system logger"
msgstr ""
"closelog, openlog, syslog, vsyslog - verschickt Nachrichten an die "
"Systemprotokollierung"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>syslog.hE<gt>>\n"
msgstr "B<#include E<lt>syslog.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int >I<facility>B<);>\n"
"B<void syslog(int >I<priority>B<, const char *>I<format>B<, ...);>\n"
"B<void closelog(void);>\n"
msgstr ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int >I<einrichtung>B<);>\n"
"B<void syslog(int >I<priorität>B<, const char *>I<format>B<, …);>\n"
"B<void closelog(void);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list >I<ap>B<);>\n"
msgstr "B<void vsyslog(int >I<priorität>B<, const char *>I<format>B<, va_list >I<ap>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<vsyslog>():"
msgstr "B<vsyslog>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"    Seit Glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 und älter:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "openlog()"
msgstr "openlog()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<openlog>()  opens a connection to the system logger for a program."
msgstr ""
"B<openlog>() öffnet für ein Programm eine Verbindung zum Syslog-Daemon."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The string pointed to by I<ident> is prepended to every message, and is "
"typically set to the program name.  If I<ident> is NULL, the program name is "
"used.  (POSIX.1-2008 does not specify the behavior when I<ident> is NULL.)"
msgstr ""
"Die Zeichenkette I<ident> wird jeder Nachricht vorangestellt und wird "
"üblicherweise auf den Programmnamen gesetzt. Falls I<ident> gleich NULL ist, "
"wird der Programmname benutzt. (POSIX.1-2008 enthält keine Spezifikation für "
"den Fall, dass I<ident> gleich NULL ist)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<option> argument specifies flags which control the operation of "
"B<openlog>()  and subsequent calls to B<syslog>().  The I<facility> argument "
"establishes a default to be used if none is specified in subsequent calls to "
"B<syslog>().  The values that may be specified for I<option> and I<facility> "
"are described below."
msgstr ""
"Der I<option>-Parameter definiert Schalter, welche die Ausführung von "
"B<Openlog> und darauffolgende Aufrufe von B<syslog>() steuern. Der "
"I<einrichtung>-Parameter definiert einen Standardwert, welcher benutzt wird, "
"falls in darauffolgenden B<syslog>()-Aufrufen nichts spezifiziert wurde. Die "
"Werte, die für I<option> und I<einrichtung> angegeben werden dürfen, werden "
"weiter unten beschrieben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The use of B<openlog>()  is optional; it will automatically be called by "
"B<syslog>()  if necessary, in which case I<ident> will default to NULL."
msgstr ""
"Die Benutzung von B<openlog()> ist optional; gegebenenfalls wird die Routine "
"automatisch von B<syslog()> aufgerufen, wenn es nötig ist, dann wird "
"I<ident> auf NULL gesetzt."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "syslog() and vsyslog()"
msgstr "syslog() und vsyslog()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<syslog>()  generates a log message, which will be distributed by "
"B<syslogd>(8)."
msgstr ""
"B<syslog()> erzeugt eine Protokoll-Nachricht, die vom B<syslogd>(8)  "
"verteilt wird."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<priority> argument is formed by ORing together a I<facility> value and "
"a I<level> value (described below).  If no I<facility> value is ORed into "
"I<priority>, then the default value set by B<openlog>()  is used, or, if "
"there was no preceding B<openlog>()  call, a default of B<LOG_USER> is "
"employed."
msgstr ""
"Der Parameter I<priorität> ist das Ergebnis einer ODER-Verknüpfung eines "
"Werts I<einrichtung> und eines Werts I<level> (weiter unten beschrieben). "
"Falls kein Wert I<einrichtung> mit I<priorität> ODER-verknüpft wird, dann "
"wird der durch B<openlog>() gesetzte Vorgabewert verwandt, oder, falls es "
"keinen vorhergehenden Aufruf B<openlog>() gab, die Vorgabe B<LOG_USER> "
"verwandt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The remaining arguments are a I<format>, as in B<printf>(3), and any "
"arguments required by the I<format>, except that the two-character sequence "
"B<%m> will be replaced by the error message string I<strerror>(I<errno>).  "
"The format string need not include a terminating newline character."
msgstr ""
"Die verbleibenden Parameter sind I<format>, wie bei B<printf>(3) und weitere "
"Parameter, die von I<format> benötigt werden. Allerdings wird die Zwei-"
"Zeichen-Folge %m durch die Fehlermeldung I<strerror>(I<errno>) ersetzt "
"werden. Die Formatzeichenkette muss kein abschließendes Zeilenumbruchzeichen "
"enthalten."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<vsyslog>()  performs the same task as B<syslog>()  with the "
"difference that it takes a set of arguments which have been obtained using "
"the B<stdarg>(3)  variable argument list macros."
msgstr ""
"Die Funktion B<vsyslog>() führt die gleiche Aufgabe aus wie B<syslog>(), mit "
"dem Unterschied, dass sie eine Liste an Parametern erhält, die mit Makros "
"aus der B<stdarg>(3)-Variablenliste abgefragt werden."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "closelog()"
msgstr "closelog()"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<closelog>()  closes the file descriptor being used to write to the system "
"logger.  The use of B<closelog>()  is optional."
msgstr ""
"B<closelog()> schließt den Dateideskriptor, der zum Schreiben der Protokoll-"
"Nachrichten benutzt wurde. Die Benutzung von B<closelog()> ist optional."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<option>"
msgstr "Werte für I<option>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<option> argument to B<openlog>()  is a bit mask constructed by ORing "
"together any of the following values:"
msgstr ""
"Der Parameter I<option> von B<openlog>() ist eine Bitmaske, die durch ODER-"
"Verknüpfung beliebiger der folgenden Werte erstellt wird:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CONS>"
msgstr "B<LOG_CONS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Write directly to the system console if there is an error while sending to "
"the system logger."
msgstr ""
"schreibt direkt auf die Systemkonsole, falls ein Fehler beim Schreiben an "
"die Systemprotokollierung auftritt."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NDELAY>"
msgstr "B<LOG_NDELAY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Open the connection immediately (normally, the connection is opened when the "
"first message is logged).  This may be useful, for example, if a subsequent "
"B<chroot>(2)  would make the pathname used internally by the logging "
"facility unreachable."
msgstr ""
"öffnet die Verbindung sofort (normalerweise wird die Verbindung geöffnet, "
"wenn die erste Nachricht protokolliert wird). Dies kann zum Beispiel "
"nützlich sein, falls durch ein nachfolgendes B<chroot>(2) die intern von der "
"Protokollierungseinrichtung verwandten Pfadnamen unerreichbar werden würden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NOWAIT>"
msgstr "B<LOG_NOWAIT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Don't wait for child processes that may have been created while logging the "
"message.  (The GNU C library does not create a child process, so this option "
"has no effect on Linux.)"
msgstr ""
"wartet nicht auf Kindprozesse, welche eventuell beim Protokollieren der "
"Nachricht erzeugt wurden. (Die GNU-C-Bibliothek erzeugt keine Kindprozesse, "
"insofern hat diese Option keine Auswirkung unter Linux.)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ODELAY>"
msgstr "B<LOG_ODELAY>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The converse of B<LOG_NDELAY>; opening of the connection is delayed until "
"B<syslog>()  is called.  (This is the default, and need not be specified.)"
msgstr ""
"das Gegenteil von B<LOG_NDELAY>; das Öffnen der Verbindung wird bis zum "
"Aufruf von B<syslog>() verzögert. (Das ist das Standardverhalten und muss "
"nicht angegeben werden.)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_PERROR>"
msgstr "B<LOG_PERROR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"(Not in POSIX.1-2001 or POSIX.1-2008.)  Also log the message to I<stderr>."
msgstr ""
"(nicht in POSIX.1-2001 oder POSIX.1-2008) die Nachricht auch nach I<stderr> "
"protokollieren."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_PID>"
msgstr "B<LOG_PID>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Include the caller's PID with each message."
msgstr "füge die PID des Aufrufenden in jede Nachricht ein."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<facility>"
msgstr "Werte für I<einrichtung>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<facility> argument is used to specify what type of program is logging "
"the message.  This lets the configuration file specify that messages from "
"different facilities will be handled differently."
msgstr ""
"Der Parameter I<einrichtung> wird benutzt, um anzugeben, welcher Programmtyp "
"die Nachricht protokolliert. Dadurch kann mit der Konfigurationsdatei "
"I<syslog.conf> erreicht werden, dass Nachrichten von unterschiedlichen "
"Einrichtungen auch unterschiedlich behandelt werden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_AUTH>"
msgstr "B<LOG_AUTH>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "security/authorization messages"
msgstr "Sicherheits-/Autorisations-Nachrichten"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_AUTHPRIV>"
msgstr "B<LOG_AUTHPRIV>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "security/authorization messages (private)"
msgstr "Sicherheits-/Autorisations-Nachrichten (privat)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CRON>"
msgstr "B<LOG_CRON>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "clock daemon (B<cron> and B<at>)"
msgstr "Uhr-Daemon (B<cron> und B<at>)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_DAEMON>"
msgstr "B<LOG_DAEMON>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "system daemons without separate facility value"
msgstr "System-Daemonen ohne spezielle Einrichtungswerte"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_FTP>"
msgstr "B<LOG_FTP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ftp daemon"
msgstr "FTP-Daemon"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_KERN>"
msgstr "B<LOG_KERN>"

#.  LOG_KERN has the value 0; if used as a facility, zero translates to:
#.  "use the default facility".
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "kernel messages (these can't be generated from user processes)"
msgstr ""
"Kernel-Nachrichten (diese können nicht von Benutzerprozessen erzeugt werden)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_LOCAL0> through B<LOG_LOCAL7>"
msgstr "B<LOG_LOCAL0> bis B<LOG_LOCAL7>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "reserved for local use"
msgstr "reserviert für den lokalen Gebrauch"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_LPR>"
msgstr "B<LOG_LPR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "line printer subsystem"
msgstr "Zeilendrucker-Untersystem"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_MAIL>"
msgstr "B<LOG_MAIL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "mail subsystem"
msgstr "Mail-Subsystem"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NEWS>"
msgstr "B<LOG_NEWS>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "USENET news subsystem"
msgstr "Usenet Nachrichten-Untersystem"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_SYSLOG>"
msgstr "B<LOG_SYSLOG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "messages generated internally by B<syslogd>(8)"
msgstr "Nachrichten, die intern vom B<syslogd>(8) erzeugt wurden"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_USER> (default)"
msgstr "B<LOG_USER> (Standardwert)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "generic user-level messages"
msgstr "generische Nachrichten auf Anwenderebene"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_UUCP>"
msgstr "B<LOG_UUCP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "UUCP subsystem"
msgstr "UUCP-Subsystem"

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<level>"
msgstr "Werte für I<level>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This determines the importance of the message.  The levels are, in order of "
"decreasing importance:"
msgstr ""
"Dieser Parameter gibt die Dringlichkeitsstufe der Nachricht an. Die Stufen "
"sind in absteigender Wichtigkeit:"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_EMERG>"
msgstr "B<LOG_EMERG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "system is unusable"
msgstr "Das System ist unbrauchbar."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ALERT>"
msgstr "B<LOG_ALERT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "action must be taken immediately"
msgstr "Es muss sofort gehandelt werden."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CRIT>"
msgstr "B<LOG_CRIT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "critical conditions"
msgstr "kritische Bedingungen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ERR>"
msgstr "B<LOG_ERR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "error conditions"
msgstr "Fehlerbedingungen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_WARNING>"
msgstr "B<LOG_WARNING>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "warning conditions"
msgstr "Warnungsbedingungen"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NOTICE>"
msgstr "B<LOG_NOTICE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "normal, but significant, condition"
msgstr "normale, aber bedeutende Bedingung"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_INFO>"
msgstr "B<LOG_INFO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "informational message"
msgstr "Information"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_DEBUG>"
msgstr "B<LOG_DEBUG>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "debug-level message"
msgstr "Debuginformation"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The function B<setlogmask>(3)  can be used to restrict logging to specified "
"levels only."
msgstr ""
"Die Funktion B<setlogmask>(3) kann dazu benutzt werden, um das "
"Protokollieren auf bestimmte Level einzuschränken."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<openlog>(),\n"
"B<closelog>()"
msgstr ""
"B<openlog>(),\n"
"B<closelog>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<syslog>(),\n"
"B<vsyslog>()"
msgstr ""
"B<syslog>(),\n"
"B<vsyslog>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env locale"
msgstr "MT-Safe env locale"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<syslog>()"
msgstr "B<syslog>()"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<openlog>()"
msgstr "B<openlog>()"

#. type: TQ
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<closelog>()"
msgstr "B<closelog>()"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<vsyslog>()"
msgstr "B<vsyslog>()"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "None."
msgstr "Keine."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "4.2BSD, SUSv2, POSIX.1-2001."
msgstr "4.2BSD, SUSv2, POSIX.1-2001."

#.  .SH HISTORY
#.  4.3BSD documents
#.  .BR setlogmask ().
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "4.3BSD, SUSv2, POSIX.1-2001."
msgstr "4.3BSD, SUSv2, POSIX.1-2001."

#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "4.3BSD-Reno."
msgstr "4.3BSD-Reno."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 specifies only the B<LOG_USER> and B<LOG_LOCAL*> values for "
"I<facility>.  However, with the exception of B<LOG_AUTHPRIV> and B<LOG_FTP>, "
"the other I<facility> values appear on most UNIX systems."
msgstr ""
"POSIX.1-2001 spezifiert nur die Werte B<LOG_USER> und B<LOG_LOCAL*> für "
"I<einrichtung>. Auf den meisten UNIX-Systemen gibt es jedoch auch die "
"anderen I<einrichtung>-Optionen mit der Ausnahme von B<LOG_AUTHPRIV> und "
"B<LOG_FTP>. "

#. #-#-#-#-#  archlinux: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  debian-bookworm: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR syslog ()
#.  function call appeared in 4.2BSD.
#.  4.3BSD documents
#.  .BR openlog (),
#.  .BR syslog (),
#.  .BR closelog (),
#.  and
#.  .BR setlogmask ().
#.  4.3BSD-Reno also documents
#.  .BR vsyslog ().
#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#. #-#-#-#-#  debian-unstable: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR syslog ()
#.  function call appeared in 4.2BSD.
#.  4.3BSD documents
#.  .BR openlog (),
#.  .BR syslog (),
#.  .BR closelog (),
#.  and
#.  .BR setlogmask ().
#.  4.3BSD-Reno also documents
#.  .BR vsyslog ().
#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#. #-#-#-#-#  fedora-38: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR syslog ()
#.  function call appeared in 4.2BSD.
#.  4.3BSD documents
#.  .BR openlog (),
#.  .BR syslog (),
#.  .BR closelog (),
#.  and
#.  .BR setlogmask ().
#.  4.3BSD-Reno also documents
#.  .BR vsyslog ().
#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR syslog ()
#.  function call appeared in 4.2BSD.
#.  4.3BSD documents
#.  .BR openlog (),
#.  .BR syslog (),
#.  .BR closelog (),
#.  and
#.  .BR setlogmask ().
#.  4.3BSD-Reno also documents
#.  .BR vsyslog ().
#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-5: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  .SH HISTORY
#.  A
#.  .BR syslog ()
#.  function call appeared in 4.2BSD.
#.  4.3BSD documents
#.  .BR openlog (),
#.  .BR syslog (),
#.  .BR closelog (),
#.  and
#.  .BR setlogmask ().
#.  4.3BSD-Reno also documents
#.  .BR vsyslog ().
#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: syslog.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<LOG_PERROR> value for I<option> is not specified by POSIX.1-2001 or "
"POSIX.1-2008, but is available in most versions of UNIX."
msgstr ""
"Der Wert B<LOG_PERROR> für den Parameter I<option> ist in POSIX.1-2001 oder "
"POSIX.1-2008 nicht spezifiert, aber auf den meisten UNIX-Versionen verfügbar."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The argument I<ident> in the call of B<openlog>()  is probably stored as-"
"is.  Thus, if the string it points to is changed, B<syslog>()  may start "
"prepending the changed string, and if the string it points to ceases to "
"exist, the results are undefined.  Most portable is to use a string constant."
msgstr ""
"Das Argument I<ident> im Aufruf von B<openlog>() wird wahrscheinlich "
"unverändert gespeichert. Falls sich demnach die Zeichenkette, auf die sich "
"I<ident> bezieht, ändert, kann B<syslog>() die geänderte Zeichenkette "
"voranstellen. Falls die Zeichenkette nicht mehr gefunden werden kann, ergibt "
"dies ein undefiniertes Ergebnis. Die portabelste Lösung ist die Verwendung "
"einer konstanten Zeichenkette."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Never pass a string with user-supplied data as a format, use the following "
"instead:"
msgstr ""
"Übergeben Sie niemals eine Zeichenkette, die Daten von Benutzeriengaben "
"enthält, als Format, sondern benutzen Sie das Folgende:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "syslog(priority, \"%s\", string);\n"
msgstr "syslog(priority, \"%s\", string);\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<journalctl>(1), B<logger>(1), B<setlogmask>(3), B<syslog.conf>(5), "
"B<syslogd>(8)"
msgstr ""
"B<journalctl>(1), B<logger>(1), B<setlogmask>(3), B<syslog.conf>(5), "
"B<syslogd>(8)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5. Februar 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The functions B<openlog>(), B<closelog>(), and B<syslog>()  (but not "
"B<vsyslog>())  are specified in SUSv2, POSIX.1-2001, and POSIX.1-2008."
msgstr ""
"Die Funktionen B<openlog>(), B<closelog>() und B<syslog>() (aber nicht "
"B<vsyslog>()) sind in SUSv2, POSIX.1-2001, and POSIX.1-2008 spezifiert. "

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYSLOG"
msgstr "SYSLOG"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>syslog.hE<gt>>"
msgstr "B<#include E<lt>syslog.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int "
">I<facility>B<);>"
msgstr ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int "
">I<einrichtung>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<void syslog(int >I<priority>B<, const char *>I<format>B<, ...);>"
msgstr "B<void syslog(int >I<priorität>B<, const char *>I<format>B<, …);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<void closelog(void);>"
msgstr "B<void closelog(void);>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list "
">I<ap>B<);>"
msgstr ""
"B<void vsyslog(int >I<priorität>B<, const char *>I<format>B<, va_list "
">I<ap>B<);>"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<vsyslog>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""
"B<vsyslog>():\n"
"    Seit Glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 und älter:\n"
"        _BSD_SOURCE\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
