# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Patrick Rother <krd@gulu.net>
# Chris Leick <c.leick@vollbio.de>, 2011.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2023-06-27 19:52+0200\n"
"PO-Revision-Date: 2023-02-23 20:17+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.2\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "stdio"
msgstr "stdio"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "stdio - standard input/output library functions"
msgstr "stdio - Standardein-/-ausgabe-Bibliotheksfunktionen"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdio.hE<gt>>\n"
msgstr "B<#include E<lt>stdio.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<FILE *>I<stdin>B<;>\n"
"B<FILE *>I<stdout>B<;>\n"
"B<FILE *>I<stderr>B<;>\n"
msgstr ""
"B<FILE *>I<stdin>B<;>\n"
"B<FILE *>I<stdout>B<;>\n"
"B<FILE *>I<stderr>B<;>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The standard I/O library provides a simple and efficient buffered stream I/O "
"interface.  Input and output is mapped into logical data streams and the "
"physical I/O characteristics are concealed.  The functions and macros are "
"listed below; more information is available from the individual man pages."
msgstr ""
"Die Standard-E/A-Bibliothek stellt eine einfache und effiziente E/A-"
"Schnittstelle für gepufferte Datenströme zur Verfügung. Ein- und Ausgabe "
"werden als logische Datenströme dargestellt und physikalische E/A-"
"Charakteristiken werden verborgen. Die Funktionen und Makros sind im "
"Folgenden aufgelistet; weitere Informationen sind auf den verschiedenen "
"Handbuchseiten verfügbar."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"A stream is associated with an external file (which may be a physical "
"device) by I<opening> a file, which may involve creating a new file.  "
"Creating an existing file causes its former contents to be discarded.  If a "
"file can support positioning requests (such as a disk file, as opposed to a "
"terminal), then a I<file position indicator> associated with the stream is "
"positioned at the start of the file (byte zero), unless the file is opened "
"with append mode.  If append mode is used, it is unspecified whether the "
"position indicator will be placed at the start or the end of the file.  The "
"position indicator is maintained by subsequent reads, writes, and "
"positioning requests.  All input occurs as if the characters were read by "
"successive calls to the B<fgetc>(3)  function; all output takes place as if "
"all characters were written by successive calls to the B<fputc>(3)  function."
msgstr ""
"Ein Datenstrom wird mit einer externen Datei (die ein physikalisches Gerät "
"sein kann) verbunden, indem eine Datei I<geöffnet> wird, was das Erzeugen "
"einer neuen Datei beinhaltet. Eine bestehende Datei zu erzeugen, verwirft "
"ihren bisherigen Inhalt. Wenn die Datei Positionierungsänderungen "
"unterstützt (wie eine Datei auf einer Festplatte, im Gegensatz zu einem "
"Terminal), dann wird ein I<Dateipositionszeiger> mit dem Datenstrom "
"verbunden und auf den Anfang der Datei positioniert (nulltes Byte), außer "
"wenn die Datei im Anhänge-Modus geöffnet wurde. Im Anhänge-Modus ist es "
"nicht spezifiziert, ob der Positionsanzeiger auf den Anfang oder das Ende "
"der Datei platziert wird. Der Positionszeiger wird durch nachfolgende Lese-, "
"Schreib- und Positionierungszugriffe bewegt. Jede Eingabe wird so "
"eingelesen, als ob die Funktion B<fgetc>(3) für jedes Zeichen einzeln "
"aufgerufen worden wäre. Die Ausgabe erfolgt, als würde jedes Zeichen einzeln "
"durch die Funktion B<fputc>(3) geschrieben."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A file is disassociated from a stream by I<closing> the file.  Output "
"streams are flushed (any unwritten buffer contents are transferred to the "
"host environment) before the stream is disassociated from the file.  The "
"value of a pointer to a I<FILE> object is indeterminate after a file is "
"closed (garbage)."
msgstr ""
"Eine Datei wird von einem Datenstrom durch I<Schließen> der Datei gelöst. "
"Ausgabedatenströme werden geleert (noch nicht geschriebener Pufferinhalt "
"wird auf den Rechner übertragen) bevor der Datenstrom von der Datei getrennt "
"wird. Der Wert eines Zeigers auf ein I<FILE> Objekt ist nicht mehr gültig, "
"nachdem die Datei geschlossen wurde (Müll)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A file may be subsequently reopened, by the same or another program "
"execution, and its contents reclaimed or modified (if it can be repositioned "
"at the start).  If the main function returns to its original caller, or the "
"B<exit>(3)  function is called, all open files are closed (hence all output "
"streams are flushed) before program termination.  Other methods of program "
"termination, such as B<abort>(3)  do not bother about closing files properly."
msgstr ""
"Eine Datei kann nachfolgend durch das selbe oder ein anderes Programm wieder "
"geöffnet und ihr Inhalt zurückgelesen oder verändert werden (falls sie auf "
"den Anfang zurückpositioniert werden kann). Wenn die Hauptfunktion zum "
"ursprünglich Aufrufenden zurückkehrt oder die Funktion B<exit>(3) aufgerufen "
"wird, werden alle offenen Dateien vor Programmende geschlossen (nachdem alle "
"Ausgabedatenströme herausgeschrieben wurden). Andere Methoden zur Beendigung "
"von Programmen, wie B<abort>(3), kümmern sich nicht um das korrekte "
"Schließen von Dateien."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"At program startup, three text streams are predefined and need not be opened "
"explicitly: I<standard input> (for reading conventional input), I<standard "
"output> (for writing conventional output), and I<standard error> (for "
"writing diagnostic output).  These streams are abbreviated I<stdin>, "
"I<stdout>, and I<stderr>.  When opened, the standard error stream is not "
"fully buffered; the standard input and output streams are fully buffered if "
"and only if the streams do not refer to an interactive device."
msgstr ""
"Bei Programmstart sind drei Textdatenströme vordefiniert und brauchen nicht "
"explizit geöffnet zu werden: I<Standardeingabe> (zum Lesen konventioneller "
"Eingabe), I<Standardausgabe> (zum Schreiben konventioneller Ausgabe) und "
"I<Standardfehlerausgabe> (zum Schreiben der Diagnoseausgabe). Diese "
"Datenströme werden abgekürzt durch I<stdin>,I<stdout> und I<stderr>. Der "
"Datenstrom der Standardfehlerausgabe ist nicht vollständig gepuffert, wenn "
"er geöffnet ist; die Datenströme der Standardein- und -ausgabe sind nur dann "
"vollständig gepuffert, wenn die Datenströme nicht auf ein interaktives Gerät "
"verweisen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Output streams that refer to terminal devices are always line buffered by "
"default; pending output to such streams is written automatically whenever an "
"input stream that refers to a terminal device is read.  In cases where a "
"large amount of computation is done after printing part of a line on an "
"output terminal, it is necessary to B<fflush>(3)  the standard output before "
"going off and computing so that the output will appear."
msgstr ""
"Ausgabedatenströme, die auf Terminalgeräte verweisen, sind standardmäßig "
"zeilengepuffert. Noch ausstehende Ausgaben zu solchen Datenströmen werden "
"automatisch geschrieben, wann immer aus einem Eingabedatenstrom gelesen "
"wird, der sich auf ein Terminalgerät bezieht. Falls nach einer Ausgabe viel "
"gerechnet wird, ist es notwendig, B<fflush>(3) vor der Rechnung aufzurufen, "
"damit die Ausgabe erscheint."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<stdio> library is a part of the library B<libc> and routines are "
"automatically loaded as needed by B<cc>(1).  The SYNOPSIS sections of the "
"following manual pages indicate which include files are to be used, what the "
"compiler declaration for the function looks like and which external "
"variables are of interest."
msgstr ""
"Die Bibliothek B<stdio> ist Teil der Bibliothek B<libc> und die Routinen "
"werden von B<cc>(1) nach Bedarf geladen. Die Abschnitte ÜBERSICHT der "
"folgenden Handbuchseiten zeigen, welche Include-Dateien zu benutzen sind, "
"wie die Deklaration für die Funktion aussieht und welche externen Variablen "
"interessant sind."

#.  Not on Linux: .BR fropen ,
#.  Not on Linux: .BR fwopen ,
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following are defined as macros; these names may not be reused without "
"first removing their current definitions with B<#undef>: B<BUFSIZ>, B<EOF>, "
"B<FILENAME_MAX>, B<FOPEN_MAX>, B<L_cuserid>, B<L_ctermid>, B<L_tmpnam>, "
"B<NULL>, B<SEEK_END>, B<SEEK_SET>, B<SEEK_CUR>, B<TMP_MAX>, B<clearerr>, "
"B<feof>, B<ferror>, B<fileno>, B<getc>, B<getchar>, B<putc>, B<putchar>, "
"B<stderr>, B<stdin>, B<stdout>.  Function versions of the macro functions "
"B<feof>, B<ferror>, B<clearerr>, B<fileno>, B<getc>, B<getchar>, B<putc>, "
"and B<putchar> exist and will be used if the macros definitions are "
"explicitly removed."
msgstr ""
"Die folgenden Namen sind als Makros definiert. Sie dürfen nicht "
"wiederbenutzt werden ohne ihre vorherigen Definitionen vorher mit B<#undef> "
"zu entfernen: B<BUFSIZ>, B<EOF>, B<FILENAME_MAX>, B<FOPEN_MAX>, "
"B<L_cuserid>, B<L_ctermid>, B<L_tmpnam>, B<NULL>, B<SEEK_END>, B<SEEK_SET>, "
"B<SEEK_CUR>, B<TMP_MAX>, B<clearerr>, B<feof>, B<ferror>, B<fileno>, "
"B<getc>, B<getchar>, B<putc>, B<putchar>, B<stderr>, B<stdin>, B<stdout>. "
"Von den Makrofunktionen B<feof>, B<ferror>, B<clearerr>, B<fileno>, B<getc>, "
"B<getchar>, B<putc> und B<putchar> existieren Funktionsversionen. Diese "
"werden benutzt, wenn die Makrodefinitionen explizit entfernt werden."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "List of functions"
msgstr "Liste der Bibliotheksfunktionen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Function"
msgstr "Funktion"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Beschreibung"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "_"
msgstr "_"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<clearerr>(3)"
msgstr "B<clearerr>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "check and reset stream status"
msgstr "Status des Datenstroms prüfen und zurücksetzen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fclose>(3)"
msgstr "B<fclose>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "close a stream"
msgstr "Datenstrom schließen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fdopen>(3)"
msgstr "B<fdopen>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "stream open functions"
msgstr "Funktionen zum Öffnen eines Datenstroms"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<feof>(3)"
msgstr "B<feof>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ferror>(3)"
msgstr "B<ferror>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fflush>(3)"
msgstr "B<fflush>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "flush a stream"
msgstr "Datenstrom herausschreiben"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fgetc>(3)"
msgstr "B<fgetc>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "get next character or word from input stream"
msgstr "nächstes Zeichen oder Wort aus dem Eingabedatenstrom einlesen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fgetpos>(3)"
msgstr "B<fgetpos>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "reposition a stream"
msgstr "einen Datenstrom neu positionieren"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fgets>(3)"
msgstr "B<fgets>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "get a line from a stream"
msgstr "eine Zeile aus dem Datenstrom einlesen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fileno>(3)"
msgstr "B<fileno>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "return the integer descriptor of the argument stream"
msgstr "den Ganzzahldeskriptor eines Argumentdatenstroms zurückgeben"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fopen>(3)"
msgstr "B<fopen>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fprintf>(3)"
msgstr "B<fprintf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "formatted output conversion"
msgstr "formatierte Ausgabeumwandlung"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fpurge>(3)"
msgstr "B<fpurge>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fputc>(3)"
msgstr "B<fputc>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "output a character or word to a stream"
msgstr "ein Zeichen oder Wort in einen Datenstrom ausgeben"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fputs>(3)"
msgstr "B<fputs>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "output a line to a stream"
msgstr "eine Zeile in einen Datenstrom ausgeben"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fread>(3)"
msgstr "B<fread>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "binary stream input/output"
msgstr "binäre Datenstromein-/-ausgabe"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<freopen>(3)"
msgstr "B<freopen>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fscanf>(3)"
msgstr "B<fscanf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "input format conversion"
msgstr "formatbasierte Eingabeumwandlung"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fseek>(3)"
msgstr "B<fseek>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fsetpos>(3)"
msgstr "B<fsetpos>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ftell>(3)"
msgstr "B<ftell>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<fwrite>(3)"
msgstr "B<fwrite>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getc>(3)"
msgstr "B<getc>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getchar>(3)"
msgstr "B<getchar>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<gets>(3)"
msgstr "B<gets>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<getw>(3)"
msgstr "B<getw>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<mktemp>(3)"
msgstr "B<mktemp>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "make temporary filename (unique)"
msgstr "einen (eindeutigen) temporären Dateinamen erzeugen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<perror>(3)"
msgstr "B<perror>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "system error messages"
msgstr "Systemfehlermeldungen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<printf>(3)"
msgstr "B<printf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<putc>(3)"
msgstr "B<putc>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<putchar>(3)"
msgstr "B<putchar>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<puts>(3)"
msgstr "B<puts>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<putw>(3)"
msgstr "B<putw>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<remove>(3)"
msgstr "B<remove>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "remove directory entry"
msgstr "einen Verzeichniseintrag löschen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<rewind>(3)"
msgstr "B<rewind>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<scanf>(3)"
msgstr "B<scanf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<setbuf>(3)"
msgstr "B<setbuf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "stream buffering operations"
msgstr "Datenstrom-Puffereinstellungen"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<setbuffer>(3)"
msgstr "B<setbuffer>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<setlinebuf>(3)"
msgstr "B<setlinebuf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<setvbuf>(3)"
msgstr "B<setvbuf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<sprintf>(3)"
msgstr "B<sprintf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<sscanf>(3)"
msgstr "B<sscanf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<strerror>(3)"
msgstr "B<strerror>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<sys_errlist>(3)"
msgstr "B<sys_errlist>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<sys_nerr>(3)"
msgstr "B<sys_nerr>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<tempnam>(3)"
msgstr "B<tempnam>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "temporary file routines"
msgstr "Routinen für temporäre Dateien"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpfile>(3)"
msgstr "B<tmpfile>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<tmpnam>(3)"
msgstr "B<tmpnam>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ungetc>(3)"
msgstr "B<ungetc>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "un-get character from input stream"
msgstr "Zeichen zurück in den Eingabedatenstrom geben"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<vfprintf>(3)"
msgstr "B<vfprintf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<vfscanf>(3)"
msgstr "B<vfscanf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<vprintf>(3)"
msgstr "B<vprintf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<vscanf>(3)"
msgstr "B<vscanf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<vsprintf>(3)"
msgstr "B<vsprintf>(3)"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<vsscanf>(3)"
msgstr "B<vsscanf>(3)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C89, POSIX.1-2001."
msgstr "C89, POSIX.1-2001."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<close>(2), B<open>(2), B<read>(2), B<write>(2), B<stdout>(3), "
"B<unlocked_stdio>(3)"
msgstr ""
"B<close>(2), B<open>(2), B<read>(2), B<write>(2), B<stdout>(3), "
"B<unlocked_stdio>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-29"
msgstr "29. Dezember 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "The I<stdio> library conforms to C99."
msgstr "Die Bibliothek B<stdio> ist zu C99 konform."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "STDIO"
msgstr "STDIO"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-11-26"
msgstr "26. November 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>stdio.hE<gt>>"
msgstr "B<#include E<lt>stdio.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<FILE *>I<stdin>B<;>"
msgstr "B<FILE *>I<stdin>B<;>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<FILE *>I<stdout>B<;>"
msgstr "B<FILE *>I<stdout>B<;>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<FILE *>I<stderr>B<;>"
msgstr "B<FILE *>I<stderr>B<;>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"A stream is associated with an external file (which may be a physical "
"device) by I<opening> a file, which may involve creating a new file.  "
"Creating an existing file causes its former contents to be discarded.  If a "
"file can support positioning requests (such as a disk file, as opposed to a "
"terminal), then a I<file position indicator> associated with the stream is "
"positioned at the start of the file (byte zero), unless the file is opened "
"with append mode.  If append mode is used, it is unspecified whether the "
"position indicator will be placed at the start or the end of the file.  The "
"position indicator is maintained by subsequent reads, writes and positioning "
"requests.  All input occurs as if the characters were read by successive "
"calls to the B<fgetc>(3)  function; all output takes place as if all "
"characters were written by successive calls to the B<fputc>(3)  function."
msgstr ""
"Ein Datenstrom wird mit einer externen Datei (die ein physikalisches Gerät "
"sein kann) verbunden, indem eine Datei I<geöffnet> wird, was das Erzeugen "
"einer neuen Datei beinhaltet. Eine bestehende Datei zu erzeugen, verwirft "
"ihren bisherigen Inhalt. Wenn die Datei Positionierungsänderungen "
"unterstützt (wie eine Datei auf einer Festplatte, im Gegensatz zu einem "
"Terminal), dann wird ein I<Dateipositionszeiger> mit dem Datenstrom "
"verbunden und auf den Anfang der Datei positioniert (nulltes Byte), außer "
"wenn die Datei im Anhänge-Modus geöffnet wurde. Im Anhänge-Modus ist es "
"nicht spezifiziert, ob der Positionsanzeiger auf den Anfang oder das Ende "
"der Datei platziert wird. Der Positionszeiger wird durch nachfolgende Lese-, "
"Schreib- und Positionierungszugriffe bewegt. Jede Eingabe wird so "
"eingelesen, als ob die Funktion B<fgetc>(3) für jedes Zeichen einzeln "
"aufgerufen worden wäre. Die Ausgabe erfolgt, als würde jedes Zeichen einzeln "
"durch die Funktion B<fputc>(3) geschrieben."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: opensuse-leap-15-5
msgid "The I<stdio> library conforms to C89."
msgstr "Die Bibliothek B<stdio> ist zu C89 konform."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
