# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Patrick Rother <krd@gulu.net>, 1996.
# Chris Leick <c.leick@vollbio.de>, 2010, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de 3.40-0.1\n"
"POT-Creation-Date: 2023-06-27 19:45+0200\n"
"PO-Revision-Date: 2023-05-07 09:45+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "qsort"
msgstr "qsort"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30. März 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux-Handbuchseiten 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "qsort, qsort_r - sort an array"
msgstr "qsort, qsort_r - ein Feld sortieren"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTHEK"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standard-C-Bibliothek (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"B<void qsort(void >I<base>B<[.>I<size>B< * .>I<nmemb>B<], size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<           int (*>I<compar>B<)(const void [.>I<size>B<], const void [.>I<size>B<]));>\n"
"B<void qsort_r(void >I<base>B<[.>I<size>B< * .>I<nmemb>B<], size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<           int (*>I<compar>B<)(const void [.>I<size>B<], const void [.>I<size>B<], void *),>\n"
"B<           void *>I<arg>B<);>\n"
msgstr ""
"B<void qsort(void >I<basis>B<[.>I<groesse>B< * .>I<nmemb>B<], size_t >I<nmemb>B<, size_t >I<groesse>B<,>\n"
"B<           int (*>I<vergleich>B<)(const void [.>I<groesse>B<], const void [.>I<groesse>B<]));>\n"
"B<void qsort_r(void >I<basis>B<[.>I<groesse>B< * .>I<nmemb>B<], size_t >I<nmemb>B<, size_t >I<groesse>B<,>\n"
"B<           int (*>I<vergleich>B<)(const void [.>I<groesse>B<], const void [.>I<groesse>B<], void *),>\n"
"B<           void *>I<argument>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Mit Glibc erforderliche Feature-Test-Makros (siehe "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<qsort_r>():"
msgstr "B<qsort_r>():"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<qsort>()  function sorts an array with I<nmemb> elements of size "
"I<size>.  The I<base> argument points to the start of the array."
msgstr ""
"Die Funktion B<qsort>() sortiert ein Feld mit I<nmemb> Elementen der Größe "
"I<groesse>. Das Argument I<basis> zeigt auf den Anfang des Feldes."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The contents of the array are sorted in ascending order according to a "
"comparison function pointed to by I<compar>, which is called with two "
"arguments that point to the objects being compared."
msgstr ""
"Die Inhalte des Feldes werden in aufsteigender Reihenfolge sortiert, bezogen "
"auf eine Vergleichsfunktion, auf die I<vergleich> zeigt. Diese wird mit zwei "
"Argumenten aufgerufen, die auf die zu vergleichenden Objekte zeigen."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The comparison function must return an integer less than, equal to, or "
"greater than zero if the first argument is considered to be respectively "
"less than, equal to, or greater than the second.  If two members compare as "
"equal, their order in the sorted array is undefined."
msgstr ""
"Die Vergleichsfunktion muss eine Ganzzahl zurückgeben, die kleiner, gleich "
"oder größer Null ist, je nachdem, ob das erste Argument kleiner, gleich oder "
"größer als das zweite ist. Wenn zwei Inhalte des Feldes gleich sind, ist "
"ihre Reihenfolge unbestimmt."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<qsort_r>()  function is identical to B<qsort>()  except that the "
"comparison function I<compar> takes a third argument.  A pointer is passed "
"to the comparison function via I<arg>.  In this way, the comparison function "
"does not need to use global variables to pass through arbitrary arguments, "
"and is therefore reentrant and safe to use in threads."
msgstr ""
"Die Funktion B<qsort_r>() ist mit B<qsort>() identisch, außer, dass die "
"Vergleichsfunktion I<compar> ein drittes Argument entgegennimmt. An die "
"Vergleichsfunktion wird ein Zeiger per I<argument> übergeben. Auf diese "
"Weise benötigt die Vergleichsfunktion keine globalen Variablen, um beliebige "
"Argumente weiterzureichen und ist daher ablaufinvariant und sicher für die "
"Verwendung in Threads."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<qsort>()  and B<qsort_r>()  functions return no value."
msgstr "Die Funktionen B<qsort>() und B<qsort_r>() geben keinen Wert zurück."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<qsort>(),\n"
"B<qsort_r>()"
msgstr ""
"B<qsort>(),\n"
"B<qsort_r>()"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<qsort>()"
msgstr "B<qsort>()"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11, POSIX.1-2008."
msgstr "C11, POSIX.1-2008."

#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, C89, SVr4, 4.3BSD."
msgstr "POSIX.1-2001, C89, SVr4, 4.3BSD."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "B<qsort_r>()"
msgstr "B<qsort_r>()"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "glibc 2.8."
msgstr "Glibc 2.8."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"To compare C strings, the comparison function can call B<strcmp>(3), as "
"shown in the example below."
msgstr ""
"Um C-Zeichenketten zu vergleichen, rufen Sie B<strcmp>(3) auf, wie es im "
"folgenden Beispiel gezeigt wird."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For one example of use, see the example under B<bsearch>(3)."
msgstr "Ein Beispiel für den Gebrauch finden Sie unter B<bsearch>(3)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Another example is the following program, which sorts the strings given in "
"its command-line arguments:"
msgstr ""
"Ein weiteres Beispiel ist das folgende Programm, das Zeichenketten sortiert, "
"die als Befehlszeilen-Argumente übergebenen werden:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>string.hE<gt>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"static int\n"
"cmpstringp(const void *p1, const void *p2)\n"
"{\n"
"    /* The actual arguments to this function are \"pointers to\n"
"       pointers to char\", but strcmp(3) arguments are \"pointers\n"
"       to char\", hence the following cast plus dereference. */\n"
msgstr ""
"static int\n"
"cmpstringp(const void *p1, const void *p2)\n"
"{\n"
"    /* Die tatsächlichen Argumente dieser Funktion sind »Zeiger auf\n"
"       Zeiger auf char«, strcmp(3)-Argumente sind aber »Zeiger auf\n"
"       char«, daher wird im Folgenden umgewandelt und zurückverfolgt. */\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    return strcmp(*(const char **) p1, *(const char **) p2);\n"
"}\n"
msgstr ""
"    return strcmp(*(const char **) p1, *(const char **) p2);\n"
"}\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>stringE<gt>...\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Aufruf: %s E<lt>ZeichenketteE<gt>…\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "    qsort(&argv[1], argc - 1, sizeof(char *), cmpstringp);\n"
msgstr "    qsort(&argv[1], argc - 1, sizeof(char *), cmpstringp);\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"    for (size_t j = 1; j E<lt> argc; j++)\n"
"        puts(argv[j]);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    for (size_t j = 1; j E<lt> argc; j++)\n"
"        puts(argv[j]);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. #-#-#-#-#  archlinux: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-bookworm: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  debian-unstable: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-38: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  fedora-rawhide: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  mageia-cauldron: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#. #-#-#-#-#  opensuse-leap-15-5: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: SH
#. #-#-#-#-#  opensuse-tumbleweed: qsort.3.pot (PACKAGE VERSION)  #-#-#-#-#
#.  SRC END
#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<sort>(1), B<alphasort>(3), B<strcmp>(3), B<versionsort>(3)"
msgstr "B<sort>(1), B<alphasort>(3), B<strcmp>(3), B<versionsort>(3)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-01-07"
msgstr "7. Januar 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: SH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONEN"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "B<qsort_r>()  was added in glibc 2.8."
msgstr "B<qsort_r>() wurde zur Glibc 2.8 hinzugefügt."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid "B<qsort>(): POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."
msgstr "B<qsort>(): POSIX.1-2001, POSIX.1-2008, C99, SVr4, 4.3BSD."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "QSORT"
msgstr "QSORT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<void qsort(void *>I<base>B<, size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<           int (*>I<compar>B<)(const void *, const void *));>\n"
msgstr ""
"B<void qsort(void *>I<basis>B<, size_t >I<nmemb>B<, size_t >I<groesse>B<,>\n"
"B<           int (*>I<vergleich>B<)(const void *, const void *));>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<void qsort_r(void *>I<base>B<, size_t >I<nmemb>B<, size_t >I<size>B<,>\n"
"B<           int (*>I<compar>B<)(const void *, const void *, void *),>\n"
"B<           void *>I<arg>B<);>\n"
msgstr ""
"B<void qsort_r(void *>I<basis>B<, size_t >I<nmemb>B<, size_t >I<groesse>B<,>\n"
"B<           int (*>I<vergleich>B<)(const void *, const void *, void *),>\n"
"B<           void *>I<argument>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<qsort_r>(): _GNU_SOURCE"
msgstr "B<qsort_r>(): _GNU_SOURCE"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<qsort_r>()  was added to glibc in version 2.8."
msgstr "B<qsort_r>() wurde Glibc in Version 2.8 hinzugefügt."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<qsort>(): POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."
msgstr "B<qsort>(): POSIX.1-2001, POSIX.1-2008, C89, C99, SVr4, 4.3BSD."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"static int\n"
"cmpstringp(const void *p1, const void *p2)\n"
"{\n"
"    /* The actual arguments to this function are \"pointers to\n"
"       pointers to char\", but strcmp(3) arguments are \"pointers\n"
"       to char\", hence the following cast plus dereference */\n"
msgstr ""
"static int\n"
"cmpstringp(const void *p1, const void *p2)\n"
"{\n"
"    /* Die tatsächlichen Argumente dieser Funktion sind »Zeiger auf\n"
"       Zeiger auf char«, strcmp(3)-Argumente sind aber »Zeiger auf\n"
"       char«, daher wird im Folgenden umgewandelt und zurückverfolgt*/\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    return strcmp(* (char * const *) p1, * (char * const *) p2);\n"
"}\n"
msgstr ""
"    return strcmp(* (char * const *) p1, * (char * const *) p2);\n"
"}\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int j;\n"
msgstr ""
"int\n"
"main(int argc, char *argv[])\n"
"{\n"
"    int j;\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Usage: %s E<lt>stringE<gt>...\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"
msgstr ""
"    if (argc E<lt> 2) {\n"
"        fprintf(stderr, \"Aufruf: %s E<lt>ZeichenketteE<gt>…\\en\", argv[0]);\n"
"        exit(EXIT_FAILURE);\n"
"    }\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"    for (j = 1; j E<lt> argc; j++)\n"
"        puts(argv[j]);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"
msgstr ""
"    for (j = 1; j E<lt> argc; j++)\n"
"        puts(argv[j]);\n"
"    exit(EXIT_SUCCESS);\n"
"}\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können, sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
