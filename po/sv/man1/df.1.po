# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:24+0200\n"
"PO-Revision-Date: 2023-05-02 13:28+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DF"
msgstr "DF"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2023"
msgstr "april 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.3"
msgstr "GNU coreutils 9.3"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "df - report file system space usage"
msgstr "df — rapportera användning av filsystemsutrymme"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<df> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<df> [I<\\,FLAGGA\\/>]... [I<\\,FIL\\/>]..."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<df>.  B<df> displays the "
"amount of space available on the file system containing each file name "
"argument.  If no file name is given, the space available on all currently "
"mounted file systems is shown.  Space is shown in 1K blocks by default, "
"unless the environment variable POSIXLY_CORRECT is set, in which case 512-"
"byte blocks are used."
msgstr ""
"Denna manualsida dokumenterar GNU-versionen av B<df>. B<df> visar mängden "
"tillgängligt utrymme på det filsystem som innehåller varje filnamnsargument. "
"Om inget sådant argument ges visas det tillgängliga utrymmet på alla just nu "
"monterade filsystem. Utrymmet visas i 1k-block som standard, om inte "
"miljövariabeln POSIXLY_CORRECT är satt, då 512-byte-block används."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"If an argument is the absolute file name of a device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires very nonportable intimate knowledge "
"of file system structures."
msgstr ""
"Om ett argument är det absoluta filnamnet på en enhetsnod som innehåller ett "
"monterat filsystem visar B<df> utrymmet på det filsystemet snarare än "
"filsystemet som innehåller enhetsnoden. Denna version av B<df> kan inte visa "
"det tillgängliga utrymmet på omonterade filsystem, eftersom det på de flesta "
"sorts system att göra så kräver väldigt oportabel ingående kunskap om "
"filsystemsstrukturer."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "FLAGGOR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Show information about the file system on which each FILE resides, or all "
"file systems by default."
msgstr ""
"Visa information om filsystemet där varje FIL ligger, eller annars alla "
"filsystem."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Obligatoriska argument till långa flaggor är obligatoriska även för de korta."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all>"
msgstr "B<-a>, B<--all>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "include pseudo, duplicate, inaccessible file systems"
msgstr "Tag med pseudo-, dubblett-, oåtkomliga filsystem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--block-size>=I<\\,SIZE\\/>"
msgstr "B<-B>, B<--block-size>=I<\\,STRL\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"scale sizes by SIZE before printing them; e.g., \\&'-BM' prints sizes in "
"units of 1,048,576 bytes; see SIZE format below"
msgstr ""
"Skala storlekar med STRL före de skrivs ut; t.ex. \\&”-BM” skriver storlekar "
"i enheter av 1 048 576 byte; se STORLEKsformat nedan."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--human-readable>"
msgstr "B<-h>, B<--human-readable>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "print sizes in powers of 1024 (e.g., 1023M)"
msgstr "Skriv storlekar i potenser av 1024 (t.ex. 1023M)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--si>"
msgstr "B<-H>, B<--si>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "print sizes in powers of 1000 (e.g., 1.1G)"
msgstr "Skriv storlekar i potenser av 1000 (t.ex. 1,1G)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--inodes>"
msgstr "B<-i>, B<--inodes>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "list inode information instead of block usage"
msgstr "Skriv inodinformation istället för blockinformation."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>"
msgstr "B<-k>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "like B<--block-size>=I<\\,1K\\/>"
msgstr "Som B<--block-size>=I<\\,1K\\/>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--local>"
msgstr "B<-l>, B<--local>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "limit listing to local file systems"
msgstr "Visa endast lokala filsystem."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-sync>"
msgstr "B<--no-sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not invoke sync before getting usage info (default)"
msgstr "Anropa inte sync innan information hämtas (normalfall)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--output>[=I<\\,FIELD_LIST\\/>]"
msgstr "B<--output>[=I<\\,FÄLTLISTA\\/>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"use the output format defined by FIELD_LIST, or print all fields if "
"FIELD_LIST is omitted."
msgstr ""
"Använd utformatet definierat av FÄLTLISTA, eller skriv alla fält om "
"FÄLTLISTA utelämnas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--portability>"
msgstr "B<-P>, B<--portability>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use the POSIX output format"
msgstr "Använd POSIX-format."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--sync>"
msgstr "B<--sync>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "invoke sync before getting usage info"
msgstr "Anropa sync innan information hämtas."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--total>"
msgstr "B<--total>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"elide all entries insignificant to available space, and produce a grand total"
msgstr ""
"Utelämna alla poster som inte påverkar tillgängligt utrymme, och skriv en "
"totalsumma."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,TYP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "limit listing to file systems of type TYPE"
msgstr "Begränsa listningen till filsystem av typen TYP."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-T>, B<--print-type>"
msgstr "B<-T>, B<--print-type>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "print file system type"
msgstr "Skriv ut filsystemtyp."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-x>, B<--exclude-type>=I<\\,TYPE\\/>"
msgstr "B<-x>, B<--exclude-type>=I<\\,TYP\\/>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "limit listing to file systems not of type TYPE"
msgstr "Utelämna filsystem av typ TYP."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>"
msgstr "B<-v>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "(ignored)"
msgstr "(ignorerad)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "visa denna hjälp och avsluta"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "visa versionsinformation och avsluta"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Display values are in units of the first available SIZE from B<--block-"
"size>, and the DF_BLOCK_SIZE, BLOCK_SIZE and BLOCKSIZE environment "
"variables.  Otherwise, units default to 1024 bytes (or 512 if "
"POSIXLY_CORRECT is set)."
msgstr ""
"Visa värden i enheter av första tillgängliga STORLEK från B<--block-size>,"
"och miljövariablerna DF_BLOCK_SIZE, BLOCK_SIZE och BLOCKSIZE.  Annars är "
"enheten som standard 1024 byte (eller 512 om POSIXLY_CORRECT är satt)."

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y,R,Q (powers of 1024) or KB,MB,... "
"(powers of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"Argumentet STORLEK är ett heltal och eventuell enhet (exempel: 10K är "
"10·1024). Enheter är K, M, G, T, P, E, Z, Y, R, Q (potenser av 1024) eller "
"KB, MB, … (potenser av 1000). Binära prefix kan också användas: KiB=K, "
"MiB=M, och så vidare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"FIELD_LIST is a comma-separated list of columns to be included.  Valid field "
"names are: 'source', 'fstype', 'itotal', 'iused', 'iavail', 'ipcent', "
"\\&'size', 'used', 'avail', 'pcent', 'file' and 'target' (see info page)."
msgstr ""
"FÄLTLISTA är en kommaseparerad lista med kolumner som skall vara med.  "
"Giltiga fältnamn är: ”source”, ”fstype”, ”itotal”, ”iused”, ”iavail”, "
"”ipcent”, ”size”, ”used”, ”avail”, ”pcent”, ”file” och ”target” (se "
"infosidan)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Torbjorn Granlund, David MacKenzie, and Paul Eggert."
msgstr "Skrivet av Torbjorn Granlund, David MacKenzie och Paul Eggert."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU coreutils hjälp på nätet: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportera anmärkningar på översättningen till E<lt>tp-sv@listor.tp-sv."
"seE<gt>"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2023 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Full documentation E<lt>https://www.gnu.org/software/coreutils/dfE<gt>"
msgstr ""
"Fullständig dokumentation E<lt>https://www.gnu.org/software/coreutils/dfE<gt>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) df invocation\\(aq"
msgstr ""
"eller tillgängligt lokalt via: info \\(aq(coreutils) df invocation\\(aq"

#. type: TH
#: debian-bookworm debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"The SIZE argument is an integer and optional unit (example: 10K is "
"10*1024).  Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers "
"of 1000).  Binary prefixes can be used, too: KiB=K, MiB=M, and so on."
msgstr ""
"Argumentet STORLEK är ett heltal och eventuell enhet (exempel: 10K är "
"10·1024). Enheter är K, M, G, T, P, E, Z, Y (potenser av 1024) eller KB, MB, "
"… (potenser av 1000). Binära prefix kan också användas: KiB=K, MiB=M, och så "
"vidare."

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: fedora-38
#, no-wrap
msgid "January 2023"
msgstr "januari 2023"

#. type: TP
#: fedora-38 fedora-rawhide
#, no-wrap
msgid "B<--direct>"
msgstr "B<--direct>"

#. type: Plain text
#: fedora-38 fedora-rawhide
msgid "show statistics for a file instead of mount point"
msgstr "visa värden för en fil istället för en monteringspunkt"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "april 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: opensuse-leap-15-5
msgid "df - report file system disk space usage"
msgstr "df — rapportera användning av filsystemsdiskutrymme"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This manual page documents the GNU version of B<df>.  B<df> displays the "
"amount of disk space available on the file system containing each file name "
"argument.  If no file name is given, the space available on all currently "
"mounted file systems is shown.  Disk space is shown in 1K blocks by default, "
"unless the environment variable POSIXLY_CORRECT is set, in which case 512-"
"byte blocks are used."
msgstr ""
"Denna manualsida dokumenterar GNU-versionen av B<df>. B<df> visar mängden "
"tillgängligt diskutrymme på det filsystem som innehåller varje "
"filnamnsargument. Om inget sådant argument ges visas det tillgängliga "
"utrymmet på alla just nu monterade filsystem. Diskutrymmet visas i 1k-block "
"som standard, om inte miljövariabeln POSIXLY_CORRECT är satt, då 512-byte-"
"block används."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"If an argument is the absolute file name of a disk device node containing a "
"mounted file system, B<df> shows the space available on that file system "
"rather than on the file system containing the device node.  This version of "
"B<df> cannot show the space available on unmounted file systems, because on "
"most kinds of systems doing so requires very nonportable intimate knowledge "
"of file system structures."
msgstr ""
"Om ett argument är det absoluta filnamnet på en diskenhetsnod som innehåller "
"ett monterat filsystem visar B<df> utrymmet på det filsystemet snarare än "
"filsystemet som innehåller enhetsnoden. Denna version av B<df> kan inte visa "
"det tillgängliga utrymmet på omonterade filsystem, eftersom det på de flesta "
"sorts system att göra så kräver väldigt oportabel ingående kunskap om "
"filsystemsstrukturer."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
