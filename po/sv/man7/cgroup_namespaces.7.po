# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Göran Uddeborg <goeran@uddeborg.se>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.19.0\n"
"POT-Creation-Date: 2023-06-27 19:22+0200\n"
"PO-Revision-Date: 2023-06-11 21:30+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <Tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "cgroup_namespaces"
msgstr "cgroup_namespaces"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 mars 2023"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "cgroup_namespaces - overview of Linux cgroup namespaces"
msgstr "cgroup_namespaces — översikt över Linux cgroup-namnrymder"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "For an overview of namespaces, see B<namespaces>(7)."
msgstr "För en översikt över namnrymder, se B<namespaces>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Cgroup namespaces virtualize the view of a process's cgroups (see "
"B<cgroups>(7))  as seen via I</proc/>pidI</cgroup> and I</proc/>pidI</"
"mountinfo>."
msgstr ""
"Cgroup-namnrymder virtualiserar vyn av en process cgroup:er (se "
"B<cgroups>(7)) som de syns via I</proc/>pidI</cgroup> och I</proc/>pidI</"
"mountinfo>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Each cgroup namespace has its own set of cgroup root directories.  These "
"root directories are the base points for the relative locations displayed in "
"the corresponding records in the I</proc/>pidI</cgroup> file.  When a "
"process creates a new cgroup namespace using B<clone>(2)  or B<unshare>(2)  "
"with the B<CLONE_NEWCGROUP> flag, its current cgroups directories become the "
"cgroup root directories of the new namespace.  (This applies both for the "
"cgroups version 1 hierarchies and the cgroups version 2 unified hierarchy.)"
msgstr ""
"Varje cgroup-namnrymd har sin egen uppsättning av cgroup-rotkataloger. Dessa "
"rotkataloger är baspunkterna för de relativa platserna som visas i "
"motsvarande poster i filen I</proc/>pidI</cgroup>. När en process skapar en "
"ny cgroup-namnrymd med B<clone>(2) eller B<unshare>(2) med flaggan "
"B<CLONE_NEWCGROUP> blir dess aktuella cgroup-kataloger cgroup-rotkataloger i "
"den nya namnrymden. (Detta gäller både för cgroup version 1-hierarkierna och "
"den unifierade hierarkin i cgroup version 2.)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When reading the cgroup memberships of a \"target\" process from I</proc/"
">pidI</cgroup>, the pathname shown in the third field of each record will be "
"relative to the reading process's root directory for the corresponding "
"cgroup hierarchy.  If the cgroup directory of the target process lies "
"outside the root directory of the reading process's cgroup namespace, then "
"the pathname will show I<../> entries for each ancestor level in the cgroup "
"hierarchy."
msgstr ""
"När man läser cgroup-medlemskapen för en ”mål”-process från I</proc/>pidI</"
"cgroup> kommer sökvägsnamnet som visas i det tredje fältet av varje post att "
"vara relativt den läsande processens rotkatalog för motsvarande cgroup-"
"hierarki. Om cgroup-katalogen för målprocessen ligger utanför rotkatalogen "
"för den läsande processens cgroup-namnrymd kommer sökvägsnamnet visa I<../>-"
"poster för varje anfadernivå i cgroup-hierarkin."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The following shell session demonstrates the effect of creating a new cgroup "
"namespace."
msgstr ""
"Följande skalsession demonstrerar effekten av att skapa en ny cgroup-"
"namnrymd."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"First, (as superuser) in a shell in the initial cgroup namespace, we create "
"a child cgroup in the I<freezer> hierarchy, and place a process in that "
"cgroup that we will use as part of the demonstration below:"
msgstr ""
"Först (som rot) i ett skal i den initiala cgroup-namnrymden skapar vi en "
"barn-cgroup i hierarkin I<freezer>, och lägger in en process i denna cgroup "
"som vi kommer använda som en del av vår demonstration nedan:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<mkdir -p /sys/fs/cgroup/freezer/sub2>\n"
"# B<sleep 10000 &>     # Create a process that lives for a while\n"
"[1] 20124\n"
"# B<echo 20124 E<gt> /sys/fs/cgroup/freezer/sub2/cgroup.procs>\n"
msgstr ""
"# B<mkdir -p /sys/fs/cgroup/freezer/sub2>\n"
"# B<sleep 10000 &>     # Skapa en process som lever ett tag\n"
"[1] 20124\n"
"# B<echo 20124 E<gt> /sys/fs/cgroup/freezer/sub2/cgroup.procs>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"We then create another child cgroup in the I<freezer> hierarchy and put the "
"shell into that cgroup:"
msgstr ""
"Sedan skapar vi en annan barn-cgroup i hierarkin I<freezer> och lägger in "
"skalet i denna cgroup:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"# B<mkdir -p /sys/fs/cgroup/freezer/sub>\n"
"# B<echo $$>                      # Show PID of this shell\n"
"30655\n"
"# B<echo 30655 E<gt> /sys/fs/cgroup/freezer/sub/cgroup.procs>\n"
"# B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/sub\n"
msgstr ""
"# B<mkdir -p /sys/fs/cgroup/freezer/sub>\n"
"# B<echo $$>                      # Visa detta skals PID\n"
"30655\n"
"# B<echo 30655 E<gt> /sys/fs/cgroup/freezer/sub/cgroup.procs>\n"
"# B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/sub\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Next, we use B<unshare>(1)  to create a process running a new shell in new "
"cgroup and mount namespaces:"
msgstr ""
"Därefter använder vi B<unshare>(1) för att skapa en process som kör ett nytt "
"skal i nya cgroup- och monteringsnamnrymder:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "# B<PS1=\"sh2# \" unshare -Cm bash>\n"
msgstr "# B<PS1=\"sh2# \" unshare -Cm bash>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"From the new shell started by B<unshare>(1), we then inspect the I</proc/"
">pidI</cgroup> files of, respectively, the new shell, a process that is in "
"the initial cgroup namespace (I<init>, with PID 1), and the process in the "
"sibling cgroup (I<sub2>):"
msgstr ""
"Från det nya skalet som startades av B<unshare>(1) inspekterar vi sedan "
"filerna I</proc/>pidI</cgroup> för det nya skalet, en process som finns i "
"den initiala cgroup-namnrymden (I<init>, med PID 1) respektive processen i "
"syskon-cgroup:en (I<sub2>):"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"sh2# B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/\n"
"sh2# B<cat /proc/1/cgroup | grep freezer>\n"
"7:freezer:/..\n"
"sh2# B<cat /proc/20124/cgroup | grep freezer>\n"
"7:freezer:/../sub2\n"
msgstr ""
"sh2# B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/\n"
"sh2# B<cat /proc/1/cgroup | grep freezer>\n"
"7:freezer:/..\n"
"sh2# B<cat /proc/20124/cgroup | grep freezer>\n"
"7:freezer:/../sub2\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"From the output of the first command, we see that the freezer cgroup "
"membership of the new shell (which is in the same cgroup as the initial "
"shell)  is shown defined relative to the freezer cgroup root directory that "
"was established when the new cgroup namespace was created.  (In absolute "
"terms, the new shell is in the I</sub> freezer cgroup, and the root "
"directory of the freezer cgroup hierarchy in the new cgroup namespace is "
"also I</sub>.  Thus, the new shell's cgroup membership is displayed as "
"\\[aq]/\\[aq].)"
msgstr ""
"Från utdata från det första kommandot ser vi att medlemskapet i cgroup:en "
"freezer för det nya skalet (vilket finns i samma cgroup som det initiala "
"skalet) visas definierat relativt rotkatalogen för cgroup:en freezer som "
"etablerades när den nya cgroup-namnrymden skapades. (I absoluta termer, det "
"nya skalet finns i freezer-cgroup:en I</sub>, och rotkataloger i freezer-"
"cgroup-hierarkin i den nya cgroup-namnrymden är också I</sub>. Alltså, det "
"nya skalets cgroup-medlemskap visas som \\[aq]/\\[aq].)"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"However, when we look in I</proc/self/mountinfo> we see the following "
"anomaly:"
msgstr "Dock, när vi tittar i I</proc/self/mountinfo> ser vi följande anomali:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"sh2# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 /.. /sys/fs/cgroup/freezer ...\n"
msgstr ""
"sh2# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 /.. /sys/fs/cgroup/freezer …\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The fourth field of this line (I</..>)  should show the directory in the "
"cgroup filesystem which forms the root of this mount.  Since by the "
"definition of cgroup namespaces, the process's current freezer cgroup "
"directory became its root freezer cgroup directory, we should see \\[aq]/"
"\\[aq] in this field.  The problem here is that we are seeing a mount entry "
"for the cgroup filesystem corresponding to the initial cgroup namespace "
"(whose cgroup filesystem is indeed rooted at the parent directory of "
"I<sub>).  To fix this problem, we must remount the freezer cgroup filesystem "
"from the new shell (i.e., perform the mount from a process that is in the "
"new cgroup namespace), after which we see the expected results:"
msgstr ""
"Det fjärde fältet på denna rad (I</..>) skulle visa katalogen i cgroup-"
"filsystemet som utgör roten för denna montering. Eftersom enligt "
"definitionen av cgroup-namnrymder processens aktuella cgroup-katalog för "
"freezer blev dess rot-freezer-cgroup-katalog borde vi se \\[aq]/\\[aq] i "
"detta fält. Problemet här är att vi ser en monteringspost för cgroup-"
"filsystemet som motsvarar den initiala cgroup-namnrymden (vars cgroup-"
"filsystem verkligen är rotat i föräldrakatalogen till I<sub>). För att lösa "
"detta problem måste vi montera om freezer-cgroup-filsystemet från det nya "
"skalet (d.v.s., utföra monteringen från en process som finns i den nya "
"cgroup-namnrymden), varefter vi ser det förväntade resultatet:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"sh2# B<mount --make-rslave />     # Don\\[aq]t propagate mount events\n"
"                               # to other namespaces\n"
"sh2# B<umount /sys/fs/cgroup/freezer>\n"
"sh2# B<mount -t cgroup -o freezer freezer /sys/fs/cgroup/freezer>\n"
"sh2# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 / /sys/fs/cgroup/freezer rw,relatime ...\n"
msgstr ""
"sh2# B<mount --make-rslave />     # Propagera inte monteringshändelser\n"
"                               # till andra namnrymder\n"
"sh2# B<umount /sys/fs/cgroup/freezer>\n"
"sh2# B<mount -t cgroup -o freezer freezer /sys/fs/cgroup/freezer>\n"
"sh2# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 / /sys/fs/cgroup/freezer rw,relatime …\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDER"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTERINGAR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Use of cgroup namespaces requires a kernel that is configured with the "
"B<CONFIG_CGROUPS> option."
msgstr ""
"Användning av cgroup-namnrymder kräver en kärna som är konfigurerad med "
"alternativet B<CONFIG_CGROUPS>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The virtualization provided by cgroup namespaces serves a number of purposes:"
msgstr ""
"Virtualiseringen som erbjuds av cgroup-namnrymder tjänar ett antal syften:"

#. type: IP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "\\[bu]"
msgstr "\\[bu]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It prevents information leaks whereby cgroup directory paths outside of a "
"container would otherwise be visible to processes in the container.  Such "
"leakages could, for example, reveal information about the container "
"framework to containerized applications."
msgstr ""
"Det förhindrar att information läcker genom att en cgroup-katalogsökväg "
"utanför en behållare annars skulle vara synlig för processer i behållaren. "
"Sådant läckage skulle, till exempel, kunna avslöja information om "
"behållarramverket för program i behållare."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It eases tasks such as container migration.  The virtualization provided by "
"cgroup namespaces allows containers to be isolated from knowledge of the "
"pathnames of ancestor cgroups.  Without such isolation, the full cgroup "
"pathnames (displayed in I</proc/self/cgroups>)  would need to be replicated "
"on the target system when migrating a container; those pathnames would also "
"need to be unique, so that they don't conflict with other pathnames on the "
"target system."
msgstr ""
"Det förenklar uppgifter såsom migrering av behållare. Virtualiseringen som "
"erbjuds av cgroup-namnrymder gör att behållare kan isoleras från kunskap om "
"sökvägarna till anfader-cgroup:er. Utan sådan isolation skulle de "
"fullständiga cgroup-sökvägarna (som visas i I</proc/self/cgroups>) behöva "
"återskapas på målsystemet när en behållare migreras; dessa sökvägsnamn "
"skulle också behöva vara unika, så att de inte skulle stå i konflikt med "
"andra sökvägsnamn på målsystemet."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"It allows better confinement of containerized processes, because it is "
"possible to mount the container's cgroup filesystems such that the container "
"processes can't gain access to ancestor cgroup directories.  Consider, for "
"example, the following scenario:"
msgstr ""
"Det möjliggör bättre begränsning av processer i behållare, för att det är "
"möjligt att montera behållarens cgroup-filsystem så att behållarprocessen "
"inte kan få tillgång till kataloger för anfäders cgroup. Betänk, till "
"exempel, följande scenario:"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "We have a cgroup directory, I</cg/1>, that is owned by user ID 9000."
msgstr "Vi har en cgroup-katalog, I</cg/1>, som ägs av användar-ID 9000."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"We have a process, I<X>, also owned by user ID 9000, that is namespaced "
"under the cgroup I</cg/1/2> (i.e., I<X> was placed in a new cgroup namespace "
"via B<clone>(2)  or B<unshare>(2)  with the B<CLONE_NEWCGROUP> flag)."
msgstr ""
"Vi har en process, I<X>, som också ägs av användar-ID 9000, som har "
"namnrymden under cgroup I</cg/1/2> (d.v.s., I<X> placerades i en ny cgroup-"
"namnrymd via B<clone>(2) eller B<unshare>(2) med flagga B<CLONE_NEWCGROUP>."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In the absence of cgroup namespacing, because the cgroup directory I</cg/1> "
"is owned (and writable) by UID 9000 and process I<X> is also owned by user "
"ID 9000, process I<X> would be able to modify the contents of cgroups files "
"(i.e., change cgroup settings) not only in I</cg/1/2> but also in the "
"ancestor cgroup directory I</cg/1>.  Namespacing process I<X> under the "
"cgroup directory I</cg/1/2>, in combination with suitable mount operations "
"for the cgroup filesystem (as shown above), prevents it modifying files in "
"I</cg/1>, since it cannot even see the contents of that directory (or of "
"further removed cgroup ancestor directories).  Combined with correct "
"enforcement of hierarchical limits, this prevents process I<X> from escaping "
"the limits imposed by ancestor cgroups."
msgstr ""
"I avsaknad av cgroup-namnrymder skulle, eftersom cgroup-katalogen I</cg/1> "
"ägs (och är skrivbar) av AID 9000 och processen I<X> också ägs av användar-"
"ID 9000, process I<X> kunna ändra innehållet i cgroup-filer (d.v.s., ändra "
"cgroup-inställningar) inte bara i I</cg/1/2> utan även i anfader-cgroup-"
"katalogen I</cg/1>. Att namnrymda processen I<X> under cgroup-katalogen I</"
"cg/1/2>, i kombination med lämpliga monteringsoperationer av cgroup-"
"filsystemet (som visas ovan), förhindrar den från att ändra filer i I</"
"cg/1>, eftersom den inte ens kan se innehållet i den katalogen (eller i mer "
"avlägsna cgroup-anfaderkataloger). Kombinerat med korrekt verkställighet av "
"hierarkiska begränsningar förhindrar detta process I<X> från att fly från "
"begränsningarna som läggs på av anfader-cgroup:er."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE ÄVEN"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<unshare>(1), B<clone>(2), B<setns>(2), B<unshare>(2), B<proc>(5), "
"B<cgroups>(7), B<credentials>(7), B<namespaces>(7), B<user_namespaces>(7)"
msgstr ""
"B<unshare>(1), B<clone>(2), B<setns>(2), B<unshare>(2), B<proc>(5), "
"B<cgroups>(7), B<credentials>(7), B<namespaces>(7), B<user_namespaces>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2023-02-05"
msgstr "5 februari 2023"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "Namespaces are a Linux-specific feature."
msgstr "Namnrymder är en Linux-specifik funktion."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "CGROUP_NAMESPACES"
msgstr "CGROUP_NAMESPACES"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 september 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux programmerarmanual"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Cgroup namespaces virtualize the view of a process's cgroups (see "
"B<cgroups>(7))  as seen via I</proc/[pid]/cgroup> and I</proc/[pid]/"
"mountinfo>."
msgstr ""
"Cgroup-namnrymder virtualiserar vyn av en process cgroup:er (se "
"B<cgroups>(7)) som de syns via I</proc/>pidI</cgroup> och I</proc[pid]/"
"mountinfo>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Each cgroup namespace has its own set of cgroup root directories.  These "
"root directories are the base points for the relative locations displayed in "
"the corresponding records in the I</proc/[pid]/cgroup> file.  When a process "
"creates a new cgroup namespace using B<clone>(2)  or B<unshare>(2)  with the "
"B<CLONE_NEWCGROUP> flag, it enters a new cgroup namespace in which its "
"current cgroups directories become the cgroup root directories of the new "
"namespace.  (This applies both for the cgroups version 1 hierarchies and the "
"cgroups version 2 unified hierarchy.)"
msgstr ""
"Varje cgroup-namnrymd har sin egen uppsättning av cgroup-rotkataloger. Dessa "
"rotkataloger är baspunkterna för de relativa platserna som visas i "
"motsvarande poster i filen I</proc/[pid]/cgroup>. När en process skapar en "
"ny cgroup-namnrymd med B<clone>(2) eller B<unshare>(2) med flaggan "
"B<CLONE_NEWCGROUP> går den in i en ny cgroup-namespace i vilken dess "
"aktuella cgroup-kataloger cgroup-rotkataloger i den nya namnrymden. (Detta "
"gäller både för cgroup version 1-hierarkierna och den unifierad hierarkin i "
"cgroup version 2.)"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"When viewing I</proc/[pid]/cgroup>, the pathname shown in the third field of "
"each record will be relative to the reading process's root directory for the "
"corresponding cgroup hierarchy.  If the cgroup directory of the target "
"process lies outside the root directory of the reading process's cgroup "
"namespace, then the pathname will show I<../> entries for each ancestor "
"level in the cgroup hierarchy."
msgstr ""
"När man betraktar I</proc/[pid]/cgroup> kommer sökvägen som visas i det "
"tredje fältet av varje post att vara relativt den läsande processens "
"rotkatalog för motsvarande cgroup-hierarki. Om cgroup-katalogen för "
"målprocessen ligger utanför rotkatalogen för den läsande processens cgroup-"
"namnrymd kommer sökvägsnamnet visa I<../>-poster för varje anfadernivå i "
"cgroup-hierarkin."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The following shell session demonstrates the effect of creating a new cgroup "
"namespace.  First, (as superuser) we create a child cgroup in the I<freezer> "
"hierarchy, and put the shell into that cgroup:"
msgstr ""
"Följande skalsession demonstrerar effekten av att skapa en ny cgroup-"
"namnrymd. Först (som rot) skapar vi en barn-cgroup i hierarkin I<freezer>, "
"och lägger in skalet i denna cgroup:"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"# B<mkdir -p /sys/fs/cgroup/freezer/sub>\n"
"# B<echo $$>                      # Show PID of this shell\n"
"30655\n"
"# B<sh -c \\(aqecho 30655 E<gt> /sys/fs/cgroup/freezer/sub/cgroup.procs\\(aq>\n"
"# B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/sub\n"
msgstr ""
"# B<mkdir -p /sys/fs/cgroup/freezer/sub>\n"
"# B<echo $$>                      # Visa PID för detta skal\n"
"30655\n"
"# B<sh -c \\(aqecho 30655 E<gt> /sys/fs/cgroup/freezer/sub/cgroup.procs\\(aq>\n"
"# B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/sub\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "# B<unshare -Cm bash>\n"
msgstr "# B<unshare -Cm bash>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"We then inspect the I</proc/[pid]/cgroup> files of, respectively, the new "
"shell process started by the B<unshare>(1)  command, a process that is in "
"the original cgroup namespace (I<init>, with PID 1), and a process in a "
"sibling cgroup (I<sub2>):"
msgstr ""
"Vi inspekterar sedan filerna I</proc/[pid]/cgroup> för den nya skalprocessen "
"som startas av kommandot B<unshare>(1), en process som finns i den initiala "
"cgroup-namnrymden (I<init>, med PID 1) respektive processen i en syskon-"
"cgroup (I<sub2>):"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"$ B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/\n"
"$ B<cat /proc/1/cgroup | grep freezer>\n"
"7:freezer:/..\n"
"$ B<cat /proc/20124/cgroup | grep freezer>\n"
"7:freezer:/../sub2\n"
msgstr ""
"$ B<cat /proc/self/cgroup | grep freezer>\n"
"7:freezer:/\n"
"$ B<cat /proc/1/cgroup | grep freezer>\n"
"7:freezer:/..\n"
"$ B<cat /proc/20124/cgroup | grep freezer>\n"
"7:freezer:/../sub2\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"From the output of the first command, we see that the freezer cgroup "
"membership of the new shell (which is in the same cgroup as the initial "
"shell)  is shown defined relative to the freezer cgroup root directory that "
"was established when the new cgroup namespace was created.  (In absolute "
"terms, the new shell is in the I</sub> freezer cgroup, and the root "
"directory of the freezer cgroup hierarchy in the new cgroup namespace is "
"also I</sub>.  Thus, the new shell's cgroup membership is displayed as \\(aq/"
"\\(aq.)"
msgstr ""
"Från utdata från det första kommandot ser vi att medlemskapet i cgroup:en "
"freezer för det nya skalet (vilket finns i samma cgroup som det initiala "
"skalet) visas definierat relativt rotkatalogen för cgroup:en freezer som "
"etablerades när den nya cgroup-namnrymden skapades. (I absoluta termer, det "
"nya skalet finns i freezer-cgroup:en I</sub>, och rotkataloger i freezer-"
"cgroup-hierarkin i den nya cgroup-namnrymden är också I</sub>. Alltså, det "
"nya skalets cgroup-medlemskap visas som \\(aq/\\(aq.)"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 /.. /sys/fs/cgroup/freezer ...\n"
msgstr ""
"# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 /.. /sys/fs/cgroup/freezer …\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The fourth field of this line (I</..>)  should show the directory in the "
"cgroup filesystem which forms the root of this mount.  Since by the "
"definition of cgroup namespaces, the process's current freezer cgroup "
"directory became its root freezer cgroup directory, we should see \\(aq/"
"\\(aq in this field.  The problem here is that we are seeing a mount entry "
"for the cgroup filesystem corresponding to our initial shell process's "
"cgroup namespace (whose cgroup filesystem is indeed rooted in the parent "
"directory of I<sub>).  We need to remount the freezer cgroup filesystem "
"inside this cgroup namespace, after which we see the expected results:"
msgstr ""
"Det fjärde fältet på denna rad (I</..>) skulle visa katalogen i cgroup-"
"filsystemet som utgör roten för denna montering. Eftersom enligt "
"definitionen av cgroup-namnrymder processens aktuella cgroup-katalog för "
"freezer blev dess rot-freezer-cgroup-katalog borde vi se \\(aq/\\(aq i detta "
"fält. Problemet här är att vi ser en monteringspost för cgroup-filsystemet "
"som motsvarar vår initiala skalprocess cgroup-namnrymd (vars cgroup-"
"filsystem verkligen är rotat i föräldrakatalogen till I<sub>). Vi måste "
"montera om freezer-cgroup-filsystemet inuti den nya cgroup-namnrymden, "
"varefter vi ser det förväntade resultatet:"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"# B<mount --make-rslave />     # Don't propagate mount events\n"
"                            # to other namespaces\n"
"# B<umount /sys/fs/cgroup/freezer>\n"
"# B<mount -t cgroup -o freezer freezer /sys/fs/cgroup/freezer>\n"
"# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 / /sys/fs/cgroup/freezer rw,relatime ...\n"
msgstr ""
"# B<mount --make-rslave />     # Propagera inte monteringshändelser\n"
"                            # till andra namnrymder\n"
"# B<umount /sys/fs/cgroup/freezer>\n"
"# B<mount -t cgroup -o freezer freezer /sys/fs/cgroup/freezer>\n"
"# B<cat /proc/self/mountinfo | grep freezer>\n"
"155 145 0:32 / /sys/fs/cgroup/freezer rw,relatime …\n"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ÖVERENSSTÄMMER MED"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Among the purposes served by the virtualization provided by cgroup "
"namespaces are the following:"
msgstr ""
"Bland syftena virtualiseringen som erbjuds av cgroup-namnrymder tjänar finns "
"följande:"

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "*"
msgstr "*"

#. type: IP
#: opensuse-leap-15-5
#, no-wrap
msgid "\\(bu"
msgstr "\\(bu"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"In the absence of cgroup namespacing, because the cgroup directory I</cg/1> "
"is owned (and writable) by UID 9000 and process I<X> is also owned by user "
"ID 9000, then process I<X> would be able to modify the contents of cgroups "
"files (i.e., change cgroup settings) not only in I</cg/1/2> but also in the "
"ancestor cgroup directory I</cg/1>.  Namespacing process I<X> under the "
"cgroup directory I</cg/1/2>, in combination with suitable mount operations "
"for the cgroup filesystem (as shown above), prevents it modifying files in "
"I</cg/1>, since it cannot even see the contents of that directory (or of "
"further removed cgroup ancestor directories).  Combined with correct "
"enforcement of hierarchical limits, this prevents process I<X> from escaping "
"the limits imposed by ancestor cgroups."
msgstr ""
"I avsaknad av cgroup-namnrymder skulle, eftersom cgroup-katalogen I</cg/1> "
"ägs (och är skrivbar) av AID 9000 och processen I<X> också ägs av användar-"
"ID 9000, då skulle process I<X> kunna ändra innehållet i cgroup-filer (d.v."
"s., ändra cgroup-inställningar) inte bara i I</cg/1/2> utan även i anfader-"
"cgroup-katalogen I</cg/1>. Att namnrymda processen I<X> under cgroup-"
"katalogen I</cg/1/2>, i kombination med lämpliga monteringsoperationer av "
"cgroup-filsystemet (som visas ovan), förhindrar den från att ändra filer i "
"I</cg/1>, eftersom den inte ens kan se innehållet i den katalogen (eller i "
"mer avlägsna cgroup-anfaderkataloger). Kombinerat med korrekt verkställighet "
"av hierarkiska begränsningar förhindrar detta process I<X> från att fly från "
"begränsningarna som läggs på av anfader-cgroup:er."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOFON"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Denna sida är del av version 4.16 av Linux I<man-pages>-projektet.  En "
"beskrivning av projektet, information om hur man rapporterar fel och den "
"senaste versionen av denna sida kan hittas på \\%https://www.kernel.org/doc/"
"man-pages/."
