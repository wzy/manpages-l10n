# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Robert Luberda <robert@debian.org>, 2005, 2012, 2019.
# Michał Kułach <michal.kulach@gmail.com>, 2014, 2016.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-06-27 19:32+0200\n"
"PO-Revision-Date: 2019-08-16 21:36+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "INFINITY"
msgstr "INFINITY"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"INFINITY, NAN, HUGE_VAL, HUGE_VALF, HUGE_VALL - floating-point constants"
msgstr ""
"INFINITY, NAN, HUGE_VAL, HUGE_VALF, HUGE_VALL - stałe zmiennoprzecinkowe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Math library (I<libm>)"
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _ISOC99_SOURCE>      /* See feature_test_macros(7) */\n"
"B<#include E<lt>math.hE<gt>>\n"
msgstr ""
"B<#define _ISOC99_SOURCE>      /* Patrz feature_test_macros(7) */\n"
"B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<INFINITY>\n"
msgstr "B<INFINITY>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<NAN>\n"
msgstr "B<NAN>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<HUGE_VAL>\n"
"B<HUGE_VALF>\n"
"B<HUGE_VALL>\n"
msgstr ""
"B<HUGE_VAL>\n"
"B<HUGE_VALF>\n"
"B<HUGE_VALL>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The macro B<INFINITY> expands to a I<float> constant representing positive "
"infinity."
msgstr ""
"Makro B<INFINITY> rozszerza się do zmiennoprzecinkowej (czyli typu I<float>) "
"stałej reprezentującej dodatnią nieskończoność."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The macro B<NAN> expands to a I<float> constant representing a quiet NaN "
"(when supported).  A I<quiet> NaN is a NaN (\"not-a-number\") that does not "
"raise exceptions when it is used in arithmetic.  The opposite is a "
"I<signaling> NaN.  See IEC 60559:1989."
msgstr ""
"Makro B<NAN> rozszerza się do stałej zmiennoprzecinkowej (I<float>) "
"reprezentującej I<cichą>  NaN (jeśli jest obsługiwana). I<Cicha> NaN to NaN "
"(\"not-a-number\" - \"nie-liczba\"), która nie wywołuje wyjątku, jeśli jest "
"używana w działaniach arytmetycznych. Jej przeciwieństwem jest  "
"I<sygnalizująca> NaN. Patrz IEC 60559:1989."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The macros B<HUGE_VAL>, B<HUGE_VALF>, B<HUGE_VALL> expand to constants of "
"types I<double>, I<float>, and I<long double>, respectively, that represent "
"a large positive value, possibly positive infinity."
msgstr ""
"Makra B<HUGE_VAL>, B<HUGE_VALF>, B<HUGE_VALL> oznaczają odpowiednio stałe "
"typów I<double>, I<float> oraz  I<long double>, reprezentujących dużą "
"wartość dodatnią, być może dodatnią nieskończoność. "

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "C11."
msgstr ""

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "C99."
msgstr "C99."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On a glibc system, the macro B<HUGE_VAL> is always available.  Availability "
"of the B<NAN> macro can be tested using B<#ifdef NAN>, and similarly for "
"B<INFINITY>, B<HUGE_VALF>, B<HUGE_VALL>.  They will be defined by "
"I<E<lt>math.hE<gt>> if B<_ISOC99_SOURCE> or B<_GNU_SOURCE> is defined, or "
"B<__STDC_VERSION__> is defined and has a value not less than 199901L."
msgstr ""
"W systemie glibc, makro B<HUGE_VAL> jest zawsze dostępne. Dostępność makra "
"B<NAN>może być testowana przez B<#ifdef NAN>, podobnie można testować "
"B<INFINITY>, B<HUGE_VALF>, B<HUGE_VALL>. Będą one zdefiniowane przez "
"I<E<lt>math.hE<gt>>, jeżeli zdefiniowano if B<_ISOC99_SOURCE> lub "
"B<_GNU_SOURCE> albo gdy jest zdefiniowana B<__STDC_VERSION__>  i jej wartość "
"jest nie mniejsza niż 199901L."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<fpclassify>(3), B<math_error>(7)"
msgstr "B<fpclassify>(3), B<math_error>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-10-09"
msgstr "9 października 2022 r."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 września 2017 r."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The macros B<HUGE_VAL>, B<HUGE_VALF>, B<HUGE_VALL> expand to constants of "
"types I<double>, I<float> and I<long double>, respectively, that represent a "
"large positive value, possibly positive infinity."
msgstr ""
"Makra B<HUGE_VAL>, B<HUGE_VALF>, B<HUGE_VALL> oznaczają odpowiednio stałe "
"typów I<double>, I<float> oraz  I<long double>, reprezentujących dużą "
"wartość dodatnią, być może dodatnią nieskończoność. "

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "AVAILABILITY"
msgstr "DOSTĘPNOŚĆ"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
