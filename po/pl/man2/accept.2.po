# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 1999.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2023-06-27 19:18+0200\n"
"PO-Revision-Date: 2002-05-31 23:22+0100\n"
"Last-Translator: Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 19.08.2\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<accept>(2)"
msgid "accept"
msgstr "B<accept>(2)"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2023-03-30"
msgstr "30 marca 2023 r."

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "Linux man-pages 6.04"
msgstr "Linux man-pages 6.04"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "accept, accept4 - accept a connection on a socket"
msgstr "accept, accept4 - przyjmowanie połączenia na gnieździe"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTEKA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Standardowa biblioteka C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int accept(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
#| "B<           socklen_t *restrict >I<addrlen>B<);>\n"
msgid ""
"B<int accept(int >I<sockfd>B<, struct sockaddr *_Nullable restrict >I<addr>B<,>\n"
"B<           socklen_t *_Nullable restrict >I<addrlen>B<);>\n"
msgstr ""
"B<int accept(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<           socklen_t *restrict >I<addrlen>B<);>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<#define _GNU_SOURCE>             /* See feature_test_macros(7) */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr ""
"B<#define _GNU_SOURCE>             /* zobacz feature_test_macros(7) */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<int accept4(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
#| "B<           socklen_t *restrict >I<addrlen>B<, int >I<flags>B<);>\n"
msgid ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *_Nullable restrict >I<addr>B<,>\n"
"B<           socklen_t *_Nullable restrict >I<addrlen>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *restrict >I<addr>B<,>\n"
"B<           socklen_t *restrict >I<addrlen>B<, int >I<flags>B<);>\n"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The B<accept> function is used with connection-based socket types "
#| "(B<SOCK_STREAM>, B<SOCK_SEQPACKET> and B<SOCK_RDM>).  It extracts the "
#| "first connection request on the queue of pending connections, creates a "
#| "new connected socket with mostly the same properties as I<s>, and "
#| "allocates a new file descriptor for the socket, which is returned.  The "
#| "newly created socket is no longer in the listening state.  The original "
#| "socket I<s> is unaffected by this call. Note that any per file descriptor "
#| "flags (everything that can be set with the B<F_SETFL> fcntl, like non "
#| "blocking or async state) are not inherited across an I<accept>."
msgid ""
"The B<accept>()  system call is used with connection-based socket types "
"(B<SOCK_STREAM>, B<SOCK_SEQPACKET>).  It extracts the first connection "
"request on the queue of pending connections for the listening socket, "
"I<sockfd>, creates a new connected socket, and returns a new file descriptor "
"referring to that socket.  The newly created socket is not in the listening "
"state.  The original socket I<sockfd> is unaffected by this call."
msgstr ""
"Funkcja B<accept> jest używana z połączeniowymi typami gniazd "
"(B<SOCK_STREAM>, B<SOCK_SEQPACKET> i B<SOCK_RDM>). Wyciąga ona pierwsze "
"żądanie połączenia z kolejki oczekujących połączeń, tworzy nowo podłączone "
"gniazdo o tych samych właściwościach co s i alokuje nowy deskryptor pliku "
"dla gniazda, który to deskryptor jest zwracany. Nowo utworzone gniazdo nie "
"jest już w stanie nasłuchiwania. Oryginalne gniazdo s pozostaje po "
"wywołaniiu  funkcji niezmienione. Należy zauważyć, że żadne znaczniki "
"dotyczące deskryptora pliku (wszystko, co można ustawić za pomocą "
"B<F_SETFL>, jak stan nieblokujący czy asynchroniczny) nie są poprzez "
"I<accept> dziedziczone."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The argument I<sockfd> is a socket that has been created with B<socket>(2), "
"bound to a local address with B<bind>(2), and is listening for connections "
"after a B<listen>(2)."
msgstr ""
"Argument I<sockfd> jest gniazdem, które zostało utworzone wywołaniem "
"B<socket>(2), przywiązanym do adresu  lokalnego z pomocą B<bind>(2), i "
"nasłuchującym połączeń po wywołaniu B<listen>(2)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The argument I<addr> is a pointer to a sockaddr structure. This structure "
#| "is filled in with the address of the connecting entity, as known to the "
#| "communications layer.  The exact format of the address passed in the "
#| "I<addr> parameter is determined by the socket's family (see B<socket>(2)  "
#| "and the respective protocol man pages).  The I<addrlen> argument is a "
#| "value-result parameter: it should initially contain the size of the "
#| "structure pointed to by I<addr>; on return it will contain the actual "
#| "length (in bytes) of the address returned. When I<addr> is NULL nothing "
#| "is filled in."
msgid ""
"The argument I<addr> is a pointer to a I<sockaddr> structure.  This "
"structure is filled in with the address of the peer socket, as known to the "
"communications layer.  The exact format of the address returned I<addr> is "
"determined by the socket's address family (see B<socket>(2)  and the "
"respective protocol man pages).  When I<addr> is NULL, nothing is filled in; "
"in this case, I<addrlen> is not used, and should also be NULL."
msgstr ""
"Argument I<addr> jest wskaźnikiem do struktury sockaddr. Do struktury tej "
"jest wpisywany adres łączącej się jednostki, przekazany przez warstwę "
"komunikacyjną. Dokładny format adresu przekazywanego w parametrze I<addr> "
"jest określony poprzez rodzinę gniazda (zobacz B<socket>(2) i strony "
"podręcznika dotyczące odpowiedniego protokołu). Argument I<addrlen> jest "
"parametrem wartościowo-wynikowym: powinien początkowo zawierać rozmiar "
"struktury, na którą wskazuje I<addr>; po zakończeniu będzie zawierał "
"rzeczywistą długość zwracanego adresu (w bajtach). Gdy I<addr> jest równe "
"NULL, to nic nie jest wypełniane."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The I<addrlen> argument is a value-result argument: the caller must "
"initialize it to contain the size (in bytes) of the structure pointed to by "
"I<addr>; on return it will contain the actual size of the peer address."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The returned address is truncated if the buffer provided is too small; in "
"this case, I<addrlen> will return a value greater than was supplied to the "
"call."
msgstr ""

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If no pending connections are present on the queue, and the socket is not "
#| "marked as non-blocking, B<accept> blocks the caller until a connection is "
#| "present.  If the socket is marked non-blocking and no pending connections "
#| "are present on the queue, B<accept> returns EAGAIN."
msgid ""
"If no pending connections are present on the queue, and the socket is not "
"marked as nonblocking, B<accept>()  blocks the caller until a connection is "
"present.  If the socket is marked nonblocking and no pending connections are "
"present on the queue, B<accept>()  fails with the error B<EAGAIN> or "
"B<EWOULDBLOCK>."
msgstr ""
"Jeśli nie ma zalegających połączeń w kolejce, a gniazdo nie jest zaznaczone "
"jako nieblokujące, to B<accept> blokuje proces wywołujący aż do uzyskania "
"połączenia. Gdy gniazdo jest zaznaczone jako nieblokujące i nie ma "
"zalegających połączeń w kolejce, B<accept> zwraca EAGAIN."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In order to be notified of incoming connections on a socket, you can use "
"B<select>(2), B<poll>(2), or B<epoll>(7).  A readable event will be "
"delivered when a new connection is attempted and you may then call "
"B<accept>()  to get a socket for that connection.  Alternatively, you can "
"set the socket to deliver B<SIGIO> when activity occurs on a socket; see "
"B<socket>(7)  for details."
msgstr ""
"Aby być informowanym o nadchodzących do gniazda połączeniach, można użyć "
"B<select>(2), B<poll>(2) lub B<poll>(2). Podczas próby nowego połączenia "
"zostanie dostarczone zdarzenie odczytywalności (readable) i wtedy można "
"wywołać B<accept>() aby uzyskać gniazdo tego połączenia. Inaczej, można "
"ustawić gniazdo tak, by dostarczało B<SIGIO> za każdym razem, gdy się na nim "
"coś zacznie dziać; szczegóły można znaleźć w B<socket>(7)."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If I<flags> is 0, then B<accept4>()  is the same as B<accept>().  The "
"following values can be bitwise ORed in I<flags> to obtain different "
"behavior:"
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SOCK_NONBLOCK>"
msgstr "B<SOCK_NONBLOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Set the B<O_NONBLOCK> file status flag on the open file description (see "
"B<open>(2))  referred to by the new file descriptor.  Using this flag saves "
"extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<SOCK_CLOEXEC>"
msgstr "B<SOCK_CLOEXEC>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Set the close-on-exec (B<FD_CLOEXEC>)  flag on the new file descriptor.  See "
"the description of the B<O_CLOEXEC> flag in B<open>(2)  for reasons why this "
"may be useful."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "WARTOŚĆ ZWRACANA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"On success, these system calls return a file descriptor for the accepted "
"socket (a nonnegative integer).  On error, -1 is returned, I<errno> is set "
"to indicate the error, and I<addrlen> is left unchanged."
msgstr ""

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Error handling"
msgstr "Obsługa błędów"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Linux B<accept>()  (and B<accept4>())  passes already-pending network errors "
"on the new socket as an error code from B<accept>().  This behavior differs "
"from other BSD socket implementations.  For reliable operation the "
"application should detect the network errors defined for the protocol after "
"B<accept>()  and treat them like B<EAGAIN> by retrying.  In the case of TCP/"
"IP, these are B<ENETDOWN>, B<EPROTO>, B<ENOPROTOOPT>, B<EHOSTDOWN>, "
"B<ENONET>, B<EHOSTUNREACH>, B<EOPNOTSUPP>, and B<ENETUNREACH>."
msgstr ""
"Linuksowe B<accept> (i B<accept4>()) przekazuje zalegające już na nowym "
"gnieździe błędy sieciowe jako kod błędu z B<accept>(). Zachowanie to różni "
"się od implementacji gniazd w BSD. Dla sensownego działania, aplikacja "
"powinna wykrywać po wykonaniu B<accept>() błędy sieciowe, zdefiniowane dla "
"danego protokołu i traktować je jak B<EAGAIN>, czyli ponawiać próbę. W "
"wypadku TCP/IP są to B<ENETDOWN>, B<EPROTO>, B<ENOPROTOOPT>, B<EHOSTDOWN>, "
"B<ENONET>, B<EHOSTUNREACH>, B<EOPNOTSUPP> i B<ENETUNREACH>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "BŁĘDY"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EAGAIN> or B<EWOULDBLOCK>"
msgstr "B<EAGAIN> lub B<EWOULDBLOCK>"

#.  Actually EAGAIN on Linux
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The socket is marked nonblocking and no connections are present to be "
"accepted.  POSIX.1-2001 and POSIX.1-2008 allow either error to be returned "
"for this case, and do not require these constants to have the same value, so "
"a portable application should check for both possibilities."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "I<sockfd> is not an open file descriptor."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ECONNABORTED>"
msgstr "B<ECONNABORTED>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "A connection has been aborted."
msgstr "Połączenie zostało przerwane."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The I<addr> parameter is not in a writable part of the user address space."
msgid ""
"The I<addr> argument is not in a writable part of the user address space."
msgstr ""
"Parametr I<addr> nie znajduje się w przestrzeni adresowej dostępnej do "
"zapisu dla użytkownika."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINTR>"
msgstr "B<EINTR>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The system call was interrupted by a signal that was caught before a "
#| "valid connection arrived."
msgid ""
"The system call was interrupted by a signal that was caught before a valid "
"connection arrived; see B<signal>(7)."
msgstr ""
"Funkcja systemowa została przerwana wskutek odebrania sygnału przed "
"prawidłowym nawiązaniem połączenia."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Socket is not listening for connections."
msgid ""
"Socket is not listening for connections, or I<addrlen> is invalid (e.g., is "
"negative)."
msgstr "Gniazdo nie nasłuchuje połączeń."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "(B<accept4>())  invalid value in I<flags>."
msgstr ""

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EMFILE>"
msgstr "B<EMFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The per-process limit on the number of open file descriptors has been "
"reached."
msgstr ""
"Zostało osiągnięte ograniczenie na liczbę otwartych deskryptorów plików dla "
"procesu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"Zostało osiągnięte systemowe ograniczenie na całkowitą liczbę otwartych "
"plików."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOBUFS>, B<ENOMEM>"
msgstr "B<ENOBUFS>, B<ENOMEM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Not enough free memory.  This often means that the memory allocation is "
"limited by the socket buffer limits, not by the system memory."
msgstr ""
"Jest niedostateczna ilość wolnej pamięci. Oznacza to zazwyczaj, że istnieje "
"ograniczenie dla przydzielania pamięci na bufory gniazd, nie zaś że zabrakło "
"pamięci w systemie."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr "B<ENOTSOCK>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "The descriptor references a file, not a socket."
msgid "The file descriptor I<sockfd> does not refer to a socket."
msgstr "Deskryptor odnosi się do pliku, zamiast do gniazda."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr "B<EOPNOTSUPP>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The referenced socket is not of type B<SOCK_STREAM>."
msgstr "Przekazane gniazdo nie jest typu B<SOCK_STREAM>."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Firewall rules forbid connection."
msgstr "Reguły firewalla zabraniają połączenia."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPROTO>"
msgstr "B<EPROTO>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Protocol error."
msgstr "Wystąpił błąd protokołu."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In addition, network errors for the new socket and as defined for the "
"protocol may be returned.  Various Linux kernels can return other errors "
"such as B<ENOSR>, B<ESOCKTNOSUPPORT>, B<EPROTONOSUPPORT>, B<ETIMEDOUT>.  The "
"value B<ERESTARTSYS> may be seen during a trace."
msgstr ""
"Dodatkowo, dla nowego gniazda mogą być zwracane błędy sieciowe zdefiniowane "
"dla danego protokołu. Różne jądra Linuksa mogą zwracać inne błędy, takie jak "
"B<ENOSR>, B<ESOCKTNOSUPPORT>, B<EPROTONOSUPPORT>, B<ETIMEDOUT>. Wartość "
"B<ERESTARTSYS> może być obserwowana podczas śledzenia."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "WERSJE"

#.  Some testing seems to show that Tru64 5.1 and HP-UX 11 also
#.  do not inherit file status flags -- MTK Jun 05
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Linux accept does _not_ inherit socket flags like B<O_NONBLOCK>.  This "
#| "behaviour differs from other BSD socket implementations.  Portable "
#| "programs should not rely on this behaviour and always set all required "
#| "flags on the socket returned from accept."
msgid ""
"On Linux, the new socket returned by B<accept>()  does I<not> inherit file "
"status flags such as B<O_NONBLOCK> and B<O_ASYNC> from the listening "
"socket.  This behavior differs from the canonical BSD sockets "
"implementation.  Portable programs should not rely on inheritance or "
"noninheritance of file status flags and always explicitly set all required "
"flags on the socket returned from B<accept>()."
msgstr ""
"Linuksowe B<accept> _nie_ dziedziczy znaczników gniazda, takich jak "
"B<O_NONBLOCK>. Takie zachowanie różni się od innych implementacji gniazd "
"BSD. Przenośne programy nie powinny zakładać takiego zachowania i zawsze  "
"ustawiać dla gniazda zwracanego przez accept wszystkie potrzebne znaczniki."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<accept>(2)"
msgid "B<accept>()"
msgstr "B<accept>(2)"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2008."
msgstr "POSIX.1-2008."

#. type: TP
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<accept>(2)"
msgid "B<accept4>()"
msgstr "B<accept>(2)"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "Linux."
msgstr "Linux."

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: SH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#.  The BSD man page documents five possible error returns
#.  (EBADF, ENOTSOCK, EOPNOTSUPP, EWOULDBLOCK, EFAULT).
#.  POSIX.1-2001 documents errors
#.  EAGAIN, EBADF, ECONNABORTED, EINTR, EINVAL, EMFILE,
#.  ENFILE, ENOBUFS, ENOMEM, ENOTSOCK, EOPNOTSUPP, EPROTO, EWOULDBLOCK.
#.  In addition, SUSv2 documents EFAULT and ENOSR.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "POSIX.1-2001, SVr4, 4.4BSD (B<accept>()  first appeared in 4.2BSD)."
msgstr ""

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
#, fuzzy
#| msgid "Since glibc 2.10:"
msgid "Linux 2.6.28, glibc 2.10."
msgstr "Od glibc 2.10:"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "UWAGI"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"There may not always be a connection waiting after a B<SIGIO> is delivered "
"or B<select>(2), B<poll>(2), or B<epoll>(7)  return a readability event "
"because the connection might have been removed by an asynchronous network "
"error or another thread before B<accept>()  is called.  If this happens, "
"then the call will block waiting for the next connection to arrive.  To "
"ensure that B<accept>()  never blocks, the passed socket I<sockfd> needs to "
"have the B<O_NONBLOCK> flag set (see B<socket>(7))."
msgstr ""
"Nie zawsze po dostarczeniu B<SIGIO> musi istnieć oczekujące połączenie. To "
"samo dotyczy B<select>(2),  B<poll>(2) i B<epoll>(2), zwracających zdarzenie "
"odczytywalności, ponieważ połączenie mogło zostać usunięte przez "
"asynchroniczny błąd sieci lub inny wątek, przed wywołaniem B<accept>(). "
"Jeśli to się zdarzy, to wywołanie będzie blokować, oczekując następnego "
"połączenia. Aby upewnić się, że B<accept> nigdy nie będzie blokowało, s "
"powinno mieć ustawiony znacznik B<O_NONBLOCK> (zobacz B<socket>(7))."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For certain protocols which require an explicit confirmation, such as "
"DECnet, B<accept>()  can be thought of as merely dequeuing the next "
"connection request and not implying confirmation.  Confirmation can be "
"implied by a normal read or write on the new file descriptor, and rejection "
"can be implied by closing the new socket.  Currently, only DECnet has these "
"semantics on Linux."
msgstr ""
"Dla niektórych protokołów wymagających bezpośredniego potwierdzania, takich "
"jak DECnet, accept może być uważane za funkcję zdejmującą z kolejki następne "
"żądanie połączenia, nie powodując potwierdzenia. Potwierdzenie można "
"spowodować przez normalny odczyt, lub zapis na nowym deskryptorze pliku, a "
"odrzucenie można spowodować, zamykając gniazdo. Obecnie pod Linuksem taką "
"semantykę ma tylko DECnet."

#. type: SS
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "The socklen_t type"
msgstr ""

#.  such as Linux libc4 and libc5, SunOS 4, SGI
#.  SunOS 5 has 'size_t *'
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In the original BSD sockets implementation (and on other older systems)  the "
"third argument of B<accept>()  was declared as an I<int\\ *>.  A POSIX.1g "
"draft standard wanted to change it into a I<size_t\\ *>C; later POSIX "
"standards and glibc 2.x have I<socklen_t\\ * >."
msgstr ""

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "PRZYKŁADY"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "See B<bind>(2)."
msgstr "Zobacz B<bind>(2)."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<bind>(2), B<connect>(2), B<listen>(2), B<select>(2), B<socket>(2), "
"B<socket>(7)"
msgstr ""
"B<bind>(2), B<connect>(2), B<listen>(2), B<select>(2), B<socket>(2), "
"B<socket>(7)"

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "2022-12-04"
msgstr "4 grudnia 2022 r."

#. type: TH
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
#, no-wrap
msgid "Linux man-pages 6.03"
msgstr "Linux man-pages 6.03"

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron
msgid ""
"The B<accept4>()  system call is available starting with Linux 2.6.28; "
"support in glibc is available starting with glibc 2.10."
msgstr ""

#.  The BSD man page documents five possible error returns
#.  (EBADF, ENOTSOCK, EOPNOTSUPP, EWOULDBLOCK, EFAULT).
#.  POSIX.1-2001 documents errors
#.  EAGAIN, EBADF, ECONNABORTED, EINTR, EINVAL, EMFILE,
#.  ENFILE, ENOBUFS, ENOMEM, ENOTSOCK, EOPNOTSUPP, EPROTO, EWOULDBLOCK.
#.  In addition, SUSv2 documents EFAULT and ENOSR.
#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid ""
"B<accept>(): POSIX.1-2001, POSIX.1-2008, SVr4, 4.4BSD (B<accept>()  first "
"appeared in 4.2BSD)."
msgstr ""

#. type: Plain text
#: debian-bookworm debian-unstable fedora-38 mageia-cauldron opensuse-leap-15-5
msgid "B<accept4>()  is a nonstandard Linux extension."
msgstr ""

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "ACCEPT"
msgstr "ACCEPT"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2016-10-08"
msgstr "8 października 2016 r."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Podręcznik programisty Linuksa"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>          /* See NOTES */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr ""
"B<#include E<lt>sys/types.hE<gt>>          /* zobacz UWAGI */\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid "B<int accept(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t *>I<addrlen>B<);>\n"
msgstr "B<int accept(int >I<sockfd>B<, struct sockaddr *>I<addr>B<, socklen_t *>I<addrlen>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *>I<addr>B<,>\n"
"B<            socklen_t *>I<addrlen>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int accept4(int >I<sockfd>B<, struct sockaddr *>I<addr>B<,>\n"
"B<            socklen_t *>I<addrlen>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Set the B<O_NONBLOCK> file status flag on the new open file description.  "
"Using this flag saves extra calls to B<fcntl>(2)  to achieve the same result."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "On success, zero is returned.  On error, -1 is returned, and I<errno> is "
#| "set appropriately."
msgid ""
"On success, these system calls return a nonnegative integer that is a file "
"descriptor for the accepted socket.  On error, -1 is returned, and I<errno> "
"is set appropriately."
msgstr ""
"Po pomyślnym zakończeniu zwracane jest zero. Po błędzie zwracane jest -1 i "
"odpowiednio ustawiane jest I<errno>."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "Linux B<accept> may fail if:"
msgid "In addition, Linux B<accept>()  may fail if:"
msgstr "W Linuksie B<accept> może również zakończyć się niepomyślnie gdy:"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<accept4>()  system call is available starting with Linux 2.6.28; "
"support in glibc is available starting with version 2.10."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "CONFORMING TO"
msgstr "ZGODNE Z"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"POSIX.1-2001 does not require the inclusion of I<E<lt>sys/types.hE<gt>>, and "
"this header file is not required on Linux.  However, some historical (BSD) "
"implementations required this header file, and portable applications are "
"probably wise to include it."
msgstr ""

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "PRZYKŁAD"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "O STRONIE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Angielska wersja tej strony pochodzi z wydania 4.16 projektu Linux I<man-"
"pages>. Opis projektu, informacje dotyczące zgłaszania błędów oraz najnowszą "
"wersję oryginału można znaleźć pod adresem \\%https://www.kernel.org/doc/man-"
"pages/."
