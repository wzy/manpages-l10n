# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Wojtek Kotwica <wkotwica@post.pl>, 2000.
# Andrzej Krzysztofowicz <ankry@green.mf.pg.gda.pl>, 2002.
# Robert Luberda <robert@debian.org>, 2014.
# Michał Kułach <michal.kulach@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2023-06-27 19:46+0200\n"
"PO-Revision-Date: 2022-02-16 21:22+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPQUOTA"
msgstr "REPQUOTA"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "repquota - summarize quotas for a filesystem"
msgstr "repquota - podsumowanie kwot dyskowych dla systemu plików"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B</usr/sbin/repquota> [ B<-vspiugP> ] [ B<-c> | B<-C> ] [ B<-t> | B<-n> ] "
"[ B<-F> I<format-name> ] I<filesystem>.\\|.\\|."
msgstr ""
"B</usr/sbin/repquota> [ B<-vspiugP> ] [ B<-c> | B<-C> ] [ B<-t> | B<-n> ] "
"[ B<-F> I<nazwa-formatu> ] I<system-plików>.\\|.\\|."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B</usr/sbin/repquota> [ B<-avtpsiugP> ] [ B<-c> | B<-C> ] [ B<-t> | B<-n> ] "
"[ B<-F> I<format-name> ]"
msgstr ""
"B</usr/sbin/repquota> [ B<-avtpsiugP> ] [ B<-c> | B<-C> ] [ B<-t> | B<-n> ] "
"[ B<-F> I<nazwa-formatu> ]"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "repquota command"
msgstr "polecenie repquota"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "\\fLrepquota\\fR \\(em summarize quotas"
msgstr "repquota - podsumowanie kwot dyskowych"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "user quotas"
msgstr "user quotas"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "disk quotas"
msgstr "disk quotas"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "quotas"
msgstr "quotas"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "filesystem"
msgstr "filesystem"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "summarize filesystem quotas repquota"
msgstr "summarize filesystem quotas repquota"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "summarize filesystem quotas \\(em \\fLrepquota\\fR"
msgstr "summarize filesystem quotas \\(em \\fLrepquota\\fR"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "report filesystem quotas repquota"
msgstr "report filesystem quotas repquota"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "report filesystem quotas \\(em \\fLrepquota\\fR"
msgstr "report filesystem quotas \\(em \\fLrepquota\\fR"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "display"
msgstr "display"

#. type: IX
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "filesystem quotas \\(em \\fLrepquota\\fR"
msgstr "filesystem quotas \\(em \\fLrepquota\\fR"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<repquota> prints a summary of the disc usage and quotas for the specified "
"file systems.  For each user the current number of files and amount of space "
"(in kilobytes) is printed, along with any quota limits set with "
"B<edquota>(8)  or B<setquota>(8).  In the second column repquota prints two "
"characters marking which limits are exceeded. If user is over his space "
"softlimit or reaches his space hardlimit in case softlimit is unset, the "
"first character is '+'. Otherwise the character printed is '-'. The second "
"character denotes the state of inode usage analogously."
msgstr ""
"B<repquota> wypisuje podsumowanie użycia dysku i limity kwot dla podanych "
"systemów plików. Dla każdego użytkownika wyświetlana jest liczba jego plików "
"oraz przestrzeń dyskowa (w kilobajtach) razem z ograniczeniami kwot "
"ustawionymi przez B<edquota>(8) lub  B<setquota>(8). W drugiej kolumnie "
"B<repquota> wypisuje dwa znaki określające, które ograniczenia zostały "
"przekroczone. Jeśli użytkownik przekroczył miękki limit przestrzeni dyskowej "
"lub osiągnął ograniczenie twarde, a ograniczenie miękkie nie jest ustawione, "
"to pierwszym znakiem jest \"+\". W przeciwnym wypadku wypisywany jest \"-\". "
"Drugi znak w podobny sposób określa stan użycia i-węzłów."

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<repquota> has to translate ids of all users/groups/projects to names "
"(unless option B<-n> was specified) so it may take a while to print all the "
"information. To make translating as fast as possible B<repquota> tries to "
"detect (by reading B</etc/nsswitch.conf>)  whether entries are stored in "
"standard plain text file or in a database and either translates chunks of "
"1024 names or each name individually. You can override this autodetection by "
"B<-c> or B<-C> options."
msgstr ""
"B<repquota> musi przetłumaczyć identyfikatory wszystkich użytkowników/grup/"
"projektów na ich nazwy (chyba że użyto opcji B<-n>), tak więc wypisanie "
"wszystkich informacji może zająć trochę czasu. Aby przyspieszyć to "
"tłumaczenie B<repquota> próbuje określić (czytając plik B</etc/nsswitch."
"conf>), czy wpisy użytkowników/grup są przechowywane w standardowych plikach "
"tekstowych, czy w bazie danych i albo tłumaczy pakiety 1024 nazw od razu, "
"albo tłumaczy każdą nazwę z osobną. Można nadpisać tę autodetekcję za pomocą "
"opcji B<-c> i B<-C>."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCJE"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-a, --all>"
msgstr "B<-a>, B<--all>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report on all filesystems indicated in B</etc/mtab> to be read-write with "
"quotas."
msgstr ""
"Pokazanie raportu kwoty ze wszystkich systemów plików wymienionych w B</etc/"
"mtab> jako do odczytu i zapisu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v, --verbose>"
msgstr "B<-v, --verbose>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report all quotas, even if there is no usage. Be also more verbose about "
"quotafile information."
msgstr ""
"Pokazanie wszystkich kwot, nawet jeśli nie ma zużycia zasobów. Bardziej "
"\"gadatliwy\" sposób wypisywania informacji z pliku kwot."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-c, --cache>"
msgstr "B<-c, --cache>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Cache entries to report and translate uids/gids to names in big chunks by "
"scanning all users (default). This is good (fast) behaviour when using /etc/"
"passwd file."
msgstr ""
"Buforuje wpisy do zaraportowania i tłumaczy identyfikatory użytkowników i "
"grup na nazwy przez skanowanie wszystkich użytkowników (zachowanie "
"domyślne). Jest to dobre (szybkie) zachowanie, kiedy używa się pliku /etc/"
"passwd."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-C, --no-cache>"
msgstr "B<-C, --no-cache>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Translate individual entries. This is faster when you have users stored in "
"database."
msgstr ""
"Tłumaczy poszczególne wpisy. Jest to szybsze, gdy użytkownicy są "
"przechowywani w bazie danych."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-t, --truncate-names>"
msgstr "B<-t, --truncate-names>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Truncate user/group names longer than 9 characters. This results in nicer "
"output when there are such names."
msgstr ""
"Skracanie nazw użytkowników i grup dłuższych niż 9 znaków. Wynik jest "
"bardziej czytelny niż normalny, gdy takie nazwy występują."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-n, --no-names>"
msgstr "B<-n, --no-names>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Don't resolve UIDs/GIDs to names. This can speedup printing a lot."
msgstr ""
"Niezamienianie identyfikatorów użytkowników i grup na nazwy. Może to "
"znacznie przyspieszyć wypisywanie informacji."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s, --human-readable[=>I<units>]"
msgstr "B<-s, --human-readable>[B<=>I<jednostki>]"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Try to report used space, number of used inodes and limits in more "
"appropriate units than the default ones. Units can be also specified "
"explicitely by an optional argument in format [ B<kgt> ],[ B<kgt> ] where "
"the first character specifies space units and the second character specifies "
"inode units."
msgstr ""
"Próbuje wypisać użyte miejsce, liczbę użytych i-węzłów i limity w "
"czytelniejszych jednostkach niż domyślne. Jednostki można także narzucić "
"opcjonalnym argumentem w postaci [ B<kgt> ],[ B<kgt> ] gdzie pierwszy znak "
"określa jednostkę użytego miejsca, a drugi jednostkę i-węzłów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-p, --raw-grace>"
msgstr "B<-p, --raw-grace>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"When user is in grace period, report time in seconds since epoch when his "
"grace time runs out (or has run out). Field is '0' when no grace time is in "
"effect.  This is especially useful when parsing output by a script."
msgstr ""
"Jeśli użytkownik jest w okresie ulgi (grace period), to wyświetla czas w "
"sekundach od początku epoki, kiedy ten okres się zakończy (lub zakończył). "
"Pole jest równe \"0\", jeśli nie ma okresu ulgi. Jest to użyteczne podczas "
"przetwarzania wyjścia za pomocą jakiegoś  skryptu."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i, --no-autofs>"
msgstr "B<-i, --no-autofs>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Ignore mountpoints mounted by automounter."
msgstr ""
"Ignoruje punkty montowania ustanowione przez program automatycznego "
"montowania (automounter)."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-F, --format=>I<format-name>"
msgstr "B<-F>, B<--format=>I<nazwa-formatu>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report quota for specified format (ie. don't perform format autodetection).  "
"Possible format names are: B<vfsold> Original quota format with 16-bit "
"UIDs / GIDs, B<vfsv0> Quota format with 32-bit UIDs / GIDs, 64-bit space "
"usage, 32-bit inode usage and limits, B<vfsv1> Quota format with 64-bit "
"quota limits and usage, B<xfs> (quota on XFS filesystem)"
msgstr ""
"Podawanie kwot według podanego formatu (tzn. nie jest wykonywana "
"autodetekcja formatu). Dozwolone są następujące nazwy formatów: B<vfsold> - "
"oryginalny format kwot z 16-bitowymi identyfikatorami użytkowników i grup, "
"B<vfsv0> - format kwoty z 32-bitowymi identyfikatorami użytkowników i grup, "
"64-bitową przestrzenią użycia i 32-bitowymi użyciem i-węzłów i "
"ograniczeniami, B<vfsv1> - format kwoty z 64-bitowymi ograniczeniami kwot i "
"użycia, B<xfs> - kwoty na systemie plików XFS."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-g, --group>"
msgstr "B<-g>, B<--group>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Report quotas for groups."
msgstr "Pokazanie kwot grup."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-P, --project>"
msgstr "B<-P, --project>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Report quotas for projects."
msgstr "Pokazanie kwot projektów."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-u, --user>"
msgstr "B<-u>, B<--user>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Report quotas for users. This is the default."
msgstr "Pokazanie kwot użytkowników. Jest to zachowanie domyślne."

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-O, --output=>I<format-name>"
msgstr "B<-O, --output=>I<nazwa-formatu>"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Output quota report in the specified format.  Possible format names are: "
"B<default> The default format, optimized for console viewing B<csv> Comma-"
"separated values, a text file with the columns delimited by commas B<xml> "
"Output is XML encoded, useful for processing with XSLT"
msgstr ""
"Wypisuje podsumowanie kwot w podanym formacie. Dopuszczalne wartości formatu "
"to: B<default> Format domyślny, zoptymalizowany do wyświetlania w terminalu; "
"B<csv> Plik tekstowy z wartościami rozdzielonymi przecinkami (CSV); B<xml> "
"Wynik w formacie XML, użyteczne do przetwarzania za pomocą XSLT."

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Only the super-user may view quotas which are not their own."
msgstr "Tylko superużytkownik może przeglądać cudze kwoty."

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "PLIKI"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<aquota.user> or B<aquota.group>"
msgstr "B<aquota.user> lub B<aquota.group>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"quota file at the filesystem root (version 2 quota, non-XFS filesystems)"
msgstr ""
"pliki kwot dyskowych umieszczone w głównym katalogu systemu plików (wersja 2 "
"kwot, systemy plików inne niż XFS)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<quota.user> or B<quota.group>"
msgstr "B<quota.user> lub B<quota.group>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"quota file at the filesystem root (version 1 quota, non-XFS filesystems)"
msgstr ""
"pliki kwot dyskowych umieszczone w głównym katalogu systemu plików (wersja 1 "
"kwot, systemy plików inne niż XFS)"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B</etc/mtab>"
msgstr "B</etc/mtab>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "default filesystems"
msgstr "domyślne systemy plików"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B</etc/passwd>"
msgstr "B</etc/passwd>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "default set of users"
msgstr "domyślny zbiór użytkowników"

#. type: TP
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B</etc/group>"
msgstr "B</etc/group>"

#
#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "default set of groups"
msgstr "domyślny zbiór grup"

#. type: SH
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bookworm debian-unstable fedora-38 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<quota>(1), B<quotactl>(2), B<edquota>(8), B<quotacheck>(8), B<quotaon>(8), "
"B<quota_nld>(8), B<setquota>(8), B<warnquota>(8)"
msgstr ""
"B<quota>(1), B<quotactl>(2), B<edquota>(8), B<quotacheck>(8), B<quotaon>(8), "
"B<quota_nld>(8), B<setquota>(8), B<warnquota>(8)"
