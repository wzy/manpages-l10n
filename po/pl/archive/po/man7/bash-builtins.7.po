# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Michał Kułach <michal.kulach@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2021-02-09 21:35+01:00\n"
"PO-Revision-Date: 2012-05-09 00:31+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "BASH-BUILTINS"
msgstr "BASH-BUILTINS"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "2001 October 29"
msgstr "29 październik 2001"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GNU Bash-2.05a"
msgstr "GNU Bash-2.05a"

#. type: SH
#: debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: debian-unstable
msgid "bash-builtins - bash built-in commands, see B<bash>(1)"
msgstr "bash-builtins - wbudowane polecenia basha, patrz B<bash>(1)"

#. type: SH
#: debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: debian-unstable
msgid ""
"bash defines the following built-in commands: :, ., [, alias, bg, bind, "
"break, builtin, case, cd, command, compgen, complete, continue, declare, "
"dirs, disown, echo, enable, eval, exec, exit, export, fc, fg, getopts, hash, "
"help, history, if, jobs, kill, let, local, logout, popd, printf, pushd, pwd, "
"read, readonly, return, set, shift, shopt, source, suspend, test, times, "
"trap, type, typeset, ulimit, umask, unalias, unset, until, wait, while."
msgstr ""
"Powłoka bash definiuje następujące polecenia wbudowane: :, ., [, alias, bg, "
"bind, break, builtin, case, cd, command, compgen, complete, continue, "
"declare, dirs, disown, echo, enable, eval, exec, exit, export, fc, fg, "
"getopts, hash, help, history, if, jobs, kill, let, local, logout, popd, "
"printf, pushd, pwd, read, readonly, return, set, shift, shopt, source, "
"suspend, test, times, trap, type, typeset, ulimit, umask, unalias, unset, "
"until, wait, while."

#. type: SH
#: debian-unstable
#, no-wrap
msgid "BASH BUILTIN COMMANDS"
msgstr "WBUDOWANE POLECENIA BASH"

#. type: SH
#: debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: debian-unstable
msgid "bash(1), sh(1)"
msgstr "bash(1), sh(1)"
